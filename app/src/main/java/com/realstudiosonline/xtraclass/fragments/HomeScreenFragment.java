package com.realstudiosonline.xtraclass.fragments;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.activities.MainActivity;
import com.realstudiosonline.xtraclass.adapters.BookmarkAdapter;
import com.realstudiosonline.xtraclass.adapters.ChangeSubjectAdapter;
import com.realstudiosonline.xtraclass.adapters.GenericAdapter;
import com.realstudiosonline.xtraclass.adapters.SemesterAdapter;
import com.realstudiosonline.xtraclass.adapters.SubtitleAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.db.AppDatabase;
import com.realstudiosonline.xtraclass.db.Subtitle;
import com.realstudiosonline.xtraclass.db.Video;
import com.realstudiosonline.xtraclass.models.BookmarkItem;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.ClassDetailsResponse;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.ClassesForCourseResponse;
import com.realstudiosonline.xtraclass.models.DrawerToggle;
import com.realstudiosonline.xtraclass.models.EmailSending;
import com.realstudiosonline.xtraclass.models.Events;
import com.realstudiosonline.xtraclass.models.Favorite;
import com.realstudiosonline.xtraclass.models.Favorites;
import com.realstudiosonline.xtraclass.models.GenericItem;
import com.realstudiosonline.xtraclass.models.Lesson;
import com.realstudiosonline.xtraclass.models.LibrariesEventBus;
import com.realstudiosonline.xtraclass.models.Library;
import com.realstudiosonline.xtraclass.models.PaymentType;
import com.realstudiosonline.xtraclass.models.PlaybackPosition;
import com.realstudiosonline.xtraclass.models.PutFavoriteResponse;
import com.realstudiosonline.xtraclass.models.ResetLibrary;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.SchoolDetailsResponse;
import com.realstudiosonline.xtraclass.models.SearchItem;
import com.realstudiosonline.xtraclass.models.SelectedLibrary;
import com.realstudiosonline.xtraclass.models.SelectedVideo;
import com.realstudiosonline.xtraclass.models.Semester;
import com.realstudiosonline.xtraclass.models.Semesters;
import com.realstudiosonline.xtraclass.models.SubjectOfClassResponse;
import com.realstudiosonline.xtraclass.models.SubscriptionType;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.models.VideoSource;
import com.realstudiosonline.xtraclass.models.Years;
import com.realstudiosonline.xtraclass.utils.Constants;
import com.realstudiosonline.xtraclass.utils.ExpandCollapseExtension;
import com.realstudiosonline.xtraclass.utils.KeyBoard;
import com.realstudiosonline.xtraclass.utils.KeyboardHeightProvider;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;
import com.realstudiosonline.xtraclass.utils.player.PlayerController;
import com.realstudiosonline.xtraclass.utils.player.VideoPlayer;
import com.realstudiosonline.xtraclass.utils.smarttablayout.SmartTabLayout;
import com.realstudiosonline.xtraclass.utils.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.realstudiosonline.xtraclass.utils.smarttablayout.utils.v4.FragmentPagerItems;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.FetchObserver;
import com.tonyodev.fetch2core.Reason;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.File;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

import static com.realstudiosonline.xtraclass.fragments.OpenSchoolFragment.view_anchor_open;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_160P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_240P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_360P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_480P;
import static com.realstudiosonline.xtraclass.utils.Constants.MEDIA_HUB;
import static com.realstudiosonline.xtraclass.utils.Constants.convertDpToPixelInt;
import static com.realstudiosonline.xtraclass.utils.Constants.generate;


public class HomeScreenFragment extends Fragment implements View.OnClickListener,
        PlayerController, FetchObserver<Download> , CalendarView.OnCalendarSelectListener,
        CalendarView.OnMonthChangeListener,SemesterAdapter.setOnSemesterLister{

    private static final String TAG = HomeScreenFragment.class.getSimpleName();

    PlayerView playerView;
    VideoPlayer player;
    AppCompatImageButton subtitle;
    AppCompatImageButton nextBtn;
    AppCompatImageButton retry;
    AppCompatImageButton fullScreenBtn;
    ProgressBar progressBar;
    AppCompatImageView imageViewSchoolCrest;
    AlertDialog alertDialog;

    PopupWindow popupWindowTakeTest;

    VideoSource videoSource,videoSourceLibrary;

    AppDatabase database;
    final List<Subtitle> subtitleList = new ArrayList<>();
    final List<Video> videoUriList = new ArrayList<>();
    final List<Video> libraryVideoUriList = new ArrayList<>();

    SmartTabLayout tabLayout;
    private static ViewPager viewPager;
    private FloatingActionButton floatingActionButtonSearch;
    private ViewFlipper scrollView,viewFlipperSearchInfo, viewFlipperSemesterCalendar;
    AppCompatImageButton btn_collapse_expand, imgCloseSearch;
    public boolean isUp = false;
    AppCompatTextView tvLearnMore, tvViewCount, tvSchoolName, tvClass, tvSubject, tvTopic, tvTutor,tvLessonDetails,tvPercentage,tvETA,tvDownloadStatus;
    View view_details_of_course, viewLessonDetails,viewPlayerContainer,btnGetNotes,view_class_details;
    TextInputLayout inputLayoutSearch;
    private TextInputEditText editTextSearch;
    //ClassesForCourse selectedClassesForCourse;
    //School selectedSchool;
    //CoursesForSchool coursesForSchool;
    //Institution selectedInstitution;
    List<ClassesForCourse> classesForCourses;
    AppCompatImageButton videoQuality, viewBookMark;
    Request request;
    Fetch fetch;
    Lesson selectedLesson;
    User mUser;
    int mProgress;

    PopupWindow popupWindowGetNotes, popupWindowBookmarkDownload, popupWindowChangeSubject, popupWindowEmailSent, popupWindowDownloading;

    FloatingActionButton floatingActionButtonCalendar;

    FloatingActionButton floatingActionButtonDetails;

    TextView mTextMonthYear;

    TextView mTextCurrentDay;

    RecyclerView recyclerViewSemesters;

    CalendarView mCalendarView;

    RelativeLayout mRelativeTool;

    View chatInputView, viewChangeSubAnchor,viewDownloadProgress, viewDownloadAnimation,viewLayoutControls;

    LottieAnimationView lottieAnimationViewDone;
    ScrollView scroll_view_subject_details;

    public static  View view_anchor;
    int mYear;

    private SemesterAdapter semesterAdapter;

    private List<Semester> semesters;

    private ClassesForCourse selectedClassesForCourse;
    Map<String, Calendar> map = new HashMap<>();

    private boolean isLibraryVideoPlaying = false;

    List<Lesson> mLessons = new ArrayList<>();

    final ArrayList<GenericItem> bookmarkVideoList = new ArrayList<GenericItem>() {{
        add(new GenericItem(1,R.drawable.star, "Bookmark"));
        add(new GenericItem(2,R.drawable.phone, "Download"));
    }};

    String learnMore = "<u>Learn more</u>";

    final ArrayList<GenericItem> emailDownloadList = new ArrayList<GenericItem>() {{
        add(new GenericItem(1,R.drawable.mail, "Email me"));
        add(new GenericItem(2,R.drawable.phone, "Download"));
    }};

    School school;

    ClassDetails classDetails;

    private String mToken ="";

    private float initialY;

    GenericAdapter emailBookmarkAdapter, adapterBookmarkDownload = null;

    private String downloadComplete ="Download finished successfully. You can check the file in the Downloads folder";

    AppCompatSeekBar circularProgressBar;

    //private KeyboardHeightProvider keyboardHeightProvider;


    private void provideTakeTestPopupWindow(View it,Activity activity) {
        if(popupWindowTakeTest !=null){
            popupWindowTakeTest.dismiss();
            popupWindowTakeTest = null;
        }
        popupWindowTakeTest = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        Drawable backgroundDrawable = ContextCompat.getDrawable(activity,R.drawable.curve_white_background);
        popupWindowTakeTest.setBackgroundDrawable(backgroundDrawable);
        popupWindowTakeTest.setOutsideTouchable(true);
        popupWindowTakeTest.setFocusable(true);
        popupWindowTakeTest.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = (View) layoutInflater.inflate(R.layout.register_dialog, null, false);

        AppCompatTextView btn_confirm = testView.findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(v -> popupWindowTakeTest.dismiss());

        popupWindowTakeTest.setContentView(testView);

        if(popupWindowTakeTest !=null) {

            popupWindowTakeTest.showAsDropDown(it, 0, 0);
        }

    }





    public HomeScreenFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_screen, container, false);
    }


/*    public StateListDrawable setImageButtonStateNew(Context mContext){
        StateListDrawable states = new StateListDrawable();
        int [] state_selected ={android.R.attr.state_selected};
        int []  _selected_state= {-android.R.attr.state_selected};

        states.addState(state_selected, ContextCompat.getDrawable(mContext, R.drawable.tab_bg_normal_blue));
        states.addState(_selected_state, ContextCompat.getDrawable(mContext, R.drawable.tab_bg_normal));

        return states;
    }*/


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getActivity()!=null)
           initializeDb(getActivity());

        selectedClassesForCourse = PreferenceHelper.getPreferenceHelperInstance().getSelectedClassesForCourse(getActivity());

        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getActivity());
        Log.d("USER", "user:"+mUser.toString());
        selectedLesson = PreferenceHelper.getPreferenceHelperInstance().getSelectedLesson(getActivity());
        view.findViewById(R.id.img_drawer_open).setOnClickListener(v -> EventBus.getDefault().post(new DrawerToggle(true)));

        setupLayout(view);

       /* chatInputView.post(() -> {
            initialY = chatInputView.getY()-100;
            Log.d("initialY", ""+initialY);
        });*/


        if(selectedClassesForCourse==null){
            if(mUser!=null)
            selectedClassesForCourse = new ClassesForCourse("",mUser.getClass_id(), mUser.getCourse_id(),mUser.getSchool_id(),"");
        }
        if(getActivity()!=null) {
            mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getActivity());

            //keyboardHeightProvider = new KeyboardHeightProvider(getActivity());
           // view.post(() -> keyboardHeightProvider.start());
        }
/*
        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(getContext(), "onPageSelected=> "+position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/
        tabLayout.setOnTabClickListener(position -> {
            if(getActivity()!=null) {
                KeyBoard.hide(getActivity(), viewPager);
                floatingActionButtonCalendar.setCompatElevation(10f);
                floatingActionButtonDetails.setCompatElevation(10f);
                if(position==0){
                    view_class_details.setVisibility(View.VISIBLE);
                   // ExpandCollapseExtension.expand(view_class_details);
                    if(isLibraryVideoPlaying){
                        EventBus.getDefault().post(new ResetLibrary(true));
                        onLessonsRequest(mLessons);
                        if(selectedLesson!=null)
                            onLessonSelected(selectedLesson);
                        else {
                            onLessonSelected(mLessons.get(0));
                        }
                    }
                    floatingActionButtonSearch.show();
                }
                else {
                    view_class_details.setVisibility(View.GONE);
                    //ExpandCollapseExtension.collapse(view_class_details);
                    if(getActivity()!=null) {
                        //showRegisterDialog(getActivity());
                        //tabLayout.setCurrentItem(0);

                    }
                    //viewPager.setCurrentItem(0);
                    floatingActionButtonSearch.hide();

                    //viewFlipperSearchInfo.setInAnimation(getActivity(), android.R.anim);
                    viewFlipperSearchInfo.setDisplayedChild(0);
                }

                //viewPager.setCurrentItem(position, false);
                scrollView.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
            }
            else {
                Log.d("getActivity", "getActivity is null");
                //TODO implement firebase crashlytics for handling getActivity() == Null
            }
        });

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add(R.string.playing, PlayListFragment.class)
                .add(R.string.question, ChatFragment.class)
                .add(R.string.library, LibraryFragment.class)
                .create());

        viewPager.setAdapter(adapter);


        tabLayout.setViewPager(viewPager);

        semesters = new ArrayList<>();

        semesterAdapter = new SemesterAdapter(semesters, getActivity(), this);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerViewSemesters.setLayoutManager(manager);
        recyclerViewSemesters.setAdapter(semesterAdapter);

        if(selectedClassesForCourse!=null)
        fetchClassYears(selectedClassesForCourse.getUuid());
        else {

            Toast.makeText(getActivity(), "Class is empty", Toast.LENGTH_SHORT).show();
        }

        int bitrate = PreferenceHelper.getPreferenceHelperInstance().getVideoQuality(getActivity());

        Log.d("BITRATE", "bitrate:"+bitrate);
        new Handler().postDelayed(()->{

            if(bitrate>0) {
                int drawable = getBitrateDrawableTinted(bitrate);
                videoQuality.setImageResource(drawable);
            }
            else {

                videoQuality.setImageResource(R.drawable.sd_blue);
            }
        },300);
        classesForCourses = new ArrayList<>();

        getDownloadDefaultInstance();



        if(mUser!=null)
        getListOfSubjectsOfClass(mUser.getClass_id());
        btn_collapse_expand.setOnClickListener(v -> onSlideViewButtonClick());

       /* tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                if(getActivity()!=null) {
                    KeyBoard.hide(getActivity(),viewPager);
                    floatingActionButtonCalendar.setCompatElevation(10f);
                    floatingActionButtonDetails.setCompatElevation(10f);
                    if(position==0){
                        if(isLibraryVideoPlaying){
                            onLessonsRequest(mLessons);
                            if(selectedLesson!=null)
                                onLessonSelected(selectedLesson);
                            else {
                                onLessonSelected(mLessons.get(0));
                            }
                        }
                        floatingActionButtonSearch.show();
                    }
                    else {
                        viewFlipperSearchInfo.setDisplayedChild(0);
                        easyReveal.hide();
                        floatingActionButtonSearch.hide();
                    }
                    scrollView.setVisibility(View.GONE);
                    viewPager.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(position, false);
                }
                else {
                    Log.d("getActivity", "getActivity is null");
                    //TODO implement firebase crashlytics for handling getActivity() == Null
                }
            }
        });*/

        if(selectedClassesForCourse!=null)
            fetchClassDetails(selectedClassesForCourse.getUuid(),getActivity());

        viewPager.setOnTouchListener((v, event) -> true);

        viewPager.setOffscreenPageLimit(2);



        try {
            if(mUser!=null){
                school = PreferenceHelper.getPreferenceHelperInstance().getSelectedSchool(getActivity());
                classDetails = PreferenceHelper.getPreferenceHelperInstance().getClassDetails(getActivity());

                if(school!=null){
                    //Toast.makeText(getActivity(), ""+school, Toast.LENGTH_SHORT).show();
                    Log.d("School", ""+school);
                    if(school.getId().equalsIgnoreCase(mUser.getSchool_id())){
                        tvSchoolName.setText(school.getName());
                        Glide.with(this).load(school.getLogo_url()).into(imageViewSchoolCrest);

                    }
                    else {
                        Log.d("School", "School is not found");
                        fetchSchoolDetails(mUser.getSchool_id());

                    }
                }
                else {
                    Log.d("School", "School is null");
                    fetchSchoolDetails(mUser.getSchool_id());
                }

                if(classDetails!=null){
                    tvClass.setText(classDetails.getFull_name());

                }
                getListOfClassesForCourse(mUser.getCourse_id());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }



        view.findViewById(R.id.btn_change_subject).setOnClickListener(v -> {
            Log.d("btn_change_subject", classesForCourses.toString());

            Log.d("sdfghjgf", classesForCourses.toString());
            if(classesForCourses.size()==0){
                try {
                    //Toast.makeText(getActivity(), ""+selectedSchool, Toast.LENGTH_SHORT).show();
                    fetchSchoolDetails(mUser.getSchool_id());
                    getListOfClassesForCourse(mUser.getCourse_id());

                    if(classDetails!=null){
                        tvClass.setText(classDetails.getFull_name());
                    }
                }
                catch (Exception e){
                    //startActivity(new Intent(getActivity(), ChangeClassActivity.class));
                    //getActivity().finish();
                }
            }
            else
                provideChangeSubjectPopupWindow(viewChangeSubAnchor,getActivity(), classesForCourses);
        });

        //makeListOfUri();

       // getDataFromIntent();

       // initSource(getActivity());

        SubjectOfClassResponse subject = PreferenceHelper.getPreferenceHelperInstance().getSubject(getActivity());

        if(subject!=null) {
            // Toast.makeText(getActivity(), "" + subject.toString(), Toast.LENGTH_SHORT).show();
            Log.d("SubjectOfClassResponse", "Subject is "+ subject.toString());
        }
        else Log.d("SubjectOfClassResponse", "Subject is null");

        String token = PreferenceHelper.getPreferenceHelperInstance().getToken(getActivity());
        new Handler().postDelayed(()-> getFavoritesBookmarks(token),500);

    }

    private void fetchClassDetails(String selectedClassesForCourse_uuid, Activity activity){

        ApiClient.getApiClient().getInstance(getContext()).fetchClassDetails(selectedClassesForCourse_uuid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassDetailsResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<ClassDetailsResponse> classDetailsResponseResponse) {


                if(classDetailsResponseResponse.isSuccessful()){
                    ClassDetailsResponse classDetailsResponse = classDetailsResponseResponse.body();
                    // Toast.makeText(activity, "fetchClassDetails", Toast.LENGTH_SHORT).show();
                    Log.d("fetchClassDetails", "Regular School "+classDetailsResponse.toString());

                    if(classDetailsResponse.getData()!=null) {
                        tvClass.setText(classDetailsResponse.getData().getFull_name());
                        if(classDetailsResponse.getData().isLocked()){
                            Toast.makeText(getActivity(), "you are trying to access a locked class", Toast.LENGTH_LONG).show();
                        }

                        else {
                            // EventBus.getDefault().post(classDetailsResponse.getData());
                            PreferenceHelper.getPreferenceHelperInstance().setClassDetails(getActivity(), classDetailsResponse.getData());
                        }
                    }
                    else{
                        Toast.makeText(activity, "No lessons available for this course", Toast.LENGTH_SHORT).show();

                    }
                }

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                e.printStackTrace();
            }
        });
    }




    public String getMonth(int month) {
        if(month>0 && month<12)
        return new DateFormatSymbols().getMonths()[month-1];
        else return "";
    }

    private Calendar getSchemeCalendar(int year, int month, int day, int color, String text) {
        Calendar calendar = new Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);
        calendar.setSchemeColor(color);//如果单独标记颜色、则会使用这个颜色
        calendar.setScheme(text);
        return calendar;
    }
/*
    *//***********************************************************
     UI config
     ***********************************************************//*
    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }*/

    @SuppressLint("ClickableViewAccessibility")
    private void setupLayout(View view) {

        viewLayoutControls = view.findViewById(R.id.layout_controls);

        tvLessonDetails = view.findViewById(R.id.tv_lesson_details);
        viewChangeSubAnchor = view.findViewById(R.id.view_change_sub_anchor);

        scroll_view_subject_details = view.findViewById(R.id.scroll_view_subject_details);

        view_anchor = view.findViewById(R.id.view_anchor);

        view_class_details = view.findViewById(R.id.view_class_details);

        mTextMonthYear = view.findViewById(R.id.tv_month_year);

        mRelativeTool = view.findViewById(R.id.rl_tool);

        mCalendarView = view.findViewById(R.id.calendarView);

        mTextCurrentDay = view.findViewById(R.id.tv_current_day);

        viewFlipperSemesterCalendar = view.findViewById(R.id.view_flipper_semester_calendar);

        recyclerViewSemesters = view.findViewById(R.id.recycler_view_semesters);

        mCalendarView.setOnMonthChangeListener(this);
        mCalendarView.setOnCalendarSelectListener(this);


        mYear = mCalendarView.getCurYear();

        //int year = mCalendarView.getCurYear();
        int month = mCalendarView.getCurMonth();

        try {
            String  monthYear = getMonth(month) +" "+ mCalendarView.getCurYear();
            mTextMonthYear.setText(monthYear);
        }
        catch (Exception e){
            e.printStackTrace();

        }
        viewPlayerContainer = view.findViewById(R.id.player_container);

        tvLearnMore = view.findViewById(R.id.tv_learn_more);

        tvLearnMore.setText(Html.fromHtml(learnMore));

        tvClass = view.findViewById(R.id.tv_class);

        tvSubject = view.findViewById(R.id.tv_subject);

        tvTopic = view.findViewById(R.id.tv_topic);

        tvTutor = view.findViewById(R.id.tv_tutor_name);

        btnGetNotes = view.findViewById(R.id.btn_get_notes);

        if(getActivity()!=null) {
            adapterBookmarkDownload= new GenericAdapter(getActivity(), R.layout.item_payment_type_dropdown,bookmarkVideoList);
            emailBookmarkAdapter = new GenericAdapter(getActivity(), R.layout.item_payment_type_dropdown, emailDownloadList);
        }
        btnGetNotes.setOnClickListener(v -> {
            //Toast.makeText(getActivity(), "get notes", Toast.LENGTH_SHORT).show();
            if(emailBookmarkAdapter!=null && getActivity()!=null)
            provideGetNotesPopupWindow(v, getActivity(), emailBookmarkAdapter);
        });

        tvViewCount = view.findViewById(R.id.tv_view_count);

        tvSchoolName = view.findViewById(R.id.tv_school_name);

        imageViewSchoolCrest  = view.findViewById(R.id.img_school_crest);

        view_details_of_course = view.findViewById(R.id.view_details_of_course);

        chatInputView = view.findViewById(R.id.view_search);

        inputLayoutSearch = view.findViewById(R.id.input_search);

        viewLessonDetails = view.findViewById(R.id.view_lesson);

        btn_collapse_expand = view.findViewById(R.id.btn_collapse_expand);

        imgCloseSearch = view.findViewById(R.id.img_close);

        editTextSearch = view.findViewById(R.id.edt_search);


        editTextSearch.setOnTouchListener((v, event) -> false);


        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //EventBus.getDefault().post(new SearchItem(s.toString()));
                Log.d("editTextSearch", "query:"+s.toString());
                PlayListFragment.filterPlaylist(new SearchItem(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        imgCloseSearch.setOnClickListener(v -> {
            KeyBoard.hide(getActivity(),chatInputView);
            PlayListFragment.filterPlaylist(new SearchItem(""));
            floatingActionButtonSearch.show();
            viewFlipperSearchInfo.setDisplayedChild(0);

        });

        scrollView = view.findViewById(R.id.scroll_view_details);

        viewFlipperSearchInfo = view.findViewById(R.id.view_filpper_search_info);

        floatingActionButtonSearch =view.findViewById(R.id.fab_search);

        floatingActionButtonSearch.setOnClickListener(v -> {
            floatingActionButtonSearch.hide();
            //viewFlipperSearchInfo.setInAnimation(getActivity(), R.anim.enter_from_right);
            viewFlipperSearchInfo.setDisplayedChild(1);


            if (isUp) {
                //Toast.makeText(getActivity(), "if isUp: "+isUp, Toast.LENGTH_SHORT).show();
                // btn_collapse_expand.setBackgroundResource(R.drawable.ic_arrow_drop_up);
                Glide.with(this).load(R.drawable.ic_arrow_drop_up).into(btn_collapse_expand);
                //view_details_of_course.setVisibility(View.VISIBLE);
                ExpandCollapseExtension.expand(view_details_of_course);
                isUp = !isUp;
            }
        });

        floatingActionButtonCalendar = view.findViewById(R.id.fab_calendar);

        floatingActionButtonDetails = view.findViewById(R.id.fab_details);



        floatingActionButtonDetails.setOnClickListener(v -> {
            floatingActionButtonCalendar.setCompatElevation(10f);
            if(floatingActionButtonDetails.getCompatElevation() == 0.85f) {
                floatingActionButtonDetails.setCompatElevation(10f);
                scrollView.setVisibility(View.GONE);
                scrollView.setDisplayedChild(1);
                viewPager.setVisibility(View.VISIBLE);
               // Toast.makeText(getActivity(), "viewPager getCurrentItem: "+viewPager.getCurrentItem() ,Toast.LENGTH_SHORT).show();
                if(viewPager.getCurrentItem() !=0){
                    floatingActionButtonSearch.hide();
                }
                else {
                    floatingActionButtonSearch.show();
                }

            }
            else {
                floatingActionButtonSearch.hide();
                floatingActionButtonDetails.setCompatElevation(0.85f);
                scrollView.setVisibility(View.VISIBLE);
                scrollView.setDisplayedChild(1);
                viewPager.setVisibility(View.GONE);
            }
        });



        floatingActionButtonCalendar.setOnClickListener(v -> {
            floatingActionButtonDetails.setCompatElevation(10f);
            if (floatingActionButtonCalendar.getCompatElevation() == 0.85f) {
                floatingActionButtonSearch.show();
                floatingActionButtonCalendar.setCompatElevation(10f);
                scrollView.setVisibility(View.GONE);
                scrollView.setDisplayedChild(0);
                viewPager.setVisibility(View.VISIBLE);
            } else {
                floatingActionButtonSearch.hide();
                floatingActionButtonCalendar.setCompatElevation(0.85f);
                scrollView.setVisibility(View.VISIBLE);
                scrollView.setDisplayedChild(0);
                viewFlipperSemesterCalendar.setDisplayedChild(0);
                viewPager.setVisibility(View.GONE);
            }
        });

        view.findViewById(R.id.btn_calendar_previous).setOnClickListener(v -> mCalendarView.scrollToPre(true));
        view.findViewById(R.id.btn_calendar_next).setOnClickListener(v -> mCalendarView.scrollToNext(true));

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tab_layout);
        playerView = view.findViewById(R.id.player_view);
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        if(isTablet){
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
            params.width= ViewGroup.LayoutParams.MATCH_PARENT;
            if(getActivity()!=null)
                params.height=convertDpToPixelInt(400, getActivity());
            playerView.setLayoutParams(params);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        }
        progressBar = view.findViewById(R.id.progress_bar);
        subtitle = view.findViewById(R.id.btn_subtitle);
        videoQuality = view.findViewById(R.id.exo_quality);

        viewBookMark = view.findViewById(R.id.btn_bookmark);

        nextBtn = view.findViewById(R.id.btn_next);
        fullScreenBtn = view.findViewById(R.id.exo_fullscreen);
        AppCompatImageButton preBtn = view.findViewById(R.id.exo_prev);
        retry = view.findViewById(R.id.retry_btn);


        //optional setting
        if(playerView.getSubtitleView()!=null)
            playerView.getSubtitleView().setVisibility(View.GONE);
        fullScreenBtn.setOnClickListener(this);
        subtitle.setOnClickListener(this);
        videoQuality.setOnClickListener(this);

        nextBtn.setOnClickListener(this);
        preBtn.setOnClickListener(this);
        viewBookMark.setOnClickListener(this);
        retry.setOnClickListener(this);

    }



    public void isBookMarked(Favorite favorite, boolean isBookmark){
        if(isBookmark)
        viewBookMark.setImageResource(R.drawable.bookmark_red);
        else viewBookMark.setImageResource(R.drawable.bookmark_icon);
    }

    public void isBookMarked(boolean isBookmark){
        if(isBookmark)
            viewBookMark.setImageResource(R.drawable.bookmark_red);
        else viewBookMark.setImageResource(R.drawable.bookmark_icon);
    }

    private void getListOfClassesForCourse(String courseId){

        ApiClient.getApiClient().getInstance(getContext())
                .getListOfClassesForCourse(courseId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassesForCourseResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<ClassesForCourseResponse> classesForCourseResponseResponse) {
                if(classesForCourseResponseResponse.isSuccessful()){
                    ClassesForCourseResponse classesForCourseResponse = classesForCourseResponseResponse.body();
                    Log.d("ListOfClassesForCourse", ""+classesForCourseResponse);

                    classesForCourses.clear();
                    try {
                         classesForCourses.addAll(classesForCourseResponse.getData());
                    }
                    catch (Exception e){
                        Log.d("ListOfClassesForCourse", "Error: "+e.getMessage());
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });
    }


    private void fetchSchoolDetails(String schoolId){

        ApiClient.getApiClient().getInstance(getContext()).fetchSchoolDetails(schoolId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<SchoolDetailsResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<SchoolDetailsResponse> schoolDetailsResponseResponse) {

                if(schoolDetailsResponseResponse.isSuccessful()){

                    SchoolDetailsResponse schoolDetailsResponse = schoolDetailsResponseResponse.body();

                    if(schoolDetailsResponse!=null) {
                        tvSchoolName.setText(schoolDetailsResponse.getData().getName());

                        PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getActivity(),schoolDetailsResponse.getData());

                       // tvClass.setText(schoolDetailsResponse.getData().getSubject_name());
                        Log.d("schoolDetailsResponse", schoolDetailsResponse.getData().toString());
                    }

                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });

    }

    private void fetchClassYears(String classId){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .fetchClassYears(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<Years>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<Years> yearsResponse) {

                        if(yearsResponse.isSuccessful()){
                            if(yearsResponse.body()!=null) {
                                Log.d("fetchClassYears", "Success" + yearsResponse.body());
                                if (yearsResponse.body().getData() != null) {

                                    if(yearsResponse.body().getData().size()>0){
                                        int year = yearsResponse.body().getData().get(0).getYear();
                                        fetchClassSemesters(classId,String.valueOf(year));
                                    }
                                    else {
                                        fetchClassSemesters(classId, "2020");
                                    }
                                }
                            }
                        }
                        else {
                            Log.d("fetchClassYears", "Success"+ yearsResponse.errorBody());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Log.d("fetchClassYears", "Error"+ e.getMessage());
                    }
                });
    }


    private void fetchEvents(String classId, String semesterId){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .fetchEvents( classId, semesterId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<Events>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<Events> eventsResponse) {


                        if(eventsResponse.isSuccessful()){
                            mCalendarView.clearSchemeDate();
                            map.clear();
                            if(eventsResponse.body()!=null) {
                                for (String event : eventsResponse.body().getData()) {
                                    String percentage = "100";
                                    int colorCode =0xFF2196F3;
                                    DateTime eventDate = DateTime.parse(event, DateTimeFormat.forPattern("yyyy-MM-dd"));
                                    map.put(getSchemeCalendar(eventDate.getYear(), eventDate.getMonthOfYear(), eventDate.getDayOfMonth(), colorCode, percentage).toString(),
                                            getSchemeCalendar(eventDate.getYear(), eventDate.getMonthOfYear(), eventDate.getDayOfMonth(), colorCode, percentage));
                                }
                            }
                            mCalendarView.setSchemeDate(map);
                            //mCalendarView
                            mCalendarView.update();

                            Log.d("fetchEvents", ""+eventsResponse.body());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                    }
                });
    }

    private void fetchClassSemesters(String classId,String year){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .fetchClassSemesters(classId,year)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<Semesters>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<Semesters> semestersResponse) {

                        if(semestersResponse.isSuccessful()){

                            if(semestersResponse.body()!=null) {
                                semesters.addAll(semestersResponse.body().getData());
                                semesterAdapter.notifyDataSetChanged();
                            }
                            Log.d("fetchClassSemesters", "Success:"+semestersResponse.body());
                        }
                        else {
                            Log.d("fetchClassSemesters", "Success:"+semestersResponse.errorBody());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                        Log.d("fetchClassSemesters", "Error:"+e.getMessage());
                    }
                });
    }

    private void  getListOfSubjectsOfClass(String classId){
        ApiClient.getApiClient().getInstance(getContext()).getListOfSubjectsOfClass(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<SubjectOfClassResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull
                                          Response<SubjectOfClassResponse> subjectOfClassResponseResponse) {
                if(subjectOfClassResponseResponse.isSuccessful()){
                    if(subjectOfClassResponseResponse.body()!=null) {
                        //  if(subjectOfClassResponseResponse.body().getData()!=null)
                        // PreferenceHelper.getPreferenceHelperInstance().setSubject(getActivity(),subjectOfClassResponseResponse.body());
                        Log.d("SubjectOfClassResponse", ""+subjectOfClassResponseResponse.body().getData());
                    }
                }
                else {
                    if(getActivity()!=null)
                    Toast.makeText(getActivity(), "Error:"+subjectOfClassResponseResponse.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                e.printStackTrace();
            }
        });
    }

    private void getDownloadDefaultInstance(){
        fetch = Fetch.Impl.getDefaultInstance();
    }

    private void initializeDb(Activity activity) {
        database = AppDatabase.Companion.getDatabase(activity);
    }

    private void downloadNote(String url) {

        final String filePath = Constants.getExtDir() + "/XtraClass/Notes/" + Constants.getNameFromUrl(url);
        request = new Request(url, filePath);
        fetch.attachFetchObserversForDownload(request.getId(),this).enqueue(request,
                result -> request = result, result -> Log.d("SingleDownloadActivity", result.toString()));

    }

    private void enqueueDownload(String url, Activity activity) {
        final String filePath = Constants.getSaveDir(activity) + "/Offline/" + Constants.getNameFromUrl(url);
        request = new Request(url, filePath);
        // request.setExtras(getExtrasForRequest(request));

        Toast.makeText(activity, "Saving lesson for offline.", Toast.LENGTH_SHORT).show();
        fetch.attachFetchObserversForDownload(request.getId(),this).enqueue(request,
                result -> request = result, result -> Log.d("SingleDownloadActivity", result.toString()));

    }




    //
    public void onSlideViewButtonClick() {

        int pos = viewPager.getCurrentItem();

        if (isUp) {
            //Toast.makeText(getActivity(), "if isUp: "+isUp, Toast.LENGTH_SHORT).show();
           // btn_collapse_expand.setBackgroundResource(R.drawable.ic_arrow_drop_up);
            Glide.with(this).load(R.drawable.ic_arrow_drop_up).into(btn_collapse_expand);
            view_details_of_course.setVisibility(View.VISIBLE);
            //ExpandCollapseExtension.expand(view_details_of_course);

            if(editTextSearch.getVisibility() == View.GONE){
                if(pos==0) {
                    if(floatingActionButtonCalendar.getCompatElevation() == 0.85f ||floatingActionButtonDetails.getCompatElevation() == 0.85f){
                        floatingActionButtonSearch.hide();
                    }
                    else
                        floatingActionButtonSearch.show();
                }else floatingActionButtonSearch.hide();
            }
        }
        else {
            if(pos==0) {
                if(floatingActionButtonCalendar.getCompatElevation() == 0.85f ||floatingActionButtonDetails.getCompatElevation() == 0.85f){
                    floatingActionButtonSearch.hide();
                }
                else
                floatingActionButtonSearch.show();
                // viewPager.postDelayed(() -> viewPager.setCurrentItem(1), 100);
            }
            else floatingActionButtonSearch.hide();
            //Toast.makeText(getActivity(), "else isUp: "+isUp, Toast.LENGTH_SHORT).show();
            //btn_collapse_expand.setBackgroundResource(R.drawable.ic_arrow_drop_down);
            Glide.with(this).load(R.drawable.ic_arrow_drop_down).into(btn_collapse_expand);
            view_details_of_course.setVisibility(View.GONE);
           //ExpandCollapseExtension.collapse(view_details_of_course);

        }

       // EventBus.getDefault().post(new ViewPagerUpdate(pos));



        viewPager.setVisibility(View.INVISIBLE);

        if(pos==0) {
            viewPager.setCurrentItem(1);
            //viewPager.postDelayed(() -> viewPager.setCurrentItem(1), 100);
        }
        else if(pos==1){
            viewPager.setCurrentItem(0);
            //viewPager.postDelayed(() -> viewPager.setCurrentItem(0), 100);
        }

        else if(pos==2){
            viewPager.setCurrentItem(1);
            //viewPager.postDelayed(() -> viewPager.setCurrentItem(1), 100);
        }

        viewPager.postDelayed(()->viewPager.setCurrentItem(pos), 200);

        if(floatingActionButtonCalendar.getCompatElevation() == 0.85f ||floatingActionButtonDetails.getCompatElevation() == 0.85f){
            viewPager.postDelayed(()->viewPager.setVisibility(View.GONE),500);
        }
        else
            viewPager.postDelayed(()->viewPager.setVisibility(View.VISIBLE),500);

       /* if(scrollView.getVisibility() == View.VISIBLE){
            viewPager.postDelayed(()->viewPager.setVisibility(View.GONE),500);
        }
        else {
            viewPager.postDelayed(()->viewPager.setVisibility(View.VISIBLE),500);
        }*/

        isUp = !isUp;
    }



    private int getBitrateDrawableTinted(int bitrate) {
        if (bitrate == Format.NO_VALUE) {
            return R.drawable.au_blue;
        }
        if (bitrate <= BITRATE_160P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_240P) {
            return R.drawable.ld_blue;
        }
        if (bitrate <= BITRATE_360P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_480P) {
            return R.drawable.sd_blue;
        }
        return R.drawable.hd_blue;
    }


    private void sendEmail(String lessonId, String token){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .postSendEmail(lessonId, "Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<EmailSending>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<EmailSending> emailSendingResponse) {

                        if(emailSendingResponse.isSuccessful()){
                            provideWindowEmailSent(viewLayoutControls,getActivity());
                            Toast.makeText(getActivity(), "Email is successfully sent to your email address", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getActivity(), "Oops. Something went wrong with sending the attachment to your email address. Try again later", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Toast.makeText(getActivity(), "Oops. Something went wrong with sending the attachment to your email address. Try again later", Toast.LENGTH_LONG).show();
                    }
                });
    }



    private void provideChangeSubjectPopupWindow(View it, Context context, List<ClassesForCourse> classesForCourses){
        if(popupWindowChangeSubject!=null){
            popupWindowChangeSubject.dismiss();
            popupWindowChangeSubject = null;
        }
        int width = 200;
        String device = Constants.getSizeName(context);
        switch (device){
            case "small":
            case "normal":
                width = 200;
                break;

            case "xlarge":
            case "large":
                width = 300;
                    /*if(libraryItems.size()%3!=0){
                        int whatsLeft = libraryItems.size()%3;

                        for(int i =1; i<whatsLeft; i++) {
                            libraryItems.add(new LibraryMenu(R.drawable.add_icon_blue, "",
                                    R.color.colorSelectedLesson, -1));
                        }
                    }*/
                break;
        }
        ChangeSubjectAdapter changeSubjectAdapter = new ChangeSubjectAdapter(context,R.layout.item_subject, classesForCourses);
        popupWindowChangeSubject = new PopupWindow(convertDpToPixelInt(width,context), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowChangeSubject.setOutsideTouchable(true);
        popupWindowChangeSubject.setFocusable(true);
        popupWindowChangeSubject.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(changeSubjectAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            ClassesForCourse classesForCourse = changeSubjectAdapter.getItem(position);

            if(getActivity()!=null){
                PreferenceHelper.getPreferenceHelperInstance().setPlayPrevious(getActivity(), false);
                PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(getActivity(),classesForCourse);
                PreferenceHelper.getPreferenceHelperInstance().setLessons(getActivity(), new ArrayList<>());
                PreferenceHelper.getPreferenceHelperInstance().setFavorites(getActivity(),new ArrayList<>());
                PreferenceHelper.getPreferenceHelperInstance().setClassVideoPlayOn(getActivity(),"");
                PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getActivity());
                if(player!=null)
                    player.releasePlayer();
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                Log.d("ChangeSubjectAdapter", classesForCourse.toString());
            }

            else {
                Log.d("getActivity", "getActivity is null");
                //TODO implement firebase crashlytics for handling getActivity() == Null
            }

            //viewModel.setLegalCountry(selectedCountry);
            popupWindowChangeSubject.dismiss();
        });
        popupWindowChangeSubject.setContentView(parent_view);
        popupWindowChangeSubject.setElevation(20);

        if(popupWindowChangeSubject!=null) {

            popupWindowChangeSubject.showAsDropDown(it, 0,0);
        }

    }


    public void provideWindowEmailSent(View it, Context context){
        if(popupWindowEmailSent !=null){
            popupWindowEmailSent.dismiss();
            popupWindowEmailSent = null;
        }
        popupWindowEmailSent = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
       // Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
      //  popupWindowEmailSent.setBackgroundDrawable(backgroundDrawable);
        popupWindowEmailSent.setOutsideTouchable(true);
        popupWindowEmailSent.setFocusable(true);
        popupWindowEmailSent.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = (View) layoutInflater.inflate(R.layout.email_dialog, null, false);
        LottieAnimationView lottieAnimationViewEmailSent = testView.findViewById(R.id.an_email_sent);
        lottieAnimationViewEmailSent.playAnimation();
        AppCompatImageButton btn_confirm = testView.findViewById(R.id.btn_close);
        btn_confirm.setOnClickListener(v -> popupWindowEmailSent.dismiss());
        popupWindowEmailSent.setContentView(testView);
        popupWindowEmailSent.setElevation(10);
        if(popupWindowEmailSent !=null) {
            popupWindowEmailSent.showAsDropDown(it, 0, -it.getHeight());
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    public void provideWindowDownloadProgress(View it, Context context){
        if(popupWindowDownloading !=null){
            popupWindowDownloading.dismiss();
            popupWindowDownloading = null;
        }
        popupWindowDownloading = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        // Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //  popupWindowEmailSent.setBackgroundDrawable(backgroundDrawable);
        popupWindowDownloading.setOutsideTouchable(false);
        popupWindowDownloading.setFocusable(false);

        popupWindowDownloading.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = (View) layoutInflater.inflate(R.layout.download_dialog, null, false);
        viewDownloadProgress = testView.findViewById(R.id.view_progress);
        viewDownloadAnimation =testView.findViewById(R.id.view_download_animation);
        circularProgressBar = testView.findViewById(R.id.circularProgressBar);
        tvETA = testView.findViewById(R.id.tv_eta);
        tvDownloadStatus = testView.findViewById(R.id.tv_download_status);
        tvPercentage = testView.findViewById(R.id.tv_percentage);
        lottieAnimationViewDone = testView.findViewById(R.id.an_download_complete);
        circularProgressBar.setOnTouchListener((View view, MotionEvent motionEvent) -> true);
        circularProgressBar.setMax(100);
        circularProgressBar.post(() -> circularProgressBar.setProgress(0));

        //circularProgressBar.setProgressDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.seekbar_style_green,null));
        AppCompatImageButton btn_confirm = testView.findViewById(R.id.btn_close);
        btn_confirm.setOnClickListener(v -> {
            popupWindowDownloading.dismiss();
            popupWindowDownloading =null;
        });
        popupWindowDownloading.setContentView(testView);
        popupWindowDownloading.setElevation(10);
        if(popupWindowDownloading !=null) {
            popupWindowDownloading.showAsDropDown(it, 0, -it.getHeight());
        }

    }


    private void provideGetNotesPopupWindow(View it, Context context, GenericAdapter adapter) {

        if(popupWindowGetNotes!=null){
            popupWindowGetNotes.dismiss();
            popupWindowGetNotes = null;
        }
        //if(scroll_view_subject_details.getMaxScrollAmount())
        scroll_view_subject_details.post(() -> scroll_view_subject_details.fullScroll(View.FOCUS_DOWN));
        popupWindowGetNotes = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
       // popupWindowGetNotes.setBackgroundDrawable(backgroundDrawable);
        popupWindowGetNotes.setOutsideTouchable(true);
        popupWindowGetNotes.setFocusable(true);
        //popupWindowGetNotes.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            GenericItem item = adapter.getItem(position);

            if(item!=null) {
                if(item.getId() == 2){
                    if(selectedLesson!=null)
                        downloadNote(selectedLesson.getNotes_url());
                    //Download finished successfully. You can check the file in the Downloads folder
                }
                else if(item.getId() ==1){
                    if(!TextUtils.isEmpty(mToken)) {
                        sendEmail(selectedLesson.getLesson_id(), mToken);
                        Toast.makeText(context, "Email is being sent...", Toast.LENGTH_LONG).show();
                    }

                    //"Email is being sent..."
                }
            }

            //viewModel.setLegalCountry(selectedCountry);
            popupWindowGetNotes.dismiss();
        });
        popupWindowGetNotes.setContentView(parent_view);
        popupWindowGetNotes.setElevation(20);

        if(popupWindowGetNotes!=null) {
            new Handler().postDelayed(()-> popupWindowGetNotes.showAsDropDown(it, 0, -(it.getHeight()*2)),200);
        }

    }

    private void provideBookmarkDownloadPopupWindow(View it, Context context, GenericAdapter adapter) {

        if(popupWindowBookmarkDownload!=null){
            popupWindowBookmarkDownload.dismiss();
            popupWindowBookmarkDownload = null;
        }
        popupWindowBookmarkDownload = new PopupWindow(convertDpToPixelInt(200,context), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowBookmarkDownload.setOutsideTouchable(true);
        popupWindowBookmarkDownload.setFocusable(true);
        popupWindowBookmarkDownload.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            GenericItem item = adapter.getItem(position);

            if(item.getId() == 2){

                String url = MEDIA_HUB+selectedLesson.getVideo_info().getOriginal_media_url();
                Log.d("DownloadBookmarkDialog",url);
                enqueueDownload(url, getActivity());
            }
            else if(item.getId() ==1){
                String mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getActivity());
                putFavorite(selectedLesson.getLesson_id(), mToken);
            }

            //viewModel.setLegalCountry(selectedCountry);
            popupWindowBookmarkDownload.dismiss();
        });
        popupWindowBookmarkDownload.setContentView(parent_view);
        popupWindowBookmarkDownload.setElevation(20);

        if(popupWindowBookmarkDownload!=null) {

            popupWindowBookmarkDownload.showAsDropDown(it, -(it.getWidth()/3),0);
        }

    }





    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLessonSelected(Lesson lesson){
        Log.d("onLessonSelected", ""+lesson);
        isLibraryVideoPlaying = false;
        String topic = "Topic: <b>"+lesson.getTopic_name() + "</b>";
        tvTopic.setText(Html.fromHtml(topic));
        String subject = "Subject: <b>"+lesson.getSubject()+ "</b>";
        tvSubject.setText(Html.fromHtml(subject));
        String tutor = "Tutor: <b>"+lesson.getTeacher_name()+"</b>";
        tvTutor.setText(Html.fromHtml(tutor));
        String count = "Views "+generate(5,20);
        tvViewCount.setText(count);
        //tvLearnMore.setVisibility(View.GONE);

        if(player!=null) {

            List<Favorite> favorites = PreferenceHelper.getPreferenceHelperInstance().getFavorites(getActivity());

            if(favorites!=null) {
                for (Favorite favorite : favorites) {
                    Log.d("Favorite=>", "" + favorite);
                    if(favorite!=null) {
                        if(favorite.getPeriodId()!=null) {
                            isBookMarked(favorite, favorite.getPeriodId().equalsIgnoreCase(lesson.getPeriod_id())
                                    && favorite.getClassId().equalsIgnoreCase(lesson.getClass_id()));
                        }
                    }
                }
            }
           // Log.d("onLessonSelected", "master_file:"+lesson.getVideo_info().getHls_info().getMaster_file());
            //Toast.makeText(getActivity(), "onLessonSelected:"+ lesson.getPeriod_title(), Toast.LENGTH_SHORT).show();
            selectedLesson = lesson;

            PlaybackPosition playbackPosition = PreferenceHelper.getPreferenceHelperInstance().getPlayBackPosition(getActivity());

            if(TextUtils.isEmpty(selectedLesson.getNotes_url())){
                btnGetNotes.setVisibility(View.GONE);
            }
            else {
                btnGetNotes.setVisibility(View.VISIBLE);
            }

           /* int orientation = this.getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                // code for portrait mode
            } else {
                // code for landscape mode
            }*/
            PreferenceHelper.getPreferenceHelperInstance().setSelectedLesson(getActivity(), selectedLesson);

            player.seekTo(lesson.getId());
            if(playbackPosition!=null) {
                String url = MEDIA_HUB+selectedLesson.getVideo_info().getHls_info().getMaster_file();
                if (selectedLesson.getLesson_id().equalsIgnoreCase(playbackPosition.getLessonId()) && url.equalsIgnoreCase(playbackPosition.getVideo_url())) {

                    player.seekToSelectedPosition(playbackPosition.getPlayBackPosition());
                }
            }

        }
        // player.getPlayer().seekTo(lesson.getId(),C.TIME_UNSET);
                /*MediaSource videoSource  = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem);
                player.setMediaSource(videoSource);*/

        // player.getPlayer()
    }


/*    private String getVideoSource(){

        List<String> videoUrls = new ArrayList<>();

       *//* videoUrls.add("https://demo.mediacms.io/media/hls/331ae49619ee445ea6298fae9c4e8734/master.m3u8");
        videoUrls.add("https://demo.mediacms.io/media/hls/8ce34ab5be644219a24ff26dfeadd2d8/master.m3u8");
        videoUrls.add("https://demo.mediacms.io/media/hls/f8c523723c4d49c9949c4f24c21f9f8e/master.m3u8");
        videoUrls.add("https://demo.mediacms.io/media/hls/3a8f9194c16f4ef6ad9881bd8bcb5e62/master.m3u8");
        videoUrls.add("https://demo.mediacms.io/media/hls/569f0603b20b4ad9a8ea4376ae61c13b/master.m3u8");
        videoUrls.add("https://demo.mediacms.io/media/hls/3ce636fb15df49e9ab333f8bab38d6ae/master.m3u8");
        videoUrls.add("https://demo.mediacms.io/media/hls/3eeba88751b44fc7a2c2521bdfab710b/master.m3u8");
        *//*


        videoUrls.add("https://mediahub.desafrica.com/media/hls/899e5673bfa94b68a021f7848628bb32/master.m3u8");
        videoUrls.add("https://mediahub.desafrica.com/media/hls/899e5673bfa94b68a021f7848628bb32/master.m3u8");
        videoUrls.add("https://mediahub.desafrica.com/media/hls/899e5673bfa94b68a021f7848628bb32/master.m3u8");
        videoUrls.add("https://mediahub.desafrica.com/media/hls/899e5673bfa94b68a021f7848628bb32/master.m3u8");
        videoUrls.add("https://mediahub.desafrica.com/media/hls/899e5673bfa94b68a021f7848628bb32/master.m3u8");
        videoUrls.add("https://mediahub.desafrica.com/media/hls/899e5673bfa94b68a021f7848628bb32/master.m3u8");
        videoUrls.add("https://mediahub.desafrica.com/media/hls/899e5673bfa94b68a021f7848628bb32/master.m3u8");

        return videoUrls.get(new Random().nextInt(videoUrls.size()));
    }*/



    private void getFavoritesBookmarks(String token){
        Log.d("USER_TOKEN", token);
        ApiClient.getApiClient()
                .getInstance(getContext())
                .getFavorites("Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<Favorites>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<Favorites> favoritesResponse) {

                        if(favoritesResponse.isSuccessful()) {

                            try {
                                if(favoritesResponse.body()!=null) {
                                    PreferenceHelper.getPreferenceHelperInstance().setFavorites(getActivity(), favoritesResponse.body().getData());

                                    for (Favorite favorite: favoritesResponse.body().getData()) {
                                        Log.d("Favorite=>",""+ favorite);

                                        if (favorite.getPeriodId()!=null) {
                                            if(favorite.getPeriodId().equalsIgnoreCase(selectedLesson.getPeriod_id())
                                                    && favorite.getClassId().equalsIgnoreCase(selectedLesson.getClass_id())){
                                                isBookMarked(favorite, true);
                                            }
                                        }

                                    }
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();

                            }
                            Log.d("getFavorites", "onSuccess=> " + favoritesResponse.body());
                        }
                        else {
                            Log.d("getFavorites", "onSuccess=> errorBody:" + favoritesResponse.errorBody());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Log.d("getFavorites", "onError=> "+e.getMessage());
                    }
                });
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLibrarySelected(Library library) {
        isLibraryVideoPlaying =true;
        if (player != null) {
           // Toast.makeText(getActivity(), "onLibrarySelected:"+library.getSource(), Toast.LENGTH_SHORT).show();
            player.seekTo(library.getId()-1);

        }
        else {

            Toast.makeText(getActivity(), "player is null", Toast.LENGTH_SHORT).show();
            getLibraryVideoSource();
            initSourceLibraryVideos(getActivity());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLibraryVideos(LibrariesEventBus libraries){

        isLibraryVideoPlaying = true;
      //  Toast.makeText(getActivity(), "onLibraryVideos:"+libraries.getLibraries().size(), Toast.LENGTH_SHORT).show();
        for (Library library: libraries.getLibraries()){
            libraryVideoUriList.add(new Video(library.getSource(), Long.getLong("zero", 0)));
        }

        if (database.videoDao().getAllUrls().size() == 0) {
            database.videoDao().insertAllVideoUrl(libraryVideoUriList);
            database.videoDao().insertAllSubtitleUrl(subtitleList);
        }
        /*if (database.videoDao().getAllUrls().size() == 0) {
            database.videoDao().insertAllVideoUrl(libraryVideoUriList);
        }*/
        //makeListOfUri();
        getLibraryVideoSource();
        initSourceLibraryVideos(getActivity());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLessonsRequest(List<Lesson> lessons){

       List<Favorite> favorites = PreferenceHelper.getPreferenceHelperInstance().getFavorites(getActivity());


        isLibraryVideoPlaying = false;
        mLessons = lessons;
        for (int i =0; i<lessons.size(); i++) {
            Lesson lesson = lessons.get(i);
            lesson.setId(i);
            File myFile;
            try {
                String video_url = MEDIA_HUB+lesson.getVideo_info().getHls_info().getMaster_file();

                if(lesson.getVideo_info().getOriginal_media_url()!=null && getActivity()!=null){
                    String filePath = Constants.getSaveDir(getActivity()) + "/Offline/" + Constants.getNameFromUrl(lesson.getVideo_info().getOriginal_media_url());
                    myFile = new File(filePath);
                    if (myFile.exists()) {
                        video_url = "file://" + myFile.getPath();
                    }

                }
                if(favorites!=null) {
                    for (Favorite favorite : favorites) {
                        Log.d("Favorite=>", "" + favorite);

                        if(favorite.getPeriodId()!=null || favorite.getClassId()!=null) {
                            if (favorite.getPeriodId().equalsIgnoreCase(lesson.getPeriod_id())
                                    && favorite.getClassId().equalsIgnoreCase(lesson.getClass_id())) {
                                lesson.setFavorite(true);
                            }
                        }
                    }
                }

                Log.d("video_url",video_url);
                videoUriList.add(new Video(video_url, Long.getLong("zero", 0)));
            }
            catch (Exception e){
                e.printStackTrace();

            }

        }

        Log.d("onLessonsRequest", "size:"+videoUriList.size());
        if (database.videoDao().getAllUrls().size() == 0) {
            database.videoDao().insertAllVideoUrl(videoUriList);
            database.videoDao().insertAllSubtitleUrl(subtitleList);
        }

        //makeListOfUri();

        getDataFromIntent();

        initSource(getActivity());

       /* mediaItems.clear();
        if(lessons.size()>0) {
            player.clearMediaItems();

            for (Lesson lesson : lessons) {
                String text = lesson.getPeriod_title() +""+lesson.getTopic_name();

                //Spannable spannable = new SpannableString(text);
                //spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.buttonColorBlue)), 0,lesson.getPeriod_title().length(),
               //         Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                if(lesson.getVimeoResponse().getRequest().getFiles()!=null){
                    if(lesson.getVimeoResponse().getRequest().getFiles().getProgressive().size()>0){

                        MediaItem mediaItem =
                                new MediaItem.Builder().setMediaMetadata(new MediaMetadata.Builder().setTitle(text).build()).setUri(lesson.getVimeoResponse().getRequest()
                                        .getFiles().getProgressive().get(0).getUrl())
                                        .setMediaId(lesson.getLesson_id())
                                        .setMimeType(MimeTypes.BASE_TYPE_VIDEO)
                                        .build();
                        mediaItems.add(mediaItem);
                    }
                }

            }

            player.setMediaItems(mediaItems, *//* resetPosition= *//* true);
         */

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
         if (player != null) {
             player.pausePlayer();
         }

    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
       /* if (player != null)
            player.resumePlayer();*/
    }

  /*  @Override
    public void onDestroy() {
        super.onDestroy();
        if(player!=null)
        player.releasePlayer();
        if(keyboardHeightProvider!=null)
            keyboardHeightProvider.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(keyboardHeightProvider!=null)
            keyboardHeightProvider.setKeyboardHeightObserver(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(keyboardHeightProvider!=null)
            keyboardHeightProvider.setKeyboardHeightObserver(null);
    }
*/

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int controllerId = view.getId();

        switch (controllerId) {

            case R.id.exo_quality:
                player.setSelectedQuality(getActivity());
                break;
            case R.id.btn_subtitle:
                prepareSubtitles();
                break;

            case R.id.retry_btn:
                initSource(getActivity());
                showProgressBar(true);
                showRetryBtn(false);
                break;
            case R.id.btn_next:
                player.seekToNext();
                checkIfVideoHasSubtitle();
                break;
            case R.id.btn_bookmark:
                //TODO bookmark or download video

                if(selectedLesson!=null) {
                    provideBookmarkDownloadPopupWindow(viewBookMark, getContext(), adapterBookmarkDownload);
                }
                //enqueueDownload("http://video.desafrica.com"+selectedLesson.getVideo_info().getEncodings_info().getFourEighty().getH264().getUrl(), getActivity());
                break;
            case R.id.exo_prev:
                player.seekToPrevious();
                checkIfVideoHasSubtitle();
                break;

            case R.id.exo_fullscreen:
                int orientation = this.getResources().getConfiguration().orientation;
                Long watchedLength = player.getPlayer().getCurrentPosition();
                PlaybackPosition playbackPosition = new PlaybackPosition(selectedLesson.getLesson_id(), MEDIA_HUB+""+selectedLesson.getVideo_info().getHls_info().getMaster_file(),
                        watchedLength);

                PreferenceHelper.getPreferenceHelperInstance().setPlayBackPosition(getActivity(), playbackPosition);

                Log.d("WatchedLength", "position:"+ watchedLength);
                if(orientation == Configuration.ORIENTATION_PORTRAIT){
                    //  view_details_controller.setVisibility(View.GONE);
                    viewPager.setVisibility(View.GONE);
                    floatingActionButtonSearch.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
                    params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                    params.height= ViewGroup.LayoutParams.MATCH_PARENT;

                    playerView.setLayoutParams(params);
                    playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                    if(getActivity()!=null)
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    // Toast.makeText(getActivity(), "land", Toast.LENGTH_SHORT).show();
                }
                else {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
                    params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                    if(getActivity()!=null) {
                        int dp = 230;
                        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
                        if(isTablet)
                            dp = 400;

                        params.height = convertDpToPixelInt(dp, getActivity());
                    }
                    playerView.setLayoutParams(params);
                    playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                    floatingActionButtonSearch.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                    if(player!=null) {
                        player.releasePlayer();
                        //TODO
                        onLessonsRequest(PreferenceHelper.getPreferenceHelperInstance().getLessons(getActivity()));
                        onLessonSelected(PreferenceHelper.getPreferenceHelperInstance().getSelectedLesson(getActivity()));

                    }
                    if(getActivity()!=null) {
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }

                }

               /* if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                } else {
                    if(getActivity()!=null) {
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    }
                }*/
                break;
            default:
                break;
        }
    }


    @Override
    public void onDestroyView() {
        if(player!=null)
            player.releasePlayer();
        super.onDestroyView();

    }

    @Override
    public void onDetach() {
        if(player!=null)
            player.releasePlayer();
        super.onDetach();

    }

    private void setTitleView(@NonNull final String fileName) {
        final Uri uri = Uri.parse(fileName);
        // titleTextView.setText(uri.getLastPathSegment());
    }


    private void setProgressView(@NonNull final Status status, final int progress, final String eta, final String speed) {
        switch (status) {
            case QUEUED: {
                Toast.makeText(getActivity(), ""+getString(R.string.queued),Toast.LENGTH_LONG).show();
                //progressTextView.setText(R.string.queued);
                break;
            }
            case ADDED: {
                Toast.makeText(getActivity(), ""+getString(R.string.added),Toast.LENGTH_LONG).show();
                // progressTextView.setText(R.string.added);
                break;
            }
            case DOWNLOADING:
                if(popupWindowDownloading==null)
                    provideWindowDownloadProgress(viewLayoutControls,getContext());
                ObjectAnimator.ofInt(circularProgressBar, "progress", progress).setDuration(2000).start();
                String finalETA = "ETA: <b>"+ eta +"</b>";
                if(TextUtils.isEmpty(eta.trim()))
                    tvETA.setVisibility(View.GONE);
                else tvETA.setVisibility(View.VISIBLE);
                tvETA.setText(Html.fromHtml(finalETA));
                String finalSpeed = "<b>"+speed+"</b>";
                tvPercentage.setText(Html.fromHtml(finalSpeed));
                //circularProgressBar.setSecondaryProgress(progress);
                if(progress==100){
                    viewDownloadProgress.setVisibility(View.GONE);
                    viewDownloadAnimation.setVisibility(View.VISIBLE);
                    lottieAnimationViewDone.playAnimation();
                }
                else {
                    viewDownloadProgress.setVisibility(View.VISIBLE);
                    viewDownloadAnimation.setVisibility(View.GONE);
                }
                break;

            case COMPLETED: {
               // if(popupWindowDownloading==null)
                  //  provideWindowDownloadProgress(viewLayoutControls,getContext());
                Toast.makeText(getActivity(), "Please wait...", Toast.LENGTH_LONG).show();
                if (progress == -1) {
                    if(tvDownloadStatus!=null)
                    tvDownloadStatus.setText(getString(R.string.downloading));
                   // Toast.makeText(getActivity(), ""+getString(R.string.downloading),Toast.LENGTH_LONG).show();
                    // progressTextView.setText(R.string.downloading);
                } else {
                    //if(popupWindowDownloading==null)
                     //   provideWindowDownloadProgress(tvLessonDetails,getContext());
                   // ObjectAnimator.ofInt(circularProgressBar, "progress", 100).setDuration(2000).start();
                    //circularProgressBar.setSecondaryProgress(progress);
                    final String progressString = getResources().getString(R.string.percent_progress, progress);
                    Log.d("progressString", " progressString:"+ progressString);
                    //Toast.makeText(getActivity(), ""+progressString,Toast.LENGTH_LONG).show();

                    //progressTextView.setText(progressString);
                }
                break;
            }
            default: {
                tvDownloadStatus.setText(getString(R.string.status_unknown));
                //Toast.makeText(getActivity(), ""+getString(R.string.status_unknown),Toast.LENGTH_LONG).show();
                // progressTextView.setText(R.string.status_unknown);
                break;
            }
        }
    }

    private void updateViews(Download download, Reason reason, Activity activity) {
        if (request.getId() == download.getId()) {
            if (reason == Reason.DOWNLOAD_QUEUED || reason == Reason.DOWNLOAD_COMPLETED) {
                setTitleView(download.getFile());
                if(reason == Reason.DOWNLOAD_COMPLETED){
                    //Toast.makeText(activity, downloadComplete, Toast.LENGTH_SHORT).show();
                    if(tvDownloadStatus!=null)
                    tvDownloadStatus.setText(downloadComplete);
                }
            }


            String eta = Constants.getETAString(activity, download.getEtaInMilliSeconds());
            String speed = Constants.getDownloadSpeedString(activity, download.getDownloadedBytesPerSecond());
            setProgressView(download.getStatus(), download.getProgress(),eta, speed);
            Log.d("SingleDownloadActivity", "ETA: "+eta);
            Log.d("SingleDownloadActivity", "SPEED: "+speed);
            //  etaTextView.setText(Constants.getETAString(activity, download.getEtaInMilliSeconds()));
            // downloadSpeedTextView.setText(Constants.getDownloadSpeedString(activity, download.getDownloadedBytesPerSecond()));
            if (download.getError() != Error.NONE) {
                // showDownloadErrorSnackBar(download.getError());
            }
        }
    }

    @Override
    public void onChanged(Download download, @NotNull Reason reason) {

        if(getActivity()!=null)
            updateViews(download,reason,getActivity());
    }

    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }

    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        String monthYear = getMonth(calendar.getMonth()) +" "+ calendar.getYear();
        mTextMonthYear.setText(monthYear);
        mYear = calendar.getYear();

        String key = getSchemeCalendar(calendar.getYear(), calendar.getMonth(), calendar.getDay(), 0xFF2196F3, "100").toString();

        if(map.containsKey(key)){
            //"yyyy-MM-dd"
            if(getActivity()!=null) {
                String eventQueryDate = calendar.getYear() + "-" + calendar.getMonth() + "-" + calendar.getDay();
                PreferenceHelper.getPreferenceHelperInstance().setLessonQueryDate(getActivity(), eventQueryDate);
                PreferenceHelper.getPreferenceHelperInstance().setPlayPrevious(getActivity(), true);
                PreferenceHelper.getPreferenceHelperInstance().setLessons(getActivity());
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
               // Toast.makeText(getActivity(), "onCalendarSelect" + eventQueryDate, Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onMonthChange(int year, int month) {
        mYear = year;
        String monthYear = getMonth(month) +" "+ year;
        mTextMonthYear.setText(monthYear);
    }

    @Override
    public void onSemesterClick(Semester semester) {
        try {
            DateTime beginDateTime = DateTime.parse(semester.getDateBegin(), DateTimeFormat.forPattern("yyyy-MM-dd"));
            DateTime endDateTime = DateTime.parse(semester.getDateEnd(), DateTimeFormat.forPattern("yyyy-MM-dd"));
            //Toast.makeText(getActivity(), "BeginDate:"+beginDateTime.getMonthOfYear() ,Toast.LENGTH_SHORT).show();
            //Toast.makeText(getActivity(), "EndDate:"+endDateTime.getMonthOfYear() ,Toast.LENGTH_SHORT).show();
            mCalendarView.setRange(
                    beginDateTime.getYear(),
                    beginDateTime.getMonthOfYear(),
                    beginDateTime.getDayOfMonth(),
                    endDateTime.getYear(),
                    endDateTime.getMonthOfYear(),
                    endDateTime.getDayOfMonth());

            if(selectedClassesForCourse!=null) {
                fetchEvents(selectedClassesForCourse.getUuid(),String.valueOf(semester.getId()));
            }
          //  mCalendarView.setSelectStartCalendar(beginDateTime.getYear(),beginDateTime.getMonthOfYear(),beginDateTime.getDayOfMonth());
            viewFlipperSemesterCalendar.setDisplayedChild(1);
        }
        catch (Exception e){
            mCalendarView.setRange(
                   2020,
                    1,
                    1,
                    2020,
                    12,
                    31);
            viewFlipperSemesterCalendar.setDisplayedChild(0);
            e.printStackTrace();

        }

    }



    private VideoSource makeVideoSource(List<Video> videos) {
        setVideosWatchLength();
        List<VideoSource.SingleVideo> singleVideos = new ArrayList<>();
        for (int i = 0; i < videos.size(); i++) {

            singleVideos.add(i, new VideoSource.SingleVideo(
                    videos.get(i).getVideoUrl(),
                    database.videoDao().getAllSubtitles(i + 1),
                    videos.get(i).getWatchedLength())
            );

        }
        return new VideoSource(singleVideos, 0);
    }

    private void setVideosWatchLength() {
        List<Video> videosInDb = database.videoDao().getVideos();

        try {
            if(videoUriList.size()==videosInDb.size() ) {
                for (int i = 0; i < videosInDb.size(); i++) {
                    videoUriList.get(i).setWatchedLength(videosInDb.get(i).getWatchedLength());
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean checkIfVideoHasSubtitle(){
        if(player.getCurrentVideo()!=null){
        if (player.getCurrentVideo().getSubtitles() == null ||
                player.getCurrentVideo().getSubtitles().size() == 0) {
            subtitle.setImageResource(R.drawable.exo_no_subtitle_btn);
            return true;
        }
        }

        subtitle.setImageResource(R.drawable.exo_subtitle_btn);
        return false;
    }


    @Override
    public void setMuteMode(boolean mute) {

    }

    @Override
    public void showProgressBar(boolean visible) {

    }

    @Override
    public void showRetryBtn(boolean visible) {

    }

    @Override
    public void showSubtitle(boolean show) {

        if (player == null || playerView.getSubtitleView() == null)
            return;

        if (!show) {
            playerView.getSubtitleView().setVisibility(View.GONE);
            return;
        }

        alertDialog.dismiss();
        playerView.getSubtitleView().setVisibility(View.VISIBLE);
    }

    @Override
    public void changeSubtitleBackground() {

    }

    @Override
    public void audioFocus() {

    }

    @Override
    public void setVideoWatchedLength() {
        if(getActivity()!=null && player.getCurrentVideo().getUrl()!=null) {

           // Toast.makeText(getActivity(), "setVideoWatchedLength", Toast.LENGTH_SHORT).show();
            AppDatabase.Companion.getDatabase(getActivity().getApplicationContext()).videoDao().
                    updateWatchedLength(player.getCurrentVideo().getUrl(), player.getWatchedLength());
        }
    }

    @Override
    public void videoEnded() {
        try {
            if(player.getCurrentVideo()!=null && getActivity()!=null) {
                String url = player.getCurrentVideo().getUrl();
                if(url!=null) {
                    AppDatabase.Companion.getDatabase(getActivity().getApplicationContext()).videoDao().
                            updateWatchedLength(url, 0);
                    player.seekToNext();
                }
            }
        }
        catch (Exception e){

        }
    }

    @Override
    public void disableNextButtonOnLastVideo(boolean disable) {
        if(disable){
            nextBtn.setImageResource(R.drawable.forward_icon_grey);
            nextBtn.setEnabled(false);
            return;
        }

        nextBtn.setImageResource(R.drawable.forward_icon);
        nextBtn.setEnabled(true);
    }

    @Override
    public void onVideoQualityChanged(int bitrate) {
        Log.d("onVideoQualityChanged", "Override Quality Select:"+bitrate);

        int drawable = getBitrateDrawableTinted(bitrate);
        videoQuality.setImageResource(drawable);
    }

    @Override
    public void onSeekToNext(int position) {
        if(isLibraryVideoPlaying){
            EventBus.getDefault().post(new SelectedLibrary(true,"video", position));
        }
        else {
            EventBus.getDefault().post(new SelectedVideo(position));
        }
    }

    @Override
    public void onSeekToPrevious(int position) {
        if(isLibraryVideoPlaying){

            EventBus.getDefault().post(new SelectedLibrary(true,"video", position));
        }
        else {
            EventBus.getDefault().post(new SelectedVideo(position));
        }
    }


    private void showSubtitleDialog(Activity activity) {
        //init subtitle dialog
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyDialogTheme);


        LayoutInflater inflater = LayoutInflater.from(activity.getApplicationContext());
        View view = inflater.inflate(R.layout.subtitle_selection_dialog, null);

        builder.setView(view);
        alertDialog = builder.create();

        // set the height and width of dialog
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.CENTER;

        alertDialog.getWindow().setAttributes(layoutParams);

        RecyclerView recyclerView = view.findViewById(R.id.subtitle_recycler_view);
        recyclerView.setAdapter(new SubtitleAdapter(player.getCurrentVideo().getSubtitles(), player));

        if(player.getCurrentVideo().getSubtitles()!=null)
            for (int i = 0; i < player.getCurrentVideo().getSubtitles().size(); i++) {
                Log.d("subtitle", "showSubtitleDialog: " + player.getCurrentVideo().getSubtitles().get(i).getTitle());
            }

        TextView noSubtitle = view.findViewById(R.id.no_subtitle_text_view);
        noSubtitle.setOnClickListener(view1 -> {
            if(playerView.getSubtitleView()!=null) {
                if (playerView.getSubtitleView().getVisibility() == View.VISIBLE)
                    showSubtitle(false);
            }
            alertDialog.dismiss();
            player.resumePlayer();
        });

        Button cancelDialog = view.findViewById(R.id.cancel_dialog_btn);
        cancelDialog.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            player.resumePlayer();
        });

        // to prevent dialog box from getting dismissed on outside touch
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    private void prepareSubtitles() {
        if (player == null || playerView.getSubtitleView() == null)
            return;

        if (checkIfVideoHasSubtitle()) {
            Toast.makeText(getActivity(), getString(R.string.no_subtitle), Toast.LENGTH_SHORT).show();
            return;
        }

        player.pausePlayer();
        if(getActivity()!=null)
            showSubtitleDialog(getActivity());

    }

    private void getDataFromIntent() {
        videoSource = makeVideoSource(videoUriList);
    }

    private void getLibraryVideoSource() {
        videoSourceLibrary = makeVideoSource(libraryVideoUriList);
    }


    private void initSourceLibraryVideos(Activity activity){
        if(player!=null){
            player.releasePlayer();
        }

        if (videoSourceLibrary.getVideos() == null) {
            Toast.makeText(getActivity(), "can not play video", Toast.LENGTH_SHORT).show();
            return;
        }

        player = new VideoPlayer(playerView, activity, videoSourceLibrary, this,videoQuality);


        checkIfVideoHasSubtitle();

        //mAudioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);

        //optional setting
        if(playerView.getSubtitleView()!=null)
            playerView.getSubtitleView().setVisibility(View.GONE);
        player.seekToOnDoubleTap();

        playerView.setControllerVisibilityListener(visibility ->
        {
            Log.i(TAG, "onVisibilityChange: " + visibility);
            if (player.isLock())
                playerView.hideController();
        });
        player.pausePlayer();

    }
    private void initSource(Activity activity) {
        if(player!=null){
            player.releasePlayer();
        }

        if (videoSource.getVideos() == null) {
            Toast.makeText(getActivity(), "can not play video", Toast.LENGTH_SHORT).show();
            return;
        }
       //
        // Toast.makeText(getActivity(), "initSource", Toast.LENGTH_SHORT).show();

        player = new VideoPlayer(playerView, activity, videoSource, this,videoQuality);


        checkIfVideoHasSubtitle();

        //mAudioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);

        //optional setting
        if(playerView.getSubtitleView()!=null)
            playerView.getSubtitleView().setVisibility(View.GONE);
        player.seekToOnDoubleTap();

        playerView.setControllerVisibilityListener(visibility ->
        {
            Log.i(TAG, "onVisibilityChange: " + visibility);
            if (player.isLock())
                playerView.hideController();
        });

    }

    public void putFavorite(String lessonId,String token){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .putFavorite(lessonId,"Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<PutFavoriteResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<PutFavoriteResponse> putFavoriteResponseResponse) {
                if(putFavoriteResponseResponse.isSuccessful()) {
                    isBookMarked(true);
                    Log.d("putFavorite", "onSuccess=> " + putFavoriteResponseResponse.body());
                }
                else {

                    Log.d("putFavorite", "onSuccess=> errorBody:" + putFavoriteResponseResponse.errorBody());
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.d("putFavorite", "onError=> "+e.getMessage());
            }
        });
    }

    /*@Override
    public void onKeyboardHeightChanged(int height, int orientation) {
        Log.d("onKeyboardHeight", "height= "+height+" orientation=> "+orientation);
        if(height == 0){
            chatInputView.setY(initialY);
            Log.d("onKeyboardHeight", "height is zero ="+height+" orientation=> "+orientation);
        }else {
            Log.d("onKeyboardHeight", "height is not zero ="+height+" orientation=> "+orientation);
            float newPosition = initialY - height;
            chatInputView.setY(newPosition);
            Log.d("onKeyboardHeight", "newPosition ="+newPosition+" orientation=> "+orientation);
        }
        chatInputView.requestLayout();
    }*/

}