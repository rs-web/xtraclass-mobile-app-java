package com.realstudiosonline.xtraclass.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Question implements Serializable {

    /*"question": "<p>Another test question without image and true false answers.</p><p><br></p>",
            "num_of_options": 2,
            "type": "true-false",
            "answer_id": 1,
            "past_question_group_id": 6,
            "subject_id": 1073,
            "answer_options": [
                "true",
                "false"
            ]*/

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;


    @SerializedName("question")
    private String text;

    private int answer_id;

    private int past_question_group_id;

    private int subject_id;

    private int num_of_options;

    public int getAnswer_id() {
        return answer_id;
    }

    public void setAnswer_id(int answer_id) {
        this.answer_id = answer_id;
    }

    public int getPast_question_group_id() {
        return past_question_group_id;
    }

    public void setPast_question_group_id(int past_question_group_id) {
        this.past_question_group_id = past_question_group_id;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public int getNum_of_options() {
        return num_of_options;
    }

    public void setNum_of_options(int num_of_options) {
        this.num_of_options = num_of_options;
    }

    private String type;

    @SerializedName("answer_options")
    private List<String> options;

    public Question(String text, String type, List<String> options) {
        this.text = text;
        this.type = type;
        this.options = options;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "Question{" +
                "text='" + text + '\'' +
                ", answer_id=" + answer_id +
                ", past_question_group_id=" + past_question_group_id +
                ", subject_id=" + subject_id +
                ", num_of_options=" + num_of_options +
                ", type='" + type + '\'' +
                ", options=" + options +
                '}';
    }
}
