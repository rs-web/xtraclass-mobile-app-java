package com.realstudiosonline.xtraclass.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.databinding.ActivityNowPlayingBinding;
import com.realstudiosonline.xtraclass.models.AudioItem;
import com.realstudiosonline.xtraclass.utils.player.AudioPlayerService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_ITEM;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_PODCAST_IMAGE;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_RESULT_ID;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_RESULT_NAME;
import static com.realstudiosonline.xtraclass.utils.Constants.FORMAT_ELAPSED_TIME;
import static com.realstudiosonline.xtraclass.utils.Constants.PROGRESS_UPDATE_INITIAL_INTERVAL;
import static com.realstudiosonline.xtraclass.utils.Constants.PROGRESS_UPDATE_INTERVAL;
import static com.realstudiosonline.xtraclass.utils.Constants.TYPE_AUDIO;
import static com.realstudiosonline.xtraclass.utils.Constants.getItemImageUrl;

public class NowPlayingActivity extends AppCompatActivity {

    private static final String TAG = NowPlayingActivity.class.getSimpleName();
    private AudioItem mItem;
    private String mPodcastImage;
    private String mItemImageUrl;
    private String mPodcastName;
    private String mPodcastId;
    private String mEnclosureUrl;
    private MediaBrowserCompat mMediaBrowser;
    private PlaybackStateCompat mLastPlaybackState;
    private ActivityNowPlayingBinding mNowPlayingBinding;
    private final ScheduledExecutorService mExecutorService =
            Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> mScheduledFuture;

    private final Handler mHandler = new Handler();

    private final Runnable mUpdateProgressTask = this::updateProgress;


    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNowPlayingBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_now_playing);

        setupUI();

        Log.d(TAG,"enclosure url: " + mEnclosureUrl);

        // Create MediaBrowserCompat
        createMediaBrowserCompat();

        // Hide title on the Toolbar
        setTitle(getString(R.string.space));
        // Show the up button on the Toolbar
        showUpButton();

        // Set a listener to receive notifications of changes to the SeekBar's progress level
     mNowPlayingBinding.playingInfo.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
          // Notification that the progress level has changed
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mNowPlayingBinding.playingInfo.tvStart.setText(DateUtils.formatElapsedTime(
                       progress/FORMAT_ELAPSED_TIME));
           }

           // Notification that the user has started a touch gesture
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Cancel the future returned by scheduleAtFixedRate() to stop the SeekBar from progressing
               stopSeekbarUpdate();
           }

            // Notification that the user has finished a touch gesture
            @Override
           public void onStopTrackingTouch(SeekBar seekBar){
               MediaControllerCompat.getMediaController(NowPlayingActivity.this)
                        .getTransportControls().seekTo(seekBar.getProgress());
                // Create and execute a periodic action to update the SeekBar progress
                scheduleSeekbarUpdate();
           }
        });

    }



    private void setupUI() {
        // Get the podcast episode, title and the podcast image URL
        getData();

        // Set episode title
        String episodeTitle = mItem.getTitle();
        if (episodeTitle != null) {
            mNowPlayingBinding.playingInfo.tvEpisodeTitle.setText(episodeTitle);
        }
        // Set podcast title
        if (mPodcastName != null) {
            mNowPlayingBinding.playingInfo.tvPodcastTitle.setText(mPodcastName);
        }

        // If an episode image exists, use it. Otherwise, use the podcast image.
        mItemImageUrl = getItemImageUrl(mItem, mPodcastImage);
        // Use Glide library to upload the image
        Glide.with(this)
                .load(mItemImageUrl)
                .into(mNowPlayingBinding.ivNowEpisode);

        // Load blurry artwork using Glide Transformations library
        Glide.with(this)
                .load(mItemImageUrl)
//                .apply(RequestOptions.bitmapTransform(new BlurTransformation(BLUR_RADIUS, BLUR_SAMPLING)))
                .into(mNowPlayingBinding.ivBlur);

        // Extract the enclosure URL
        mEnclosureUrl = mItem.getEnclosures().get(0).getUrl();
        // If the current episode is not audio, displays a snackbar message.
        handleEnclosureType();
    }

    /**
     * Constructs a MediaBrowserCompat.
     */
    private void createMediaBrowserCompat() {
        mMediaBrowser = new MediaBrowserCompat(this,
                new ComponentName(this, AudioPlayerService.class),
                mConnectionCallbacks,
                null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Connect to the MediaBrowserService
        mMediaBrowser.connect();
        // Add a DownloadManager.Listener. Please note that we must remove the listener in onStop()
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Set the audio stream so the app responds to the volume control on the device
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    protected void onStop() {
        super.onStop();

        //Toast.makeText(this, "OnStop", Toast.LENGTH_SHORT).show();
        // Disconnect the MediaBrowser and unregister the MediaController.Callback when the
        // activity stops
        if (MediaControllerCompat.getMediaController(this) != null) {
            MediaControllerCompat.getMediaController(this).unregisterCallback(controllerCallback);
        }
        mMediaBrowser.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Cancel the future returned by scheduleAtFixedRate() to stop the seekBar from progressing
        stopSeekbarUpdate();
        // Cancel currently executing tasks
        mExecutorService.shutdown();
    }

    /**
     * Get the podcast episode, title and the podcast image URL from the DetailActivity via Intent.
     */
    private void getData() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(EXTRA_ITEM)) {
                Bundle b = intent.getBundleExtra(EXTRA_ITEM);
                mItem = b.getParcelable(EXTRA_ITEM);
            }
            // Get podcast id
            if (intent.hasExtra(EXTRA_RESULT_ID)) {
                mPodcastId = intent.getStringExtra(EXTRA_RESULT_ID);
            }
            // Get podcast title
            if (intent.hasExtra(EXTRA_RESULT_NAME)) {
                mPodcastName = intent.getStringExtra(EXTRA_RESULT_NAME);
            }
            if (intent.hasExtra(EXTRA_PODCAST_IMAGE)) {
                mPodcastImage = intent.getStringExtra(EXTRA_PODCAST_IMAGE);
            }
        }
    }

    private final MediaBrowserCompat.ConnectionCallback mConnectionCallbacks =
            new MediaBrowserCompat.ConnectionCallback() {
                @Override
                public void onConnected() {

                    // Get the token for the MediaSession
                    MediaSessionCompat.Token token = mMediaBrowser.getSessionToken();

                    // Create a MediaControllerCompat
                    MediaControllerCompat mediaController = null;
                    try {
                        mediaController = new MediaControllerCompat(NowPlayingActivity.this, token);
                    } catch (RemoteException e) {
                        Log.e(TAG,"Error creating media controller");
                    }

                    // Save the controller
                    MediaControllerCompat.setMediaController(NowPlayingActivity.this,
                            mediaController);

                    // Finish building the UI
                    buildTransportControls();
                }

                @Override
                public void onConnectionSuspended() {
                    super.onConnectionSuspended();
                    // The Service has crashed. Disable transport controls until it automatically
                    // reconnects.
                }

                @Override
                public void onConnectionFailed() {
                    super.onConnectionFailed();
                    // The Service has refused our connection
                }
            };

    void buildTransportControls() {
        // Attach a listener to the play/pause button
        mNowPlayingBinding.playingInfo.ibPlayPause.setOnClickListener(v -> {
            PlaybackStateCompat pbState = MediaControllerCompat.getMediaController(NowPlayingActivity.this).getPlaybackState();
            if (pbState != null) {
                MediaControllerCompat.TransportControls controls =
                        MediaControllerCompat.getMediaController(NowPlayingActivity.this).getTransportControls();
                Log.d(TAG,"buildTransportControls with state " + pbState.getPlaybackState());
                //Toast.makeText(this, "Player state"+ pbState.getState(), Toast.LENGTH_SHORT).show();



                switch (pbState.getState()) {
                    case PlaybackStateCompat.STATE_PLAYING:
                        Toast.makeText(this, "Player state STATE_PLAYING", Toast.LENGTH_SHORT).show();
                        controls.pause();
                        break;
                        // fall through
                    case PlaybackStateCompat.STATE_BUFFERING:
                        controls.play();
                        Toast.makeText(this, "Player state STATE_BUFFERING", Toast.LENGTH_SHORT).show();
                        // Cancel the future returned by scheduleAtFixedRate() to stop the seekBar
                        // from progressing
                        stopSeekbarUpdate();
                        break;
                    case PlaybackStateCompat.STATE_PAUSED:
                        Toast.makeText(this, "Player state STATE_PAUSED", Toast.LENGTH_SHORT).show();
                        controls.play();
                        break;

                    case PlaybackStateCompat.STATE_STOPPED:
                        Toast.makeText(this, "Player state STATE_STOPPED", Toast.LENGTH_SHORT).show();
                        controls.play();
                        // Create and execute a periodic action to update the seekBar progress
                        scheduleSeekbarUpdate();
                        break;
                    default:
                        Toast.makeText(this, "Player state "+pbState.getState(), Toast.LENGTH_SHORT).show();
                        Log.d(TAG,"onClick with state " + pbState);
                }
            }
        });

        // Attach a listener to the fast forward button
        mNowPlayingBinding.playingInfo.ibFastforward.setOnClickListener(v -> {
            MediaControllerCompat.getMediaController(NowPlayingActivity.this)
                    .getTransportControls().fastForward();
            updateProgress();
        });

        // Attach a listener to the rewind button
        mNowPlayingBinding.playingInfo.ibRewind.setOnClickListener(v -> {
            MediaControllerCompat.getMediaController(NowPlayingActivity.this)
                    .getTransportControls().rewind();
            updateProgress();
        });

        MediaControllerCompat mediaController =
                MediaControllerCompat.getMediaController(NowPlayingActivity.this);
        // Display the initial state
        MediaMetadataCompat metadata = mediaController.getMetadata();
        PlaybackStateCompat pbState = mediaController.getPlaybackState();

        // Update the playback state
        updatePlaybackState(pbState);

        if (metadata != null) {
            // Get the episode duration from the metadata and sets the end time to the textView
            updateDuration(metadata);
        }
        // Set the current progress to the current position
        updateProgress();
        if (pbState != null && (pbState.getState() == PlaybackStateCompat.STATE_PLAYING ||
                pbState.getState() == PlaybackStateCompat.STATE_BUFFERING)) {
            // Create and execute a periodic action to update the SeekBar progress
            scheduleSeekbarUpdate();
        }

        // Register a Callback to stay in sync
        mediaController.registerCallback(controllerCallback);
    }

    /**
     * Callback for receiving updates from the session.
     */
    MediaControllerCompat.Callback controllerCallback = new MediaControllerCompat.Callback() {
        @Override
        public void onMetadataChanged(MediaMetadataCompat metadata) {
            super.onMetadataChanged(metadata);
            if (metadata != null) {
                // Get the episode duration from the metadata and sets the end time to the textView
                updateDuration(metadata);
            }
        }

        @Override
        public void onPlaybackStateChanged(PlaybackStateCompat state) {
            super.onPlaybackStateChanged(state);
            // Update the playback state
            Log.d(TAG,"onPlaybackStateChanged:"+state);
            updatePlaybackState(state);
        }
    };

    /**
     * Creates and executes a periodic action that becomes enabled first after the given initial delay,
     * and subsequently with the given period;that is executions will commence after initialDelay
     * then initialDelay + period, then initialDelay + 2 * period, and so on.
     */
    private void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!mExecutorService.isShutdown()) {
            mScheduledFuture = mExecutorService.scheduleAtFixedRate(
                    // Cause the Runnable updateProgress to be added to the message queue
                    () -> mHandler.post(mUpdateProgressTask),
                    PROGRESS_UPDATE_INITIAL_INTERVAL,// initial delay (100 milliseconds)
                    PROGRESS_UPDATE_INTERVAL, // period (1000 milliseconds)
                    TimeUnit.MILLISECONDS); // the time unit of the initialDelay and period
        }
    }

    /**
     * Cancels the future returned by scheduleAtFixedRate() to stop the SeekBar from progressing.
     */
    private void stopSeekbarUpdate() {
        if (mScheduledFuture != null) {
            mScheduledFuture.cancel(false);
        }
    }

    /**
     * Gets the episode duration from the metadata and sets the end time to be displayed in the TextView.
     */
    private void updateDuration(MediaMetadataCompat metadata) {
        if (metadata == null) {
            return;
        }
        int duration = (int) metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION)
                * FORMAT_ELAPSED_TIME;
        mNowPlayingBinding.playingInfo.seekBar.setMax(duration);
        mNowPlayingBinding.playingInfo.tvEnd.setText(
                DateUtils.formatElapsedTime(duration / FORMAT_ELAPSED_TIME));
    }

    /**
     * Calculates the current position (distance = timeDelta * velocity) and sets the current progress
     * to the current position.
     */
    private void updateProgress() {
        if (mLastPlaybackState == null) {
            return;
        }
        long currentPosition = mLastPlaybackState.getPosition();
        if (mLastPlaybackState.getState() == PlaybackStateCompat.STATE_PLAYING) {
            // Calculate the elapsed time between the last position update and now and unless
            // paused, we can assume (delta * speed) + current position is approximately the
            // latest position. This ensure that we do not repeatedly call the getPlaybackState()
            // on MediaControllerCompat.
            long timeDelta = SystemClock.elapsedRealtime() -
                    mLastPlaybackState.getLastPositionUpdateTime();
            currentPosition += (int) timeDelta * mLastPlaybackState.getPlaybackSpeed();
        }
        mNowPlayingBinding.playingInfo.seekBar.setProgress((int) currentPosition);
    }

    /**
     * Updates the playback state.
     * @param state Playback state for a MediaSessionCompat
     */
    private void updatePlaybackState(PlaybackStateCompat state) {

        if (state == null) {
            return;
        }
        Log.d("updatePlaybackState:", "Updates the playback state with state:"+state.getState());

        mLastPlaybackState = state;
        switch (state.getState()) {
            case PlaybackStateCompat.STATE_PLAYING:
                hideLoading();
                mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.pause_icon);
                // Create and execute a periodic action to update the SeekBar progress
                scheduleSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_PAUSED:
                hideLoading();
                mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.play_icon);
                // Cancel the future returned by scheduleAtFixedRate() to stop the SeekBar from progressing
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_NONE:
            case PlaybackStateCompat.STATE_STOPPED:
                hideLoading();
                mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.play_icon);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_BUFFERING:
                showLoading();
                mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.play_icon);
                stopSeekbarUpdate();
                break;
            default:
                Log.d(TAG,"Unhandled state " + state.getState());
        }
    }

    /**
     * Shows loading indicator and text when the player is buffering.
     */
    private void showLoading() {
        mNowPlayingBinding.playingInfo.pbLoadingIndicator.setVisibility(View.VISIBLE);
        mNowPlayingBinding.playingInfo.tvLoading.setVisibility(View.VISIBLE);
        mNowPlayingBinding.playingInfo.tvLoading.setText(R.string.loading);
    }

    /**
     * Hides loading indicator and text when the player is playing.
     */
    private void hideLoading() {
        mNowPlayingBinding.playingInfo.pbLoadingIndicator.setVisibility(View.INVISIBLE);
        mNowPlayingBinding.playingInfo.tvLoading.setVisibility(View.INVISIBLE);
    }

    /**
     * Shows the up button on the tool bar.
     */
    private void showUpButton() {
        // Set the toolbar as the app bar
        setSupportActionBar(mNowPlayingBinding.toolbar);

        ActionBar actionBar = getSupportActionBar();
        // Set the action bar back button to look like an up button
        if (actionBar != null) {
            //actionBar.setHomeAsUpIndicator(R.drawable.back_press);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * This app currently only supports audio podcasts. If the current episode is not audio,
     * displays a snackbar message.
     */
    private void handleEnclosureType() {
        String enclosureType = mItem.getEnclosures().get(0).getType();
        if (!enclosureType.contains(TYPE_AUDIO)) {
            String snackMessage = getString(R.string.snackbar_support_audio);
            Snackbar snackbar = Snackbar.make(mNowPlayingBinding.coordinator, snackMessage, Snackbar.LENGTH_LONG);
            // Set the background color of the snackbar
            View sbView = snackbar.getView();
            sbView.setBackgroundColor(Color.RED);
            // Set the text color of the snackbar
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.now_playing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            supportFinishAfterTransition();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }






}
