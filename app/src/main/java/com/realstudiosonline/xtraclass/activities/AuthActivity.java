package com.realstudiosonline.xtraclass.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.CountryDropDownAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.Country;
import com.realstudiosonline.xtraclass.models.GenericItem;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.models.UserRequest;
import com.realstudiosonline.xtraclass.models.UserResponse;
import com.realstudiosonline.xtraclass.models.VideoQuality;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.UUID;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

import static com.realstudiosonline.xtraclass.XtraClassApp.getFirebaseAnalytics;
import static com.realstudiosonline.xtraclass.utils.Constants.convertDpToPixelInt;

public class AuthActivity extends AppCompatActivity {

    View spinnerCountry;
    private CountryDropDownAdapter countryDropDownAdapter;
    ArrayList<Country> countries;
    private AppCompatTextView tvConditions;
    AlertDialog alertDialog, alertDialogExit;
    Country selectedCountry;
    TextInputEditText editTextPhone;
    PopupWindow popupWindowCountry;
    TextView tvCountry;
    AppCompatImageView ivCountry;
    AppCompatCheckBox checkBoxPrivacy;
    TextInputLayout view_phone_input_layout;
    private boolean isPrivacyChecked = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        tvConditions = findViewById(R.id.tv_terms_condition);

        view_phone_input_layout =findViewById(R.id.view_phone_input_layout);

        tvCountry = findViewById(R.id.tvCountry);

        ivCountry = findViewById(R.id.ivCountry);

        checkBoxPrivacy = findViewById(R.id.chk_privacy);

        checkBoxPrivacy.setOnCheckedChangeListener((buttonView, isChecked) -> isPrivacyChecked = isChecked);

        editTextPhone = findViewById(R.id.edt_phone);

        tvConditions.setText(Html.fromHtml("I accept the "+
                "<a href=\"https://dextraclass.com\"><font color='red'>privacy, terms & condition</font></a>"));

        //  String conditionString = "I accept the <u>privacy, terms & condition</u>";
        tvConditions.setMovementMethod(LinkMovementMethod.getInstance());
        //tvConditions.setText(Html.fromHtml(conditionString));
        spinnerCountry = findViewById(R.id.view_country_selector);
        countries = new ArrayList<>();

        countries.add(new Country("Ghana","GH","233", R.drawable.gh));
        countries.add(new Country("Gambia","GM", "220", R.drawable.gm));
        countries.add(new Country("Nigeria","NG", "234", R.drawable.ng));
        countries.add(new Country("Sierra Leone","SL", "232", R.drawable.sl));
        countries.add(new Country("Liberia","LR", "231", R.drawable.lr));
        countries.add(new Country("Guinea","GN", "224", R.drawable.guinea));

        selectedCountry = countries.get(0);
        String selectedCountryString = "+"+selectedCountry.getCode()+" "+selectedCountry.getName();
        tvCountry.setText(selectedCountryString);

        countryDropDownAdapter = new CountryDropDownAdapter(this,R.layout.country_adapter, countries);


        spinnerCountry.setOnClickListener(v -> providePopWindowCountry(v,getApplicationContext(),countryDropDownAdapter));


        findViewById(R.id.btn_confirm).setOnClickListener(v ->{
            if(editTextPhone.getText()!=null && selectedCountry!=null) {
                if(isPrivacyChecked) {
                    if (editTextPhone.getText().toString().length() == 0)
                        Toast.makeText(this, "Enter phone number", Toast.LENGTH_SHORT).show();
                    else
                        postPhoneAuth(editTextPhone.getText().toString(), selectedCountry.getCode(), UUID.randomUUID().toString());
                }
                else {
                    Toast.makeText(this, "Accept privacy to continue", Toast.LENGTH_LONG).show();
                    //checkBoxPrivacy.setError("Accept privacy to continue");

                }
            }
        });
    }

    private void providePopWindowCountry(View it, Context context, CountryDropDownAdapter adapter){
        if(popupWindowCountry!=null){
            popupWindowCountry.dismiss();
            popupWindowCountry = null;
        }

        popupWindowCountry = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowCountry.setOutsideTouchable(true);
        popupWindowCountry.setFocusable(true);
        //popupWindowCountry.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            selectedCountry = adapter.getItem(position);
            String selectedCountryString = "+"+selectedCountry.getCode()+" "+selectedCountry.getName();
            tvCountry.setText(selectedCountryString);
            Glide.with(getApplicationContext()).load(selectedCountry.getIcon()).into(ivCountry);
            popupWindowCountry.dismiss();

        });
        popupWindowCountry.setContentView(parent_view);
        popupWindowCountry.setElevation(20);

        if(popupWindowCountry!=null) {

            popupWindowCountry.showAsDropDown(it, 0,-it.getHeight());
        }
    }
    private void postPhoneAuth(String phone, String country_code, String device_token){
        PreferenceHelper.getPreferenceHelperInstance().setCountryCode(getApplicationContext(),selectedCountry.getCode());
        PreferenceHelper.getPreferenceHelperInstance().setPhone(getApplicationContext(),phone);
        showDialogProgressDialog();
        ApiClient.getApiClient().getInstance(getApplicationContext())
                .postLogin("application/json",new UserRequest(phone,country_code, "Android",
                        device_token))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<UserResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<UserResponse> userResponseResponse) {
                hideDialogProgressDialog();
                if(userResponseResponse.isSuccessful()) {
                    UserResponse userResponse = userResponseResponse.body();
                    if (userResponse != null) {
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID,UUID.randomUUID().toString());
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Login Attempt");
                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "application/json");
                            bundle.putSerializable(FirebaseAnalytics.Param.CONTENT, userResponse.getData());
                            getFirebaseAnalytics(getApplicationContext()).logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
                            if (userResponse.getData().getUser().getClass_id()!=null) {
                                ClassesForCourse classesForCourse = new ClassesForCourse("",
                                        userResponse.getData().getUser().getClass_id(),
                                        userResponse.getData().getUser().getCourse_id(),
                                        userResponse.getData().getUser().getSchool_id(), "");
                                PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(AuthActivity.this,classesForCourse);

                            }

                            if(userResponse.getData().getUser()!=null) {
                                User user = userResponse.getData().getUser();
                                int id =0;
                                if(user.getInstitution_type().equalsIgnoreCase("regular_school")){
                                    id = 1;
                                }

                                PreferenceHelper.getPreferenceHelperInstance().setSchoolType(getApplicationContext(),id);
                                PreferenceHelper.getPreferenceHelperInstance().setUser(AuthActivity.this, userResponse.getData().getUser());
                                PreferenceHelper.getPreferenceHelperInstance().setToken(AuthActivity.this, userResponse.getData().getAccess());
                                PreferenceHelper.getPreferenceHelperInstance().setIsRegistered(AuthActivity.this, userResponse.isRegistered());

                                Intent intent = new Intent(AuthActivity.this, ConfirmOTPActivity.class);
                                intent.putExtra(PreferenceHelper.PHONE, phone);
                                intent.putExtra(PreferenceHelper.USER_UUID, userResponse.getData().getUser().getUuid());

                                intent.putExtra(PreferenceHelper.COUNTRY_CODE, selectedCountry.getCode());
                                PreferenceHelper.getPreferenceHelperInstance().setCountryCode(getApplicationContext(),selectedCountry.getCode());
                                PreferenceHelper.getPreferenceHelperInstance().setPhone(getApplicationContext(),phone);
                                startActivity(intent);
                                finish();
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                            Toast.makeText(AuthActivity.this, "Error authenticating user", Toast.LENGTH_LONG).show();

                            try {
                                if(userResponse.getData().getUser()!=null) {
                                    User user = userResponse.getData().getUser();
                                    int id =0;
                                    if(user.getInstitution_type().equalsIgnoreCase("regular_school")){
                                        id = 1;
                                    }
                                    PreferenceHelper.getPreferenceHelperInstance().setSchoolType(getApplicationContext(),id);
                                    PreferenceHelper.getPreferenceHelperInstance().setUser(AuthActivity.this, userResponse.getData().getUser());
                                    PreferenceHelper.getPreferenceHelperInstance().setToken(AuthActivity.this, userResponse.getData().getAccess());
                                    PreferenceHelper.getPreferenceHelperInstance().setIsRegistered(AuthActivity.this, userResponse.isRegistered());

                                    Intent intent = new Intent(AuthActivity.this, ConfirmOTPActivity.class);
                                    intent.putExtra(PreferenceHelper.PHONE, phone);
                                    intent.putExtra(PreferenceHelper.USER_UUID, userResponse.getData().getUser().getUuid());

                                    intent.putExtra(PreferenceHelper.COUNTRY_CODE, selectedCountry.getCode());
                                    PreferenceHelper.getPreferenceHelperInstance().setCountryCode(getApplicationContext(),selectedCountry.getCode());
                                    PreferenceHelper.getPreferenceHelperInstance().setPhone(getApplicationContext(),phone);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                            catch (Exception e1){
                                Toast.makeText(AuthActivity.this, "Error authenticating user", Toast.LENGTH_LONG).show();
                                Bundle bundle = new Bundle();
                                bundle.putString(FirebaseAnalytics.Param.ITEM_ID,UUID.randomUUID().toString());
                                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Login Exception");
                                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "application/json");
                                bundle.putString(FirebaseAnalytics.Param.CONTENT,"login failed");
                                getFirebaseAnalytics(getApplicationContext()).logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
                            }
                        }

                    }
                }
                else {
                    Toast.makeText(AuthActivity.this, ""+userResponseResponse.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                hideDialogProgressDialog();
            }
        });
    }

    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(AuthActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }


    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        cancelDialogProgressDialog();
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        super.onDestroy();
    }

    public void hideDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.hide();
        }
    }

    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}