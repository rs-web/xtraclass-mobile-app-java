package com.realstudiosonline.xtraclass.models;

public class UserRequest {
    private String phone;
    private String country_code;
    private String platform;
    private String device_token;

    public UserRequest(String phone, String country_code, String platform, String device_token) {
        this.phone = phone;
        this.country_code = country_code;
        this.platform = platform;
        this.device_token = device_token;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }
}
