package com.realstudiosonline.xtraclass.utils.player;

import android.view.MotionEvent;

/**
 * Created by Brajendr on 1/26/2017.
 */

public interface IGestureListener {

  void onHorizontalScroll(MotionEvent event, float delta);

  void onVerticalScroll(MotionEvent event, float delta);

  void onSwipeBottom();

  void onSwipeTop();

}
