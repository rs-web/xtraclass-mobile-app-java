package com.realstudiosonline.xtraclass.models;

public class School {
    private String id;
    private int is_locked;
    private String name;
    private String logo_url;
    private String institution_id;

    public School() {
    }

    public School(String id, int is_locked, String name, String logo_url, String institution_id) {
        this.id = id;
        this.is_locked = is_locked;
        this.name = name;
        this.logo_url = logo_url;
        this.institution_id = institution_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(int is_locked) {
        this.is_locked = is_locked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getInstitution_id() {
        return institution_id;
    }

    public void setInstitution_id(String institution_id) {
        this.institution_id = institution_id;
    }

    @Override
    public String toString() {
        return "School{" +
                "id='" + id + '\'' +
                ", is_locked=" + is_locked +
                ", name='" + name + '\'' +
                ", logo_url='" + logo_url + '\'' +
                ", institution_id='" + institution_id + '\'' +
                '}';
    }

    /*
    id": "897a5ab9-24b9-4511-9a32-392667fe2a95",
            "is_locked": 0,
            "name": "Class 1",
            "logo_url": "https://dextraclass.com/uploads/schools/1412419070731931603203161__Primary.png",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"*/
}
