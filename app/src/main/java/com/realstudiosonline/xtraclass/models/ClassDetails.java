package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class ClassDetails {
    private String id;
    private String name;
    private String full_name;
    private String course_id;
    private String school_id;
    private String school_name;
    private String subject_id;
    private String subject_name;
    private boolean locked;
    private String vimeo_url;
    private String vimeo_id;
    private int view_count;
    private List<Video> video;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getVimeo_url() {
        return vimeo_url;
    }

    public void setVimeo_url(String vimeo_url) {
        this.vimeo_url = vimeo_url;
    }

    public String getVimeo_id() {
        return vimeo_id;
    }

    public void setVimeo_id(String vimeo_id) {
        this.vimeo_id = vimeo_id;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public List<Video> getVideo() {
        return video;
    }

    public void setVideo(List<Video> video) {
        this.video = video;
    }

    @Override
    public String toString() {
        return "ClassDetails{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", full_name='" + full_name + '\'' +
                ", course_id='" + course_id + '\'' +
                ", school_id='" + school_id + '\'' +
                ", school_name='" + school_name + '\'' +
                ", subject_id='" + subject_id + '\'' +
                ", subject_name='" + subject_name + '\'' +
                ", locked=" + locked +
                ", vimeo_url='" + vimeo_url + '\'' +
                ", vimeo_id='" + vimeo_id + '\'' +
                ", view_count='" + view_count + '\'' +
                ", video=" + video +
                '}';
    }
}
/*
*  "data": {
        "id": "c1698279-ffa9-42a6-abee-ff75b9bc0898",
        "name": "Core Mathematics",
        "full_name": "Core Mathematics General Arts",
        "course_id": "619cda2c-6cc7-402f-aa13-5ae3b9169a83",
        "school_id": "ff313bb0-d3e4-4507-a2f0-078931890784",
        "school_name": "SHS 2",
        "subject_id": null,
        "subject_name": null,
        "locked": false,
        "vimeo_url": "https://vimeo.com/427375476/d98dcc9523",
        "vimeo_id": "427375476",
        "view_count": 6,
        "video": [
            {
                "id": "9bd94fd8-990f-4eda-a73c-a8d5680b77b0",
                "class_id": "c1698279-ffa9-42a6-abee-ff75b9bc0898",
                "title": "Core Mathematics - Modulo Arithmetic",
                "topic": "Modulo Arithmetic",
                "play_on": "2020-10-01",
                "content": "Core Mathematics - Modulo Arithmetic",
                "note_url": "",
                "learn_more_url": "",
                "teacher": {
                    "profile_id": "59e9caf6-427d-471a-a15e-8b53c1714ec2",
                    "name": "Jeremiah Nii Annan Dodoo"
                }
            }
        ]
    }*/