package com.realstudiosonline.xtraclass.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.PaymentTypeAdapter;
import com.realstudiosonline.xtraclass.adapters.SubscriptionTypeAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.PaymentType;
import com.realstudiosonline.xtraclass.models.Plan;
import com.realstudiosonline.xtraclass.models.PlansResponse;
import com.realstudiosonline.xtraclass.models.SubscribePlan;
import com.realstudiosonline.xtraclass.models.SubscribePlanResponse;
import com.realstudiosonline.xtraclass.models.SubscriptionOTP;
import com.realstudiosonline.xtraclass.models.SubscriptionOTPResponse;
import com.realstudiosonline.xtraclass.models.SubscriptionPlan;
import com.realstudiosonline.xtraclass.models.SubscriptionType;
import com.realstudiosonline.xtraclass.models.TransactionState;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.models.VerifySubscriptionResponse;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import kotlin.io.TextStreamsKt;
import retrofit2.Response;

@SuppressLint("ClickableViewAccessibility")
public class SubscriptionActivity extends AppCompatActivity {

    AutoCompleteTextView tvPayBy,edtSubscribeTo;
    TextInputEditText edtSubscriptionNo;
    PopupWindow popupWindowPaymentType,popupWindowSubscribeTo;
    PaymentTypeAdapter paymentTypeAdapter;
    SubscriptionTypeAdapter subscriptionTypeAdapter;
    View viewPayBy, viewSubscribeTo;
    List<PaymentType> paymentTypes;
    List<Plan> subscriptionTypes;
    Plan selectedPlan;
    PaymentType selectedPaymentType;
    AppCompatTextView tvSubscribe;
    private User mUser;
    private PopupWindow popupWindowOTP,popupWindowOTPVerified,popupWindowSubscriptionVerified,popupWindowAuthPayment;
    SmsVerifyCatcher smsVerifyCatcher;

    AlertDialog alertDialog;
    private String mToken ="";
    SubscribePlanResponse subscribePlanResponse;


    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        tvPayBy = findViewById(R.id.spinner_pay_by);
        viewPayBy = findViewById(R.id.view_pay_by);
        viewSubscribeTo = findViewById(R.id.view_subscribe_to);
        edtSubscribeTo = findViewById(R.id.edt_subscribe_to);
        edtSubscriptionNo = findViewById(R.id.edt_subscription_no);


        tvSubscribe = findViewById(R.id.btn_start_learning);
        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getApplicationContext());

        tvSubscribe.setOnClickListener(v -> {
            selectedPlan.setAmount("0.01");
             if(edtSubscriptionNo.getText()==null) {
                    Toast.makeText(this, "Enter number", Toast.LENGTH_LONG).show();
            }
             else if(selectedPlan==null)
                 Toast.makeText(this, "Select plan ", Toast.LENGTH_LONG).show();


             else if(selectedPaymentType==null)
                Toast.makeText(this, "Select payment type ", Toast.LENGTH_LONG).show();

            else if(edtSubscriptionNo.getText()!=null) {
                if(TextUtils.isEmpty(edtSubscriptionNo.getText().toString().trim()))
                    Toast.makeText(this, "Enter number", Toast.LENGTH_LONG).show();
                else if(edtSubscriptionNo.getText().toString().trim().length()<10 || edtSubscriptionNo.getText().toString().trim().length()>10){
                    Toast.makeText(this, "Enter 10 digit number", Toast.LENGTH_LONG).show();
                }
                else {
                    if(selectedPlan!=null && selectedPaymentType!=null){

                      //  Toast.makeText(this, "Subscribing, Please wait...", Toast.LENGTH_LONG).show();


                        SubscriptionPlan subscriptionPlan = new SubscriptionPlan(
                                selectedPlan.getAmount(),
                               TextUtils.isEmpty( mUser.getEmail()) ? mUser.getPhone()+"@gmail.com" : mUser.getEmail(),
                                edtSubscriptionNo.getText().toString().trim(),
                                selectedPaymentType.getShortName(),
                                mUser.getFirst_name()+ " "+ mUser.getLast_name(),
                                mUser.getUser_id(),
                                "Xtraclass Subscription",
                                String.valueOf(selectedPlan.getId()));

                        initiateSubscription(subscriptionPlan,edtSubscriptionNo,mToken);

                       /* new Handler().postDelayed(()->{
                            hideDialogProgressDialog();
                            provideOTPPopupWindow(v,getApplicationContext());
                            },3000);*/

                    }
                }

            }
        });

        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());

        edtSubscriptionNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtSubscribeTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        try {
                edtSubscriptionNo.setText(mUser.getPhone());
        }
        catch (Exception e){
            e.printStackTrace();
        }
        paymentTypes = new ArrayList<>();

        subscriptionTypes = new ArrayList<>();

        getPlans();

        paymentTypes.add(new PaymentType("MTN MOBILE MONEY", 1,"mtn"));
        paymentTypes.add(new PaymentType("VODAFONE CASH", 2, "vod"));
        paymentTypes.add(new PaymentType("AIRTELTIGO MONEY", 3, "tgo"));

        paymentTypeAdapter = new PaymentTypeAdapter(getApplicationContext(), R.layout.item_payment_type_dropdown, paymentTypes);

        subscriptionTypeAdapter = new SubscriptionTypeAdapter(getApplicationContext(), R.layout.item_payment_type_dropdown, subscriptionTypes);

        tvPayBy.setKeyListener(null);
        tvPayBy.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus)
                providePaymentTypePopupWindow(viewPayBy);
        });
        tvPayBy.setOnClickListener(v -> providePaymentTypePopupWindow(viewPayBy));

        edtSubscribeTo.setKeyListener(null);
        edtSubscribeTo.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus)
                provideSubscriptionPopupWindow(viewSubscribeTo);
        });
        edtSubscribeTo.setOnClickListener(v ->  provideSubscriptionPopupWindow(viewSubscribeTo));

        smsVerifyCatcher = new SmsVerifyCatcher(this, message -> {
            String code = parseCode(message);//Parse verification code


            try {
                if(subscribePlanResponse!=null)
                    sendTransactionOTP(edtSubscribeTo, mToken, subscribePlanResponse.getData(), code);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            //editTextOTP.setText(code);//set code in edit text

            //then you can send verification code to server
        });
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        Log.d("OTP", "OTP is "+ code);
        return code;
    }



    public void provideWindowOTPVerified(View it, Context context,SubscribePlan subscriptionPlan){

        try {
            if (popupWindowOTPVerified != null) {
                popupWindowOTPVerified.dismiss();
                popupWindowOTPVerified = null;
            }
        }
        catch (Exception e){
            e.printStackTrace();

        }
        popupWindowOTPVerified = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        // Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //  popupWindowEmailSent.setBackgroundDrawable(backgroundDrawable);
        popupWindowOTPVerified.setOutsideTouchable(true);
        popupWindowOTPVerified.setFocusable(true);
        popupWindowOTPVerified.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = (View) layoutInflater.inflate(R.layout.email_dialog, null, false);
        LottieAnimationView lottieAnimationViewEmailSent = testView.findViewById(R.id.an_email_sent);
        AppCompatTextView tvMessage = testView.findViewById(R.id.tv_register_message);
        tvMessage.setText(subscriptionPlan.getDisplay_text());
        lottieAnimationViewEmailSent.playAnimation();
        AppCompatImageButton btn_confirm = testView.findViewById(R.id.btn_close);
        btn_confirm.setOnClickListener(v -> popupWindowOTPVerified.dismiss());

        new Handler().postDelayed(()-> verifySubscription(edtSubscriptionNo,subscriptionPlan,mUser.getUser_id(),String.valueOf(selectedPlan.getId()),mToken),30000);
        popupWindowOTPVerified.setContentView(testView);
        popupWindowOTPVerified.setElevation(10);
        if(popupWindowOTPVerified !=null) {
            popupWindowOTPVerified.showAsDropDown(it, 0, -it.getHeight());
        }

    }


    public void provideWindowSubscriptionVerified(View it, Context context,SubscribePlan subscriptionPlan){

        try {
            if (popupWindowSubscriptionVerified != null) {
                popupWindowSubscriptionVerified.dismiss();
                popupWindowSubscriptionVerified = null;
            }
        }
        catch (Exception e){
            e.printStackTrace();

        }
        popupWindowSubscriptionVerified = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        // Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //  popupWindowEmailSent.setBackgroundDrawable(backgroundDrawable);
        popupWindowSubscriptionVerified.setOutsideTouchable(true);
        popupWindowSubscriptionVerified.setFocusable(true);
        popupWindowSubscriptionVerified.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = (View) layoutInflater.inflate(R.layout.email_dialog, null, false);
        LottieAnimationView lottieAnimationViewEmailSent = testView.findViewById(R.id.an_email_sent);


        switch (subscriptionPlan.getState()){
            case TransactionState.SUCCESS:
                try {
                    popupWindowSubscriptionVerified.dismiss();

                }
                catch (Exception e){
                    e.printStackTrace();

                }
                lottieAnimationViewEmailSent.setAnimation(R.raw.done);
                break;
            case TransactionState.FAILED:
            case TransactionState.ABANDONED:
                lottieAnimationViewEmailSent.setAnimation(R.raw.unapproved_cross);
                //new Handler().postDelayed(() -> verifySubscription(edtSubscriptionNo, subscribePlanResponse.getData(), mUser.getUser_id(), String.valueOf(selectedPlan.getId()), mToken), 30000);
                break;
            case TransactionState.ONGOING:
                lottieAnimationViewEmailSent.setAnimation(R.raw.ongoing);

                break;
        }
        AppCompatTextView tvMessage = testView.findViewById(R.id.tv_register_message);
        tvMessage.setText(subscriptionPlan.getDisplay_text());

        lottieAnimationViewEmailSent.playAnimation();
        AppCompatImageButton btn_confirm = testView.findViewById(R.id.btn_close);
        btn_confirm.setOnClickListener(v -> popupWindowSubscriptionVerified.dismiss());

        popupWindowSubscriptionVerified.setContentView(testView);
        popupWindowSubscriptionVerified.setElevation(10);
        if(popupWindowSubscriptionVerified !=null) {
            popupWindowSubscriptionVerified.showAsDropDown(it, 0, -it.getHeight());
        }

    }


    private void verifySubscription(View view,SubscribePlan subscribePlan,String user_id, String plan_id,String token){
        ApiClient.getApiClient()
                .getInstance(getApplicationContext())
                .verifySubscription(subscribePlan.getTransaction_reference(),user_id,plan_id,"application/json","Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<VerifySubscriptionResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<VerifySubscriptionResponse> verifySubscriptionResponseResponse) {

                if(verifySubscriptionResponseResponse.isSuccessful()){

                    VerifySubscriptionResponse verifySubscriptionResponse = verifySubscriptionResponseResponse.body();

                    if(verifySubscriptionResponse!=null) {
                        provideWindowSubscriptionVerified(view,getApplicationContext(),
                                verifySubscriptionResponse.getData());
                    }
                    else {
                        Toast.makeText(SubscriptionActivity.this, "Error occurred in verifying subscription", Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                e.printStackTrace();
            }
        });
    }

    private void sendTransactionOTP(View view,String token, SubscribePlan subscribePlan, String otp){
        ApiClient.getApiClient()
                .getInstance(getApplicationContext())
                .submitSubscriptionOTP("application/json",
                        "Bearer "+token,new SubscriptionOTP(subscribePlan.getTransaction_reference(),otp))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<SubscriptionOTPResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<SubscriptionOTPResponse> subscriptionOTPResponseResponse) {

                if(subscriptionOTPResponseResponse.isSuccessful()){
                   SubscriptionOTPResponse subscriptionOTPResponse = subscriptionOTPResponseResponse.body();
                    if(subscriptionOTPResponse!=null) {
                        provideWindowOTPVerified(view,getApplicationContext(),subscribePlan);
                        /*if (subscriptionOTPResponse.getData().getState().equals(TransactionState.SUCCESS)) {

                        }
                        else {

                        }*/
                    }
                    //TODO show dialog
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

                e.printStackTrace();
            }
        });
    }


    private void initiateSubscription(SubscriptionPlan subscriptionPlan, View view,String token){
        showDialogProgressDialog();
        ApiClient.getApiClient()
                .getInstance(getApplicationContext())
                .subscribePlan(subscriptionPlan, "application/json","Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(
                        new DisposableSingleObserver<Response<SubscribePlanResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<SubscribePlanResponse> subscribePlanResponseResponse) {
                hideDialogProgressDialog();
                if (subscribePlanResponseResponse.isSuccessful()) {

                    subscribePlanResponse =  subscribePlanResponseResponse.body();

                    if(subscribePlanResponse!=null){

                        SubscribePlan subscribePlan = subscribePlanResponse.getData();
                        if(subscribePlan!=null){
                            switch (subscribePlanResponse.getData().getState()) {
                                case TransactionState.OTP:
                                    provideOTPPopupWindow(view, getApplicationContext(), subscribePlanResponse);
                                case TransactionState.OFFLINE:
                                    provideAUthPaymentPopupWindow(view, getApplicationContext(), subscribePlanResponse);
                                    //Toast.makeText(SubscriptionActivity.this, ""+subscribePlan.getDisplay_text(), Toast.LENGTH_LONG).show();

                                    break;
                                case TransactionState.FAILED:
                                    provideAUthPaymentPopupWindow(view, getApplicationContext(), subscribePlanResponse);
                                    Toast.makeText(SubscriptionActivity.this, "" + subscribePlanResponse.getData().getDisplay_text(), Toast.LENGTH_LONG).show();
                                    break;
                                case TransactionState.SUCCESS:
                                    new Handler().postDelayed(() -> verifySubscription(edtSubscriptionNo, subscribePlanResponse.getData(), mUser.getUser_id(), String.valueOf(selectedPlan.getId()), mToken), 30000);
                                    Toast.makeText(SubscriptionActivity.this, "" + subscribePlanResponse.getData().getDisplay_text(), Toast.LENGTH_LONG).show();
                                    break;
                            }

                        }

                       else {

                            if(subscribePlanResponseResponse.errorBody()!=null){
                                JSONObject jsonObj = null;
                                try {
                                    jsonObj = new JSONObject(TextStreamsKt.readText(subscribePlanResponseResponse.errorBody().charStream()));
                                    Log.d("jsonObj", jsonObj.toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }else{
                                Toast.makeText(SubscriptionActivity.this, "Error occurred in instantiating subscription", Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                }
                else {
                    try {
                        JSONObject jObjError = new JSONObject(subscribePlanResponseResponse.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    Toast.makeText(SubscriptionActivity.this, "Error occurred subscribing to plan", Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onError(@NonNull Throwable e) {
                hideDialogProgressDialog();
                Toast.makeText(SubscriptionActivity.this, "Error occurred subscribing to plan", Toast.LENGTH_LONG).show();

                e.printStackTrace();
            }
        });
    }


    @SuppressLint({"SetTextI18n","InflateParams"})
    private void provideOTPPopupWindow(View it, Context activity, SubscribePlanResponse response) {
        try {
            if(popupWindowOTP !=null){
                popupWindowOTP.dismiss();
                popupWindowOTP = null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }


        popupWindowOTP = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //      Drawable backgroundDrawable = ContextCompat.getDrawable(activity,R.drawable.curve_white_background);
        // popupWindowLogOut.setBackgroundDrawable(backgroundDrawable);


        //popupWindowLogOut.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = layoutInflater.inflate(R.layout.otp_dialog, null, false);

        AppCompatTextView textViewMessage = testView.findViewById(R.id.tv_register_message);

        if(response.getData()!=null){
            textViewMessage.setText(response.getData().getDisplay_text());
        }
        AppCompatImageButton btnClose = testView.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(v -> popupWindowOTP.dismiss());
        TextInputEditText editTextOTP = testView.findViewById(R.id.edt_otp);
        AppCompatTextView btn_confirm = testView.findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(v -> {
            if(editTextOTP.getText()!=null) {
                sendTransactionOTP(it, mToken, response.getData(), editTextOTP.getText().toString().trim());
                popupWindowOTP.dismiss();
            }
        });
        popupWindowOTP.setFocusable(true);
        popupWindowOTP.setContentView(testView);

        popupWindowOTP.setElevation(10);

        if(popupWindowOTP !=null) {
            //popupWindowOTP.showAsDropDown(it, 0, 0);
            popupWindowOTP.showAtLocation(it, Gravity.CENTER, 0, 0);
            popupWindowOTP.setOutsideTouchable(false);


        }

    }


    @SuppressLint({"SetTextI18n","InflateParams"})
    private void provideAUthPaymentPopupWindow(View it, Context activity, SubscribePlanResponse response) {
        try {
            if(popupWindowAuthPayment !=null){
                popupWindowAuthPayment.dismiss();
                popupWindowAuthPayment = null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }


        popupWindowAuthPayment = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //      Drawable backgroundDrawable = ContextCompat.getDrawable(activity,R.drawable.curve_white_background);
        // popupWindowLogOut.setBackgroundDrawable(backgroundDrawable);


        //popupWindowLogOut.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = layoutInflater.inflate(R.layout.email_dialog, null, false);

        AppCompatTextView textViewMessage = testView.findViewById(R.id.tv_register_message);

        if(response.getData()!=null){
            textViewMessage.setText(response.getData().getDisplay_text());
        }
        AppCompatImageButton btnClose = testView.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(v -> popupWindowAuthPayment.dismiss());

        LottieAnimationView lottieAnimationView = testView.findViewById(R.id.an_email_sent);
        lottieAnimationView.setAnimation(R.raw.confirmation);
        lottieAnimationView.setRepeatMode(LottieDrawable.RESTART);
        lottieAnimationView.setRepeatCount(3);
        lottieAnimationView.playAnimation();
        popupWindowAuthPayment.setFocusable(true);
        popupWindowAuthPayment.setContentView(testView);

        new Handler().postDelayed(() -> verifySubscription(edtSubscriptionNo, subscribePlanResponse.getData(), mUser.getUser_id(), String.valueOf(selectedPlan.getId()), mToken), 30000);
        popupWindowAuthPayment.setElevation(10);

        if(popupWindowAuthPayment !=null) {
            //popupWindowOTP.showAsDropDown(it, 0, 0);
            popupWindowAuthPayment.showAtLocation(it, Gravity.CENTER, 0, 0);
            popupWindowAuthPayment.setOutsideTouchable(false);


        }

    }

    public void hideDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.hide();
        }
    }

    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(SubscriptionActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = (androidx.appcompat.app.AlertDialog) builder.create();
        AppCompatTextView textViewMessage = alert_view.findViewById(R.id.tv_message);
        if(textViewMessage!=null)
            textViewMessage.setText("Subscribing, Please wait...");
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }


    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }



    private void getPlans(){
        ApiClient.getApiClient()
                .getInstance(getApplicationContext())
                .getPlans()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        new DisposableSingleObserver<Response<PlansResponse>>() {
                    @Override
                    public void onSuccess(@NonNull Response<PlansResponse>
                                                  plansResponseResponse) {
                        if(plansResponseResponse.isSuccessful()) {
                            if (plansResponseResponse.body() != null) {
                                subscriptionTypes.addAll(plansResponseResponse.body().getData());
                                subscriptionTypeAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
    }
    private void provideSubscriptionPopupWindow(View it) {
        if(popupWindowSubscribeTo!=null){
            popupWindowSubscribeTo.dismiss();
            popupWindowSubscribeTo = null;
        }
        popupWindowSubscribeTo = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        Drawable backgroundDrawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.curve_white_background);
        popupWindowSubscribeTo.setBackgroundDrawable(backgroundDrawable);
        popupWindowSubscribeTo.setOutsideTouchable(true);
        popupWindowSubscribeTo.setFocusable(true);
        popupWindowSubscribeTo.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ListView listView = (ListView)layoutInflater.inflate(R.layout.layout_payment_dropdown, null, false);
        listView.setAdapter(subscriptionTypeAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            selectedPlan = subscriptionTypeAdapter.getItem(position);

            if(selectedPlan!=null) {
                if(edtSubscribeTo!=null)
                edtSubscribeTo.setText("GHC "+selectedPlan.getAmount(),false);
            }
            //viewModel.setLegalCountry(selectedCountry);
            if(popupWindowSubscribeTo!=null)
            popupWindowSubscribeTo.dismiss();
        });
        popupWindowSubscribeTo.setContentView(listView);

        if(popupWindowSubscribeTo!=null) {

            popupWindowSubscribeTo.showAsDropDown(viewSubscribeTo, 0, viewSubscribeTo.getHeight());
        }

    }

    private void providePaymentTypePopupWindow(View it) {
        if(popupWindowPaymentType !=null){
            popupWindowPaymentType.dismiss();
            popupWindowPaymentType = null;
        }
        popupWindowPaymentType = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        Drawable backgroundDrawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.curve_white_background);
        popupWindowPaymentType.setBackgroundDrawable(backgroundDrawable);
        popupWindowPaymentType.setOutsideTouchable(true);
        popupWindowPaymentType.setFocusable(true);
        popupWindowPaymentType.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ListView listView = (ListView)layoutInflater.inflate(R.layout.layout_payment_dropdown, null, false);
        listView.setAdapter(paymentTypeAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
             selectedPaymentType = paymentTypeAdapter.getItem(position);

            if(selectedPaymentType!=null) {
                if(tvPayBy!=null)
                tvPayBy.setText(selectedPaymentType.getName(),false);
            }
            //viewModel.setLegalCountry(selectedCountry);
            if(popupWindowPaymentType!=null)
            popupWindowPaymentType.dismiss();
        });
        popupWindowPaymentType.setContentView(listView);

        if(popupWindowPaymentType !=null) {

            popupWindowPaymentType.showAsDropDown(viewPayBy, 0, -(viewPayBy.getHeight()*3));
        }

    }


    @Override
    protected void onDestroy() {
        cancelDialogProgressDialog();
        super.onDestroy();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
