package com.realstudiosonline.xtraclass.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.realstudiosonline.xtraclass.R;

public class SecondFragment extends Fragment {

    MotionLayout motion_layout;
    ImageFilterView iv_home,iv_search, iv_like, iv_profile;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.motion_test, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        motion_layout = view.findViewById(R.id.motion_layout);

        iv_home = view.findViewById(R.id.iv_home);
        iv_search = view.findViewById(R.id.iv_search);
        iv_like = view.findViewById(R.id.iv_like);
        iv_profile = view.findViewById(R.id.iv_profile);

        setClickListener();

    }

    private void setClickListener() {
        // Apply default transition on app launch
        motion_layout.setTransition(motion_layout.getCurrentState(), R.id.home_expand);
        motion_layout.transitionToEnd();

        iv_home.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.home_expand));

        iv_search.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.search_expand));

        iv_like.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.like_expand));

        iv_profile.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.profile_expand));
    }

    private void setTransition(int startState,int endState) {
        motion_layout.setTransition(startState, endState);
        motion_layout.setTransitionDuration(200);
        motion_layout.transitionToEnd();
    }
}