package com.realstudiosonline.xtraclass.models;

public class SearchItem {
    private String query;

    public SearchItem(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
