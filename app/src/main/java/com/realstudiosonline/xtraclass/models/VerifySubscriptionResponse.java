package com.realstudiosonline.xtraclass.models;

public class VerifySubscriptionResponse {
    private SubscribePlan data;

    public SubscribePlan getData() {
        return data;
    }

    public void setData(SubscribePlan data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SubscriptionOTPResponse{" +
                "data=" + data +
                '}';
    }
}
