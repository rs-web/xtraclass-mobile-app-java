package com.realstudiosonline.xtraclass.models;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class Favorite {

    @SerializedName("school_uuid")
    private String schoolId;

    @SerializedName("class_id")
    String classId;

    @SerializedName("period_id")
    String periodId;

    @SerializedName("subject")
    String subject;

    @SerializedName("description")
    String description;

    @SerializedName("vimeo_id")
    String vimeoId;

    @SerializedName("teacher_id")
    String teacherId;

    @SerializedName("play_on")
    String playOn;

    @SerializedName("starts_at")
    String startsAt;

    @SerializedName("school_name")
    String schoolName;

    private DateTime date;


    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getPeriodId() {
        return periodId;
    }

    public void setPeriodId(String periodId) {
        this.periodId = periodId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVimeoId() {
        return vimeoId;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getPlayOn() {
        return playOn;
    }

    public void setPlayOn(String playOn) {
        this.playOn = playOn;
    }

    public String getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(String startsAt) {
        this.startsAt = startsAt;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public DateTime getDate() {
        try {
            this.date= DateTime.parse(playOn + " "+startsAt, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        }
        catch (Exception e){
            this.date =  DateTime.now();
        }
        return date;
    }

    @Override
    public String toString() {
        return "Favorite{" +
                "classId='" + classId + '\'' +
                ", periodId='" + periodId + '\'' +
                ", subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                ", vimeoId='" + vimeoId + '\'' +
                ", teacherId='" + teacherId + '\'' +
                ", playOn='" + playOn + '\'' +
                ", startsAt='" + startsAt + '\'' +
                ", schoolName='" + schoolName + '\'' +
                '}';
    }
}
