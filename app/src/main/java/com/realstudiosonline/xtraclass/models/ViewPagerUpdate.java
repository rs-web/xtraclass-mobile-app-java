package com.realstudiosonline.xtraclass.models;

public class ViewPagerUpdate {
    private int position;

    public ViewPagerUpdate(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
