package com.realstudiosonline.xtraclass.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.airbnb.lottie.LottieAnimationView;
import com.realstudiosonline.xtraclass.R;

import java.util.Timer;
import java.util.TimerTask;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

import static com.realstudiosonline.xtraclass.utils.Constants.round;

public class ActivationSuccessful extends AppCompatActivity {

    LottieAnimationView appCompatImageView3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation_successful);

        appCompatImageView3 = findViewById(R.id.appCompatImageView3);



        appCompatImageView3.addAnimatorUpdateListener(animation -> {
            double round = round(animation.getAnimatedFraction(),2);
            if(round ==  0.75) {
                appCompatImageView3.setProgress(0.75f);
                appCompatImageView3.pauseAnimation();
                Log.d("addAnimatorUpdate", "" + animation.getAnimatedFraction());
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {

                         startActivity(new Intent(ActivationSuccessful.this, MainActivity.class));
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();

                    }
                }, 1000);
            }
        });

        appCompatImageView3.playAnimation();

    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}