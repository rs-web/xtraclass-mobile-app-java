package com.realstudiosonline.xtraclass.models;

import java.io.Serializable;
import java.util.List;

public class QuizDataResponse implements Serializable {

    private List<Question> data;

    public List<Question> getData() {
        return data;
    }

    public void setData(List<Question> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QuizDataResponse{" +
                "data=" + data +
                '}';
    }
}
