package com.realstudiosonline.xtraclass.models;

public class Resolution {
    private H264 h264;

    public H264 getH264() {
        return h264;
    }

    public void setH264(H264 h264) {
        this.h264 = h264;
    }

    @Override
    public String toString() {
        return "Resolution{" +
                "h264=" + h264 +
                '}';
    }
}
