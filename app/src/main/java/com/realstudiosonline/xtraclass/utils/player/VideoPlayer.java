package com.realstudiosonline.xtraclass.utils.player;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoListener;
import com.realstudiosonline.xtraclass.db.Subtitle;
import com.realstudiosonline.xtraclass.models.Video;
import com.realstudiosonline.xtraclass.models.VideoSource;
import com.realstudiosonline.xtraclass.utils.Constants;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_1080P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_160P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_240P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_360P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_480P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_720P;


public class VideoPlayer {
    private static final String TAG = "VideoPlayer";
    Context context;
    PlayerController playerController;
    PlayerView playerView;
    private SimpleExoPlayer player;
    private MediaSource mediaSource;
    private DefaultTrackSelector trackSelector;
    private int widthOfScreen, index;
    private ComponentListener componentListener;
    private ComponentVideoListener componentVideoListener;
    private CacheDataSourceFactory cacheDataSourceFactory;
    VideoSource videoSource;
    int videoQuality;
    final boolean isLock = false;
    private AudioAttributes mAudioAttributes;
    private View qualityView;


    private void initAudioAttributes() {
        mAudioAttributes = new AudioAttributes.Builder()
                // If audio focus should be handled, the AudioAttributes.usage must be C.USAGE_MEDIA
                // or C.USAGE_GAME. Other usages will throw an IllegalArgumentException.
                .setUsage(C.USAGE_MEDIA)
                // Since the app is playing a podcast, set contentType to CONTENT_TYPE_SPEECH.
                // SimpleExoPlayer will pause while the notification, such as when a message arrives,
                // is playing and will automatically resume afterwards.
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();
    }

    public VideoPlayer(PlayerView playerView,
                       Context context,
                       VideoSource videoSource,
                       PlayerController mView, View qualityView) {

        this.playerView = playerView;
        this.qualityView = qualityView;
        this.context = context;
        this.playerController = mView;
        this.videoSource = videoSource;
        this.index = videoSource.getSelectedSourceIndex();
        initAudioAttributes();
        initializePlayer();

    }

    /******************************************************************
     initialize ExoPlayer
     ******************************************************************/
    public  void initializePlayer() {
        playerView.requestFocus();

        componentListener = new ComponentListener();

        componentVideoListener = new ComponentVideoListener();

        cacheDataSourceFactory = new CacheDataSourceFactory(
                context,
                100 * 1024 * 1024,
                100 * 1024 * 1024);

        //TrackSelection.Factory  trackSelectionFactory = new AdaptiveTrackSelection.Factory();

        trackSelector = new DefaultTrackSelector(context);
        videoQuality = PreferenceHelper.getPreferenceHelperInstance().getVideoQuality(context);
        trackSelector.setParameters(trackSelector.getParameters().buildUpon().setMaxVideoBitrate(videoQuality).setMinVideoBitrate(BITRATE_480P).build());
//        trackSelector.setParameters(trackSelector
//                .buildUponParameters());

        player = new SimpleExoPlayer.Builder(context)
                .setTrackSelector(trackSelector)
                .build();
        player.setAudioAttributes(mAudioAttributes, /* handleAudioFocus= */ true);
        playerView.setPlayer(player);
        playerView.setKeepScreenOn(true);
        player.setPlayWhenReady(true);
        player.addListener(componentListener);
        player.addVideoListener(componentVideoListener);
        //build mediaSource depend on video type (Regular, HLS, DASH, etc)
        if(videoSource.getVideos()!=null) {
            if(videoSource.getVideos().size()>index)
            mediaSource = buildMediaSource(videoSource.getVideos().get(index), cacheDataSourceFactory);
        }
        if(mediaSource!=null)
        player.setMediaSource(mediaSource);
        player.prepare();

        //resume video
        if(videoSource.getVideos()!=null) {
            if(videoSource.getVideos().size()>0) {
                VideoSource.SingleVideo videoSource1 = videoSource.getVideos().get(index);
                if(videoSource1.getWatchedLength()!=null) {
                    seekToSelectedPosition(videoSource1.getWatchedLength(), false);
                }
            }
        }

        if (videoSource.getVideos().size() == 1 || isLastVideo())
            playerController.disableNextButtonOnLastVideo(true);
    }

    /******************************************************************
     building mediaSource depend on stream type and caching
     ******************************************************************/
    private MediaSource buildMediaSource(VideoSource.SingleVideo singleVideo, CacheDataSourceFactory cacheDataSourceFactory) {
        Uri source = Uri.parse(singleVideo.getUrl());
        @C.ContentType int type = Util.inferContentType(source);


        MediaItem mediaItem =
                new MediaItem.Builder().setMediaMetadata(new MediaMetadata.Builder().build()).setUri(source)
                        .build();

        switch (type) {
            case C.TYPE_SS:
                Log.d(TAG, "buildMediaSource() C.TYPE_SS = [" + C.TYPE_SS + "]");
                return new SsMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem);

            case C.TYPE_DASH:
                Log.d(TAG, "buildMediaSource() C.TYPE_DASH = [" + C.TYPE_DASH + "]");
                return new DashMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem);

            case C.TYPE_HLS:
                Log.d(TAG, "buildMediaSource() C.TYPE_HLS = [" + C.TYPE_HLS + "]");
                return new HlsMediaSource.Factory(cacheDataSourceFactory).createMediaSource( mediaItem);

            case C.TYPE_OTHER:
                Log.d(TAG, "buildMediaSource() C.TYPE_OTHER = [" + C.TYPE_OTHER + "]");
                return new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem);

            default: {
                throw new IllegalStateException("Unsupported type: " + source);
            }
        }
    }

    public void pausePlayer() {
        if(player!=null)
        player.setPlayWhenReady(false);
    }

    public void resumePlayer() {
        if(player !=null)
        player.setPlayWhenReady(true);
    }

    public void releasePlayer() {
        if (player == null)
            return;

        playerController.setVideoWatchedLength();
        playerView.setPlayer(null);
        player.release();
        player.removeListener(componentListener);
        player.removeVideoListener(componentVideoListener);
        player = null;

       // Toast.makeText(context, "releasePlayer", Toast.LENGTH_SHORT).show();
    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public VideoSource.SingleVideo getCurrentVideo() {
        VideoSource.SingleVideo singleVideo =null;
        if(videoSource.getVideos()!=null) {
            if(videoSource.getVideos().size()>index)
            singleVideo =  videoSource.getVideos().get(index);
        }

        return singleVideo;
    }

    /************************************************************
     mute, unMute
     ***********************************************************/
    public void setMute(boolean mute) {
        float currentVolume = player.getVolume();
        if (currentVolume > 0 && mute) {
            player.setVolume(0);
            playerController.setMuteMode(true);
        } else if (!mute && currentVolume == 0) {
            player.setVolume(1);
            playerController.setMuteMode(false);
        }
    }



    public void onVideoQualityChanged(int bitrate){
        Log.d("onVideoQualityChanged", "Quality Select:"+bitrate);
    }

    /***********************************************************
     manually select stream quality
     ***********************************************************/
    public void setSelectedQuality(Activity activity) {

        MappingTrackSelector.MappedTrackInfo mappedTrackInfo;



        if (trackSelector != null) {

            mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();



            if (mappedTrackInfo != null) {

                int rendererIndex = 0; // renderer for video
                int rendererType = mappedTrackInfo.getRendererType(rendererIndex);
                Log.d("MappingTrackSelector:",""+mappedTrackInfo.getTrackGroups(0));
                boolean allowAdaptiveSelections =
                        rendererType == C.TRACK_TYPE_VIDEO
                                || (rendererType == C.TRACK_TYPE_AUDIO
                                && mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                                == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_NO_TRACKS);


                if(player.getVideoFormat()!=null) {

                    Pair<PopupWindow, MyTrackSelectionView> dialogPair = MyTrackSelectionView.getDialog(activity, trackSelector,
                            rendererIndex,
                            player.getVideoFormat().bitrate);

                    dialogPair.second.setShowDisableOption(false);
                    dialogPair.second.setAllowAdaptiveSelections(allowAdaptiveSelections);
                    dialogPair.second.animate();
                    Log.d(TAG, "exoPlayer.getVideoFormat()" + player.getVideoFormat().bitrate);

                    //dialogPair.first.getWindow().getAttributes().windowAnimations = R.style.Animation;
                    dialogPair.first.showAsDropDown(qualityView,-(qualityView.getWidth()/3),0);

                }

            }

        }
    }

    /***********************************************************
     double tap event and seekTo
     ***********************************************************/
    public void seekToSelectedPosition(int hour, int minute, int second) {
        long playbackPosition = (hour * 3600 + minute * 60 + second) * 1000;
        player.seekTo(playbackPosition);
    }

    public void seekToSelectedPosition(Long millisecond, boolean rewind) {
        if (rewind) {
            player.seekTo(player.getCurrentPosition() - 15000);
            return;
        }
        player.seekTo(millisecond * 1000);
    }

    public void seekToSelectedPosition(long millisecond) {
        player.seekTo(millisecond);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void seekToOnDoubleTap() {
        getWidthOfScreen();
        final GestureDetector gestureDetector = new GestureDetector(context,
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onDoubleTap(MotionEvent e) {

                        float positionOfDoubleTapX = e.getX();

                        if (positionOfDoubleTapX < (float)(widthOfScreen/2) ) {
                            player.seekTo(player.getCurrentPosition() - 5000);
                           // PreferenceHelper.getPreferenceHelperInstance().setPlayBackPosition(context, exoPlayer.getCurrentPosition() - 5000);
                        }
                        else {
                            player.seekTo(player.getCurrentPosition() + 5000);
                           // PreferenceHelper.getPreferenceHelperInstance().setPlayBackPosition(context, exoPlayer.getCurrentPosition() - 5000);
                        }

                        Log.d(TAG, "onDoubleTap(): widthOfScreen >> " + widthOfScreen +
                                " positionOfDoubleTapX >>" + positionOfDoubleTapX);

                        return true;
                    }
                });

       playerView.setOnTouchListener((v, event) -> gestureDetector.onTouchEvent(event));
    }

    private void getWidthOfScreen() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        widthOfScreen = metrics.widthPixels;
    }

    public void seekToNext() {
        if(videoSource.getVideos()!=null) {
            if (index < videoSource.getVideos().size()) {
                setCurrentVideoPosition();
                ++index;
               // Toast.makeText(context, "seekToNext:"+index, Toast.LENGTH_SHORT).show();
                mediaSource = buildMediaSource(videoSource.getVideos().get(index), cacheDataSourceFactory);
                player.setMediaSource(mediaSource, true);
                player.prepare();
                if (videoSource.getVideos().get(index).getWatchedLength() != null) {
                    Long watched = videoSource.getVideos().get(index).getWatchedLength();
                    seekToSelectedPosition(watched, false);
                    playerController.onSeekToNext(index);
                    if (isLastVideo())
                        playerController.disableNextButtonOnLastVideo(true);
                }
                player.play();
            }
        }
    }

    public void seekTo(int myIndex) {
        index = myIndex;
        if(videoSource.getVideos()!=null) {
            //Toast.makeText(context, "videoSource.getVideos()!=null", Toast.LENGTH_SHORT).show();
            if (index < videoSource.getVideos().size()) {

               // Toast.makeText(context, "index < videoSource.getVideos().size() - 1 ", Toast.LENGTH_SHORT).show();
                setCurrentVideoPosition();
                mediaSource = buildMediaSource(videoSource.getVideos().get(index), cacheDataSourceFactory);
                Log.d("seekTo", "mediaSource:"+videoSource.getVideos().get(index).getUrl());
               // Toast.makeText(context, "mediaSource:"+videoSource.getVideos().get(index).getUrl(), Toast.LENGTH_SHORT).show();
                player.setMediaSource(mediaSource, true);
                player.prepare();
                if (videoSource.getVideos().get(index).getWatchedLength() != null) {
                    Long watched = videoSource.getVideos().get(index).getWatchedLength();
                    seekToSelectedPosition(watched, false);
                }
                player.play();

                if (isLastVideo())
                    playerController.disableNextButtonOnLastVideo(true);

            }
            else {
                Toast.makeText(context, "else index < videoSource.getVideos().size() - 1 ", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(context, "seekTo getVideos is null ", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isLastVideo() {
        return index == (videoSource.getVideos()==null? 0: videoSource.getVideos().size() - 1);
    }

    public void seekToPrevious() {
        playerController.disableNextButtonOnLastVideo(false);

        if (index == 0) {
            seekToSelectedPosition(0L, false);
            return;
        }

        if (index > 0) {
            if(videoSource.getVideos()!=null) {
                if (videoSource.getVideos().size() > 0) {
                    setCurrentVideoPosition();
                    --index;
                    mediaSource = buildMediaSource(videoSource.getVideos().get(index), cacheDataSourceFactory);
                    player.setMediaSource(mediaSource, true);
                    player.prepare();
                    playerController.onSeekToPrevious(index);
                    if (videoSource.getVideos().get(index).getWatchedLength() != null) {
                        Long watched = videoSource.getVideos().get(index).getWatchedLength();
                        seekToSelectedPosition(watched, false);
                    }
                }
            }
        }
    }

    private void setCurrentVideoPosition() {
        if (getCurrentVideo() == null)
            return;
        getCurrentVideo().setWatchedLength(player.getCurrentPosition() / 1000);//second
    }

  /*  public int getCurrentVideoIndex() {
        return index;
    }*/

    public long getWatchedLength() {
        if (getCurrentVideo() == null)
            return 0;
        return player.getCurrentPosition() / 1000;//second
    }

    /***********************************************************
     manually select subtitle
     ***********************************************************/
    public void setSelectedSubtitle(Subtitle subtitle) {

        if (TextUtils.isEmpty(subtitle.getTitle()))
            Log.d(TAG, "setSelectedSubtitle: subtitle title is empty");



        Format subtitleFormat = new Format.Builder().setId(0).setSampleMimeType(MimeTypes.APPLICATION_SUBRIP).setSelectionFlags(Format.NO_VALUE).setLanguage(null).build();

//        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
//                Util.getUserAgent(context,CLASS_NAME ));

        if(subtitleFormat.containerMimeType!=null) {
            MediaItem.Subtitle subtitle1 = new MediaItem.Subtitle(Uri.parse(subtitle.getSubtitleUrl()), subtitleFormat.containerMimeType, null, (int) C.TIME_UNSET);
            MediaSource subtitleSource = new SingleSampleMediaSource
                    .Factory(cacheDataSourceFactory)
                    .createMediaSource(subtitle1, 0);


            //optional
            playerController.changeSubtitleBackground();

            player.setMediaSource(new MergingMediaSource(mediaSource, subtitleSource), true);
            player.prepare();
            playerController.showSubtitle(true);
        }
        resumePlayer();

    }

    /***********************************************************
     playerView listener for lock and unlock screen
     ***********************************************************/
/*
    public void lockScreen(boolean isLock) {
        this.isLock = isLock;
    }
*/

    public boolean isLock() {
        return isLock;
    }


    /***********************************************************
     Video Listeners
     ***********************************************************/


    private static class ComponentVideoListener implements VideoListener{
        @Override
        public void onVideoSizeChanged(int width, int height, int unAppliedRotationDegrees, float pixelWidthHeightRatio) {
            Log.d("ComponentVideoListener",  " height="+ height+ " width="+ width);
        }
    }
    /***********************************************************
     Listeners
     ***********************************************************/
    private class ComponentListener implements EventListener {



        @Override
        public void onTimelineChanged(Timeline timeline, int reason) {
            Log.d("onTimelineChanged", "timeline"+reason);
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            //int bitrate = trackGroups.get(0).getFormat(0).bitrate;

           //int rendererIndex = PreferenceHelper.getPreferenceHelperInstance().getRendererIndex(context);
            //int trackGroup = PreferenceHelper.getPreferenceHelperInstance().getTrackGroup(context);
            int bitrate = PreferenceHelper.getPreferenceHelperInstance().getVideoQuality(context);

            Log.d("onTracksChanged","bitrate: "+bitrate);
            playerController.onVideoQualityChanged(bitrate);

           // Log.d("onTracksChanged", "rendererIndex: "+rendererIndex);
           // Log.d("onTracksChanged", "trackGroup: "+trackGroup);
          /*  if(trackSelections.get(trackGroup)!=null) {
                TrackSelection trackSelection = trackSelections.get(trackGroup);

                if(trackSelection!=null){
                    int  bitrate =  trackSelection.getFormat(rendererIndex).bitrate;
                    playerController.onVideoQualityChanged(bitrate);
                    //trackSelections.get(trackGroup).getFormat(rendererIndex);
                    Log.d("onTracksChanged", "BITRATE: " + bitrate);
                    setBitrate(bitrate);
                }

            }*/
           // if(trackSelections.get(trackGroup)!=null)
           // Log.d("onTracksChanged", "BITRATE: "+trackSelections.get(trackGroup).getSelectedFormat().bitrate);
        }




        public void setBitrate(int bitrate){

                if (bitrate == Format.NO_VALUE) {

                    PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);

                }
                if (bitrate <= BITRATE_160P) {
                    PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);
                }
                if (bitrate <= BITRATE_240P) {
                    PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);

                }
                if (bitrate <= BITRATE_360P) {
                    PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_360P);
                }
                if (bitrate <= BITRATE_480P) {
                    PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);
                }
                if (bitrate <= BITRATE_720P) {
                    PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_720P);
                }
                if (bitrate <= BITRATE_1080P) {
                    PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_1080P);
                }
        }
        @Override
        public void onPlaybackStateChanged(int playbackState) {
            Log.d("onPlaybackStateChanged", "onPlayerStateChanged:"+playbackState);

            switch (playbackState) {
                case Player.STATE_IDLE:
                    playerController.showProgressBar(false);
                    playerController.showRetryBtn(true);
                    break;
                case Player.STATE_BUFFERING:
                    playerController.showProgressBar(true);
                    break;
                case Player.STATE_READY:
                    playerController.showProgressBar(false);
                    playerController.audioFocus();
                    break;
                case Player.STATE_ENDED:
                    playerController.showProgressBar(false);
                    playerController.videoEnded();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            playerController.showProgressBar(false);
            playerController.showRetryBtn(true);
        }
    }

}