package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Message;

import java.util.List;

import static com.realstudiosonline.xtraclass.utils.TimeAgo.getTimeAgo;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {


    private List<Message> messageList;
    private Context context;
    private setOnMessageClickListener listener;

    public ChatAdapter(List<Message> messageList, Context context, setOnMessageClickListener listener) {
        this.messageList = messageList;
        this.context = context;
        this.listener = listener;
    }

    static class ChatViewHolder extends RecyclerView.ViewHolder {

        CircularImageView imageViewAvatar;
        AppCompatTextView textViewQuestion,textViewDateTime, btnReply, textViewSenderName;
        View viewMessage;
        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewAvatar = itemView.findViewById(R.id.img_user_avatar);

            textViewQuestion = itemView.findViewById(R.id.tv_chat_text);

            textViewDateTime = itemView.findViewById(R.id.tv_date_time);

            viewMessage = itemView.findViewById(R.id.view_message);

            btnReply = itemView.findViewById(R.id.btn_reply);

            textViewSenderName = itemView.findViewById(R.id.tv_sender_name);

        }
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChatViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {

        Message message = messageList.get(position);

        holder.textViewQuestion.setText(message.getContent());

        if(message.getUser()!=null)
        holder.textViewSenderName.setText(message.getUser().getUsername());

        String formattedTime = getTimeAgo(message.getDate().getMillis());

        holder.textViewDateTime.setText(formattedTime);

        Glide.with(context).load(R.drawable.profile_demo).into(holder.imageViewAvatar);

        holder.btnReply.setOnClickListener(v -> listener.onReply(message));

        holder.imageViewAvatar.setOnClickListener(v -> listener.onProfileClicked(message));

    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }


    public interface setOnMessageClickListener{
        void onReply(Message message);

        void onProfileClicked(Message message);

    }
}
