package com.realstudiosonline.xtraclass.models;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class VideoQ {

    public final String videoQuality;

    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({ TWO_FORTY,THREE_SIXTY,FOUR_TWENTY,FIVE_FORTY,SEVEN_TWENTY, TEN_EIGHTY,AUTO})
    // Create an interface for validating String types
    public @interface FilterSessionTypeDef {}
    // Declare the constants
    public static final String TWO_FORTY = "240p";
    public static final String THREE_SIXTY = "360P";
    public static final String FOUR_TWENTY = "420p";
    public static final String FIVE_FORTY = "540p";
    public static final String SEVEN_TWENTY = "720p";
    public static final String TEN_EIGHTY = "1080p";
    public static final String AUTO = "Auto";

    // Mark the argument as restricted to these enumerated types
    public VideoQ(@FilterSessionTypeDef String videoQuality) {
        this.videoQuality = videoQuality;
    }
}
