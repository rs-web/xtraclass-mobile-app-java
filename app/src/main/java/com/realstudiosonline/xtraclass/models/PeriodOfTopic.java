package com.realstudiosonline.xtraclass.models;

public class PeriodOfTopic {
    private String id;
    private String title;
    private int number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "PeriodOfTopic{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", number=" + number +
                '}';
    }

    /*       "id": "8022e639-9a09-474b-b3b6-290a6e1bbcad",
               "number": 1,
               "title": "Lesson 1"*/
}
