package com.realstudiosonline.xtraclass.models;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import java.util.List;

public class Message {
    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;
    private int message_type;
    private int id;
    private int video_id;
    private int subject_id;
    private String content;
    private String type;
    private int sender_id;
    private int parent_id;
    private String status;
    private String created_at;
    private String updated_at;
    private String uuid;
    private ChatUser user;
    private DateTime date;
    private String student_name;
    private List<Message> children_accounts;

    public Message(int id, int subject_id, String content, int sender_id, int parent_id, String uuid, ChatUser user) {
        this.id = id;
        this.subject_id = subject_id;
        this.content = content;
        this.sender_id = sender_id;
        this.parent_id = parent_id;
        this.uuid = uuid;
        this.user = user;
    }


    public DateTime getDate() {
        try {
            this.date= DateTime.parse(this.created_at, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        }
        catch (Exception e){
            this.date =  DateTime.now();
        }
        return date;
    }



    public ChatUser getUser() {
        return user;
    }

    public void setUser(ChatUser user) {
        this.user = user;
    }

    public int getMessage_type() {
        return message_type;
    }

    public void setMessage_type(int message_type) {
        this.message_type = message_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVideo_id() {
        return video_id;
    }

    public void setVideo_id(int video_id) {
        this.video_id = video_id;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public List<Message> getChildren_accounts() {
        return children_accounts;
    }

    public void setChildren_accounts(List<Message> children_accounts) {
        this.children_accounts = children_accounts;
    }


}
