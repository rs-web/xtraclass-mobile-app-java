package com.realstudiosonline.xtraclass.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemImage implements Parcelable{

    private String mHref;

    public ItemImage() {
    }

    public ItemImage(String href) {
        mHref = href;
    }

    protected ItemImage(Parcel in) {
        mHref = in.readString();
    }

    public static final Parcelable.Creator<ItemImage> CREATOR = new Parcelable.Creator<ItemImage>() {
        @Override
        public ItemImage createFromParcel(Parcel in) {
            return new ItemImage(in);
        }

        @Override
        public ItemImage[] newArray(int size) {
            return new ItemImage[size];
        }
    };

    public String getItemImageHref() {
        return mHref;
    }

    public void setItemImageHref(String href) {
        mHref = href;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mHref);
    }
}
