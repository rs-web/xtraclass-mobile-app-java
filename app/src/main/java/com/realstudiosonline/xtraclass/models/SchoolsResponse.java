package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class SchoolsResponse {
    private String message;
    private List<School> data;
    private int statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<School> getData() {
        return data;
    }

    public void setData(List<School> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }


}

/*
*
* {
    "statusCode": 200,
    "message": "",
    "data": [
        {
            "id": "897a5ab9-24b9-4511-9a32-392667fe2a95",
            "is_locked": 0,
            "name": "Class 1",
            "logo_url": "https://dextraclass.com/uploads/schools/1412419070731931603203161__Primary.png",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "30ee05c9-fc9b-46ee-b2fa-bea1610e56d1",
            "is_locked": 0,
            "name": "Class 2",
            "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "27e296a2-3bf0-44fa-b5c0-9e5ea5790cd7",
            "is_locked": 0,
            "name": "Class 3",
            "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "1ee4ad6e-5641-4e4a-a024-aeccab60316f",
            "is_locked": 0,
            "name": "Class 4",
            "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "264e64d4-3e88-4153-bbf4-53e9199da1a3",
            "is_locked": 0,
            "name": "Class 5",
            "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "90e70e78-cfc6-4367-95b0-c87c2b160554",
            "is_locked": 0,
            "name": "Class 6",
            "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "7287ed76-4318-48ed-9831-b29fac886bf9",
            "is_locked": 0,
            "name": "KINDERGARTEN",
            "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "790ff93d-6aba-4e15-97be-38b216047459",
            "is_locked": 0,
            "name": "NURSERY",
            "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        },
        {
            "id": "4f735cba-d963-4f85-8701-a9be9acecf08",
            "is_locked": 0,
            "name": "PLAYGROUP",
            "logo_url": "https://dextraclass.com/uploads/schools/16040018689647011606493040__Playgroup.png",
            "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"
        }
    ]
}*/
