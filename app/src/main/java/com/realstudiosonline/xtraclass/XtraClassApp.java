package com.realstudiosonline.xtraclass;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.realstudiosonline.xtraclass.utils.Constants;
import com.realstudiosonline.xtraclass.utils.PrefManager;
import com.realstudiosonline.xtraclass.utils.agora.rtc.AgoraEventHandler;
import com.realstudiosonline.xtraclass.utils.agora.rtc.EngineConfig;
import com.realstudiosonline.xtraclass.utils.agora.rtc.EventHandler;
import com.realstudiosonline.xtraclass.utils.agora.stats.StatsManager;
import com.realstudiosonline.xtraclass.utils.player.VideoPlayer;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.HttpUrlConnectionDownloader;
import com.tonyodev.fetch2core.Downloader;

import java.io.File;
import java.net.URISyntaxException;

import io.agora.rtc.RtcEngine;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.Cache;

public class XtraClassApp extends Application {
    private RtcEngine mRtcEngine;
    private final EngineConfig mGlobalConfig = new EngineConfig();
    private final AgoraEventHandler mHandler = new AgoraEventHandler();
    private final StatsManager mStatsManager = new StatsManager();


    private static  VideoPlayer videoPlayer;
    private static FirebaseAnalytics mFirebaseAnalytics;




    public static VideoPlayer getInstance() {
        //Double check locking pattern
        if (videoPlayer == null) { //Check for the first time

            synchronized (VideoPlayer.class) {
                //if (videoPlayer == null) videoPlayer = new SingletonClass();
            }
        }

        return videoPlayer;
    }

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(Constants.CHAT_SERVER_URL);
        } catch (URISyntaxException e) {
            Log.d("mSocket", "Error"+ e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    public  static FirebaseAnalytics getFirebaseAnalytics(Context context){
        return mFirebaseAnalytics== null ? FirebaseAnalytics.getInstance(context) : mFirebaseAnalytics;

    }
    @Override
    public void onCreate() {
        super.onCreate();

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/roboto_regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());


        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);



        final FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .enableRetryOnNetworkGain(true)
                .setDownloadConcurrentLimit(3)
                .setHttpDownloader(new HttpUrlConnectionDownloader(Downloader.FileDownloaderType.PARALLEL))
                // OR
                //.setHttpDownloader(getOkHttpDownloader())
                .build();
        Fetch.Impl.setDefaultInstanceConfiguration(fetchConfiguration);

       /* Configuration.Builder configBuilder =
                new Configuration.Builder(Utils.access_token)
                        .setCacheDirectory(this.getCacheDir());
        VimeoClient.initialize(configBuilder.build());*/

       /* try {
            mRtcEngine = RtcEngine.create(getApplicationContext(), getString(R.string.private_app_id), mHandler);
            mRtcEngine.setLogFile(FileUtil.initializeLogFile(this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        initConfig();*/

    }



    private void initConfig() {
        SharedPreferences pref = PrefManager.getPreferences(getApplicationContext());
        mGlobalConfig.setVideoDimenIndex(pref.getInt(
                Constants.PREF_RESOLUTION_IDX, Constants.DEFAULT_PROFILE_IDX));

        boolean showStats = pref.getBoolean(Constants.PREF_ENABLE_STATS, false);
        mGlobalConfig.setIfShowVideoStats(showStats);
        mStatsManager.enableStats(showStats);

        mGlobalConfig.setMirrorLocalIndex(pref.getInt(Constants.PREF_MIRROR_LOCAL, 0));
        mGlobalConfig.setMirrorRemoteIndex(pref.getInt(Constants.PREF_MIRROR_REMOTE, 0));
        mGlobalConfig.setMirrorEncodeIndex(pref.getInt(Constants.PREF_MIRROR_ENCODE, 0));
        mGlobalConfig.setChannelName("test");
    }

    public EngineConfig engineConfig() {
        return mGlobalConfig;
    }

    public RtcEngine rtcEngine() {
        return mRtcEngine;
    }

    public StatsManager statsManager() {
        return mStatsManager;
    }

    public void registerEventHandler(EventHandler handler) {
        mHandler.addHandler(handler);
    }

    public void removeEventHandler(EventHandler handler) {
        mHandler.removeHandler(handler);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        RtcEngine.destroy();
    }
}
