package com.realstudiosonline.xtraclass.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import com.realstudiosonline.xtraclass.fragments.ChatFragment;
import com.realstudiosonline.xtraclass.fragments.LibraryFragment;
import com.realstudiosonline.xtraclass.fragments.PlayListFragment;


public class HomePagerAdapter extends FragmentStateAdapter {


    /*public interface setSearchListener{
        void onSearch(String query);
    }

    public setSearchListener listener;

    public HomePagerAdapter(@NonNull FragmentManager fragmentManager,
                            @NonNull Lifecycle lifecycle, setSearchListener listener) {
        super(fragmentManager, lifecycle);
        this.listener = listener;

    }*/

    public HomePagerAdapter(@NonNull FragmentManager fragmentManager,
                            @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);

    }



    @Override
    public long getItemId(int position) {

        return super.getItemId(position);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return PlayListFragment.newInstance();

            case 1:
                return ChatFragment.newInstance();

            case 2:
                return LibraryFragment.newInstance();

        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 3;
    }



    /*
    *
    * switch (position){
            case 0:
                return StudentInfoFragment.newInstance("","");

            case 1:
                return PhotoBioFragment.newInstance("","");
            case 2:
                return GenerateCardFragment.newInstance("","");
        }
        return null;
    * */
}

