package com.realstudiosonline.xtraclass.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.ChangeSubjectAdapter;
import com.realstudiosonline.xtraclass.adapters.GenericAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.fragments.HomeScreenFragment;
import com.realstudiosonline.xtraclass.fragments.OpenSchoolFragment;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.ClassesForCourseResponse;
import com.realstudiosonline.xtraclass.models.CoursesForSchool;
import com.realstudiosonline.xtraclass.models.DrawerToggle;
import com.realstudiosonline.xtraclass.models.GenericItem;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.models.UserPlan;
import com.realstudiosonline.xtraclass.models.VideoQuality;
import com.realstudiosonline.xtraclass.utils.Constants;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

import static com.realstudiosonline.xtraclass.fragments.HomeScreenFragment.view_anchor;
import static com.realstudiosonline.xtraclass.fragments.OpenSchoolFragment.view_anchor_open;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_480P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_720P;
import static com.realstudiosonline.xtraclass.utils.Constants.convertDpToPixelInt;
import static com.realstudiosonline.xtraclass.utils.Constants.convertToPixelDpInt;
import static com.realstudiosonline.xtraclass.utils.Constants.getBitrateString;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private DrawerLayout mDrawerLayout;
    Toolbar toolbar;
    NavigationView navigation;
    AutoCompleteTextView spinnerMyClass,spinnerVideoQuality;
    View viewRegister, viewScrollDrawer;
    User mUser;
    AppCompatTextView tvEmailAddress,tvMobileNumber,tvStudentID, tvMySchool;
    AppCompatImageButton imgClose;
    List<ClassesForCourse> classesForCourses;
    CoursesForSchool coursesForSchool;
    ClassDetails classDetails;
    PopupWindow popupWindowChangeSubject, popupWindowVideoQuality;
    ClassesForCourse classesForCourse;
    PopupWindow popupWindowExit;
    private int schoolType;
    FragmentTransaction fragmentTransaction;

    final ArrayList<GenericItem> videoQualities = new ArrayList<GenericItem>() {{
        add(new GenericItem(BITRATE_480P,R.drawable.sd, "480p"));
        add(new GenericItem(BITRATE_720P,R.drawable.hd, "720p"));
    }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout =  findViewById(R.id.drawer_layout);
        navigation = findViewById(R.id.nav_view);
        spinnerVideoQuality = findViewById(R.id.spinner_video_quality);
        tvEmailAddress = findViewById(R.id.tv_email_address);
        tvMobileNumber = findViewById(R.id.tv_mobile_number);
        tvStudentID = findViewById(R.id.tv_student_id);
        tvMySchool = findViewById(R.id.tv_my_school);
        spinnerMyClass = findViewById(R.id.spinner_my_class);

        classesForCourse = PreferenceHelper.getPreferenceHelperInstance().getSelectedClassesForCourse(getApplicationContext());
        coursesForSchool = PreferenceHelper.getPreferenceHelperInstance().getCoursesForSchool(getApplicationContext());
        classDetails = PreferenceHelper.getPreferenceHelperInstance().getClassDetails(getApplicationContext());
        schoolType = PreferenceHelper.getPreferenceHelperInstance().getSchoolType(getApplicationContext());
        classesForCourses = new ArrayList<>();


        spinnerMyClass.setKeyListener(null);
        spinnerMyClass.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                if(classesForCourses.size()>0)
                provideChangeSubjectPopupWindow(spinnerMyClass,getApplicationContext(),classesForCourses);
            }
        });
        spinnerMyClass.setOnClickListener(v -> {
            if(classesForCourses.size()>0)
            provideChangeSubjectPopupWindow(spinnerMyClass,getApplicationContext(),classesForCourses);
        });

        spinnerVideoQuality.setKeyListener(null);
        spinnerVideoQuality.setOnClickListener(v -> provideVideoQualityPopupWindow(spinnerVideoQuality,getApplicationContext(),videoQualities));

        spinnerVideoQuality.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideVideoQualityPopupWindow(spinnerVideoQuality,getApplicationContext(),videoQualities);

            }
        });



        if(classesForCourse!=null){
            spinnerMyClass.setText(classesForCourse.getClass_name());
        }
        else {
            //Toast.makeText(this, "classesForCourse is null", Toast.LENGTH_SHORT).show();
        }

        imgClose = navigation.getHeaderView(0).findViewById(R.id.img_back_press);


        if(classDetails!=null)
        tvMySchool.setText(classDetails.getSchool_name());


        if(schoolType==1){
            replaceFragmentInDefaultLayout(new HomeScreenFragment(), true, true);

        }

        else{
            replaceFragmentInDefaultLayout(new OpenSchoolFragment(), true, true);
        }


        imgClose.setOnClickListener(v -> mDrawerLayout.close());
       PreferenceHelper.getPreferenceHelperInstance().setOtpVerified(getApplicationContext(),true);

        boolean otpVerified =PreferenceHelper.getPreferenceHelperInstance().getOtpVerified(getApplicationContext());

        //Toast.makeText(getApplicationContext(), "Subscribe to access this feature.", Toast.LENGTH_LONG).show();
       if(!otpVerified){
           startActivity(new Intent(this, ConfirmOTPActivity.class));
           finish();
        }
        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());

        int bitrate = PreferenceHelper.getPreferenceHelperInstance().getVideoQuality(getApplicationContext());

        if(bitrate==0){
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(getApplicationContext(), BITRATE_480P);
        }

        spinnerVideoQuality.setText(getBitrateString(bitrate));

        if(mUser!=null) {
            String phone = PreferenceHelper.getPreferenceHelperInstance().getCountryCode(getApplicationContext())+ mUser.getPhone();

            try {
                tvEmailAddress.setText(mUser.getEmail());

            }
            catch (Exception e){
                e.printStackTrace();
            }
            tvMobileNumber.setText(phone);
            tvStudentID.setText(mUser.getUser_id());
            if (schoolType ==1) {
                getListOfClassesForCourse(mUser.getCourse_id());
                //navigation.findViewById(R.id.view_change_classs).setVisibility(View.GONE);
            }
            else {
                //navigation.findViewById(R.id.view_change_classs).setVisibility(View.VISIBLE);
                if(coursesForSchool!=null)
                getListOfClassesForCourse(coursesForSchool.getId());
                else {
                    try {
                        getListOfClassesForCourse(mUser.getCourse_id());
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            Log.d("User", mUser.toString());
        }
        //PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(getApplicationContext(), 0);

        navigation.findViewById(R.id.tv_change_class).setOnClickListener(v -> {

            startActivity(new Intent(this, HowDoYouWantToLearnActivity.class));
                }
        );


        navigation.findViewById(R.id.view_profile).setOnClickListener(v -> {
           // gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
            startActivity(new Intent(this, StudentProfileActivity.class));
        });

        navigation.findViewById(R.id.view_subscription).setOnClickListener(v ->

                {
                    UserPlan userPlan = PreferenceHelper.getPreferenceHelperInstance().getCurrentPlan(getApplicationContext());

                    boolean isPaid = false;
                    int daysLeft=0;
                    if(userPlan!=null) {
                        DateTime startOnDateTime = DateTime.parse(userPlan.getStarts_on(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
                        DateTime endOnDateTime = DateTime.parse(userPlan.getExpired_on(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
                        daysLeft = Days.daysBetween(startOnDateTime.toLocalDate(), endOnDateTime.toLocalDate()).getDays();
                        isPaid = userPlan.getPayment_status().equals("paid");
                    }

                    if(daysLeft<=0 || !isPaid)
                    startActivity(new Intent(this, SubscriptionActivity.class));

                    else {
                        //todo show current subscription
                    }

                }
        );

        viewRegister = navigation.findViewById(R.id.view_register);

        viewRegister.setOnClickListener(v -> startActivity(new Intent(this, TellAsMoreAboutYouActivity.class)));
        viewScrollDrawer = navigation.findViewById(R.id.view_drawer);
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                mDrawerLayout.bringToFront();
                Log.d(TAG,"onDrawerSlide");
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                Log.d(TAG,"onDrawerOpened");
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                Log.d(TAG,"onDrawerClosed");
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });



        //navController.navigate(R.id.HomeScreenFragment);

        boolean isRegistered = PreferenceHelper.getPreferenceHelperInstance().getIsRegistered(getApplicationContext());

        if(!isRegistered) {
            viewScrollDrawer.setVisibility(View.INVISIBLE);
            viewRegister.setVisibility(View.VISIBLE);
        }
        else {
            viewScrollDrawer.setVisibility(View.VISIBLE);
            viewRegister.setVisibility(View.GONE);

        }
        setupDrawerToggle();

    }



    public void replaceFragmentInDefaultLayout(Fragment fragmentToBeLoaded,
                                               boolean addToBackStack, boolean allowStateLoss) {
        if (!getSupportFragmentManager().isDestroyed()) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.nav_host_fragment, fragmentToBeLoaded,
                    fragmentToBeLoaded.getClass().getCanonicalName());

            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragmentToBeLoaded.getClass().getCanonicalName());
            }
            if (allowStateLoss) {
                fragmentTransaction.commitAllowingStateLoss();
            } else {
                fragmentTransaction.commit();
            }
        }
    }


    private void provideChangeSubjectPopupWindow(View it, Context context, List<ClassesForCourse> classesForCourses){
        if(popupWindowChangeSubject!=null){
            popupWindowChangeSubject.dismiss();
            popupWindowChangeSubject = null;
        }
        int width = 200;
        String device = Constants.getSizeName(context);
        switch (device){
            case "small":
            case "normal":
                width = 200;
                break;

            case "xlarge":
            case "large":
                width = 300;
                    /*if(libraryItems.size()%3!=0){
                        int whatsLeft = libraryItems.size()%3;

                        for(int i =1; i<whatsLeft; i++) {
                            libraryItems.add(new LibraryMenu(R.drawable.add_icon_blue, "",
                                    R.color.colorSelectedLesson, -1));
                        }
                    }*/
                break;
        }
        ChangeSubjectAdapter changeSubjectAdapter = new ChangeSubjectAdapter(context,R.layout.item_subject, classesForCourses);
        popupWindowChangeSubject = new PopupWindow(convertDpToPixelInt(width,context), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowChangeSubject.setOutsideTouchable(true);
        popupWindowChangeSubject.setFocusable(true);
        popupWindowChangeSubject.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(changeSubjectAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            ClassesForCourse classesForCourse = changeSubjectAdapter.getItem(position);

            PreferenceHelper.getPreferenceHelperInstance().setPlayPrevious(getApplicationContext(), false);
            PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(getApplicationContext(),classesForCourse);
            PreferenceHelper.getPreferenceHelperInstance().setLessons(getApplicationContext(), new ArrayList<>());
            PreferenceHelper.getPreferenceHelperInstance().setFavorites(getApplicationContext(),new ArrayList<>());
            PreferenceHelper.getPreferenceHelperInstance().setClassVideoPlayOn(getApplicationContext(),"");
            PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext());
            Log.d("ChangeSubjectAdapter", classesForCourse.toString());
            popupWindowChangeSubject.dismiss();
            startActivity(new Intent(MainActivity.this, MainActivity.class));

            finish();


            //viewModel.setLegalCountry(selectedCountry);

        });
        popupWindowChangeSubject.setContentView(parent_view);
        popupWindowChangeSubject.setElevation(20);

        if(popupWindowChangeSubject!=null) {

            popupWindowChangeSubject.showAsDropDown(it, 0,-it.getHeight());
        }

    }


    private void provideVideoQualityPopupWindow(View it, Context context, List<GenericItem> videoQualities){
        if(popupWindowVideoQuality!=null){
            popupWindowVideoQuality.dismiss();
            popupWindowVideoQuality = null;
        }
        int width = 200;
        String device = Constants.getSizeName(context);
        switch (device){
            case "small":
            case "normal":
                width = 200;
                break;
            case "xlarge":
            case "large":
                width = 300;
                break;
        }
        GenericAdapter videoQualityAdapter = new GenericAdapter(context,R.layout.item_payment_type_dropdown, videoQualities);
        popupWindowVideoQuality = new PopupWindow(convertDpToPixelInt(width,context), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowVideoQuality.setOutsideTouchable(true);
        popupWindowVideoQuality.setFocusable(true);
        popupWindowVideoQuality.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(videoQualityAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {

            GenericItem item = videoQualityAdapter.getItem(position);
            VideoQuality videoQuality = new VideoQuality(item.getName(),item.getId());
            spinnerVideoQuality.setText(videoQuality.getQuality());
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(getApplicationContext(), videoQuality.getBitrate());
            //viewModel.setLegalCountry(selectedCountry);
            popupWindowVideoQuality.dismiss();

        });
        popupWindowVideoQuality.setContentView(parent_view);
        popupWindowVideoQuality.setElevation(20);

        if(popupWindowVideoQuality!=null) {

            popupWindowVideoQuality.showAsDropDown(it, 0,-it.getHeight());
        }

    }



    @Override
    public void onBackPressed() {
        if (schoolType == 1) {
            provideExitPopupWindow(view_anchor, getApplicationContext());
        }
        else {
            provideExitPopupWindow(view_anchor_open, getApplicationContext());
        }
    }


    @SuppressLint({"SetTextI18n","InflateParams"})
    private void provideExitPopupWindow(View it, Context activity) {
        if(popupWindowExit !=null){
            popupWindowExit.dismiss();
            popupWindowExit = null;
        }


        popupWindowExit = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //      Drawable backgroundDrawable = ContextCompat.getDrawable(activity,R.drawable.curve_white_background);
        // popupWindowLogOut.setBackgroundDrawable(backgroundDrawable);
        popupWindowExit.setOutsideTouchable(true);
        popupWindowExit.setFocusable(true);
        //popupWindowLogOut.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = layoutInflater.inflate(R.layout.register_dialog, null, false);

        AppCompatImageButton btnClose = testView.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(v -> popupWindowExit.dismiss());
        AppCompatTextView textViewMessage = testView.findViewById(R.id.tv_register_message);
        AppCompatTextView btn_confirm = testView.findViewById(R.id.btn_confirm);
        btn_confirm.setText("Exit");
        textViewMessage.setText("Are you sure you want to exit?");
        btn_confirm.setOnClickListener(v -> {

            popupWindowExit.dismiss();
            finish();
        });

        popupWindowExit.setContentView(testView);
        popupWindowExit.setElevation(10);

        if(popupWindowExit !=null) {
            popupWindowExit.showAsDropDown(it, 0, 0);
            //popupWindowExit.showAtLocation(it, Gravity.CENTER, 0, 0);

        }

    }


    void setupDrawerToggle(){
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }

   /* private void gotoLiveActivity(int role) {
        Intent intent = new Intent(getIntent());
        intent.putExtra(com.realstudiosonline.xtraclass.utils.Constants.KEY_CLIENT_ROLE, role);
        intent.setClass(getApplicationContext(), LiveActivity.class);
        startActivity(intent);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void toggleDrawer(DrawerToggle drawerToggle){
        if(!mDrawerLayout.isOpen())
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


   /* @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }*/



    private void getListOfClassesForCourse(String courseId){

        ApiClient.getApiClient().getInstance(getApplicationContext())
                .getListOfClassesForCourse(courseId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassesForCourseResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<ClassesForCourseResponse> classesForCourseResponseResponse) {
                if(classesForCourseResponseResponse.isSuccessful()){
                    ClassesForCourseResponse classesForCourseResponse = classesForCourseResponseResponse.body();
                      /* if(getActivity()!=null)
                showChangeClassDialog(getActivity(), classesForCourseResponse.getData());*/
                    Log.d("ListOfClassesForCourse", ""+classesForCourseResponse);
                    Log.d("mUser.getCourse_id()", ""+mUser.getCourse_id());
                    /*
                    * ClassesForCourse{
                    * class_name='Mathematics',
                    * uuid='dac9d503-b71e-4059-aeb6-aa80e6bfc0c3',
                    * course_id='e93e7590-375a-4d92-8d69-45e8f89b44b4',
                    * school_id='897a5ab9-24b9-4511-9a32-392667fe2a95',
                    * school_name='Basic 1'
                    * }*
                    e93e7590-375a-4d92-8d69-45e8f89b44b4
                    /
                     */
                    classesForCourses.clear();
                    if(classesForCourseResponse!=null) {
                        classesForCourses.addAll(classesForCourseResponse.getData());
                        //changeSubjectAdapter.notifyDataSetChanged();
                    }
                    ClassesForCourse classesForCourse = PreferenceHelper.getPreferenceHelperInstance().getSelectedClassesForCourse(getApplicationContext());
                    if (classesForCourse==null){
                        for (ClassesForCourse classesForCourse1 :classesForCourses){

                            try {
                                if(classesForCourse1.getCourse_id().equals(mUser.getCourse_id())){
                                    PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(getApplicationContext(),classesForCourse1);
                                    spinnerMyClass.setText(classesForCourse1.getClass_name());
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                    else{
                        spinnerMyClass.setText(classesForCourse.getClass_name());
                       // Toast.makeText(MainActivity.this, "not null", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}