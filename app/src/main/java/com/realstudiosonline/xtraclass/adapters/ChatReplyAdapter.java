package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Message;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.realstudiosonline.xtraclass.utils.TimeAgo.getTimeAgo;

public class ChatReplyAdapter extends RecyclerView.Adapter<ChatReplyAdapter.ChatReplyViewHolder> {

    private setOnMessageRelyClickListener listener;
    private Context context;
    private List<Message> mMessages;


    public ChatReplyAdapter(Context context, List<Message> mMessages,setOnMessageRelyClickListener listener) {
        this.listener = listener;
        this.context = context;
        this.mMessages = mMessages;
    }

    @NonNull
    @NotNull
    @Override
    public ChatReplyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return  new ChatReplyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.message_reply_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ChatReplyAdapter.ChatReplyViewHolder holder, int position) {
        Message message = mMessages.get(position);
        holder.bind(message);
    }

    @Override
    public int getItemCount() {
        return mMessages == null ? 0 : mMessages.size();
    }

    public interface setOnMessageRelyClickListener{
        void onChatReplySelected(Message message);

        void onProfileClicked(Message message);

    }
    public class ChatReplyViewHolder extends RecyclerView.ViewHolder {
        CircularImageView imageViewAvatar;
        AppCompatTextView mMessageView,textViewDateTime, btnReply, mUsernameView;
        View viewMessage;
        RecyclerView recyclerView;


        public void bind(Message message){
            try {
                setMessage(""+message.getContent());
                if(message.getUser()!=null)
                    setUsername(""+message.getUser().getUsername());
                String formattedTime = getTimeAgo(message.getDate().getMillis());
                textViewDateTime.setText(formattedTime);
                Glide.with(context).load(R.drawable.profile_demo).into(imageViewAvatar);
                btnReply.setOnClickListener(v -> listener.onChatReplySelected(message));
                imageViewAvatar.setOnClickListener(v -> listener.onProfileClicked(message));
            }
            catch (Exception e){
                e.printStackTrace();

            }
        }

        public ChatReplyViewHolder(View itemView) {
            super(itemView);

            recyclerView = itemView.findViewById(R.id.recycler_view_chat_reply);

            mUsernameView = (AppCompatTextView) itemView.findViewById(R.id.tv_sender_name);

            mMessageView = (AppCompatTextView) itemView.findViewById(R.id.tv_chat_text);

            imageViewAvatar = itemView.findViewById(R.id.img_user_avatar);

            textViewDateTime = itemView.findViewById(R.id.tv_date_time);

            viewMessage = itemView.findViewById(R.id.view_message);

            btnReply = itemView.findViewById(R.id.btn_reply);
        }

        public void setUsername(String username) {
            if (null == mUsernameView) return;
            mUsernameView.setText(username);
            //mUsernameView.setTextColor(getUsernameColor(username));
        }

        public void setMessage(String message) {
            if (null == mMessageView) return;
            mMessageView.setText(message);
        }


    }

}
