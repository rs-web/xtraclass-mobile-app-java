package com.realstudiosonline.xtraclass.models;

import java.io.Serializable;

public class Option implements Serializable {
   private int value;
   private String detail;
   private boolean correct;


    public Option(int value, String detail, boolean correct) {
        this.value = value;
        this.detail = detail;
        this.correct = correct;
    }

    public Option() {
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    @Override
    public String toString() {
        return "Option{" +
                "value=" + value +
                ", detail='" + detail + '\'' +
                ", correct=" + correct +
                '}';
    }
}
