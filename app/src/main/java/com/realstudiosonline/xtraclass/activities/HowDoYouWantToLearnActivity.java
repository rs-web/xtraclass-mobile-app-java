package com.realstudiosonline.xtraclass.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class HowDoYouWantToLearnActivity  extends AppCompatActivity {

    User mUser;
    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_do_you_want_to_learn);

        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
        //AT MY OWN PACE
        findViewById(R.id.btn_at_my_own_pace).setOnClickListener(v -> {

            PreferenceHelper.getPreferenceHelperInstance().setSchoolType(getApplicationContext(),0);
            startActivity(new Intent(this, ChangeClassActivity.class));
            finish();
        });

        //TAKE A COURSE
        findViewById(R.id.btn_take_a_course).setOnClickListener(v -> {

            //startActivity(new Intent(this, ChangeClassActivity.class));
           // finish();
        });


        //JOIN A CLASS
        findViewById(R.id.btn_join_a_class).setOnClickListener(v -> {
            if(mUser.getInstitution_type().equalsIgnoreCase("regular_school")) {
                PreferenceHelper.getPreferenceHelperInstance().setPlayPrevious(getApplicationContext(), false);
                // PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(getApplicationContext(),classesForCourse);
                PreferenceHelper.getPreferenceHelperInstance().setLessons(getApplicationContext(), new ArrayList<>());
                PreferenceHelper.getPreferenceHelperInstance().setFavorites(getApplicationContext(), new ArrayList<>());
                PreferenceHelper.getPreferenceHelperInstance().setClassVideoPlayOn(getApplicationContext(), "");
                PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext());
                //  PreferenceHelper.getPreferenceHelperInstance().setClassDetails(getApplicationContext());
                PreferenceHelper.getPreferenceHelperInstance().setSelectedLesson(getApplicationContext());
                PreferenceHelper.getPreferenceHelperInstance().setSchoolType(getApplicationContext(), 1);

                try {
                    ClassesForCourse classesForCourse = new ClassesForCourse("",
                            mUser.getClass_id(),
                            mUser.getCourse_id(),
                            mUser.getSchool_id(), "");
                    PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(this, classesForCourse);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }

            else Toast.makeText(this, "Cannot join selected", Toast.LENGTH_SHORT).show();
        });
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
