package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.GenericItem;

import java.util.List;
import java.util.Locale;

public class ChangeSubjectAdapter extends ArrayAdapter<ClassesForCourse> {
    Context context;
    List<ClassesForCourse> classesForCourses;

    public ChangeSubjectAdapter(@NonNull Context context, int resource,
                                @NonNull List<ClassesForCourse> classesForCourses) {
        super(context, resource, classesForCourses);
        this.classesForCourses = classesForCourses;
        this.context = context;
    }


    /*@Override
    public int getCount() {
        return classesForCourses == null  ? 0: classesForCourses.size();
    }*/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

      /*  val view: View
        if (position == 0) {
            view = layoutInflater.inflate(R.layout.header_country, parent, false)
            view.setOnClickListener {
                val root = parent.rootView
                root.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK))
                root.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK))
            }
        } else {
            view = layoutInflater.inflate(R.layout.item_country_dropdown, parent, false)
            getItem(position)?.let { country ->
                    setItemForCountry(view, country)
            }
        }
        return view*/

        ViewHolder holder;
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subject, parent, false);
            // get all UI view
            /*convertView.setOnClickListener(v -> {
                View root = parent.getRootView();
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            });*/

            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }
        ClassesForCourse genericItem = classesForCourses.get(position);
        String  countryName = new Locale("", genericItem.getClass_name()).getDisplayName();
        holder.tvCountry.setText(countryName);
        return convertView;

    }

    /*@Override
    public View getDropDownView(int position, @Nullable @org.jetbrains.annotations.Nullable View convertView, @NonNull @NotNull ViewGroup parent) {

        View view;
        if (position == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_country, parent, false);
            view.setOnClickListener(v -> {
                View root = parent.getRootView();
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            });
        }
        else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country_dropdown, parent, false);
            OperatedCountry country = getItem(position);
            if(country!=null)
            setItemForCountry(view, country);
        }
        return view;
    }
*/

    private static class ViewHolder {

        TextView tvCountry;

        public ViewHolder(View v) {
            tvCountry = (TextView) v.findViewById(R.id.tvCountry);
        }
    }




}
