package com.realstudiosonline.xtraclass.models;

import java.util.Comparator;
import java.util.List;

public class Lesson implements Comparator<Lesson> {

    private int lessonType;
    private String lesson_id;
    private String class_id;
    private String period_id;
    private String period_title;
    private String subject;
    private String vimeo_id;
    private String teacher_id;
    private String teacher_name;
    private String starts_at;
    private String topic_name;
    private String notes_url;
    private String mobile_video_url;
    private String quiz_id;
    private List<Question> questions;
    private boolean quiz_taken;
    private boolean has_quiz;
    private boolean quiz_passed;
    private String video_quiz_taken_id;
    private int quiz_marks;
    private String pass_mark;
    private int next_video_weight;
    private boolean isSelected;
    private MediaCMS video_info;
    private int id;
    private String subject_id;
    private boolean isFavorite;

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MediaCMS getVideo_info() {
        return video_info;
    }

    public void setVideo_info(MediaCMS video_info) {
        this.video_info = video_info;
    }

    public int getLessonType() {
        return lessonType;
    }

    public void setLessonType(int lessonType) {
        this.lessonType = lessonType;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public String getLesson_id() {
        return lesson_id;
    }

    public void setLesson_id(String lesson_id) {
        this.lesson_id = lesson_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getPeriod_id() {
        return period_id;
    }

    public void setPeriod_id(String period_id) {
        this.period_id = period_id;
    }

    public String getPeriod_title() {
        return period_title;
    }

    public void setPeriod_title(String period_title) {
        this.period_title = period_title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getVimeo_id() {
        return vimeo_id;
    }

    public void setVimeo_id(String vimeo_id) {
        this.vimeo_id = vimeo_id;
    }

    public String getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(String teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getStarts_at() {
        return starts_at;
    }

    public void setStarts_at(String starts_at) {
        this.starts_at = starts_at;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getNotes_url() {
        return notes_url;
    }

    public void setNotes_url(String notes_url) {
        this.notes_url = notes_url;
    }

    public String getMobile_video_url() {
        return mobile_video_url;
    }

    public void setMobile_video_url(String mobile_video_url) {
        this.mobile_video_url = mobile_video_url;
    }

    public String getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(String quiz_id) {
        this.quiz_id = quiz_id;
    }

    public boolean isQuiz_taken() {
        return quiz_taken;
    }

    public void setQuiz_taken(boolean quiz_taken) {
        this.quiz_taken = quiz_taken;
    }

    public boolean isHas_quiz() {
        return has_quiz;
    }

    public void setHas_quiz(boolean has_quiz) {
        this.has_quiz = has_quiz;
    }

    public boolean isQuiz_passed() {
        return quiz_passed;
    }

    public void setQuiz_passed(boolean quiz_passed) {
        this.quiz_passed = quiz_passed;
    }

    public String getVideo_quiz_taken_id() {
        return video_quiz_taken_id;
    }

    public void setVideo_quiz_taken_id(String video_quiz_taken_id) {
        this.video_quiz_taken_id = video_quiz_taken_id;
    }

    public int getQuiz_marks() {
        return quiz_marks;
    }

    public void setQuiz_marks(int quiz_marks) {
        this.quiz_marks = quiz_marks;
    }

    public String getPass_mark() {
        return pass_mark;
    }

    public void setPass_mark(String pass_mark) {
        this.pass_mark = pass_mark;
    }

    public int getNext_video_weight() {
        return next_video_weight;
    }

    public void setNext_video_weight(int next_video_weight) {
        this.next_video_weight = next_video_weight;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "lessonType=" + lessonType +
                ", lesson_id='" + lesson_id + '\'' +
                ", class_id='" + class_id + '\'' +
                ", period_id='" + period_id + '\'' +
                ", period_title='" + period_title + '\'' +
                ", subject='" + subject + '\'' +
                ", vimeo_id='" + vimeo_id + '\'' +
                ", teacher_id='" + teacher_id + '\'' +
                ", teacher_name='" + teacher_name + '\'' +
                ", starts_at='" + starts_at + '\'' +
                ", topic_name='" + topic_name + '\'' +
                ", notes_url='" + notes_url + '\'' +
                ", mobile_video_url='" + mobile_video_url + '\'' +
                ", quiz_id='" + quiz_id + '\'' +
                ", questions=" + questions +
                ", quiz_taken=" + quiz_taken +
                ", has_quiz=" + has_quiz +
                ", quiz_passed=" + quiz_passed +
                ", video_quiz_taken_id='" + video_quiz_taken_id + '\'' +
                ", quiz_marks=" + quiz_marks +
                ", pass_mark='" + pass_mark + '\'' +
                ", next_video_weight=" + next_video_weight +
                ", isSelected=" + isSelected +
                ", video_info=" + video_info +
                ", id=" + id +
                ", subject_id='" + subject_id + '\'' +
                '}';
    }

    @Override
    public int compare(Lesson lesson1, Lesson lesson2) {
        return lesson1.getNext_video_weight() - lesson2.getNext_video_weight();
    }

   /*
    *    {
            "lesson_id": "95d0d096-85c6-4667-8ce4-14679800bddc",
            "class_id": "d76968d3-c5c7-4901-a378-34932cc3671e",
            "period_id": "6e71cf66-0041-44f6-b2ca-1ac64d7e405d",
            "period_title": "Lesson 1",
            "subject": "R.M.E",
            "subject_id": "36000e01-7092-49ee-88d6-9acdf2d07c2d",
            "vimeo_id": "483072805",
            "mobile_video_url": "https://vimeo.com/483072805/3c70ee7a45",
            "teacher_id": 2352,
            "teacher_name": "",
            "starts_at": "2021-03-31",
            "topic_name": "Teenage Pregnancy",
            "notes_url": "",
            "quiz_id": 2,
            "questions": [
                {
                    "text": "Good deeds comes with punishment",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": false
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": true
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "Effects of teenage pregnancy. choose the odd one out",
                    "options": [
                        {
                            "value": 1,
                            "detail": "Option 1",
                            "correct": true
                        }
                    ],
                    "type": "multi-choice"
                },
                {
                    "text": "Immorality is behaving in an unacceptable manner",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": true
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": false
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "The basis for repentance is acknowledge of the guilt",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": true
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": false
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "What is Religion?",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": true
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": false
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "Decency is a behaviour that shows disrespect",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": false
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": true
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "Good deeds comes with punishment",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": false
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": true
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "Effects of teenage pregnancy. choose the odd one out",
                    "options": [
                        {
                            "value": 1,
                            "detail": "Option 1",
                            "correct": true
                        }
                    ],
                    "type": "multi-choice"
                },
                {
                    "text": "Immorality is behaving in an unacceptable manner",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": true
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": false
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "The basis for repentance is acknowledge of the guilt",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": true
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": false
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "Who is a man",
                    "options": [],
                    "type": "multi-choice"
                },
                {
                    "text": "good boy",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": true
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": false
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "What is Religion?",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": true
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": false
                        }
                    ],
                    "type": "true-false"
                },
                {
                    "text": "Good deeds comes with punishment",
                    "options": [
                        {
                            "value": 1,
                            "detail": "true",
                            "correct": false
                        },
                        {
                            "value": 2,
                            "detail": "false",
                            "correct": true
                        }
                    ],
                    "type": "true-false"
                }
            ],
            "quiz_taken": false,
            "has_quiz": true,
            "quiz_passed": true,
            "video_quiz_taken_id": null,
            "quiz_marks": 0,
            "pass_mark": 0,
            "next_video_weight": 1
        },*/
}
