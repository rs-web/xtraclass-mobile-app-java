package com.realstudiosonline.xtraclass.utils.widgets;

public interface OnTabSelectListener {
    void onTabSelect(int position);
    void onTabReselect(int position);
}