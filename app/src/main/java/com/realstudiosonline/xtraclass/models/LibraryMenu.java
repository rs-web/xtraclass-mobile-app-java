package com.realstudiosonline.xtraclass.models;

public class LibraryMenu {
    private int icon;
    private String text;
    private int color;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LibraryMenu(int icon, String text, int color, int id) {
        this.icon = icon;
        this.text = text;
        this.color = color;
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
