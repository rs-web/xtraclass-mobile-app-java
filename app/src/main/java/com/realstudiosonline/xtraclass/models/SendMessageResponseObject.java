package com.realstudiosonline.xtraclass.models;

public class SendMessageResponseObject {

    private String content;
    private String type;
    private String parent_id;
    private String sender_id;
    private int class_id;
    private int video_id;
    private int subject_id;
    private String uuid;
    private String updated_at;
    private String created_at;
    private int id;
    private String student_name;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public int getVideo_id() {
        return video_id;
    }

    public void setVideo_id(int video_id) {
        this.video_id = video_id;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    @Override
    public String toString() {
        return "SendMessageResponseObject{" +
                "content='" + content + '\'' +
                ", type='" + type + '\'' +
                ", parent_id='" + parent_id + '\'' +
                ", sender_id='" + sender_id + '\'' +
                ", class_id=" + class_id +
                ", video_id=" + video_id +
                ", subject_id=" + subject_id +
                ", uuid='" + uuid + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", created_at='" + created_at + '\'' +
                ", id=" + id +
                ", student_name='" + student_name + '\'' +
                '}';
    }


    /*
    *  "content": "message from mick using postman mick",
        "type": "question",
        "parent_id": "0",
        "sender_id": "2317",
        "class_id": 229,
        "video_id": 6488,
        "subject_id": 1172,
        "uuid": "621b2388-6536-423a-905c-f833f52380e4",
        "updated_at": "2021-07-16 10:09:49",
        "created_at": "2021-07-16 10:09:49",
        "id": 8,
        "student_name": null*/
}
