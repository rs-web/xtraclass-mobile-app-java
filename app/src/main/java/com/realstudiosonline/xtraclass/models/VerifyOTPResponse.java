package com.realstudiosonline.xtraclass.models;

public class VerifyOTPResponse {
    private String message;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /*    {"status":false,"message":"Verification failed"}*/
}
