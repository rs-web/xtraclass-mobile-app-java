package com.realstudiosonline.xtraclass.models;

import com.google.gson.annotations.SerializedName;

public class EncodingProfile {
    @SerializedName("1080")
    private Resolution tenEighty;

    @SerializedName("720")
    private Resolution sevenTwenty;

    @SerializedName("480")
    private Resolution fourEighty;

    public Resolution getTenEighty() {
        return tenEighty;
    }

    public void setTenEighty(Resolution tenEighty) {
        this.tenEighty = tenEighty;
    }

    public Resolution getSevenTwenty() {
        return sevenTwenty;
    }

    public void setSevenTwenty(Resolution sevenTwenty) {
        this.sevenTwenty = sevenTwenty;
    }

    public Resolution getFourEighty() {
        return fourEighty;
    }

    public void setFourEighty(Resolution fourEighty) {
        this.fourEighty = fourEighty;
    }

    @Override
    public String toString() {
        return "EncodingProfile{" +
                "tenEighty=" + tenEighty +
                ", sevenTwenty=" + sevenTwenty +
                ", fourEighty=" + fourEighty +
                '}';
    }
}
