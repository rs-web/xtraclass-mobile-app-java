package com.realstudiosonline.xtraclass.models;

import java.io.Serializable;

public class User implements Serializable {

    private int id;
    private int country;
    private int status;
    private String username;
    private String google_id;
    private String name;
    private String email;
    private String email_verified_at;
    private String remember_token;
    private String token;
    private String mobile_verified_at;
    private String device_token;
    private String device_type;
    private String uuid;
    private String user_id;
    private String first_name;
    private String last_name;
    private String phone;
    private String profile_image;
    private String file_url;
    private String country_name;
    private String institution_id;
    private String cat_id;
    private String school_id;
    private String co_id;
    private String course_id;
    private String class_id;
    private String institution_type;

    public String getInstitution_type() {
        return institution_type;
    }

    public void setInstitution_type(String institution_type) {
        this.institution_type = institution_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public void setEmail_verified_at(String email_verified_at) {
        this.email_verified_at = email_verified_at;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMobile_verified_at() {
        return mobile_verified_at;
    }

    public void setMobile_verified_at(String mobile_verified_at) {
        this.mobile_verified_at = mobile_verified_at;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getInstitution_id() {
        return institution_id;
    }

    public void setInstitution_id(String institution_id) {
        this.institution_id = institution_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getCo_id() {
        return co_id;
    }

    public void setCo_id(String co_id) {
        this.co_id = co_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", country=" + country +
                ", status=" + status +
                ", username='" + username + '\'' +
                ", google_id='" + google_id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", email_verified_at='" + email_verified_at + '\'' +
                ", remember_token='" + remember_token + '\'' +
                ", token='" + token + '\'' +
                ", mobile_verified_at='" + mobile_verified_at + '\'' +
                ", device_token='" + device_token + '\'' +
                ", device_type='" + device_type + '\'' +
                ", uuid='" + uuid + '\'' +
                ", user_id='" + user_id + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", phone='" + phone + '\'' +
                ", profile_image='" + profile_image + '\'' +
                ", file_url='" + file_url + '\'' +
                ", country_name='" + country_name + '\'' +
                ", institution_id='" + institution_id + '\'' +
                ", cat_id='" + cat_id + '\'' +
                ", school_id='" + school_id + '\'' +
                ", co_id='" + co_id + '\'' +
                ", course_id='" + course_id + '\'' +
                ", class_id='" + class_id + '\'' +
                ", institution_type='" + institution_type + '\'' +
                '}';
    }

   /*id": 2527,
            "username": "550770376",
            "google_id": null,
            "name": null,
            "email": "550770376@email.com",
            "email_verified_at": null,
            "status": 1,
            "remember_token": null,
            "token": null,
            "mobile_verified_at": null,
            "device_token": null,
            "device_type": null,
            "deleted_at": null,
            "created_at": "2021-01-26 15:14:40",
            "updated_at": "2021-01-26 15:14:40",
            "uuid": "c030722a-f00a-4c08-9fe9-75838ecacaf6",
            "provider_id": null,
            "provider": null,
            "user_id": 2527,
            "first_name": "",
            "last_name": "",
            "phone": "550770376",
            "country": 233,
            "profile_image": null,
            "file_url": null,
            "country_name": "GHANA",
            "institution_id": null,
            "cat_id": null,
            "school_id": null,
            "co_id": null,
            "course_id": null,
            "class_id": null*/
}
