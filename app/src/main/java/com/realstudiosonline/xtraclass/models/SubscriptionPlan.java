package com.realstudiosonline.xtraclass.models;

public class SubscriptionPlan {
    private String amount;
    private String email;
    private String phone_number;
    private String network;
    private String fullname;
    private String user_id;
    private String description;
    private String plan_id;


    public SubscriptionPlan(String amount,
                            String email,
                            String phone_number,
                            String network,
                            String fullname,
                            String user_id,
                            String description,
                            String plan_id) {
        this.amount = amount;
        this.email = email;
        this.phone_number = phone_number;
        this.network = network;
        this.fullname = fullname;
        this.user_id = user_id;
        this.description = description;
        this.plan_id = plan_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    @Override
    public String toString() {
        return "SubscriptionPlan{" +
                "amount='" + amount + '\'' +
                ", email='" + email + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", network='" + network + '\'' +
                ", fullname='" + fullname + '\'' +
                ", user_id='" + user_id + '\'' +
                ", description='" + description + '\'' +
                ", plan_id='" + plan_id + '\'' +
                '}';
    }
}
