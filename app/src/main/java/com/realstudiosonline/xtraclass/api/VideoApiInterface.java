package com.realstudiosonline.xtraclass.api;

import com.realstudiosonline.xtraclass.models.MediaCMS;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface VideoApiInterface {
   String baseUrl = "http://video.desafrica.com";

   @GET("/api/v1/media/{media_id}")
   Single<Response<MediaCMS>> getMedia(@Path("media_id") String media_id);
}
