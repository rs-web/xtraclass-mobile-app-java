package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;

import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;

import java.util.ArrayList;

public class ClassForCourseDownAdapter extends ArrayAdapter<ClassesForCourse> {

    private Context context;
    private ArrayList<ClassesForCourse> classesForCourses;


    public ClassForCourseDownAdapter(@NonNull Context context, int resource, ArrayList<ClassesForCourse> classesForCourses) {
        super(context, resource,classesForCourses);
        this.context = context;
        this.classesForCourses = classesForCourses;
    }

    @Override
    public int getCount() {
        return classesForCourses.size(); // Return the size of the suggestions list.
    }

    @Override
    public ClassesForCourse getItem(int position) {
        return classesForCourses.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }



    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.institution_view, parent, false);
            holder = new ViewHolder();
            holder.autoText = (AppCompatTextView) convertView.findViewById(R.id.autoText);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        String text = classesForCourses.get(position).getClass_name();
        holder.autoText.setText(text);
        return convertView;
    }


   /* @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
         *//*   convertView = inflater.inflate(R.layout.adapter_autotext,
                    parent,
                    false);*//*

         if(position == 0){
             convertView = inflater.inflate(R.layout.institution_adapter_top, parent, false);
         }
         else if(position ==classesForCourses.size()-1){
             convertView = inflater.inflate(R.layout.institution_adapter_bottom, parent, false);
         }

         else {
             convertView = inflater.inflate(R.layout.institution_adapter, parent, false);
         }

            holder = new ViewHolder();
            holder.autoText = (AppCompatTextView) convertView.findViewById(R.id.autoText);
            holder.cardView = (CardView) convertView.findViewById(R.id.card_video_quality);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(position == classesForCourses.size()-1){
            holder.cardView.setBackgroundResource(R.drawable.card_bottom_radius);
        }

        if(position == 0){
            holder.cardView.setBackgroundResource(R.drawable.card_top_radius);
        }
        holder.autoText.setText(classesForCourses.get(position).getClass_name());

        return convertView;
    }
*/
    public int convertDpToPixelInt(float dp, Context context) {
        return (int) (dp * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }


    private static class ViewHolder {
        AppCompatTextView autoText;
        AppCompatImageView qualityIcon;
        CardView cardView;
    }
}
