package com.realstudiosonline.xtraclass.models;

public class VideoQuality {
    private String quality;
    private int bitrate;

    public VideoQuality(String quality, int bitrate) {
        this.quality = quality;
        this.bitrate = bitrate;
    }

    public VideoQuality(int bitrate) {
        this.bitrate = bitrate;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public int getBitrate() {
        return bitrate;
    }

    public void setBitrate(int bitrate) {
        this.bitrate = bitrate;
    }
}
