package com.realstudiosonline.xtraclass.models;

public class PatchUser {
    private String first_name;
    private String last_name;
    private String email;
    private String username;
    private String institute_id;
    private String school_id;
    private String course_id;
    private String class_id;

    public PatchUser(String first_name, String last_name, String email, String username, String institute_id, String school_id, String course_id, String class_id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.username = username;
        this.institute_id = institute_id;
        this.school_id = school_id;
        this.course_id = course_id;
        this.class_id = class_id;
    }

    public PatchUser() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getInstitute_id() {
        return institute_id;
    }

    public void setInstitute_id(String institute_id) {
        this.institute_id = institute_id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    @Override
    public String toString() {
        return "PatchUser{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", institute_id='" + institute_id + '\'' +
                ", school_id='" + school_id + '\'' +
                ", course_id='" + course_id + '\'' +
                ", class_id='" + class_id + '\'' +
                '}';
    }
}
