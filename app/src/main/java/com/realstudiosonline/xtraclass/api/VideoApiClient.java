package com.realstudiosonline.xtraclass.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class VideoApiClient {

    private static VideoApiClient apiClient;

    private VideoApiInterface apiCallback;
    private final int RESULT_OK = 200;

    public static VideoApiClient getApiClient() {
        if (apiClient == null) {
            apiClient = new VideoApiClient();
        }
        return apiClient;
    }

    /**
     * Static method to to get api client instance
     *
     * @return ApiInterface instance
     */
    public VideoApiInterface getInstance() {
        try {
            if (apiCallback == null) {

               // TokenInterceptor interceptor=new TokenInterceptor();

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                //httpClient.addInterceptor(interceptor);
                httpClient.connectTimeout(60, TimeUnit.SECONDS);
                httpClient.readTimeout(60, TimeUnit.SECONDS);
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(logging);


                Retrofit client = new Retrofit.Builder()
                        .baseUrl(VideoApiInterface.baseUrl)
                        .client(httpClient.build())
                        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                apiCallback = client.create(VideoApiInterface.class);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return apiCallback;
    }

}
