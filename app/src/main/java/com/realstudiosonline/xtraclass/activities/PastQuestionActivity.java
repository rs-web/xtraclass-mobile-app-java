package com.realstudiosonline.xtraclass.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.viewpager2.widget.ViewPager2;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.google.android.material.textview.MaterialTextView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.QuizAdapter;
import com.realstudiosonline.xtraclass.databinding.ActivityPastQuestionBinding;
import com.realstudiosonline.xtraclass.databinding.ActivityQuizBinding;
import com.realstudiosonline.xtraclass.models.Question;
import com.realstudiosonline.xtraclass.models.QuizDataResponse;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class PastQuestionActivity extends AppCompatActivity implements QuizAdapter.setOnAnswerSelectedListener{
    ActivityPastQuestionBinding binding;
    //private RecyclerView recyclerView;
    RecyclerView recyclerView;
    private QuizAdapter quizAdapter;
    private List<Question> questions;
    //private int lastPosition =0;
    //SnapHelper helper;
    private double correctAnswers=0;
    AppCompatSeekBar seekBar, seekBarSuccess, seekBarFail;
    //View view_content_quiz, view_quiz_result;
    AppCompatImageView imageViewResult, imageViewScoreStar;
    AppCompatTextView textViewScoreMessage, textViewScore, textViewPlusAddedScore;
    ViewFlipper viewFlipper, viewFlipperResult;
    View postLeaderBoard, retryAgain, continueView;
    LottieAnimationView lottieAnimationViewSuccessConfetti;
    boolean isInitialTransform = false;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPastQuestionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);



        recyclerView = binding.viewContentPastQuestion.recyclerViewPastQuestion;

        binding.closePastQuestion.setOnClickListener(v -> finish());

        postLeaderBoard = binding.viewQuizResult.viewLeaderBoard;

        retryAgain = binding.viewQuizResult.viewTryAgain;

        lottieAnimationViewSuccessConfetti = binding.viewQuizResult.viewSuccessConfetti;

        retryAgain.setOnClickListener(v -> {

            correctAnswers = 0.0;
            // lastPosition = 0;
            viewFlipper.setDisplayedChild(0);
            //recyclerView.getLayoutManager().scrollToPosition(0);
            questions.clear();
            seekBar.setProgress(0);

            quizAdapter = new QuizAdapter(questions,getApplicationContext(), this);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

            recyclerView.setAdapter(quizAdapter);

        });
        questions = new ArrayList<>();

        if(getIntent()!=null){
            QuizDataResponse response = (QuizDataResponse)getIntent().getSerializableExtra("quiz_data");

            questions.addAll(response.getData());
        }
        continueView = binding.viewQuizResult.viewContinue;

        continueView.setOnClickListener(v -> finish());
        textViewScoreMessage = binding.viewQuizResult.tvScoreMessage;

        textViewScore = binding.viewQuizResult.tvScore;

        textViewPlusAddedScore = binding.viewQuizResult.viewPlusAddedScore;

        viewFlipperResult = binding.viewQuizResult.viewFilpperSeek;

        imageViewResult = binding.viewQuizResult.imageQuizBadge;

        imageViewScoreStar = binding.viewQuizResult.imgScoreStar;

        seekBar = binding.viewContentPastQuestion.seekBarProgress;

        seekBarSuccess = binding.viewQuizResult.seekBarSuccess;

        seekBarFail = binding.viewQuizResult.seekBarFail;

        seekBar.setOnTouchListener((v, event) -> true);

        seekBarSuccess.setOnTouchListener((v, event) -> true);

        seekBarFail.setOnTouchListener((v, event) -> true);

       // view_content_quiz = binding.viewContentQuiz.viewQuiz;

      //  view_quiz_result = binding.viewQuizResult.viewQuizResult;

        viewFlipper = binding.viewFlipperPastQuestion;
        viewFlipper.setDisplayedChild(0);

        quizAdapter = new QuizAdapter(questions,getApplicationContext(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(quizAdapter);
/*

        questions.add(new Question("Good deeds comes with punishment", "true-false",options));
        questions.add(new Question("Who is a man","multi-choice",options1));
        questions.add(new Question("What is this?","image-as-question",options2));
        helper.attachToRecyclerView(recyclerView);
        quizAdapter = new QuizAdapter(questions,getApplicationContext(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(quizAdapter);
*/


    }

   /* private void setAnimation(View viewToAnimate, int position, Context context)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }*/

    @Override
    public void onAnswerSelected(Question question, int selectedOption, int previousPosition, int size) {


        final double mPos = (double)previousPosition+1;
        double percentage = (mPos/((double)size))*100;


        if(selectedOption == question.getAnswer_id()){
            correctAnswers+=1.0;
        }

        /*for (Option option:question.getOptions()) {
            if(option.isCorrect()){
                if(selectedOption ==option.getValue()){
                    correctAnswers+=1.0;
                }

            }

        }*/
        int mPercentage = (int) (percentage);
        //Toast.makeText(this, "correctAnswers:"+correctAnswers, Toast.LENGTH_SHORT).show();

        seekBar.post(() -> seekBar.setProgress(mPercentage));

        if(previousPosition!=size-1){
            lottieAnimationViewSuccessConfetti.setVisibility(View.GONE);
            recyclerView.scrollToPosition(previousPosition + 1);
       /*     AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(1000);
            //alphaAnimation.setRepeatCount(1);
            alphaAnimation.setRepeatMode(Animation.REVERSE);
            viewPagerQuiz.startAnimation(alphaAnimation);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                    viewPagerQuiz.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    viewPagerQuiz.setVisibility(View.VISIBLE);
                    viewPagerQuiz.setCurrentItem(previousPosition + 1);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });*/
        }

        else {
            String result = ((int)correctAnswers)+"/"+size;
            textViewScore.setText(result);

            double mOverAllPercentage = (correctAnswers/((double)size))*100;
            viewFlipper.setDisplayedChild(1);

            if(mOverAllPercentage>60) {
                //  lottieAnimationViewSuccessConfetti.setAnimation(R.raw.confetti);
                lottieAnimationViewSuccessConfetti.setVisibility(View.VISIBLE);
                lottieAnimationViewSuccessConfetti.setRepeatCount(2);
                lottieAnimationViewSuccessConfetti.playAnimation();

                imageViewScoreStar.setVisibility(View.VISIBLE);
                viewFlipperResult.setDisplayedChild(0);
                Glide.with(PastQuestionActivity.this).load(R.drawable.success_ico).into(imageViewResult);
                textViewScoreMessage.setText(R.string.congrats);
                String plusAddScore = "+"+(int)correctAnswers;
                textViewPlusAddedScore.setText(plusAddScore);
            }
            else {
                //lottieAnimationViewSuccessConfetti.setAnimation(R.raw.sad);
                //  lottieAnimationViewSuccessConfetti.setVisibility(View.VISIBLE);
                //  lottieAnimationViewSuccessConfetti.setRepeatCount(2);
                //  lottieAnimationViewSuccessConfetti.playAnimation();
                imageViewScoreStar.setVisibility(View.GONE);
                Glide.with(PastQuestionActivity.this).load(R.drawable.neg_ico).into(imageViewResult);
                viewFlipperResult.setDisplayedChild(1);
                textViewScoreMessage.setText(R.string.too_bad);
            }
        }
/*

        if(recyclerView.getLayoutManager()!=null) {


            final double mPos = (double)previousPosition+1;
            double percentage = (mPos/((double)size))*100;

            for (Option option:question.getOptions()) {
                if(option.isCorrect()){
                    if(selectedOption ==option.getValue()){
                        correctAnswers+=1.0;
                    }

                }

            }
            int mPercentage = (int) (percentage);
            //Toast.makeText(this, "correctAnswers:"+correctAnswers, Toast.LENGTH_SHORT).show();

            seekBar.post(() -> seekBar.setProgress(mPercentage));

            if(previousPosition!=size-1){
                lottieAnimationViewSuccessConfetti.setVisibility(View.GONE);
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration(1000);
                //alphaAnimation.setRepeatCount(1);
                alphaAnimation.setRepeatMode(Animation.REVERSE);
                recyclerView.startAnimation(alphaAnimation);
                alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                        recyclerView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.getLayoutManager().scrollToPosition(previousPosition + 1);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }

            else {
                String result = ((int)correctAnswers)+"/"+size;
                textViewScore.setText(result);

                double mOverAllPercentage = (correctAnswers/((double)size))*100;
                viewFlipper.setDisplayedChild(1);

                if(mOverAllPercentage>60) {
                  //  lottieAnimationViewSuccessConfetti.setAnimation(R.raw.confetti);
                    lottieAnimationViewSuccessConfetti.setVisibility(View.VISIBLE);
                    lottieAnimationViewSuccessConfetti.setRepeatCount(2);
                    lottieAnimationViewSuccessConfetti.playAnimation();

                    imageViewScoreStar.setVisibility(View.VISIBLE);
                    viewFlipperResult.setDisplayedChild(0);
                    Glide.with(QuizActivity.this).load(R.drawable.success_ico).into(imageViewResult);
                    textViewScoreMessage.setText(R.string.congrats);
                    String plusAddScore = "+"+(int)correctAnswers;
                    textViewPlusAddedScore.setText(plusAddScore);
                }
                else {
                    //lottieAnimationViewSuccessConfetti.setAnimation(R.raw.sad);
                  //  lottieAnimationViewSuccessConfetti.setVisibility(View.VISIBLE);
                  //  lottieAnimationViewSuccessConfetti.setRepeatCount(2);
                  //  lottieAnimationViewSuccessConfetti.playAnimation();
                    imageViewScoreStar.setVisibility(View.GONE);
                    Glide.with(QuizActivity.this).load(R.drawable.neg_ico).into(imageViewResult);
                    viewFlipperResult.setDisplayedChild(1);
                    textViewScoreMessage.setText(R.string.too_bad);
                }
            }


        }
        else Toast.makeText(this, "null called on getLayoutManager", Toast.LENGTH_SHORT).show();
*/


        //setAnimation(recyclerView,0, getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}