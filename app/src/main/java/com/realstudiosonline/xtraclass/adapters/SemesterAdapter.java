package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Semester;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SemesterAdapter extends RecyclerView.Adapter<SemesterAdapter.SemesterViewHolder> {

    private List<Semester> semesters;
    private Context mContext;
    private setOnSemesterLister lister;

    public SemesterAdapter(List<Semester> semesters, Context mContext, setOnSemesterLister lister) {
        this.semesters = semesters;
        this.mContext = mContext;
        this.lister = lister;
    }

    @NonNull
    @NotNull
    @Override
    public SemesterViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new SemesterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_semester, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull SemesterViewHolder holder, int position) {

        holder.bind(semesters.get(position), lister);
    }

    @Override
    public int getItemCount() {
        return semesters!=null ? semesters.size() : 0;
    }

    public static class SemesterViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView textViewSemesterName;
        CardView cardViewSemester;
        public SemesterViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textViewSemesterName = itemView.findViewById(R.id.tv_semester_name);
            cardViewSemester = itemView.findViewById(R.id.view_semester);
        }

        public void bind(Semester semester, setOnSemesterLister lister) {
            cardViewSemester.setOnClickListener(v -> lister.onSemesterClick(semester));
            textViewSemesterName.setText(semester.getName());
        }
    }

    public interface setOnSemesterLister{
        void onSemesterClick(Semester semester);
    }
}
