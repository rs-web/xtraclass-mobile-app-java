package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class InstitutionResponse extends DataResponse{
    private List<Institution> data;

    public List<Institution> getData() {
        return data;
    }

    public void setData(List<Institution> data) {

        this.data = data;
    }

}

/*
*
* {
    "statusCode": 200,
    "message": "",
    "data": [
        {
            "id": "a8889d0b-46d5-490c-96b4-64288fe0ecd3",
            "name": "Basic school"
        },
        {
            "id": "246db292-0af8-49d8-94ad-77f1aa61f338",
            "name": "JHS"
        },
        {
            "id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528",
            "name": "Primary"
        },
        {
            "id": "430583d0-f296-4b72-940c-7e81cb405ebf",
            "name": "Professional(Take a Course)"
        },
        {
            "id": "6a0b7d15-36d6-4bc5-82e5-27aa7873d2f7",
            "name": "Senior High - TVET"
        },
        {
            "id": "1944075d-f962-4a54-96c5-6db543763dbb",
            "name": "SHS"
        },
        {
            "id": "70dbc3a1-8188-4450-a1c4-b48a24423b31",
            "name": "SHS - TVET"
        }
    ]
}*/