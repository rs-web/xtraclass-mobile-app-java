package com.realstudiosonline.xtraclass.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.activities.ChangeClassActivity;
import com.realstudiosonline.xtraclass.activities.QuizActivity;
import com.realstudiosonline.xtraclass.adapters.PlayListAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.ClassDetailsResponse;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.Lesson;
import com.realstudiosonline.xtraclass.models.LessonResponse;
import com.realstudiosonline.xtraclass.models.PlaybackPosition;
import com.realstudiosonline.xtraclass.models.PostHistoryResponse;
import com.realstudiosonline.xtraclass.models.Question;
import com.realstudiosonline.xtraclass.models.QuizDataResponse;
import com.realstudiosonline.xtraclass.models.SearchItem;
import com.realstudiosonline.xtraclass.models.SelectedVideo;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

import static com.realstudiosonline.xtraclass.fragments.HomeScreenFragment.view_anchor;
import static com.realstudiosonline.xtraclass.fragments.OpenSchoolFragment.view_anchor_open;
import static com.realstudiosonline.xtraclass.utils.Constants.MEDIA_HUB;

public class PlayListFragment extends Fragment implements PlayListAdapter.setOnLessonClickListener{

    ShimmerRecyclerView recyclerView;
    public static PlayListAdapter playListAdapter;
    List<Lesson> lessonList;
    private ClassesForCourse selectedClassesForCourse;
    String mToken ="";
    private User mUser;
    PopupWindow popupWindowTakeTest;
    ShimmerFrameLayout shimmer;
    private int schoolType =0;


    public PlayListFragment() {
        // Required empty public constructor
    }


    public static PlayListFragment newInstance() {
        return new PlayListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_play_list, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recycler_view_play_list);

        shimmer = view.findViewById(R.id.shimmer_view_container);

        shimmer.startShimmer();

        recyclerView.showShimmerAdapter();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        lessonList = new ArrayList<>();
        Collections.sort(lessonList, (lesson1, lesson2) -> lesson1.getNext_video_weight() - lesson2.getNext_video_weight());

        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getActivity());

        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getActivity());

        schoolType = PreferenceHelper.getPreferenceHelperInstance().getSchoolType(getContext());

        playListAdapter = new PlayListAdapter(lessonList, getActivity(),this);
        recyclerView.setAdapter(playListAdapter);

        selectedClassesForCourse = PreferenceHelper.getPreferenceHelperInstance().getSelectedClassesForCourse(getActivity());


        new Handler(Looper.getMainLooper()).postDelayed(()->{

            if(schoolType == 1){
                List<Lesson> lessons = PreferenceHelper.getPreferenceHelperInstance().getLessons(getActivity());
                Log.d("PLAYLIST", "lesson"+ lessons.toString());
                Collections.sort(lessons, (lesson1, lesson2) -> lesson1.getNext_video_weight() - lesson2.getNext_video_weight());
                lessonList.clear();
                Lesson selectedLesson = PreferenceHelper.getPreferenceHelperInstance().getSelectedLesson(getActivity());
                if(lessons.size()>0) {
                    for (int i = 0; i < lessons.size(); i++) {
                        if(selectedLesson!=null){
                            Log.d("LESSON", "selectedLesson:"+ selectedLesson.toString());
                            if(selectedLesson.getLesson_id().equalsIgnoreCase(lessons.get(i).getLesson_id())){
                                lessons.get(i).setSelected(true);
                            }
                            else {
                                lessons.get(i).setSelected(false);
                            }
                        }
                        else {
                            lessons.get(i).setSelected(i == 0);
                        }

                        lessons.get(i).setHas_quiz(true);
                        Log.d("@@@@@", "else has no quiz");
                        Lesson lesson = lessons.get(i);
                        lesson.setLessonType(0);
                        lesson.setId(i);
                        List<Question> questions = new ArrayList<>();
                        List<String> options = new ArrayList<>();
                        options.add("true");
                        options.add("false");

                        List<String> options2 = new ArrayList<>();
                        options2.add("Elephant");
                        options2.add("Lion");
                        options2.add("Zebra");
                        options2.add("Giraffe");
                        Question question = new Question("True or false sample?", "true-false",options);
                        question.setAnswer_id(1);
                        Question question1 = new Question("True or false sample 2", "true-false",options);
                        question1.setAnswer_id(2);
                        Question question2 =  new Question("What best describes the above image?", "image-as-question", options2);
                        question2.setAnswer_id(1);
                        question2.setImageUrl("https://a-z-animals.com/media/2021/05/How-Long-Do-Elephants-Live-header.jpg");
                        questions.add(question);
                        questions.add(question1);
                        questions.add(question2);
                        lesson.setQuestions(questions);
                        lessonList.add(lesson);
                                    /*if(lessons.get(i).isHas_quiz()){
                                        Lesson lesson = lessons.get(i);
                                        lesson.setLessonType(0);
                                        lesson.setId(i);
                                        lessonList.add(lesson);
                                        Lesson lesson1 = lessons.get(i);
                                        lesson1.setId(i);
                                        lesson1.setLessonType(1);
                                        lessonList.add(lesson1);
                                        Log.d("@@@@@", "if has quiz");
                                    }
                                    else{
                                        Log.d("@@@@@", "else has no quiz");
                                        Lesson lesson = lessons.get(i);
                                        lesson.setLessonType(0);
                                        lesson.setId(i);
                                        lessonList.add(lesson);
                                    }*/

                    }

                    for (Lesson lesson: lessons) {

                        Log.d("PLAYLISTLessonDetails: ", "Lesson Period Title "+lesson.getPeriod_title());
                    }


                    PreferenceHelper.getPreferenceHelperInstance().setLessons(getActivity(), lessonList);

                    //   Collections.sort(lessonList, (lesson1, lesson2) -> lesson1.getPeriod_title().compareToIgnoreCase(lesson2.getPeriod_title()));
                    playListAdapter.notifyDataSetChanged();

                    EventBus.getDefault().post(lessonList);

                    new Handler(Looper.getMainLooper()).postDelayed(() -> {

                        if(selectedLesson!=null){
                            EventBus.getDefault().post(selectedLesson);
                        }
                        else {
                            if (lessonList.size() > 0)
                                EventBus.getDefault().post(lessonList.get(0));

                        }

                    }, 100);

                }

                else {

                    if(selectedClassesForCourse!=null) {
                        fetchClassDetails(selectedClassesForCourse.getUuid(), getActivity());
                        Log.d("selectedClassesFor", "selectedClassesForCourse:"+selectedClassesForCourse.toString());
                    }

                    else {
                        Log.d("mUser.getClass_id()", "mUser.getClass_id():"+mUser.getClass_id());
                        if(schoolType == 1){
                            fetchClassDetails(mUser.getClass_id(), getActivity());
                        }
                    }

                }

            }
            else {
                if(selectedClassesForCourse!=null) {
                    if(getActivity()!=null) {
                        ClassDetails classDetails = PreferenceHelper.getPreferenceHelperInstance().getClassDetails(getActivity());
                        if(classDetails!=null){

                            List<Lesson> lessons = PreferenceHelper.getPreferenceHelperInstance().getLessons(getActivity());
                            Log.d("PLAYLIST", "lessom"+ lessons.toString());
                            Collections.sort(lessons, (lesson1, lesson2) -> lesson1.getNext_video_weight() - lesson2.getNext_video_weight());
                            lessonList.clear();
                            Lesson selectedLesson = PreferenceHelper.getPreferenceHelperInstance().getSelectedLesson(getActivity());


                            if(lessons.size()>0) {
                                for (int i = 0; i < lessons.size(); i++) {
                                    if(selectedLesson!=null){
                                        Log.d("LESSON", "selectedLesson:"+ selectedLesson.toString());
                                        if(selectedLesson.getLesson_id().equalsIgnoreCase(lessons.get(i).getLesson_id())){
                                            lessons.get(i).setSelected(true);
                                        }
                                        else {
                                            lessons.get(i).setSelected(false);
                                        }
                                    }
                                    else {
                                        lessons.get(i).setSelected(i == 0);
                                    }

                                    lessons.get(i).setHas_quiz(i%2==0);
                                    Log.d("@@@@@", "else has no quiz");
                                    Lesson lesson = lessons.get(i);
                                    lesson.setLessonType(0);
                                    lesson.setId(i);
                                    List<Question> questions = new ArrayList<>();
                                    List<String> options = new ArrayList<>();
                                    options.add("true");
                                    options.add("false");
                                    Question question = new Question("What is this?", "true-false",options);
                                    Question question1 = new Question("What the heck is this?", "true-false",options);
                                    questions.add(question);
                                    questions.add(question1);
                                    lesson.setQuestions(questions);
                                    lessonList.add(lesson);
                                    /*if(lessons.get(i).isHas_quiz()){
                                        Lesson lesson = lessons.get(i);
                                        lesson.setLessonType(0);
                                        lesson.setId(i);
                                        lessonList.add(lesson);
                                        Lesson lesson1 = lessons.get(i);
                                        lesson1.setId(i);
                                        lesson1.setLessonType(1);
                                        lessonList.add(lesson1);
                                        Log.d("@@@@@", "if has quiz");
                                    }
                                    else{
                                        Log.d("@@@@@", "else has no quiz");
                                        Lesson lesson = lessons.get(i);
                                        lesson.setLessonType(0);
                                        lesson.setId(i);
                                        lessonList.add(lesson);
                                    }*/

                                }

                                for (Lesson lesson: lessons) {

                                    Log.d("PLAYLISTLessonDetails: ", "Lesson Period Title "+lesson.getPeriod_title());
                                }


                                PreferenceHelper.getPreferenceHelperInstance().setLessons(getActivity(), lessonList);

                                //   Collections.sort(lessonList, (lesson1, lesson2) -> lesson1.getPeriod_title().compareToIgnoreCase(lesson2.getPeriod_title()));
                                playListAdapter.notifyDataSetChanged();

                                EventBus.getDefault().post(lessonList);

                                new Handler(Looper.getMainLooper()).postDelayed(() -> {

                                    if(selectedLesson!=null){
                                        EventBus.getDefault().post(selectedLesson);
                                    }
                                    else {
                                        if (lessonList.size() > 0)
                                            EventBus.getDefault().post(lessonList.get(0));

                                    }

                                }, 100);

                            }

                            else {
                                fetchClassDetails(selectedClassesForCourse.getUuid(), getActivity());

                            }                    //Log.d("VIMEO", " " +lessonList.toString());
                        }
                        else
                            fetchClassDetails(selectedClassesForCourse.getUuid(), getActivity());
                    }
                }
                else {
                    if(getActivity()!=null) {
                         startActivity(new Intent(getActivity(), ChangeClassActivity.class));
                         getActivity().finish();
                    }
                }
            }

            shimmer.hideShimmer();
        },200);
    }





    public static void filterPlaylist(SearchItem query){
        playListAdapter.getFilter().filter(query.getQuery());
    }

    private void fetchClassDetails(String selectedClassesForCourse_uuid, Activity activity){
        shimmer.startShimmer();
        lessonList.clear();
        ApiClient.getApiClient().getInstance(getContext()).fetchClassDetails(selectedClassesForCourse_uuid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassDetailsResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<ClassDetailsResponse> classDetailsResponseResponse) {

                shimmer.hideShimmer();
                if(classDetailsResponseResponse.isSuccessful()){
                    ClassDetailsResponse classDetailsResponse = classDetailsResponseResponse.body();
                    // Toast.makeText(activity, "fetchClassDetails", Toast.LENGTH_SHORT).show();
                    Log.d("fetchClassDetails", ""+classDetailsResponse.toString());

                    if(classDetailsResponse.getData()!=null) {
                        if(classDetailsResponse.getData().isLocked()){
                            Toast.makeText(getActivity(), "you are trying to access a locked class", Toast.LENGTH_LONG).show();
                        }

                        else {
                            EventBus.getDefault().post(classDetailsResponse.getData());
                            PreferenceHelper.getPreferenceHelperInstance().setClassDetails(getActivity(), classDetailsResponse.getData());

                            if(classDetailsResponse.getData().getVideo()!=null){
                                if(classDetailsResponse.getData().getVideo().size()>0) {

                                    String playOn = classDetailsResponse.getData().getVideo().get(0).getPlay_on();
                                    PreferenceHelper.getPreferenceHelperInstance().setClassVideoPlayOn(getContext(),playOn);

                                    boolean playPrevious=PreferenceHelper.getPreferenceHelperInstance().getPlayPrevious(getActivity());
                                    String classVideoPlayOn=PreferenceHelper.getPreferenceHelperInstance().getClassVideoPlayOn(getActivity());
                                    String lessonQueryDate=PreferenceHelper.getPreferenceHelperInstance().getLessonQueryDate(getActivity());
                                    if(selectedClassesForCourse!=null) {

                                        fetchLessons(mUser.getUuid(), selectedClassesForCourse.getUuid(), playPrevious?lessonQueryDate:classVideoPlayOn);
                                    }
                                    else {
                                        fetchLessons(mUser.getUuid(),mUser.getClass_id(),playPrevious?lessonQueryDate:classVideoPlayOn);

                                    }
                                }
                                else {
                                    Toast.makeText(activity, "No lessons available for this course", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else {
                                Toast.makeText(activity, "No lessons available for this course", Toast.LENGTH_SHORT).show();

                                if(getActivity()!=null) {
                                    //startActivity(new Intent(getActivity(), ChangeClassActivity.class));
                                    //getActivity().finish();
                                }
                            }
                        }
                    }
                    else{
                        Toast.makeText(activity, "No lessons available for this course", Toast.LENGTH_SHORT).show();

                    }
                }

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                shimmer.hideShimmer();
                e.printStackTrace();
            }
        });
    }




    private void fetchLessons(String userUUID, String classId, String query){
        shimmer.startShimmer();
        //String userUUID  ="c030722a-f00a-4c08-9fe9-75838ecacaf6";
        ApiClient.getApiClient().getInstance(getContext()).fetchLessons(classId,userUUID,query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<LessonResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<LessonResponse> lessonResponseResponse) {
                //Log.d("fetchLessons", ""+lessonResponseResponse.body());
                shimmer.hideShimmer();
                if(lessonResponseResponse.isSuccessful()){
                    if(lessonResponseResponse.body()!=null) {


                        try {
                            if(lessonResponseResponse.body().getData().size()>0) {
                                Lesson firstLesson = lessonResponseResponse.body().getData().get(0);
                                firstLesson.setId(0);
                                EventBus.getDefault().post(firstLesson);
                                PreferenceHelper.getPreferenceHelperInstance().setSelectedLesson(getActivity(),firstLesson);
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                        lessonList.clear();

                        Collections.sort(lessonResponseResponse.body().getData(), (lesson1, lesson2) -> lesson1.getNext_video_weight() -
                                lesson2.getNext_video_weight());
                        lessonList.addAll(lessonResponseResponse.body().getData());

                        for (Lesson lesson : lessonResponseResponse.body().getData()) {
                            Log.d("fetchLessons: ", lesson.toString());
                        }

                        PreferenceHelper.getPreferenceHelperInstance().setLessons(getActivity(), lessonResponseResponse.body().getData());
                        playListAdapter.notifyDataSetChanged();
                        if(lessonList!=null)
                        EventBus.getDefault().post(lessonList);
                    }

                }


            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                shimmer.hideShimmer();
                e.printStackTrace();
            }
        });

    }


    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVideoNextPreviousSelected(SelectedVideo video){
       // Toast.makeText(getActivity(), "onVideoNextPreviousSelected:"+video.getId(), Toast.LENGTH_SHORT).show();
        for (Lesson lesson: lessonList) {
            if(lesson.getId() ==video.getId()){
                PreferenceHelper.getPreferenceHelperInstance().setSelectedLesson(getActivity(), lesson);
                PlaybackPosition playbackPosition = new PlaybackPosition(lesson.getLesson_id(), MEDIA_HUB+""+lesson.getVideo_info().getHls_info().getMaster_file(),0L);
                _addLessonToHistory(lesson.getLesson_id(),mToken);
                PreferenceHelper.getPreferenceHelperInstance().setPlayBackPosition(getActivity(), playbackPosition);
                lesson.setSelected(true);
            }
            else lesson.setSelected(false);
        }



        playListAdapter.notifyDataSetChanged();

    }


    public  void _addLessonToHistory(String lessonID, String token){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .postHistory(lessonID, "Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<PostHistoryResponse>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<PostHistoryResponse> historyResponse) {
                        if(historyResponse.isSuccessful()) {
                            Log.d("_addLessonToHistory", "onSuccess=>" +historyResponse.toString());
                           // Toast.makeText(getActivity(), "History => "+historyResponse.body(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Log.d("_addLessonToHistory", "onError=>" +e.getMessage());
                        e.printStackTrace();
                        if(getActivity()!=null) {
                            Toast.makeText(getActivity(), "Error occurred in adding history", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    @Override
    public void onLessonSelected(Lesson lesson) {

        try {
            PlaybackPosition playbackPosition = new PlaybackPosition(lesson.getLesson_id(), MEDIA_HUB+""+lesson.getVideo_info().getHls_info().getMaster_file(),0L);
            PreferenceHelper.getPreferenceHelperInstance().setPlayBackPosition(getActivity(),playbackPosition );
        }
        catch (Exception e){
            e.printStackTrace();

        }
        for (int i=0; i<lessonList.size();i++) {

            lessonList.get(i).setSelected(lessonList.get(i).getLesson_id().equals(lesson.getLesson_id()));
        }
        _addLessonToHistory(lesson.getLesson_id(),mToken);
        playListAdapter.notifyDataSetChanged();
        EventBus.getDefault().post(lesson);
       //Toast.makeText(getActivity(), "getMaster_file "+lesson.getVideo_info().getHls_info().getMaster_file(), Toast.LENGTH_SHORT).show();
    }


    private void provideTakeTestPopupWindow(View it,Context context,List<Question> questions ) {
        if(popupWindowTakeTest !=null){
            popupWindowTakeTest.dismiss();
            popupWindowTakeTest = null;
        }
        popupWindowTakeTest = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowTakeTest.setBackgroundDrawable(backgroundDrawable);
        popupWindowTakeTest.setOutsideTouchable(true);
        popupWindowTakeTest.setFocusable(true);
        //popupWindowTakeTest.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alert_view = (View) layoutInflater.inflate(R.layout.register_dialog, null, false);

        AppCompatTextView btnTakeTest = alert_view.findViewById(R.id.btn_confirm);
        AppCompatTextView tvTakeTest = alert_view.findViewById(R.id.tv_register_message);
        AppCompatImageButton btnClose = alert_view.findViewById(R.id.btn_close);


        if(btnClose!=null){
            btnClose.setOnClickListener(v ->  popupWindowTakeTest.dismiss());
        }
        if(tvTakeTest!=null){
            tvTakeTest.setText(R.string.take_test_message);
        }
        if(btnTakeTest!=null){
            btnTakeTest.setText(R.string.take_test);
            btnTakeTest.setOnClickListener(v -> {
                if(popupWindowTakeTest!=null)
                popupWindowTakeTest.dismiss();
              //  Toast.makeText(getActivity(), "Quiz length is => "+questions.size(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), QuizActivity.class);
                QuizDataResponse quizDataResponse = new QuizDataResponse();
                quizDataResponse.setData(questions);
                intent.putExtra("quiz_data", quizDataResponse);
                startActivity(intent);
            });
        }

        popupWindowTakeTest.setContentView(alert_view);

        if(popupWindowTakeTest !=null) {

            popupWindowTakeTest.showAsDropDown(it, 0, 0);
        }

    }






    @Override
    public void onQuizSelected(List<Question> questions) {
        if (schoolType == 1) {
            provideTakeTestPopupWindow(view_anchor,getActivity(),questions);
        }
        else {

            provideTakeTestPopupWindow(view_anchor_open, getActivity(), questions);
        }


    }
}