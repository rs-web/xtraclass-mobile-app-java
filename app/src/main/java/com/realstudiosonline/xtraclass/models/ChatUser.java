package com.realstudiosonline.xtraclass.models;

public class ChatUser {
    private int id;
    private String username;
    private String email;
    private String name;
    private int status;
    private String uuid;
    private String device_type;
    private String device_token;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    /* "id": 2317,
             "username": "kan21",
             "google_id": null,
             "name": null,
             "email": "bzzb21@gmail.com",
             "email_verified_at": null,
             "status": 1,
             "token": null,
             "mobile_verified_at": "2020-11-05 17:21:46",
             "device_token": "1612187458420",
             "device_type": "ANDROID",
             "deleted_at": null,
             "created_at": "2020-11-05 17:21:25",
             "updated_at": "2021-02-01 13:51:02",
             "uuid": "d329773c-39ff-4bcf-b49b-dbd3ad9468fc",
             "provider_id": null,
             "provider": null*/
}
