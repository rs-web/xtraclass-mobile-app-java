package com.realstudiosonline.xtraclass.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.OTPRequest;
import com.realstudiosonline.xtraclass.models.OTPResponse;
import com.realstudiosonline.xtraclass.models.VerifyOTPRequest;
import com.realstudiosonline.xtraclass.models.VerifyOTPResponse;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

public class ConfirmOTPActivity extends AppCompatActivity {

    SmsVerifyCatcher smsVerifyCatcher;
    TextInputEditText editTextOTP;
    AppCompatTextView textViewResendOTP, tvIfo, tvOTPHint;
    String phone_number;
    String country_code;
    String user_id;
    TextInputLayout inputLayoutOTP;
    AlertDialog alertDialog, alertDialogExit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_o_t_p);


        phone_number = PreferenceHelper.getPreferenceHelperInstance().getPhone(getApplicationContext());
        country_code = PreferenceHelper.getPreferenceHelperInstance().getCountryCode(getApplicationContext());
        //phone_number = getIntent().getStringExtra(PreferenceHelper.PHONE);

        //country_code = getIntent().getStringExtra(PreferenceHelper.COUNTRY_CODE);

        tvOTPHint = findViewById(R.id.tv_enter_confirmation_code);

        inputLayoutOTP = findViewById(R.id.view_otp_input_layout);
        user_id = getIntent().getStringExtra(PreferenceHelper.USER_ID);

        editTextOTP = findViewById(R.id.edt_otp);

        tvIfo = findViewById(R.id.tv_info);

        String activationText = "Activate your account with\n code  sent to "+country_code+""+phone_number;
        tvIfo.setText(activationText);

        textViewResendOTP = findViewById(R.id.tv_resend_otp);

        textViewResendOTP.setOnClickListener(v -> sendOtpAgain(phone_number, country_code, user_id));

        editTextOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.toString().trim().length()>0){
                    inputLayoutOTP.setErrorEnabled(false);

                }
                tvOTPHint.setText(R.string.enter_confirmation_code_nor_wait_for_auto_confirmation_sms);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.btn_confirm).setOnClickListener(v -> {

            if(editTextOTP.getText()!=null) {
                try {
                    String p = phone_number;
                    //String phone  = p.replace(p.substring(0,1),"");
                    String code = editTextOTP.getText().toString();
                    if(!TextUtils.isEmpty(code))
                    verifyOTP(code, user_id, phone_number);
                    else {
                        inputLayoutOTP.setBoxStrokeErrorColor(ColorStateList.valueOf(getResources().getColor(R.color.colorActivationFailed)));
                        inputLayoutOTP.setErrorEnabled(true);
                        inputLayoutOTP.setHintTextAppearance(R.style.TextInputLayoutStyle);
                        inputLayoutOTP.setHintTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorActivationFailed)));
                        tvOTPHint.setText(R.string.activation_failed);
                        //Activation code is not correct Please enter the code again
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        findViewById(R.id.btn_change_phone_number).setOnClickListener(v -> {
            startActivity(new Intent(ConfirmOTPActivity.this, AuthActivity.class));
            finish();
        });

        smsVerifyCatcher = new SmsVerifyCatcher(this, message -> {
            String code = parseCode(message);//Parse verification code
            editTextOTP.setText(code);//set code in edit text

            String p = phone_number;

            //String phone  = p.replace(p.substring(0,1),"");

            verifyOTP(code,user_id,phone_number);
            //then you can send verification code to server
        });
    }


    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(ConfirmOTPActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }


    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        cancelDialogProgressDialog();
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        super.onDestroy();
    }

    public void hideDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.hide();
        }
    }

    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }

    private void sendOtpAgain(String phoneNo, String country_code, String userId){
        showDialogProgressDialog();
        ApiClient.getApiClient().getInstance(getApplicationContext()).sendOTP(new OTPRequest(userId, phoneNo, country_code, "send-otp"))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<OTPResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<OTPResponse> otpResponseResponse) {
                hideDialogProgressDialog();
                if(otpResponseResponse.isSuccessful()) {
                  OTPResponse otpResponse =   otpResponseResponse.body();
                    if (otpResponse != null) {

                        Toast.makeText(ConfirmOTPActivity.this, "OTP has been sent to " + phoneNo, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ConfirmOTPActivity.this, "An error occured. Please tried again", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ConfirmOTPActivity.this, "Error occurred. Please try again", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                hideDialogProgressDialog();
                Toast.makeText(ConfirmOTPActivity.this, "Error occurred. Please try again", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
    }
    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        Log.d("ConfirmOTPActivity", "OTP is "+ code);
        return code;
    }

    private void verifyOTP(String opt, String user_id, String mobile){
        showDialogProgressDialog();
        ApiClient.getApiClient().getInstance(getApplicationContext())
                .verifyOTP(new VerifyOTPRequest(user_id, "verify-otp", opt, mobile))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<VerifyOTPResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<VerifyOTPResponse> verifyOTPResponseResponse) {
                hideDialogProgressDialog();

                if(verifyOTPResponseResponse.isSuccessful()) {
                    VerifyOTPResponse verifyOTPResponse = verifyOTPResponseResponse.body();

                    Toast.makeText(ConfirmOTPActivity.this, "" + verifyOTPResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    if (verifyOTPResponse.isStatus()) {
                        PreferenceHelper.getPreferenceHelperInstance().setOtpVerified(getApplicationContext(),true);
                        startActivity(new Intent(ConfirmOTPActivity.this, ActivationSuccessful.class));
                        finish();
                    }
                    else {
                    }

                }
                else {
                    Toast.makeText(ConfirmOTPActivity.this, "Request not successful. Try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                hideDialogProgressDialog();
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}