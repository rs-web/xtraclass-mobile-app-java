package com.realstudiosonline.xtraclass.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.utils.widget.ImageFilterView;

import android.os.Bundle;
import android.view.View;

import com.realstudiosonline.xtraclass.R;

public class MotionLayoutActivity extends AppCompatActivity {

    MotionLayout motion_layout;
    View iv_home,iv_search, iv_like, iv_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion_layout);

        motion_layout = findViewById(R.id.motion_layout);

        iv_home = findViewById(R.id.iv_home);
        iv_search = findViewById(R.id.iv_search);
        iv_like = findViewById(R.id.iv_like);
        iv_profile = findViewById(R.id.iv_profile);

        setClickListener();
    }

    private void setClickListener() {
        // Apply default transition on app launch
        motion_layout.setTransition(motion_layout.getCurrentState(), R.id.home_expand);
        motion_layout.transitionToEnd();

        iv_home.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.home_expand));

        iv_search.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.search_expand));

        iv_like.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.like_expand));

        iv_profile.setOnClickListener(v -> setTransition(motion_layout.getCurrentState(), R.id.profile_expand));
    }

    private void setTransition(int startState,int endState) {
        motion_layout.setTransition(startState, endState);
        motion_layout.setTransitionDuration(200);
        motion_layout.transitionToEnd();
    }
}