package com.realstudiosonline.xtraclass.utils.onboarding.listeners;

public interface PaperOnboardingOnChangeListener {

    void onPageChanged(int oldElementIndex, int newElementIndex);

}
