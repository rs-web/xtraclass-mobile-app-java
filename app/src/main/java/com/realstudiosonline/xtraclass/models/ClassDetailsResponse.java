package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class ClassDetailsResponse extends DataResponse {
    private ClassDetails data;

    public ClassDetails getData() {
        return data;
    }

    public void setData(ClassDetails data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassDetailsResponse{" +
                "data=" + data +
                '}';
    }
}
