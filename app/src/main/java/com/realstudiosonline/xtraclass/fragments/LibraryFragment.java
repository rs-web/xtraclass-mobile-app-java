package com.realstudiosonline.xtraclass.fragments;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.airbnb.lottie.LottieAnimationView;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.activities.PastQuestionActivity;
import com.realstudiosonline.xtraclass.adapters.LibraryAdapter;
import com.realstudiosonline.xtraclass.adapters.LibraryItemAdapter;
import com.realstudiosonline.xtraclass.adapters.PastQuestionsAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.AudioItem;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.Enclosure;
import com.realstudiosonline.xtraclass.models.ItemImage;
import com.realstudiosonline.xtraclass.models.Lesson;
import com.realstudiosonline.xtraclass.models.LibrariesEventBus;
import com.realstudiosonline.xtraclass.models.Library;
import com.realstudiosonline.xtraclass.models.LibraryItemResponse;
import com.realstudiosonline.xtraclass.models.LibraryMenu;
import com.realstudiosonline.xtraclass.models.PlaybackPosition;
import com.realstudiosonline.xtraclass.models.QuizDataResponse;
import com.realstudiosonline.xtraclass.models.QuizResponse;
import com.realstudiosonline.xtraclass.models.QuizSubject;
import com.realstudiosonline.xtraclass.models.ResetLibrary;
import com.realstudiosonline.xtraclass.models.SelectedLibrary;
import com.realstudiosonline.xtraclass.utils.Constants;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;
import com.realstudiosonline.xtraclass.utils.player.AudioPlayerService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import im.delight.android.webview.AdvancedWebView;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

import static com.realstudiosonline.xtraclass.XtraClassApp.getFirebaseAnalytics;
import static com.realstudiosonline.xtraclass.utils.Constants.ACTION_RELEASE_OLD_PLAYER;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_ITEM;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_PODCAST_IMAGE;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_RESULT_ID;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_RESULT_NAME;
import static com.realstudiosonline.xtraclass.utils.Constants.FORMAT_ELAPSED_TIME;
import static com.realstudiosonline.xtraclass.utils.Constants.MEDIA_HUB;
import static com.realstudiosonline.xtraclass.utils.Constants.PROGRESS_UPDATE_INITIAL_INTERVAL;
import static com.realstudiosonline.xtraclass.utils.Constants.PROGRESS_UPDATE_INTERVAL;
import static com.realstudiosonline.xtraclass.utils.Constants.TYPE_AUDIO;

public class LibraryFragment extends Fragment implements LibraryAdapter.onLibraryItemSelectedListener,
        LibraryItemAdapter.setOnLibraryItemCLickListener{

    private static final String TAG = LibraryFragment.class.getSimpleName();
    RecyclerView recyclerView;
    LibraryAdapter libraryAdapter;
    private List<LibraryMenu> libraryItems;
    private List<QuizSubject> quizSubjects;
    private List<Library> libraries,dynamicLibrary;
    private AlertDialog alertDialogPastQuestions;
    ClassesForCourse classesForCourse;
    String mToken;
    Lesson selectedLesson;
    ViewFlipper viewFlipperLibrary, viewFlipperLibraryItems;
    FloatingActionButton floatingActionButtonCloseLibraryItems;
    PDFView pdfView;
    View  fragmentLibrary;
    private AudioItem mItem;
    private String mPodcastImage;
    private String mItemImageUrl;
    private String mPodcastName;
    private String mPodcastId;
    private String mEnclosureUrl;
    private MediaBrowserCompat mMediaBrowser;
    private PlaybackStateCompat mLastPlaybackState;
    private final ScheduledExecutorService mExecutorService =
            Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> mScheduledFuture;
    private final Handler mHandler = new Handler();
    private final Runnable mUpdateProgressTask = this::updateProgress;
    InputStream input = null;
    AudioManager mAudioManager;
    ShimmerFrameLayout lottie_shimmer;
    public LibraryFragment() {
        // Required empty public constructor
    }



    ShimmerRecyclerView recyclerViewLibraryItems;
    LibraryItemAdapter libraryItemAdapter;


    private void initLibraries(Activity activity){

        LinearLayoutManager layoutManager =  new LinearLayoutManager(getActivity());
        libraryItemAdapter = new LibraryItemAdapter(dynamicLibrary,getActivity(), this);
 /*
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewLibraryItems.getContext(),
                layoutManager.getOrientation());
        */
        recyclerViewLibraryItems.setLayoutManager(layoutManager);
        //recyclerViewLibraryItems.addItemDecoration(dividerItemDecoration);
        recyclerViewLibraryItems.setAdapter(libraryItemAdapter);
        libraryItemAdapter.notifyDataSetChanged();
    }





    public static LibraryFragment newInstance() {
        return new LibraryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    private void  getLibraryItems(String subject_id, String class_id, String token){

        ApiClient.getApiClient().getInstance(getContext())
                .getLibraryItems(subject_id,class_id,"Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<LibraryItemResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull
                                          Response<LibraryItemResponse> libraryItemResponseResponse) {

                if(libraryItemResponseResponse.isSuccessful()) {
                    libraries.clear();
                    libraries.addAll(libraryItemResponseResponse.body().getData());
                    if(libraryItemResponseResponse.body().getData().size()>0)
                    //PreferenceHelper.getPreferenceHelperInstance().setLibraryItems(getActivity(),libraryItemResponseResponse.body().getData());

                    Log.d("getLibraryItems", "isSuccessful: "+libraryItemResponseResponse.body().toString());
                }
                else {
                    Log.d("getLibraryItems","!isSuccessful: "+libraryItemResponseResponse.message());
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.d("getLibraryItems", "onError: "+e.getMessage());
            }
        });
    }


    private void getPastQuestionsBySubjectID(String subjectID,String token){
        Log.d("QuestionsBySubjectID", "subjectID:"+ subjectID);
        ApiClient.getApiClient().getInstance(getContext())
                .getPastQuestionsBySubjectID(subjectID,"Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<QuizResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<QuizResponse> quizResponseResponse) {

                if(quizResponseResponse.isSuccessful()){
                    quizSubjects.clear();
                    if(quizResponseResponse.body()!=null) {
                        Collections.sort(quizResponseResponse.body().getData(), (o1, o2) -> o1.getYear() - o2.getYear());
                        quizSubjects = quizResponseResponse.body().getData();
                        Log.d(TAG, "" + quizResponseResponse.body().toString());
                    }
                }
                else {
                    Log.d(TAG, "MESSAGE:"+ quizResponseResponse.message());
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                e.printStackTrace();
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_library, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pdfView = view.findViewById(R.id.pdfView);

        lottie_shimmer = view.findViewById(R.id.shimmer_view_container);

        floatingActionButtonCloseLibraryItems =  view.findViewById(R.id.fab_close_library_items);

        floatingActionButtonCloseLibraryItems.setOnClickListener(v -> {
            if(viewFlipperLibraryItems.getDisplayedChild() == 1){
                viewFlipperLibraryItems.setDisplayedChild(0);
                for (Library library:dynamicLibrary) {
                    library.setSelected(false);
                }
                libraryItemAdapter.notifyDataSetChanged();
            }

            else {
                floatingActionButtonCloseLibraryItems.hide();
                viewFlipperLibrary.setDisplayedChild(0);
            }
        });
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);

        fragmentLibrary = view.findViewById(R.id.fragment_library);

        viewFlipperLibrary = view.findViewById(R.id.view_flipper_library);


        viewFlipperLibraryItems = view.findViewById(R.id.view_flipper_library_items);

        recyclerViewLibraryItems = view.findViewById(R.id.recycler_view_library_list);

        recyclerViewLibraryItems.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy)
            {
                if (dy > 0 ||dy<0 && floatingActionButtonCloseLibraryItems.isShown())
                {
                    floatingActionButtonCloseLibraryItems.hide();
                }
            }

            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState)
            {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    floatingActionButtonCloseLibraryItems.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });


        recyclerView = view.findViewById(R.id.recycler_view_library);

        libraryItems = new ArrayList<>();

        quizSubjects = new ArrayList<>();

        libraries = new ArrayList<>();

        dynamicLibrary = new ArrayList<>();

        setLibraryItems();

        initLibraries(getActivity());

        int span = 2;

        if(getActivity()!=null) {
            String device = Constants.getSizeName(getActivity());
            switch (device){
                case "small":
                case "normal":
                    span = 2;
                    break;

                case "xlarge":
                case "large":
                    span = 3;
                    /*if(libraryItems.size()%3!=0){
                        int whatsLeft = libraryItems.size()%3;

                        for(int i =1; i<whatsLeft; i++) {
                            libraryItems.add(new LibraryMenu(R.drawable.add_icon_blue, "",
                                    R.color.colorSelectedLesson, -1));
                        }
                    }*/
                    break;
            }
        }


        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),span));

        libraryAdapter = new LibraryAdapter(getActivity(),libraryItems, this);

        recyclerView.setAdapter(libraryAdapter);

        classesForCourse = PreferenceHelper.getPreferenceHelperInstance().getSelectedClassesForCourse(getActivity());

        selectedLesson = PreferenceHelper.getPreferenceHelperInstance().getSelectedLesson(getActivity());

        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getActivity());


        if(classesForCourse!=null) {
            try {
                if(selectedLesson==null) {
                    selectedLesson = PreferenceHelper.getPreferenceHelperInstance().getLessons(getActivity()).get(0);
                     PreferenceHelper.getPreferenceHelperInstance().setSelectedLesson(getActivity(),selectedLesson);
                }
                setLibrary();
               // getLibraryItems(selectedLesson.getSubject_id(), classesForCourse.getUuid(), mToken);
                getPastQuestionsBySubjectID(selectedLesson.getSubject_id(), mToken);
            }
            catch (Exception e){
                e.printStackTrace();
            }


        }

        //String subId = "960fd64a-aaf3-471a-b480-73d6b5024d7f";
       // String classId = "e70bd5ad-acea-4dcb-b171-1bbee371f74b";

    }

    private void setLibrary(){
        //Audios
        libraries.add(new Library(0,"admin","Audio Item 1","Library Item description", "audio",21,20,"https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_5MG.mp3",".mp3" ));
        libraries.add(new Library(1,"admin","Audio Item 1","Library Item description", "audio",21,20,"https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_5MG.mp3",".mp3" ));
        libraries.add(new Library(2,"admin","Audio Item 1","Library Item description", "audio",21,20,"https://demo.mediacms.io/media/original/user/soroush/4a9e2b188a7947f393fe0e28ea4a0791.طعم_صلوات.mp3",".mp3" ));
        libraries.add(new Library(3,"admin","Audio Item 1","Library Item description", "audio",21,20,"https://demo.mediacms.io/media/original/user/user/f44f8837e9b948c1ae662e53a879650e.JeffSpeed68_-_So_Long_Twenty_Twenty_Pantry.mp3",".mp3" ));
        libraries.add(new Library(4,"admin","Audio Item 1","Library Item description", "audio",21,20,"https://demo.mediacms.io/media/original/user/user/fd4abe35a3ac4b1cb8458419f051b938.airtone_-_moonlight.mp3",".mp3" ));



        libraries.add(new Library(0,"admin","Video Item 1","PERIODIC TABLE", "video",21,20,"https://mediahub.desafrica.com/media/hls/317fc5ada71741c4958aab37157324cc/master.m3u8",".mp4" ));
        libraries.add(new Library(1,"admin","Video Item 2","MEASUREMENT OF LENGTH", "video",21,20,"https://mediahub.desafrica.com/media/hls/d732ec964ce14ec8b1e47563f6abb6d1/master.m3u8",".mp4" ));
        libraries.add(new Library(2,"admin","Video Item 3","SIMPLEST FORM OR LOWEST TERM", "video",21,20,"https://mediahub.desafrica.com/media/hls/a19040e0ca704e539a6b146bed508b82/master.m3u8",".mp4" ));
        libraries.add(new Library(3,"admin","Video Item 4","ALLOYS", "video",21,20,"https://mediahub.desafrica.com/media/hls/34d49bda0d6e4eccafba47da684239b5/master.m3u8",".mp4" ));
        libraries.add(new Library(4,"admin","Video Item 5","USES OF WATER", "video",21,20,"https://mediahub.desafrica.com/media/hls/85799ca26e5e455ba90837c8a28115db/master.m3u8",".mp4" ));


        //https://www.ets.org/Media/Tests/GRE/pdf/gre_research_validity_data.pdf

        libraries.add(new Library(0,"admin","PDF Item 1","PERIODIC TABLE", "pdf",21,20,"https://www.ets.org/Media/Tests/GRE/pdf/gre_research_validity_data.pdf",".pdf" ));
        libraries.add(new Library(0,"admin","PDF Item 1","PERIODIC TABLE", "pdf",21,20,"https://uwaterloo.ca/onbase/sites/ca.onbase/files/uploads/files/samplecertifiedpdf.pdf",".pdf" ));
        libraries.add(new Library(1,"admin","PDF Item 2","MEASUREMENT OF LENGTH", "pdf",21,20,"https://www.adobe.com/content/dam/acom/en/devnet/pdf/pdfs/pdf_reference_archives/PDFReference.pdf",".pdf" ));
        libraries.add(new Library(2,"admin","PDF Item 3","SIMPLEST FORM OR LOWEST TERM", "pdf",21,20,"https://juventudedesporto.cplp.org/files/sample-pdf_9359.pdf",".pdf" ));
        libraries.add(new Library(3,"admin","PDF Item 4","ALLOYS", "pdf",21,20,"https://www.hq.nasa.gov/alsj/a17/A17_FlightPlan.pdf",".pdf" ));
        libraries.add(new Library(4,"admin","PDF Item 5","USES OF WATER", "pdf",21,20,"https://www.uottawa.ca/respect/sites/www.uottawa.ca.respect/files/fss-fixing-accessibility-errors-in-pdfs.pdf",".pdf" ));




        PreferenceHelper.getPreferenceHelperInstance().setLibraryItems(getActivity(),libraries);
    }


    public void setLibraryItems() {
        libraryItems.add(new LibraryMenu(R.drawable.notes,"Notes", R.color.colorNotes, 1));
        libraryItems.add(new LibraryMenu(R.drawable.books,"Books", R.color.colorBooks,2));
        libraryItems.add(new LibraryMenu(R.drawable.quiz,"Quiz", R.color.colorQuiz,3));
        libraryItems.add(new LibraryMenu(R.drawable.articles,"Articles", R.color.colorArticles,4));

        libraryItems.add(new LibraryMenu(R.drawable.pasqo,"PasQo", R.color.colorPasQ,5));
        libraryItems.add(new LibraryMenu(R.drawable.games,"Games", R.color.colorGames,6));
        libraryItems.add(new LibraryMenu(R.drawable.audio,"Audio", R.color.colorAudio,7));
        libraryItems.add(new LibraryMenu(R.drawable.videos,"Video", R.color.colorVideos,8));
    }


    void showChangePastQuestionsDialog(Activity activity,List<QuizSubject> classesForCourses){
        if(alertDialogPastQuestions==null)
            createChangePastQuestionsDialog(activity,classesForCourses);
        alertDialogPastQuestions.show();
    }


    private void getPasQuestionsData(String quiz_group_id, String token){
        ApiClient.getApiClient().getInstance(getContext()).getQuestionsData(quiz_group_id,token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<QuizDataResponse>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<QuizDataResponse> quizDataResponseResponse) {
                        if(quizDataResponseResponse.isSuccessful()) {
                            if(quizDataResponseResponse.body()!=null) {
                                Log.d("getPasQuestionsData", quizDataResponseResponse.body().toString());
                                Intent intent = new Intent(getActivity(), PastQuestionActivity.class);
                                intent.putExtra("quiz_data", quizDataResponseResponse.body());
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }
    private void createChangePastQuestionsDialog(Activity activity, List<QuizSubject> quizSubjects){


        PastQuestionsAdapter pastQuestionsAdapter = new PastQuestionsAdapter(activity, quizSubjects);

        MaterialAlertDialogBuilder dialogBuilderQuality = new MaterialAlertDialogBuilder(activity, R.style.MaterialAlertDialog_rounded);

        int selectedBookmark = 0;
        dialogBuilderQuality.setSingleChoiceItems(pastQuestionsAdapter, selectedBookmark, (dialog, which) -> {
            if(getActivity()!=null){
                QuizSubject quizSubject =  pastQuestionsAdapter.getItem(which);
                getPasQuestionsData("6"/*String.valueOf(quizSubject.getId())*/, "Bearer "+mToken);
                Log.d("PastQuestionsAdapter", quizSubject.toString());
            }
            dialog.dismiss();
        });

        dialogBuilderQuality.setCancelable(true);
        alertDialogPastQuestions =  dialogBuilderQuality.create();
        alertDialogPastQuestions.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogPastQuestions.getWindow()!=null) {
            //alertDialogPastQuestions.getWindow().getAttributes().windowAnimations = R.style.Animation;
            Window window = alertDialogPastQuestions.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            alertDialogPastQuestions.getWindow().setAttributes(wlp);
            alertDialogPastQuestions.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        }
    }

    private List<Library> getLibraries(List<Library> libraries, String type){
        List<Library> libraryList = new ArrayList<>();
        for(Library library:libraries) {
            if(library.getFile_type().equalsIgnoreCase(type)){
                libraryList.add(library);
            }
        }

        return  libraryList;
    }


    @Override
    public  void onLibraryItemSelected(LibraryMenu libraryItem){
        //Toast.makeText(getActivity(), ""+libraries.toString(), Toast.LENGTH_SHORT).show();

        switch (libraryItem.getId()){
            case 1:
                PreferenceHelper.getPreferenceHelperInstance().setLibrarySelected(getActivity(),"note");
                dynamicLibrary.clear();
                dynamicLibrary.addAll(getLibraries(libraries, "pdf"));
                viewFlipperLibrary.setDisplayedChild(1);
                viewFlipperLibraryItems.setDisplayedChild(0);
                libraryItemAdapter.notifyDataSetChanged();
                floatingActionButtonCloseLibraryItems.show();
                Log.d("Libraries", "dynamic library: ");
                break;

            case 2:

                PreferenceHelper.getPreferenceHelperInstance().setLibrarySelected(getActivity(),"books");
                dynamicLibrary.clear();
                dynamicLibrary.addAll(getLibraries(libraries, "pdf"));
                viewFlipperLibrary.setDisplayedChild(1);
                viewFlipperLibraryItems.setDisplayedChild(0);
                libraryItemAdapter.notifyDataSetChanged();
                floatingActionButtonCloseLibraryItems.show();
                Log.d("Libraries", "dynamic library: ");

                break;

            case 3:
                Toast.makeText(getActivity(), "Quiz", Toast.LENGTH_SHORT).show();
                break;

            case 4:
                Toast.makeText(getActivity(), "Articles", Toast.LENGTH_SHORT).show();
                break;

            case 5:
                if(quizSubjects.size()>0)
                showChangePastQuestionsDialog(getActivity(), quizSubjects);
                else getPastQuestionsBySubjectID(classesForCourse.getUuid(), mToken);
                break;

            case 6:
                Toast.makeText(getActivity(), "Games", Toast.LENGTH_SHORT).show();
                break;

            case 7:
                PreferenceHelper.getPreferenceHelperInstance().setLibrarySelected(getActivity(),"audio");
                dynamicLibrary.clear();
                dynamicLibrary.addAll(getLibraries(libraries, "audio"));
                viewFlipperLibrary.setDisplayedChild(1);
                viewFlipperLibraryItems.setDisplayedChild(0);
                libraryItemAdapter.notifyDataSetChanged();
                floatingActionButtonCloseLibraryItems.show();
                Log.d("Libraries", "dynamic library: ");
                //intent.putExtra("library_type", "Audios");
                //intent.putExtra("library_items",(Serializable)list);
                //startActivity(intent);
                //Toast.makeText(getActivity(), "Audio: "+list.toString(), Toast.LENGTH_SHORT).show();
                break;

            case 8:
                PreferenceHelper.getPreferenceHelperInstance().setLibrarySelected(getActivity(),"video");
                dynamicLibrary.clear();
                dynamicLibrary.addAll(getLibraries(libraries, "video"));
                viewFlipperLibrary.setDisplayedChild(1);
                viewFlipperLibraryItems.setDisplayedChild(0);
                libraryItemAdapter.notifyDataSetChanged();
                floatingActionButtonCloseLibraryItems.show();
                if(dynamicLibrary.size()>0) {
                    dynamicLibrary.get(0).setSelected(true);
                    EventBus.getDefault().post(new LibrariesEventBus(dynamicLibrary));
                }
                Log.d("Libraries", "dynamic library:");
                //intent.putExtra("library_type", "Videos");
                //startActivity(intent);
                //Toast.makeText(getActivity(), "Video", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onLibraryClicked(Library library) {

        for (Library library1:dynamicLibrary) {
            if(library1.getId() == library.getId()){
                library1.setSelected(true);
            }
            else {
                library1.setSelected(false);
            }
        }
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID,String.valueOf(library.getId()));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "onLibraryClicked");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "application/json");
        bundle.putSerializable(FirebaseAnalytics.Param.CONTENT,library);
        getFirebaseAnalytics(getActivity()).logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle);
        libraryItemAdapter.notifyDataSetChanged();

       // mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if(library.getFile_type().equalsIgnoreCase("audio")) {
            List<Enclosure> enclosures = new ArrayList<>();
            enclosures.add(new Enclosure(library.getSource(), "audio", "1000"));
            List<ItemImage> itemImages = new ArrayList<>();
            itemImages.add(new ItemImage("https://picsum.photos/200/300?grayscale"));
            AudioItem item = new AudioItem("title", "description", "summary", "2020-04-12", "1000", enclosures, itemImages);
            mPodcastId = library.getSource() + library.getLibrary_item();
            mPodcastName = "Audio Sample Name";
            mPodcastImage =  itemImages.get(0).getItemImageHref();
            mItem = item;
            Bundle b = new Bundle();
            b.putParcelable(EXTRA_ITEM, item);
            createMediaBrowserCompat();
            connectMediaBrowser();
            Intent serviceIntent = new Intent(getActivity(), AudioPlayerService.class);
            // Set the action to check if the old player should be released in PodcastService
            serviceIntent.setAction(ACTION_RELEASE_OLD_PLAYER);
            // Pass an Item object
            serviceIntent.putExtra(EXTRA_ITEM, b);
            // Pass podcast title and podcast image
            serviceIntent.putExtra(EXTRA_RESULT_NAME, "Audio Sample Name");
            serviceIntent.putExtra(EXTRA_PODCAST_IMAGE, mPodcastImage);
            // Start the PodcastService
            if (getActivity() != null)
                getActivity().startService(serviceIntent);
        }


        else if(library.getFile_type().equalsIgnoreCase("video")){

            EventBus.getDefault().post(library);
        }

        else if(library.getFile_type().equalsIgnoreCase("pdf")){

            new Handler(Looper.getMainLooper()).post(() -> {
                lottie_shimmer.startShimmer();
                //lottieAnimationView.playAnimation();
                //pdfView.setVisibility(View.INVISIBLE);
            });

            AsyncTask.execute(() -> {

                try {
                    if(input!=null){
                        input.close();
                        input = null;
                    }
                    input = new URL(library.getSource()).openStream();

                } catch (IOException e) {
                    e.printStackTrace();
                }

               try {
                    if(input!=null) {
                   if(getActivity()!=null){
                       pdfView.recycle();


                       getActivity().runOnUiThread(() -> pdfView.fromStream(input).defaultPage(0)
                               .enableSwipe(true)
                               .onPageChange((page, pageCount) -> Toast.makeText(getActivity(), "onPageChanged: "+ (page+1)+"/"+pageCount, Toast.LENGTH_SHORT).show())
                               .onError(t -> {

                                   Toast.makeText(getContext(), "onError", Toast.LENGTH_SHORT).show();

                               }).onLoad(nbPages -> {
                                   new Handler(Looper.getMainLooper()).post(() -> {
                                       //lottieAnimationView.pauseAnimation();
                                       lottie_shimmer.hideShimmer();
                                       pdfView.setVisibility(View.VISIBLE);
                                   });
                               }).load());
                   }
                }
               }
               catch (Exception e){
                   e.printStackTrace();
               }
            });
            viewFlipperLibrary.setDisplayedChild(1);
            viewFlipperLibraryItems.setDisplayedChild(1);
            floatingActionButtonCloseLibraryItems.show();
        }
       /* else {
            Intent intent  = new Intent(getActivity(), VideoPlayerActivity.class);
            intent.putExtra("library",library);
            startActivity(intent);
        }*/


      /*  if(library.getFile_type().equalsIgnoreCase("audio")) {
            // Update the episode data using SharedPreferences each time the user selects the episode.
            // CandyPodUtils.updateSharedPreference(this, item, mResultName,
            //getItemImageUrl(item, mPodcastImage);
            Intent intent = new Intent(getActivity(), NowPlayingActivity.class);
            // Wrap the parcelable into a bundle
            Bundle b = new Bundle();

            List<Enclosure> enclosures = new ArrayList<>();
            enclosures.add(new Enclosure(library.getSource(), "audio", "1000"));
            List<ItemImage> itemImages = new ArrayList<>();
            itemImages.add(new ItemImage("https://picsum.photos/200/300?grayscale"));
            AudioItem item = new AudioItem("title", "description", "summary", "2020-04-12", "1000", enclosures, itemImages);

            b.putParcelable(EXTRA_ITEM, item);

            // Pass the bundle through intent
            intent.putExtra(EXTRA_ITEM, b);
            // Pass podcast id
            intent.putExtra(EXTRA_RESULT_ID, library.getSource() + library.getLibrary_item());
            // Pass podcast title
            intent.putExtra(EXTRA_RESULT_NAME, "Audio Sample Name");
            // Pass the podcast image URL. If there is no item image, use this podcast image.
            //intent.putExtra(EXTRA_PODCAST_IMAGE, mPodcastImage);

            startActivity(intent);

            Intent serviceIntent = new Intent(getActivity(), AudioPlayerService.class);
            // Set the action to check if the old player should be released in PodcastService
            serviceIntent.setAction(ACTION_RELEASE_OLD_PLAYER);
            // Pass an Item object
            serviceIntent.putExtra(EXTRA_ITEM, b);
            // Pass podcast title and podcast image
            serviceIntent.putExtra(EXTRA_RESULT_NAME, "Audio Sample Name");
            // serviceIntent.putExtra(EXTRA_PODCAST_IMAGE, mPodcastImage);
            // Start the PodcastService
            if (getActivity() != null)
                getActivity().startService(serviceIntent);
        }
*/
    }


    private void  connectMediaBrowser(){
        mMediaBrowser.connect();
    }

    private void disconnectMediaBrowser(){
        try {
            //Toast.makeText(this, "OnStop", Toast.LENGTH_SHORT).show();
            // Disconnect the MediaBrowser and unregister the MediaController.Callback when the
            // activity stops
            if (MediaControllerCompat.getMediaController(getActivity()) != null) {
                MediaControllerCompat.getMediaController(getActivity()).unregisterCallback(controllerCallback);
            }
            if(mMediaBrowser!=null)
            mMediaBrowser.disconnect();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

      private void updatePlaybackState(PlaybackStateCompat state) {

        if (state == null) {
            return;
        }
        Log.d("updatePlaybackState:", "Updates the playback state with state:"+state.getState());

        mLastPlaybackState = state;
        switch (state.getState()) {

            case PlaybackStateCompat.STATE_PLAYING:
                hideLoading();
                //mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.pause_icon);
                // Create and execute a periodic action to update the SeekBar progress
                scheduleSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_PAUSED:
                hideLoading();
                //mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.play_icon);
                // Cancel the future returned by scheduleAtFixedRate() to stop the SeekBar from progressing
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_NONE:
            case PlaybackStateCompat.STATE_STOPPED:
                hideLoading();
               //. mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.play_icon);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_BUFFERING:
                showLoading();
               // mNowPlayingBinding.playingInfo.ibPlayPause.setImageResource(R.drawable.play_icon);
                stopSeekbarUpdate();
                break;
            default:
                Log.d(TAG,"Unhandled state " + state.getState());
        }
    }

    /**
     * Shows loading indicator and text when the player is buffering.
     */
    private void showLoading() {
       // mNowPlayingBinding.playingInfo.pbLoadingIndicator.setVisibility(View.VISIBLE);
       // mNowPlayingBinding.playingInfo.tvLoading.setVisibility(View.VISIBLE);
       // mNowPlayingBinding.playingInfo.tvLoading.setText(R.string.loading);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        disconnectMediaBrowser();
    }

    /**
     * Hides loading indicator and text when the player is playing.
     */
    private void hideLoading() {
       // mNowPlayingBinding.playingInfo.pbLoadingIndicator.setVisibility(View.INVISIBLE);
       // mNowPlayingBinding.playingInfo.tvLoading.setVisibility(View.INVISIBLE);
    }

    private void createMediaBrowserCompat() {
        mMediaBrowser = new MediaBrowserCompat(getContext(),
                new ComponentName(getActivity(), AudioPlayerService.class),
                mConnectionCallbacks,
                null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Cancel the future returned by scheduleAtFixedRate() to stop the seekBar from progressing
        stopSeekbarUpdate();
        // Cancel currently executing tasks
        mExecutorService.shutdown();
    }
    /**
     * This app currently only supports audio podcasts. If the current episode is not audio,
     * displays a snackbar message.
     */
    private void handleEnclosureType() {
        String enclosureType = mItem.getEnclosures().get(0).getType();
        if (!enclosureType.contains(TYPE_AUDIO)) {
            String snackMessage = getString(R.string.snackbar_support_audio);
            Snackbar snackbar = Snackbar.make(fragmentLibrary, snackMessage, Snackbar.LENGTH_LONG);
            // Set the background color of the snackbar
            View sbView = snackbar.getView();
            sbView.setBackgroundColor(Color.RED);
            // Set the text color of the snackbar
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }



    /**
     * Creates and executes a periodic action that becomes enabled first after the given initial delay,
     * and subsequently with the given period;that is executions will commence after initialDelay
     * then initialDelay + period, then initialDelay + 2 * period, and so on.
     */
    private void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!mExecutorService.isShutdown()) {
            mScheduledFuture = mExecutorService.scheduleAtFixedRate(
                    // Cause the Runnable updateProgress to be added to the message queue
                    () -> mHandler.post(mUpdateProgressTask),
                    PROGRESS_UPDATE_INITIAL_INTERVAL,// initial delay (100 milliseconds)
                    PROGRESS_UPDATE_INTERVAL, // period (1000 milliseconds)
                    TimeUnit.MILLISECONDS); // the time unit of the initialDelay and period
        }
    }

    /**
     * Cancels the future returned by scheduleAtFixedRate() to stop the SeekBar from progressing.
     */
    private void stopSeekbarUpdate() {
        if (mScheduledFuture != null) {
            mScheduledFuture.cancel(false);
        }
    }

    /**
     * Calculates the current position (distance = timeDelta * velocity) and sets the current progress
     * to the current position.
     */
    private void updateProgress() {
        if (mLastPlaybackState == null) {
            return;
        }
        long currentPosition = mLastPlaybackState.getPosition();
        if (mLastPlaybackState.getState() == PlaybackStateCompat.STATE_PLAYING) {
            // Calculate the elapsed time between the last position update and now and unless
            // paused, we can assume (delta * speed) + current position is approximately the
            // latest position. This ensure that we do not repeatedly call the getPlaybackState()
            // on MediaControllerCompat.
            long timeDelta = SystemClock.elapsedRealtime() -
                    mLastPlaybackState.getLastPositionUpdateTime();
            currentPosition += (int) timeDelta * mLastPlaybackState.getPlaybackSpeed();
        }
        //mNowPlayingBinding.playingInfo.seekBar.setProgress((int) currentPosition);
    }



    private final MediaBrowserCompat.ConnectionCallback mConnectionCallbacks =
            new MediaBrowserCompat.ConnectionCallback() {
                @Override
                public void onConnected() {

                    // Get the token for the MediaSession
                    MediaSessionCompat.Token token = mMediaBrowser.getSessionToken();

                    // Create a MediaControllerCompat
                    MediaControllerCompat mediaController = null;
                    try {
                        mediaController = new MediaControllerCompat(getContext(), token);
                    } catch (RemoteException e) {
                        Log.e(TAG,"Error creating media controller");
                    }

                    // Save the controller
                    MediaControllerCompat.setMediaController(getActivity(),
                            mediaController);

                    // Finish building the UI
                    buildTransportControls();
                }

                @Override
                public void onConnectionSuspended() {
                    super.onConnectionSuspended();
                    // The Service has crashed. Disable transport controls until it automatically
                    // reconnects.
                }

                @Override
                public void onConnectionFailed() {
                    super.onConnectionFailed();
                    // The Service has refused our connection
                }
            };




    void buildTransportControls() {
        // Attach a listener to the play/pause button
      /*  mNowPlayingBinding.playingInfo.ibPlayPause.setOnClickListener(v -> {
            PlaybackStateCompat pbState = MediaControllerCompat.getMediaController(getActivity()).getPlaybackState();
            if (pbState != null) {
                MediaControllerCompat.TransportControls controls =
                        MediaControllerCompat.getMediaController(getActivity()).getTransportControls();
                Log.d(TAG,"buildTransportControls with state " + pbState.getPlaybackState());
                //Toast.makeText(this, "Player state"+ pbState.getState(), Toast.LENGTH_SHORT).show();


                switch (pbState.getState()) {
                    case PlaybackStateCompat.STATE_PLAYING:
                        Toast.makeText(getActivity(), "Player state STATE_PLAYING", Toast.LENGTH_SHORT).show();
                        controls.pause();
                        break;
                    // fall through
                    case PlaybackStateCompat.STATE_BUFFERING:
                        controls.play();
                        Toast.makeText(getActivity(), "Player state STATE_BUFFERING", Toast.LENGTH_SHORT).show();
                        // Cancel the future returned by scheduleAtFixedRate() to stop the seekBar
                        // from progressing
                        stopSeekbarUpdate();
                        break;
                    case PlaybackStateCompat.STATE_PAUSED:
                        Toast.makeText(getActivity(), "Player state STATE_PAUSED", Toast.LENGTH_SHORT).show();
                        controls.play();
                        break;

                    case PlaybackStateCompat.STATE_STOPPED:
                        Toast.makeText(getActivity(), "Player state STATE_STOPPED", Toast.LENGTH_SHORT).show();
                        controls.play();
                        // Create and execute a periodic action to update the seekBar progress
                        scheduleSeekbarUpdate();
                        break;
                    default:
                        Toast.makeText(getActivity(), "Player state "+pbState.getState(), Toast.LENGTH_SHORT).show();
                        Log.d(TAG,"onClick with state " + pbState);
                }
            }
        });

        // Attach a listener to the fast forward button
        mNowPlayingBinding.playingInfo.ibFastforward.setOnClickListener(v -> {
            MediaControllerCompat.getMediaController(getActivity())
                    .getTransportControls().fastForward();
            updateProgress();
        });

        // Attach a listener to the rewind button
        mNowPlayingBinding.playingInfo.ibRewind.setOnClickListener(v -> {
            MediaControllerCompat.getMediaController(getActivity())
                    .getTransportControls().rewind();
            updateProgress();
        });*/

        MediaControllerCompat mediaController =
                MediaControllerCompat.getMediaController(getActivity());
        // Display the initial state
        MediaMetadataCompat metadata = mediaController.getMetadata();
        PlaybackStateCompat pbState = mediaController.getPlaybackState();

        // Update the playback state
        updatePlaybackState(pbState);

        if (metadata != null) {
            // Get the episode duration from the metadata and sets the end time to the textView
            updateDuration(metadata);
        }
        // Set the current progress to the current position
        updateProgress();
        if (pbState != null && (pbState.getState() == PlaybackStateCompat.STATE_PLAYING ||
                pbState.getState() == PlaybackStateCompat.STATE_BUFFERING)) {
            // Create and execute a periodic action to update the SeekBar progress
            scheduleSeekbarUpdate();
        }

        // Register a Callback to stay in sync
        mediaController.registerCallback(controllerCallback);
    }

    /**
     * Callback for receiving updates from the session.
     */
    MediaControllerCompat.Callback controllerCallback = new MediaControllerCompat.Callback() {
        @Override
        public void onMetadataChanged(MediaMetadataCompat metadata) {
            super.onMetadataChanged(metadata);
            if (metadata != null) {
                // Get the episode duration from the metadata and sets the end time to the textView
                updateDuration(metadata);
            }
        }

        @Override
        public void onPlaybackStateChanged(PlaybackStateCompat state) {
            super.onPlaybackStateChanged(state);
            // Update the playback state
            Log.d(TAG,"onPlaybackStateChanged:"+state);
            updatePlaybackState(state);
        }
    };

    /**
     * Gets the episode duration from the metadata and sets the end time to be displayed in the TextView.
     */
    private void updateDuration(MediaMetadataCompat metadata) {
        if (metadata == null) {
            return;
        }
        int duration = (int) metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION)
                * FORMAT_ELAPSED_TIME;
       // mNowPlayingBinding.playingInfo.seekBar.setMax(duration);
       // mNowPlayingBinding.playingInfo.tvEnd.setText(DateUtils.formatElapsedTime(duration / FORMAT_ELAPSED_TIME));
    }


    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();

    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void resetLibrary(ResetLibrary resetLibrary){
        floatingActionButtonCloseLibraryItems.hide();
        viewFlipperLibrary.setDisplayedChild(0);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVideoNextPreviousSelected(SelectedLibrary selectedLibrary){
       // Toast.makeText(getActivity(), "onVideoNextPreviousSelected:"+selectedLibrary.getId(), Toast.LENGTH_SHORT).show();
        for (Library library: dynamicLibrary) {
            if(library.getId() ==selectedLibrary.getId()){
                library.setSelected(true);
            }
            else library.setSelected(false);
        }
        libraryItemAdapter.notifyDataSetChanged();

    }

}