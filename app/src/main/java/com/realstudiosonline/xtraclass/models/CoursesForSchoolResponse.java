package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class CoursesForSchoolResponse extends DataResponse {
    private List<CoursesForSchool> data;

    public List<CoursesForSchool> getData() {
        return data;
    }

    public void setData(List<CoursesForSchool> data) {
        this.data = data;
    }
}
