package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class LessonResponse extends DataResponse {

  private List<Lesson> data;

    public List<Lesson> getData() {
        return data;
    }

    public void setData(List<Lesson> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LessonResponse{" +
                "data=" + data +
                '}';
    }
}
