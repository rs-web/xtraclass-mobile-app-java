package com.realstudiosonline.xtraclass.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;

import com.google.android.exoplayer2.Format;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.AudioItem;
import com.realstudiosonline.xtraclass.models.ItemImage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;

import io.agora.rtc.video.BeautyOptions;
import io.agora.rtc.video.VideoEncoderConfiguration;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Constants {


    public static int getBitrateDrawableTinted(int bitrate) {
        if (bitrate == Format.NO_VALUE) {
            return R.drawable.au_blue;
        }
        if (bitrate <= BITRATE_160P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_240P) {
            return R.drawable.ld_blue;
        }
        if (bitrate <= BITRATE_360P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_480P) {
            return R.drawable.sd_blue;
        }
        return R.drawable.hd_blue;
    }

    public static String getBitrateString(int bitrate) {
        if (bitrate == Format.NO_VALUE) {
            return "AUTO";
        }
        if (bitrate <= BITRATE_160P) {
            return "160P";
        }
        if (bitrate <= BITRATE_240P) {
            return "240P";
        }
        if (bitrate <= BITRATE_360P) {
            return "360P";
        }
        if (bitrate <= BITRATE_480P) {
            return "480P";
        }
        return "720P";
    }

    public static MultipartBody.Part createFormData(String name, String fileName, File file){
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData(name,fileName,requestFile);
    }

    public static int generate(int min,int max) {
        return min + (int)(Math.random() * ((max - min) + 1));
    }

    public static String ordinal(int i) {
        String[] suffixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + suffixes[i % 10];

        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    public static String getSizeName(Context context) {
        int screenLayout = context.getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenLayout) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "small";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "normal";
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "large";
            case 4: // Configuration.SCREENLAYOUT_SIZE_XLARGE is API >= 9
                return "xlarge";
            default:
                return "undefined";
        }
    }

    @NonNull
    public static String getSaveDir(Context context) {
        return context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString();
    }

    public  static String getExtDir(){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
    }

    @NonNull
    public static String getNameFromUrl(final String url) {
        return Uri.parse(url).getLastPathSegment();
    }

    @NonNull
    public static String getMimeType(@NonNull final Context context, @NonNull final Uri uri) {
        final ContentResolver cR = context.getContentResolver();
        final MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = mime.getExtensionFromMimeType(cR.getType(uri));
        if (type == null) {
            type = "*/*";
        }
        return type;
    }

    public static void deleteFileAndContents(@NonNull final File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                final File[] contents = file.listFiles();
                if (contents != null) {
                    for (final File content : contents) {
                        deleteFileAndContents(content);
                    }
                }
            }
            file.delete();
        }
    }

    @NonNull
    public static String getETAString(@NonNull final Context context, final long etaInMilliSeconds) {
        if (etaInMilliSeconds < 0) {
            return "";
        }
        int seconds = (int) (etaInMilliSeconds / 1000);
        long hours = seconds / 3600;
        seconds -= hours * 3600;
        long minutes = seconds / 60;
        seconds -= minutes * 60;
        if (hours > 0) {
            return context.getString(R.string.download_eta_hrs, hours, minutes, seconds);
        } else if (minutes > 0) {
            return context.getString(R.string.download_eta_min, minutes, seconds);
        } else {
            return context.getString(R.string.download_eta_sec, seconds);
        }
    }

    @NonNull
    public static String getDownloadSpeedString(@NonNull final Context context, final long downloadedBytesPerSecond) {
        if (downloadedBytesPerSecond < 0) {
            return "";
        }
        double kb = (double) downloadedBytesPerSecond / (double) 1000;
        double mb = kb / (double) 1000;
        final DecimalFormat decimalFormat = new DecimalFormat(".##");
        if (mb >= 1) {
            return context.getString(R.string.download_speed_mb, decimalFormat.format(mb));
        } else if (kb >= 1) {
            return context.getString(R.string.download_speed_kb, decimalFormat.format(kb));
        } else {
            return context.getString(R.string.download_speed_bytes, downloadedBytesPerSecond);
        }
    }

    @NonNull
    public static File createFile(String filePath) {
        final File file = new File(filePath);
        if (!file.exists()) {
            final File parent = file.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static int getProgress(long downloaded, long total) {
        if (total < 1) {
            return -1;
        } else if (downloaded < 1) {
            return 0;
        } else if (downloaded >= total) {
            return 100;
        } else {
            return (int) (((double) downloaded / (double) total) * 100);
        }
    }

    public static final String CHAT_SERVER_URL = "http://18.184.172.239:3000/";

    public static int convertDpToPixelInt(float dp, Context context) {
        return (int) (dp * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }
    public static int convertToPixelDpInt(float pixels, Context context) {
        return (int) (pixels * (((float) context.getResources().getDisplayMetrics().densityDpi) *160.0f));
    }
    private static final int BEAUTY_EFFECT_DEFAULT_CONTRAST = BeautyOptions.LIGHTENING_CONTRAST_NORMAL;
    private static final float BEAUTY_EFFECT_DEFAULT_LIGHTNESS = 0.7f;
    private static final float BEAUTY_EFFECT_DEFAULT_SMOOTHNESS = 0.5f;
    private static final float BEAUTY_EFFECT_DEFAULT_REDNESS = 0.1f;

    public static final BeautyOptions DEFAULT_BEAUTY_OPTIONS = new BeautyOptions(
            BEAUTY_EFFECT_DEFAULT_CONTRAST,
            BEAUTY_EFFECT_DEFAULT_LIGHTNESS,
            BEAUTY_EFFECT_DEFAULT_SMOOTHNESS,
            BEAUTY_EFFECT_DEFAULT_REDNESS);

    public static VideoEncoderConfiguration.VideoDimensions[] VIDEO_DIMENSIONS = new VideoEncoderConfiguration.VideoDimensions[]{
            VideoEncoderConfiguration.VD_320x240,
            VideoEncoderConfiguration.VD_480x360,
            VideoEncoderConfiguration.VD_640x360,
            VideoEncoderConfiguration.VD_640x480,
            new VideoEncoderConfiguration.VideoDimensions(960, 540),
            VideoEncoderConfiguration.VD_1280x720
    };

    public static int[] VIDEO_MIRROR_MODES = new int[]{
            io.agora.rtc.Constants.VIDEO_MIRROR_MODE_AUTO,
            io.agora.rtc.Constants.VIDEO_MIRROR_MODE_ENABLED,
            io.agora.rtc.Constants.VIDEO_MIRROR_MODE_DISABLED,
    };

    public static final String PREF_NAME = "io.agora.openlive";
    public static final int DEFAULT_PROFILE_IDX = 2;
    public static final String PREF_RESOLUTION_IDX = "pref_profile_index";
    public static final String PREF_ENABLE_STATS = "pref_enable_stats";
    public static final String PREF_MIRROR_LOCAL = "pref_mirror_local";
    public static final String PREF_MIRROR_REMOTE = "pref_mirror_remote";
    public static final String PREF_MIRROR_ENCODE = "pref_mirror_encode";

    public static final String MEDIA_HUB = "https://mediahub.desafrica.com";
    public static final String KEY_CLIENT_ROLE = "key_client_role";
//1015952
    public static final int BITRATE_1080P = 2800000;
    public static final int BITRATE_720P = 1600000;
    public static final int BITRATE_480P = 700000;
    public static final int BITRATE_360P = 530000;
    public static final int BITRATE_240P = 400000;
    public static final int BITRATE_160P = 300000;



    /** The parameter value for a search field. The  "podcast" is the media type to search. */
    public static final String SEARCH_MEDIA_PODCAST = "podcast";

    /** Index for representing a default fragment */
    public static final int INDEX_ZERO = 0;

    /** A key for the Extra to pass data via Intent */
    public static final String EXTRA_RESULT_ID = "extra_result_id";
    public static final String EXTRA_RESULT_NAME = "extra_result_name";
    public static final String EXTRA_ITEM = "extra_item";
    public static final String EXTRA_PODCAST_IMAGE = "extra_podcast_image";
    public static final String EXTRA_DOWNLOAD_ENTRY = "extra_download_entry";
    public static final String EXTRA_RESULT_ARTWORK_100 = "extra_result_artwork_100";

    public static final String ACTION_RELEASE_OLD_PLAYER = "action_release_old_player";

    /** Database name */
    public static final String DATABASE_NAME = "podcast";

    /** Used to replace the HTML img tag with empty string */
    public static final String IMG_HTML_TAG = "<img.+?>";
    public static final String REPLACEMENT_EMPTY = "";

    /** The value of column width in the GridAutofitLayoutManager */
    public static final int GRID_AUTO_FIT_COLUMN_WIDTH = 380;
    /** Default value for column width in the GridAutofitLayoutManager */
    public static final int GRID_COLUMN_WIDTH_DEFAULT = 48;
    /** Initial span count for the GridAutofitLayoutManager */
    public static final int GRID_SPAN_COUNT = 1;

    /** Context menu option in the PodcastsAdapter */
    public static final String DELETE = "Delete";
    public static final int GROUP_ID_DELETE = 0;
    public static final int ORDER_DELETE = 0;

    /** Notification channel id */
    public static final String PLAYBACK_CHANNEL_ID = "candy_pod_playback_channel";
    public static final int PLAYBACK_NOTIFICATION_ID = 1;
    public static final String DOWNLOAD_CHANNEL_ID = "candy_pod_download_channel";
    public static final int DOWNLOAD_NOTIFICATION_ID = 2;

    /** The pending intent id is used to uniquely reference the pending intent */
    public static final int NOTIFICATION_PENDING_INTENT_ID = 0;
    public static final int WIDGET_PENDING_INTENT_ID = 1;

    /** The fast forward increment and rewind increment (milliseconds) */
    public static final int FAST_FORWARD_INCREMENT = 30000; // 30 sec
    public static final int REWIND_INCREMENT = 10000; // 10 sec

    /** Constant used to format elapsed time */
    public static final int FORMAT_ELAPSED_TIME = 1000;
    /** The initial delay, the time to delay first execution */
    public static final long PROGRESS_UPDATE_INTERVAL = 1000;
    /** The period between successive executions */
    public static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;

    /** Scheduler Job id in the PodcastDownloadService*/
    public static final int JOB_ID = 1;

    /** Strings used to convert ITunes duration */
    public static final String SPLIT_COLON = ":";
    public static final int SPLIT_INDEX_ZERO = 0;
    public static final int SPLIT_INDEX_ONE = 1;
    public static final int SPLIT_INDEX_TWO = 2;

    /** The pubDate pattern */
    public static final String PUB_DATE_PATTERN = "EEE, d MMM yyyy HH:mm:ss Z";
    public static final String PUB_DATE_PATTERN_TIME_ZONE = "EEE, d MMM yyyy HH:mm z";
    /** The formatted date pattern */
    public static final String FORMATTED_PATTERN = "MMM d, yyyy";

    /** Type of the share intent data */
    public static final String SHARE_INTENT_TYPE_TEXT = "text/plain";

    /** The type of request method for reading information from the server */
    public static final String REQUEST_METHOD_GET = "GET";

    /** The default value of String from SharedPreference */
    public static final String PREF_DEF_VALUE = "";

    /** Desired width or height in pixels of the bitmap (Needs to be manually put because of
     * RemoteViews limitation) */
    public static final int SIZE_BITMAP = 300;

    /** Glide Transformation */
    public static final int BLUR_RADIUS = 25;
    public static final int BLUR_SAMPLING = 3;

    /** The maximum color count for the palette */
    public static final int MAX_COLOR_COUNT = 16;
    /** The default value of vibrant color used if the Vibrant swatch is null*/
    public static final int DEF_VIBRANT_COLOR = 0xFFDB7093;

    /** The value used to set CompoundDrawablesWithIntrinsicBounds */
    public static final int NO_DRAWABLES = 0;

    /** The enclosure type */
    public static final String TYPE_AUDIO = "audio";

    /** Cache directory */
    public static final String FILE_DOWNLOADS = "downloads";
    /** Action file */
    public static final String FILE_ACTIONS = "actions";

    /** A key for saving the current state */
    public static final String STATE_SEARCH_QUERY = "state_search_query";

    private static URL createUrl(String urlString) {
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    public static String getItemImageUrl(AudioItem item, String podcastImage) {
        List<ItemImage> itemImages = item.getItemImages();
        String itemImageUrl = null;
        if (itemImages != null) {
            itemImageUrl = itemImages.get(0).getItemImageHref();
        }
        if (TextUtils.isEmpty(itemImageUrl)) {
            itemImageUrl = podcastImage;
        }
        return itemImageUrl;
    }

    public static Bitmap loadImage(String urlString) throws IOException {
        HttpURLConnection connection = null;
        InputStream stream = null;
        URL url = createUrl(urlString);
        Bitmap bitmap = null;
        try {
            // If the URL is null, then return early
            if (url == null) {
                return null;
            }
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(REQUEST_METHOD_GET);
            connection.connect();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                stream = connection.getInputStream();
            } else {
                Log.e("loadImage","Error response code: " + connection.getResponseCode());
            }

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 1;
            bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (stream != null) {
                stream.close();
            }
        }
        return bitmap;
    }

}
