package com.realstudiosonline.xtraclass.models;

public class CoursesForSchool {
    private String id;
    private String name;
    private String department_id;
    private String school_id;


    public CoursesForSchool() {
    }

    public CoursesForSchool(String id, String name, String department_id, String school_id) {
        this.id = id;
        this.name = name;
        this.department_id = department_id;
        this.school_id = school_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    @Override
    public String toString() {
        return "CoursesForSchool{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", department_id='" + department_id + '\'' +
                ", school_id='" + school_id + '\'' +
                '}';
    }


    /*    "id": "0b5ec159-69dd-48d6-981d-9245d63d118e",
            "name": "Business",
            "department_id": "",
            "school_id": "fe96a0c2-c739-4be7-be68-587f1be03e6b"*/
}
