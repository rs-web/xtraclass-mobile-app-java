package com.realstudiosonline.xtraclass.models;

public class StudentProfileResponse {
    private int statusCode;
    private String message;
    private StudentProfile data;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StudentProfile getData() {
        return data;
    }

    public void setData(StudentProfile data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "StudentProfileResponse{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
