package com.realstudiosonline.xtraclass.models;

public class Classx {
    private String id;
    private String name;
    private String full_name;
    private String course_id;
    private String school_id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    @Override
    public String toString() {
        return "Classx{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", full_name='" + full_name + '\'' +
                ", course_id='" + course_id + '\'' +
                ", school_id='" + school_id + '\'' +
                '}';
    }
}

/*
* "id": "c7f3c461-4347-4901-85e0-b18c3536c33e",
            "name": "English Language",
            "full_name": "OSP English Language",
            "course_id": "e93e7590-375a-4d92-8d69-45e8f89b44b4",
            "school_id": "897a5ab9-24b9-4511-9a32-392667fe2a95"*/