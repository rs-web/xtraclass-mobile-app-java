package com.realstudiosonline.xtraclass.models;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class TransactionState {

    public final String transactionState;

    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({ OFFLINE,OTP,FAILED,SUCCESS, ABANDONED,ONGOING})
    // Create an interface for validating String types
    public @interface FilterTransactionSateDef {}
    // Declare the constants
    public static final String OFFLINE = "offline";
    public static final String OTP = "otp";
    public static final String FAILED = "failed";
    public static final String SUCCESS = "success";
    public static final String ABANDONED="abandoned";
    public static final String ONGOING = "ongoing";



    // Mark the argument as restricted to these enumerated types
    public TransactionState(@FilterTransactionSateDef String transactionState) {
        this.transactionState = transactionState;
    }
}
