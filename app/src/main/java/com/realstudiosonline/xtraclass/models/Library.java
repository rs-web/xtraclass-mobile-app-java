package com.realstudiosonline.xtraclass.models;

import java.io.Serializable;

public class Library implements Serializable {

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String author;
    private String library_item;
    private String description;
    private String file_type;
    private int view_count;
    private int total_download;
    private String source;
    private String file_extension;

    public Library(int id,String author, String library_item, String description, String file_type, int view_count, int total_download, String source, String file_extension) {
        this.id = id;
        this.author = author;
        this.library_item = library_item;
        this.description = description;
        this.file_type = file_type;
        this.view_count = view_count;
        this.total_download = total_download;
        this.source = source;
        this.file_extension = file_extension;
    }

    public Library() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLibrary_item() {
        return library_item;
    }

    public void setLibrary_item(String library_item) {
        this.library_item = library_item;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public int getTotal_download() {
        return total_download;
    }

    public void setTotal_download(int total_download) {
        this.total_download = total_download;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFile_extension() {
        return file_extension;
    }

    public void setFile_extension(String file_extension) {
        this.file_extension = file_extension;
    }

    @Override
    public String toString() {
        return "Library{" +
                "author='" + author + '\'' +
                ", library_item='" + library_item + '\'' +
                ", description='" + description + '\'' +
                ", file_type='" + file_type + '\'' +
                ", view_count=" + view_count +
                ", total_download=" + total_download +
                ", source='" + source + '\'' +
                ", file_extension='" + file_extension + '\'' +
                '}';
    }


    /*
            "author": "admin",
                    "library_item": "Library Item",
                    "description": "Library Item description",
                    "file_type": "audio",
                    "view_count": 21,
                    "total_download": 20,
                    "source": "https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_5MG.mp3",
                    "file_extension": ".mp3"*/
}
