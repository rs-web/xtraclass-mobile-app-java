package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Question;


import org.jetbrains.annotations.NotNull;

import java.util.List;

import im.delight.android.webview.AdvancedWebView;

public class QuizAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Question> questions;
    Context context;
    setOnAnswerSelectedListener listener;
    RecyclerView.ViewHolder viewHolder;
    public QuizAdapter(List<Question> questions, Context context,setOnAnswerSelectedListener listener ) {
        this.questions = questions;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                viewHolder = new TrueFalseQuizViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question, parent, false));
                break;
            case 1:
                viewHolder = new MultiChoiceQuizViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question, parent, false));
                break;
            case 2:
                viewHolder = new ImageAsQuizViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_as_question, parent, false));
                break;
        }

        return viewHolder;

    }

    @Override
    public int getItemViewType(int position) {
        Log.d("getItemViewType", "lessonItems.get(position).getLessonType() :"+questions.get(position).getType());
        if(questions.get(position).getType().equalsIgnoreCase("true-false")) {
            return 0;
        }
        else if(questions.get(position).getType().equalsIgnoreCase("multi-choice")) {
            return 1;
        }
        else
            return 2;

    }



    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {

        Question question = questions.get(position);
        switch (holder.getItemViewType()){
            case 0:
                TrueFalseQuizViewHolder trueFalseQuizViewHolder = (TrueFalseQuizViewHolder) holder;
                trueFalseQuizViewHolder.bind(question, position,questions.size(), listener);
                break;
            case 1:
                MultiChoiceQuizViewHolder multiChoiceQuizViewHolder = (MultiChoiceQuizViewHolder) holder;
                multiChoiceQuizViewHolder.bind(context,question, position,questions.size(), listener);
                break;

            case 2:
                ImageAsQuizViewHolder imageAsQuizViewHolder = (ImageAsQuizViewHolder) holder;
                imageAsQuizViewHolder.bind(question,context,position,questions.size(), listener);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    static class TrueFalseQuizViewHolder extends RecyclerView.ViewHolder{
        RadioGroup radioGroup;
        AdvancedWebView textViewQuestion;
        AppCompatRadioButton rBtn1,rBtn2,rBtn3,rBtn4;

        public TrueFalseQuizViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            radioGroup = itemView.findViewById(R.id.rg_options);
            textViewQuestion = itemView.findViewById(R.id.tv_question_text);
            rBtn1 = itemView.findViewById(R.id.rb_option_1);
            rBtn2 = itemView.findViewById(R.id.rb_option_2);
            rBtn3 = itemView.findViewById(R.id.rb_option_3);
            rBtn4 = itemView.findViewById(R.id.rb_option_4);
        }

        public void bind(Question question, int position, int size, setOnAnswerSelectedListener listener){
            String text = (position+1)+" ."+question.getText();
            textViewQuestion.loadHtml(text);//.setText(Html.fromHtml(text));
            radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                View radioButton = group.findViewById(checkedId);
                int index = group.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        listener.onAnswerSelected(question,1, position, size);
                        // perform your action here
                        break;
                    case 1:
                        listener.onAnswerSelected(question,2, position, size);
                        // perform your action here
                        break;
                    case 2:
                        listener.onAnswerSelected(question, 3,position, size);
                        // perform your action here
                        break;
                    case 3:
                        listener.onAnswerSelected(question, 4,position, size);
                        // perform your action here
                        break;
                    case 4:
                        listener.onAnswerSelected(question,5, position, size);
                        // perform your action here
                        break;
                }
            });

            if(question.getOptions()!=null) {
                if (question.getOptions().size() > 0) {
                    Log.d("OPTIONS", "options size :"+question.getNum_of_options());
                    switch (question.getOptions().size()) {
                        case 2:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setVisibility(View.GONE);
                            rBtn4.setVisibility(View.GONE);
                            break;

                        case 3:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setText(question.getOptions().get(2));
                            rBtn4.setVisibility(View.GONE);
                            break;

                        case 4:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setText(question.getOptions().get(2));
                            rBtn4.setText(question.getOptions().get(3));
                            break;

                    }


                }
            }

        }

    }

    static class MultiChoiceQuizViewHolder extends RecyclerView.ViewHolder{
        RadioGroup radioGroup;
        AdvancedWebView textViewQuestion;
        AppCompatRadioButton rBtn1,rBtn2,rBtn3,rBtn4;
        public MultiChoiceQuizViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            radioGroup = itemView.findViewById(R.id.rg_options);
            textViewQuestion = itemView.findViewById(R.id.tv_question_text);
            rBtn1 = itemView.findViewById(R.id.rb_option_1);
            rBtn2 = itemView.findViewById(R.id.rb_option_2);
            rBtn3 = itemView.findViewById(R.id.rb_option_3);
            rBtn4 = itemView.findViewById(R.id.rb_option_4);


        }
        public void bind(Context context, Question question, int position, int size, setOnAnswerSelectedListener listener){
            String text = (position+1)+" ."+question.getText();
            //textViewQuestion.setText(Html.fromHtml(text));

            Log.d("QuizAdapter", "getText: "+question.getText());

            textViewQuestion.loadHtml(question.getText());
                    //.setText(Html.fromHtml(question.getText(), new URLImageParser(textViewQuestion, context), null));


            try{
                Html.fromHtml(question.getText(), new Html.ImageGetter() {
                    @Override
                    public Drawable getDrawable(String source) {
                        byte[] data = Base64.decode(source, Base64.DEFAULT);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                        return new BitmapDrawable(context.getResources(), bitmap);
                    }
                }, null);
            }
            catch (Exception e){
                e.printStackTrace();

            }

            if(question.getOptions()!=null) {
                if (question.getOptions().size() > 0) {
                    Log.d("OPTIONS", "options size :"+question.getNum_of_options());
                    switch (question.getOptions().size()) {
                        case 2:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setVisibility(View.GONE);
                            rBtn4.setVisibility(View.GONE);
                            break;

                        case 3:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setText(question.getOptions().get(2));
                            rBtn4.setVisibility(View.GONE);
                            break;

                        case 4:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setText(question.getOptions().get(2));
                            rBtn4.setText(question.getOptions().get(3));
                            break;

                    }


                }
            }

            radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                View radioButton = group.findViewById(checkedId);
                int index = group.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        listener.onAnswerSelected(question,1, position, size);
                        // perform your action here
                        break;
                    case 1:
                        listener.onAnswerSelected(question,2, position, size);
                        // perform your action here
                        break;
                    case 2:
                        listener.onAnswerSelected(question, 3,position, size);
                        // perform your action here
                        break;
                    case 3:
                        listener.onAnswerSelected(question, 4,position, size);
                        // perform your action here
                        break;
                    case 4:
                        listener.onAnswerSelected(question,5, position, size);
                        // perform your action here
                        break;
                }
            });

        }
    }

    static class ImageAsQuizViewHolder extends RecyclerView.ViewHolder{
        RadioGroup radioGroup;
        AppCompatTextView textViewQuestion;
        CardView cardView;
        RoundedImageView roundedImageView;
        AppCompatRadioButton rBtn1,rBtn2,rBtn3,rBtn4;
        public ImageAsQuizViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            radioGroup = itemView.findViewById(R.id.rg_options);
            textViewQuestion = itemView.findViewById(R.id.tv_question_text);
            cardView = itemView.findViewById(R.id.view_image_as_question);
            roundedImageView = itemView.findViewById(R.id.imageView1);
            rBtn1 = itemView.findViewById(R.id.rb_option_1);
            rBtn2 = itemView.findViewById(R.id.rb_option_2);
            rBtn3 = itemView.findViewById(R.id.rb_option_3);
            rBtn4 = itemView.findViewById(R.id.rb_option_4);
        }

        public void bind(Question question, Context context, int  position,int size, setOnAnswerSelectedListener listener){
            String string =(position+1)+" ."+question.getText();
            textViewQuestion.setText(Html.fromHtml(string));
            cardView.setPreventCornerOverlap(false);

            try {
                Glide.with(context).load(question.getImageUrl()).into(roundedImageView);
            }
            catch (Exception e){
                e.printStackTrace();

            }

            if(question.getOptions()!=null) {
                if (question.getOptions().size() > 0) {
                    Log.d("OPTIONS", "options size :"+question.getNum_of_options());
                    switch (question.getOptions().size()) {
                        case 2:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setVisibility(View.GONE);
                            rBtn4.setVisibility(View.GONE);
                            break;

                        case 3:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setText(question.getOptions().get(2));
                            rBtn4.setVisibility(View.GONE);
                            break;

                        case 4:
                            rBtn1.setText(question.getOptions().get(0));
                            rBtn2.setText(question.getOptions().get(1));
                            rBtn3.setText(question.getOptions().get(2));
                            rBtn4.setText(question.getOptions().get(3));
                            break;

                    }


                }
            }

            radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                View radioButton = group.findViewById(checkedId);
                int index = group.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        listener.onAnswerSelected(question,1, position, size);
                        // perform your action here
                        break;
                    case 1:
                        listener.onAnswerSelected(question,2, position, size);
                        // perform your action here
                        break;
                    case 2:
                        listener.onAnswerSelected(question, 3,position, size);
                        // perform your action here
                        break;
                    case 3:
                        listener.onAnswerSelected(question, 4,position, size);
                        // perform your action here
                        break;
                    case 4:
                        listener.onAnswerSelected(question,5, position, size);
                        // perform your action here
                        break;
                }
            });

        }

    }

    public interface setOnAnswerSelectedListener{
        void onAnswerSelected(Question question,int selectedOption, int previousPosition, int size);
    }
}
