package com.realstudiosonline.xtraclass.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.CoursesForSchoolDownAdapter;
import com.realstudiosonline.xtraclass.adapters.InstitutionDownAdapter;
import com.realstudiosonline.xtraclass.adapters.SchoolDownAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.databinding.ActivityTellAsMoreAboutYouBinding;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.ClassesForCourseResponse;
import com.realstudiosonline.xtraclass.models.CoursesForSchool;
import com.realstudiosonline.xtraclass.models.CoursesForSchoolResponse;
import com.realstudiosonline.xtraclass.models.Institution;
import com.realstudiosonline.xtraclass.models.InstitutionResponse;
import com.realstudiosonline.xtraclass.models.PatchUser;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.SchoolsResponse;
import com.realstudiosonline.xtraclass.models.StudentProfile;
import com.realstudiosonline.xtraclass.models.StudentProfileResponse;
import com.realstudiosonline.xtraclass.models.UpdateUserResponse;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

public class TellAsMoreAboutYouActivity extends AppCompatActivity {

    private ActivityTellAsMoreAboutYouBinding binding;

    AutoCompleteTextView spinnerInstitution, spinnerSchool, spinnerCourse;
    private InstitutionDownAdapter institutionDownAdapter;
    private SchoolDownAdapter schoolDownAdapter;
    private CoursesForSchoolDownAdapter coursesForSchoolDownAdapter;
    private Institution selectedInstitution;
    private School selectedSchool;
    private CoursesForSchool selectedCoursesForSchool;
    private ArrayList<Institution> institutions;
    private ArrayList<School> schools;

    PopupWindow popupWindowCoursesForSchools, popupWindowInstitutions,popupWindowSchools;
    private ArrayList<CoursesForSchool> coursesForSchools;

    private final Institution defaultInstitution = new Institution("-1", "Select Institution");

    private final School defaultSchool = new School("-1",0,"Select School","-1","-1");


    private final CoursesForSchool  defaultCoursesForSchool = new CoursesForSchool("-1", "Select Course", "-1", "-1");

    private final  ClassesForCourse defaultClassesForCourse = new ClassesForCourse("Select Class", "-1", "-1", "-1", "Select School");

    private User mUser;
    private View view_course;
    String mToken;

    TextInputEditText editTextFullName, editTextEmail;

    private AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityTellAsMoreAboutYouBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        initView();



        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getApplicationContext());

        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
        setUpInstitution();

        setUpSchool();

        setUpCoursesForSchool();


        getListOfInstitutes("open");

    }


    private void getListOfInstitutes(String school){
        ApiClient.getApiClient().getInstance(getApplicationContext()).getListOfInstitutes(school)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<InstitutionResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<InstitutionResponse> institutionResponseResponse) {
                if(institutionResponseResponse.isSuccessful()){
                    InstitutionResponse institutionResponse = institutionResponseResponse.body();
                    if(institutionResponse!=null) {
                        for (Institution institution: institutionResponse.getData()) {

                            if(!institution.getId().equalsIgnoreCase("a8889d0b-46d5-490c-96b4-64288fe0ecd3")){
                                institutions.add(institution);
                            }

                        }
                        // institutions.addAll(institutionResponse.getData());
                        institutions.add(0,defaultInstitution);
                        institutionDownAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }


    private void setUpCoursesForSchool(){
        coursesForSchools = new ArrayList<>();
        coursesForSchoolDownAdapter = new CoursesForSchoolDownAdapter(TellAsMoreAboutYouActivity.this,R.layout.institution_view,coursesForSchools);
        spinnerCourse.setAdapter(coursesForSchoolDownAdapter);
    }
    private void provideCoursesForSchoolPopWindow(View it, Context context,CoursesForSchoolDownAdapter adapter){
        if(popupWindowCoursesForSchools!=null){
            popupWindowCoursesForSchools.dismiss();
            popupWindowCoursesForSchools = null;
        }

        popupWindowCoursesForSchools = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowCoursesForSchools.setOutsideTouchable(true);
        popupWindowCoursesForSchools.setFocusable(true);
        popupWindowCoursesForSchools.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            selectedCoursesForSchool = adapter.getItem(position);
            spinnerCourse.setText(selectedCoursesForSchool.getName());
            if(!selectedCoursesForSchool.getId().equalsIgnoreCase("-1")){
                popupWindowCoursesForSchools.dismiss();;
               // PreferenceHelper.getPreferenceHelperInstance().setCoursesForSchool(getApplicationContext(),selectedCoursesForSchool);
                getListOfClassesForCourse(selectedCoursesForSchool.getId());
            }


        });
        popupWindowCoursesForSchools.setContentView(parent_view);
        popupWindowCoursesForSchools.setElevation(20);

        if(popupWindowCoursesForSchools!=null) {

            popupWindowCoursesForSchools.showAsDropDown(it, 0,-it.getHeight());
        }

    }






/*{
"class_id":"897a5ab9-24b9-4511-9a32-392667fe2a95",
"course_id":"e93e7590-375a-4d92-8d69-45e8f89b44b4",
"email":"mobile@realstudiosonline.com",
"first_name":"Michael",
"institute_id":"a5d479e5-5e27-4e5f-81c4-12f06ab4e528",
"last_name":"Dovoh",
"school_id":"897a5ab9-24b9-4511-9a32-392667fe2a95"
}*/
    private void setUpInstitution(){
        institutions = new ArrayList<>();
        institutionDownAdapter = new InstitutionDownAdapter(TellAsMoreAboutYouActivity.this,R.layout.institution_view,institutions);
        spinnerInstitution.setAdapter(institutionDownAdapter);
    }
    private void provideSetUpInstitutionPopWindow(View it, Context context,InstitutionDownAdapter adapter){
        if(popupWindowInstitutions!=null){
            popupWindowInstitutions.dismiss();
            popupWindowInstitutions = null;
        }

        popupWindowInstitutions = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowInstitutions.setOutsideTouchable(true);
        popupWindowInstitutions.setFocusable(true);
        popupWindowInstitutions.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            popupWindowInstitutions.dismiss();
            schools.clear();
            schoolDownAdapter.notifyDataSetChanged();
            coursesForSchools.clear();
            coursesForSchoolDownAdapter.notifyDataSetChanged();
            selectedInstitution =adapter.getItem(position);
            spinnerInstitution.setText(selectedInstitution.getName());
            if(!selectedInstitution.getId().equalsIgnoreCase("-1")){
               // PreferenceHelper.getPreferenceHelperInstance().setSelectedInstitution(getApplicationContext(), selectedInstitution);

                if(selectedInstitution.getId().equalsIgnoreCase("246db292-0af8-49d8-94ad-77f1aa61f338")||
                        selectedInstitution.getId().equalsIgnoreCase("a5d479e5-5e27-4e5f-81c4-12f06ab4e528")
                ){
                    view_course.setVisibility(View.GONE);
                    spinnerCourse.setVisibility(View.GONE);
                }

                else {
                    view_course.setVisibility(View.VISIBLE);
                    spinnerCourse.setVisibility(View.VISIBLE);
                }
                getListOfSchoolsForInstitution(selectedInstitution.getId());
            }

        });
        popupWindowInstitutions.setContentView(parent_view);
        popupWindowInstitutions.setElevation(20);

        if(popupWindowInstitutions!=null) {

            popupWindowInstitutions.showAsDropDown(it, 0,-it.getHeight());
        }

    }









    private void setUpSchool(){
        schools = new ArrayList<>();
        schoolDownAdapter = new SchoolDownAdapter(TellAsMoreAboutYouActivity.this,R.layout.institution_view,schools);
        spinnerSchool.setAdapter(schoolDownAdapter);
    }
    private void provideSetUpSchoolPopWindow(View it, Context context,SchoolDownAdapter adapter){
        if(popupWindowSchools!=null){
            popupWindowInstitutions.dismiss();
            popupWindowInstitutions = null;
        }

        popupWindowSchools = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowSchools.setOutsideTouchable(true);
        popupWindowSchools.setFocusable(true);
        popupWindowSchools.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            popupWindowSchools.dismiss();
            selectedSchool = adapter.getItem(position);

            spinnerSchool.setText(selectedSchool.getName());
            Log.d("spinnerSchool", selectedSchool.toString());

            if(selectedSchool.getInstitution_id().equalsIgnoreCase("246db292-0af8-49d8-94ad-77f1aa61f338")||
                    selectedSchool.getInstitution_id().equalsIgnoreCase("a5d479e5-5e27-4e5f-81c4-12f06ab4e528")
            ){
                view_course.setVisibility(View.GONE);
                spinnerCourse.setVisibility(View.GONE);
                if(selectedSchool.getInstitution_id().equalsIgnoreCase("a5d479e5-5e27-4e5f-81c4-12f06ab4e528")) {

                    selectedCoursesForSchool = new CoursesForSchool("e93e7590-375a-4d92-8d69-45e8f89b44b4", "Basic Education", "", selectedSchool.getId());
                    //PreferenceHelper.getPreferenceHelperInstance().setCoursesForSchool(getApplicationContext(), selectedCoursesForSchool);
                   // PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext(), selectedSchool);
                    getListOfClassesForCourse(selectedCoursesForSchool.getId());
                }
                else if(selectedSchool.getInstitution_id().equalsIgnoreCase("246db292-0af8-49d8-94ad-77f1aa61f338")){
                    //PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext(), selectedSchool);
                    selectedCoursesForSchool = new CoursesForSchool("af550511-ce30-4513-82de-a15bb46f17d7", "Basic Education", "", selectedSchool.getId());
                    //PreferenceHelper.getPreferenceHelperInstance().setCoursesForSchool(getApplicationContext(), selectedCoursesForSchool);
                    getListOfClassesForCourse(selectedCoursesForSchool.getId());
                }
            }
            else {
                view_course.setVisibility(View.VISIBLE);
                spinnerCourse.setVisibility(View.VISIBLE);
                if(!selectedSchool.getId().equalsIgnoreCase("-1")){
                    //PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext(), selectedSchool);
                    //Toast.makeText(this, "SHS ", Toast.LENGTH_SHORT).show();
                    view_course.setVisibility(View.VISIBLE);
                    spinnerCourse.setVisibility(View.VISIBLE);
                    //fetchSchoolDetails(selectedSchool.getId());
                    getListOfCoursesForSchool(selectedSchool.getId());

                }
            }

        });
        popupWindowSchools.setContentView(parent_view);
        popupWindowSchools.setElevation(20);

        if(popupWindowSchools!=null) {

            popupWindowSchools.showAsDropDown(it, 0,-it.getHeight());
        }

    }






    private void getStudentProfile(String token){
        ApiClient.getApiClient().getInstance(getApplicationContext()).getStudentProfile("Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<StudentProfileResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<StudentProfileResponse> studentProfileResponseResponse) {

                if(studentProfileResponseResponse.isSuccessful()){
                    if(studentProfileResponseResponse.body()!=null) {
                        if(studentProfileResponseResponse.body().getData()!=null) {
                            StudentProfile profile = studentProfileResponseResponse.body().getData();
                            User user = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
                            user.setSchool_id(profile.getSchool_id());
                            user.setCourse_id(profile.getCourse_id());
                            user.setInstitution_id(profile.getInstitution_id());
                            user.setEmail(profile.getEmail());
                            user.setName(profile.getFirst_name() +" " +profile.getLast_name());
                            user.setUsername(profile.getUsername());
                            PreferenceHelper.getPreferenceHelperInstance().setUser(getApplicationContext(), user);
                            PreferenceHelper.getPreferenceHelperInstance().setIsRegistered(getApplicationContext(),true);
                        }
                    }
                    startActivity(new Intent(TellAsMoreAboutYouActivity.this, MainActivity.class));
                    finish();
                }



            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });
    }

    private void patchProfile(String firstName, String lastName, String email, String username, String institution_id,String school_id,
                              String course_id, String class_id,String token){
        showDialogProgressDialog();
        PatchUser patchUser = new PatchUser(firstName,lastName, email, username, institution_id,school_id,course_id, class_id);
        ApiClient.getApiClient().getInstance(getApplicationContext()).patchProfile(patchUser, "Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<UpdateUserResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<UpdateUserResponse> updateUserResponseResponse) {

                cancelDialogProgressDialog();

                if(updateUserResponseResponse.isSuccessful()){
                    getStudentProfile(token);


                }

            }

            @Override
            public void onError(@NonNull Throwable e) {

                cancelDialogProgressDialog();
            }
        });
    }

    private static boolean hasLowerCase(CharSequence data) {
        String password = String.valueOf(data);
        boolean hasLowercase = !password.equals(password.toUpperCase());
        return hasLowercase;
    }
   /* public final boolean containsDigit(String s) {
        boolean containsLower = false;


        Pattern pattern =  Pattern.compile("r\"(?=.*[a-z])(?=.*[A-Z])\\w+\"");
        Matcher m = pattern.matcher(s);
        if (m.find()) {
            return true;
        } else {
            System.out.println("No match.");
        }
        return containsLower;
    }*/
    private void initView(){

        editTextEmail = binding.tellUsMore.edtEmail;

        editTextFullName = binding.tellUsMore.edtFullName;

        binding.tellUsMore.btnStartLearning.setOnClickListener(v -> {
            if(editTextFullName.getText()!=null && editTextEmail.getText()!=null) {
                String[] names = editTextFullName.getText().toString().split("\\s+");
                String email = editTextEmail.getText().toString();
                String firstName = "" , lastName= " ";

                if(names.length>1) {
                    lastName = names[1];
                    firstName = names[0];
                }
                else if(names.length==1){
                    firstName = names[0];
                }

                if(mUser!=null) {
                    Random r = new Random();
                    char c = (char)(r.nextInt(26) + 'a');

                    ClassesForCourse classesForCourse = PreferenceHelper.getPreferenceHelperInstance().getSelectedClassesForCourse(getApplicationContext());
                    patchProfile(firstName, lastName, email, mUser.getUsername()==null ? "username": (hasLowerCase(mUser.getUsername()) ? mUser.getUsername() : c+ mUser.getUsername()) , selectedInstitution.getId(), selectedSchool.getId(),
                            selectedCoursesForSchool.getId(), classesForCourse.getUuid(), mToken);
                }
            }
        });

        view_course =  binding.tellUsMore.viewCourse;

        spinnerInstitution =  binding.tellUsMore.spinnerInstitution;


        spinnerInstitution.setKeyListener(null);
        spinnerInstitution.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideSetUpInstitutionPopWindow(v,getApplicationContext(),institutionDownAdapter);

            }
        });
        spinnerInstitution.setOnClickListener(v -> provideSetUpInstitutionPopWindow(v,getApplicationContext(),institutionDownAdapter));



        spinnerSchool =  binding.tellUsMore.spinnerClass;
        spinnerSchool.setKeyListener(null);
        spinnerSchool.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideSetUpSchoolPopWindow(v,getApplicationContext(),schoolDownAdapter);
            }
        });
        spinnerSchool.setOnClickListener(v -> provideSetUpSchoolPopWindow(v,getApplicationContext(),schoolDownAdapter));

        spinnerCourse = binding.tellUsMore.spinnerCourse;

        spinnerCourse.setKeyListener(null);
        spinnerCourse.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideCoursesForSchoolPopWindow(v,getApplicationContext(),coursesForSchoolDownAdapter);
            }
        });
        spinnerCourse.setOnClickListener(v -> provideCoursesForSchoolPopWindow(v,getApplicationContext(),coursesForSchoolDownAdapter));

    }

    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(TellAsMoreAboutYouActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }

    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }

    private void getListOfCoursesForSchool(String schoolId){
        coursesForSchools.clear();
        ApiClient.getApiClient().getInstance(getApplicationContext()).getListOfCoursesForSchool(schoolId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<CoursesForSchoolResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<CoursesForSchoolResponse> coursesForSchoolResponseResponse) {
                if(coursesForSchoolResponseResponse.isSuccessful()){
                    if(coursesForSchoolResponseResponse.body()!=null) {
                        CoursesForSchoolResponse coursesForSchoolResponse = coursesForSchoolResponseResponse.body();
                        Log.d("ListOfCoursesForSchool", coursesForSchoolResponse.getData().toString());
                        coursesForSchools.addAll(coursesForSchoolResponse.getData());
                        coursesForSchools.add(0, defaultCoursesForSchool);
                        coursesForSchoolDownAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }

    private void getListOfClassesForCourse(String courseId){

        ApiClient.getApiClient().getInstance(getApplicationContext())
                .getListOfClassesForCourse(courseId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassesForCourseResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<ClassesForCourseResponse> classesForCourseResponseResponse) {
                if(classesForCourseResponseResponse.isSuccessful()){
                    if(classesForCourseResponseResponse.body()!=null) {
                        ClassesForCourseResponse classesForCourseResponse = classesForCourseResponseResponse.body();
                        Log.d("ListOfClassesForCourse", "" + classesForCourseResponse.toString());


                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }


    private void getListOfSchoolsForInstitution(String institutionId){
        schools.clear();
        ApiClient.getApiClient().getInstance(getApplicationContext()).getListOfSchoolsForInstitution(institutionId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<SchoolsResponse>>() {
                    @Override
                    public void onSuccess(@NonNull Response<SchoolsResponse> schoolsResponseResponse) {
                        if(schoolsResponseResponse.isSuccessful()){
                            SchoolsResponse  schoolsResponse =  schoolsResponseResponse.body();
                            if(schoolsResponse!=null){
                                schools.addAll(schoolsResponse.getData());
                                schools.add(0,defaultSchool);
                                schoolDownAdapter.notifyDataSetChanged();
                            }
                        }
                        else {
                            Toast.makeText(TellAsMoreAboutYouActivity.this, "Error occurred", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}