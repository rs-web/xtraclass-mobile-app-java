package com.realstudiosonline.xtraclass.models;

public class TabSelected {
    private int tab;

    public TabSelected(int tab) {
        this.tab = tab;
    }

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }
}
