package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Library;
import com.realstudiosonline.xtraclass.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

public class LibraryItemAdapter extends RecyclerView.Adapter<LibraryItemAdapter.LibraryItemViewHolder> {

    private List<Library> libraryList;
    private Context context;
    private setOnLibraryItemCLickListener listener;

    public LibraryItemAdapter(List<Library> libraryList,
                              Context context,
                              setOnLibraryItemCLickListener listener) {
        this.libraryList = libraryList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public LibraryItemViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new LibraryItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_library_view,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull LibraryItemAdapter.LibraryItemViewHolder holder, int position) {
        Library library = libraryList.get(position);
        holder.bind(library,context,listener);
    }



    @Override
    public int getItemCount() {
        return libraryList == null ? 0 : libraryList.size();
    }


    protected static class LibraryItemViewHolder extends RecyclerView.ViewHolder{

        AppCompatImageView circularImageView;
        AppCompatTextView tv_library_title;

        public LibraryItemViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            circularImageView = itemView.findViewById(R.id.img_lesson_play_icon);
            tv_library_title = itemView.findViewById(R.id.tv_lesson_title);
        }

        public void bind(Library library, Context context,setOnLibraryItemCLickListener listener){


            if(library.isSelected()){
                circularImageView.setImageResource(R.drawable.play_icon_blue);
                itemView.setBackgroundColor(context.getResources().getColor(R.color.colorSelectedLesson));
            }

            else {
                itemView.setBackgroundColor(context.getResources().getColor(R.color.colorViewPagerBackground));
                circularImageView.setImageResource(R.drawable.eye_icon_new);
            }

            tv_library_title.setText(library.getDescription());
            itemView.setOnClickListener(v -> listener.onLibraryClicked(library));

             if(library.getFile_type().equalsIgnoreCase("pdf") || library.getFile_type().equalsIgnoreCase("note")){
                Glide.with(context).load(R.drawable.files).into(circularImageView);
            }
             else if(library.getFile_type().equalsIgnoreCase("audio")){
                 Glide.with(context).load(R.drawable.ic_music_note_24).into(circularImageView);
             }
        }
    }

    public interface setOnLibraryItemCLickListener{
        void onLibraryClicked(Library library);
    }
}
