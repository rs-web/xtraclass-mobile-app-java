package com.realstudiosonline.xtraclass.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.util.Timer;
import java.util.TimerTask;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class SplashScreenActivity extends AppCompatActivity {

    AppCompatImageView img_splash;

    User mUser;
    String mToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getApplicationContext());

        img_splash = findViewById(R.id.img_splash);

        Glide.with(this).load(R.drawable.background_blur).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(img_splash);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

               //startActivity(new Intent(SplashScreenActivity.this,ActivationSuccessful.class));
                if(!TextUtils.isEmpty(mToken) && mUser!=null) {
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                }
                else {
                    startActivity(new Intent(SplashScreenActivity.this, AuthActivity.class));
                }
               // overridePendingTransition(0, 0);
                finish();
            }
        }, 3000);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}

/*
*
*
*
*
*  private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    * */