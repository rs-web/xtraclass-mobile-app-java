package com.realstudiosonline.xtraclass.models;

public class SchoolDetailsResponse {
    private int statusCode;
    private String message;
    private School data;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public School getData() {
        return data;
    }

    public void setData(School data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SchoolDetailsResponse{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}


/*
* id": "264e64d4-3e88-4153-bbf4-53e9199da1a3",
        "name": "Class 5",
        "logo_url": "https://dextraclass.com/uploads/schools/noimage.jpg",
        "institution_id": "a5d479e5-5e27-4e5f-81c4-12f06ab4e528"*/