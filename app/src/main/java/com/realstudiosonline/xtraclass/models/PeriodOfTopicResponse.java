package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class PeriodOfTopicResponse extends DataResponse {
    private List<PeriodOfTopic> data;

    public List<PeriodOfTopic> getData() {
        return data;
    }

    public void setData(List<PeriodOfTopic> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PeriodOfTopicResponse{" +
                "data=" + data +
                '}';
    }
}

/*
* {
    "statusCode": 200,
    "message": "",
    "data": [
        {
            "id": "8022e639-9a09-474b-b3b6-290a6e1bbcad",
            "number": 1,
            "title": "Lesson 1"
        },
        {
            "id": "4304f62e-4e77-4338-99a8-4c2d7922bdd2",
            "number": 10,
            "title": "Lesson 10"
        },
        {
            "id": "40d3e1ff-a0ca-4ada-852d-5ed91daa7234",
            "number": 2,
            "title": "Lesson 2"
        },
        {
            "id": "72b8fe6c-3c2a-403b-9359-711c6b5babaf",
            "number": 3,
            "title": "Lesson 3"
        },
        {
            "id": "9225c4d6-9a82-4b21-922b-bcd001d1edf7",
            "number": 4,
            "title": "Lesson 4"
        },
        {
            "id": "abbe1021-f487-4dbc-890d-49dc4540d08a",
            "number": 5,
            "title": "Lesson 5"
        },
        {
            "id": "7e2175d8-afa7-40ee-8ec0-e539fa4d0c93",
            "number": 6,
            "title": "Lesson 6"
        },
        {
            "id": "6ffbe0a3-e340-4b98-a9fe-b989c0d8b366",
            "number": 7,
            "title": "Lesson 7"
        },
        {
            "id": "5daf79c0-3f38-4527-af45-5791e164246f",
            "number": 8,
            "title": "Lesson 8"
        },
        {
            "id": "61d157a7-88a5-41e2-887d-f3f0aa6783a4",
            "number": 9,
            "title": "Lesson 9"
        }
    ]
}*/