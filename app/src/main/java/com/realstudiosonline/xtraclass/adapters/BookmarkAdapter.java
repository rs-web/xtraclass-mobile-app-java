package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.BookmarkItem;

import java.util.List;

public class BookmarkAdapter extends BaseAdapter  {

    private Context context;
    private List<BookmarkItem> videoQualities;

    /**
     * @param context      Context
     * @param videoQualities Original list used to compare in constraints.
     */
    public BookmarkAdapter(Context context, List<BookmarkItem> videoQualities) {
        this.context = context;
        this.videoQualities = videoQualities;
    }

    @Override
    public int getCount() {
        return videoQualities.size(); // Return the size of the suggestions list.
    }

    @Override
    public BookmarkItem getItem(int position) {
        return videoQualities.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
         /*   convertView = inflater.inflate(R.layout.adapter_autotext,
                    parent,
                    false);*/
            if(position == 0){
                convertView = inflater.inflate(R.layout.adapter_autotext_top, parent, false);
            }
            else if(position == videoQualities.size()-1){
                convertView = inflater.inflate(R.layout.adapter_autotext_bottom, parent, false);
            }
            else {
                convertView = inflater.inflate(R.layout.adapter_autotext, parent, false);
            }

            holder = new ViewHolder();
            holder.autoText = (AppCompatTextView) convertView.findViewById(R.id.autoText);
            holder.qualityIcon = (AppCompatImageView) convertView.findViewById(R.id.img_quality_icon);
            holder.cardView = (CardView) convertView.findViewById(R.id.card_video_quality);
            holder.view = (LinearLayout)convertView.findViewById(R.id.view_des);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        if(position ==0){
            holder.cardView.setBackgroundResource(R.drawable.card_top_radius);
        }
        else if(position == videoQualities.size()-1){
            holder.cardView.setBackgroundResource(R.drawable.card_bottom_radius);
        }
        holder.autoText.setText(videoQualities.get(position).getName());
        Glide.with(context).load(videoQualities.get(position).getIcon()).into(holder.qualityIcon);


        return convertView;
    }

    public int convertDpToPixelInt(float dp, Context context) {
        return (int) (dp * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }


    private static class ViewHolder {
        AppCompatTextView autoText;
        AppCompatImageView qualityIcon;
        CardView cardView;
        View view;
    }
}
