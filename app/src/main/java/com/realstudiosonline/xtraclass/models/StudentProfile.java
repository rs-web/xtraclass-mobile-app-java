package com.realstudiosonline.xtraclass.models;

public class StudentProfile {
    private String email;
    private String username;
    private int usertype;
    private String first_name;
    private String last_name;
    private String phone;
    private int  country;
    private String profile_image;
    private String country_name;
    private String institution_id;
    private String school_id;
    private String course_id;
    private String class_id;
    private String department_id;
    private String role;
    private int stat_classes_watched;
    private int stat_questions;
    private int stat_answers;
    private int stat_downloads;
    private int classes_hosted;
    private int notes_added;
    private String id;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUsertype() {
        return usertype;
    }

    public void setUsertype(int usertype) {
        this.usertype = usertype;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getInstitution_id() {
        return institution_id;
    }

    public void setInstitution_id(String institution_id) {
        this.institution_id = institution_id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getStat_classes_watched() {
        return stat_classes_watched;
    }

    public void setStat_classes_watched(int stat_classes_watched) {
        this.stat_classes_watched = stat_classes_watched;
    }

    public int getStat_questions() {
        return stat_questions;
    }

    public void setStat_questions(int stat_questions) {
        this.stat_questions = stat_questions;
    }

    public int getStat_answers() {
        return stat_answers;
    }

    public void setStat_answers(int stat_answers) {
        this.stat_answers = stat_answers;
    }

    public int getStat_downloads() {
        return stat_downloads;
    }

    public void setStat_downloads(int stat_downloads) {
        this.stat_downloads = stat_downloads;
    }

    public int getClasses_hosted() {
        return classes_hosted;
    }

    public void setClasses_hosted(int classes_hosted) {
        this.classes_hosted = classes_hosted;
    }

    public int getNotes_added() {
        return notes_added;
    }

    public void setNotes_added(int notes_added) {
        this.notes_added = notes_added;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "StudentProfile{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", usertype=" + usertype +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", phone='" + phone + '\'' +
                ", country=" + country +
                ", profile_image='" + profile_image + '\'' +
                ", country_name='" + country_name + '\'' +
                ", institution_id='" + institution_id + '\'' +
                ", school_id='" + school_id + '\'' +
                ", course_id='" + course_id + '\'' +
                ", class_id='" + class_id + '\'' +
                ", department_id='" + department_id + '\'' +
                ", role='" + role + '\'' +
                ", stat_classes_watched=" + stat_classes_watched +
                ", stat_questions=" + stat_questions +
                ", stat_answers=" + stat_answers +
                ", stat_downloads=" + stat_downloads +
                ", classes_hosted=" + classes_hosted +
                ", notes_added=" + notes_added +
                ", id='" + id + '\'' +
                '}';
    }
}
