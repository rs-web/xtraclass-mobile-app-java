package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Favorite;
import com.realstudiosonline.xtraclass.models.History;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.realstudiosonline.xtraclass.utils.TimeAgo.getTimeAgo;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {

    List<Favorite> favorites;
    Context context;
    SetFavoriteClickListener listener;

    public FavoriteAdapter(List<Favorite> favorites, Context context, SetFavoriteClickListener listener) {
        this.favorites = favorites;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public FavoriteAdapter.FavoriteViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new FavoriteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull FavoriteAdapter.FavoriteViewHolder holder, int position) {
        Favorite favorite = favorites.get(position);
        holder.bind(favorite, listener);
    }

    @Override
    public int getItemCount() {
        return favorites ==null ? 0: favorites.size();
    }


    public static class FavoriteViewHolder extends RecyclerView.ViewHolder{
        View historyView;
        AppCompatTextView tv_lesson_title,tv_school_name,tv_date_time;
        public FavoriteViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            historyView = itemView.findViewById(R.id.view_history);
            tv_lesson_title = itemView.findViewById(R.id.tv_lesson_title);
            tv_school_name = itemView.findViewById(R.id.tv_school_name);
            tv_date_time = itemView.findViewById(R.id.tv_date_time);
        }

        public void bind(Favorite favorite, SetFavoriteClickListener listener){
            try {
                String formattedTime = getTimeAgo(favorite.getDate().getMillis());
                tv_date_time.setText(formattedTime);
            }
            catch (Exception e){
                e.printStackTrace();
            }

            tv_lesson_title.setText(favorite.getSubject());
            tv_school_name.setText(favorite.getSchoolName());

            historyView.setOnClickListener(v -> listener.onFavoriteClick(favorite));
        }
    }

    public interface SetFavoriteClickListener {
        void onFavoriteClick(Favorite favorite);
    }
}
