package com.realstudiosonline.xtraclass.models;

public class Plan {

    /* "plan": {
            "id": 0,
            "name": "",
            "duration": "",
            "amount": 0
        }*/
    private int id;
    private String name;
    private String duration;
    private String amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Plan{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", duration='" + duration + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }


    /*  "id": 1,
            "name": "Monthly",
            "duration": "1 month",
            "amount": "25"*/
}
