package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.GenericItem;

import java.util.List;
import java.util.Locale;

public class GenericIconAdapter extends ArrayAdapter<GenericItem> {
    Context context;
    List<GenericItem> genericItems;

    public GenericIconAdapter(@NonNull Context context, int resource,
                              @NonNull List<GenericItem> genericItems) {
        super(context, resource, genericItems);
        this.genericItems = genericItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return genericItems == null ? 0 : genericItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_type_dropdown, parent, false);
            // get all UI view
            /*convertView.setOnClickListener(v -> {
                View root = parent.getRootView();
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            });*/

            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }
        GenericItem genericItem = genericItems.get(position);
        String  countryName = new Locale("", genericItem.getName()).getDisplayName();
        holder.tvCountry.setText(countryName);
        Glide.with(context).load(genericItem.getIcon()).into(holder.ivICon);
        return convertView;

    }

    private static class ViewHolder {

        TextView tvCountry;
        AppCompatImageView ivICon;

        public ViewHolder(View v) {
            tvCountry = (TextView) v.findViewById(R.id.tvCountry);
            ivICon = (AppCompatImageView)v.findViewById(R.id.img_quality_icon);
        }
    }




}
