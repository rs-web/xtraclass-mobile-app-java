package com.realstudiosonline.xtraclass.models;

public class ClassesForCourse {


    /*
    *private String id;
    private String name;
    private String full_name;
    private String course_id;
    private String school_id;
    * */
    private String class_name;
    private String uuid;
    private String course_id;
    private String school_id;
    private String school_name;

    public ClassesForCourse() {
    }

    public ClassesForCourse(String class_name, String uuid, String course_id, String school_id, String school_name) {
        this.class_name = class_name;
        this.uuid = uuid;
        this.course_id = course_id;
        this.school_id = school_id;
        this.school_name = school_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    @Override
    public String toString() {
        return "ClassesForCourse{" +
                "class_name='" + class_name + '\'' +
                ", uuid='" + uuid + '\'' +
                ", course_id='" + course_id + '\'' +
                ", school_id='" + school_id + '\'' +
                ", school_name='" + school_name + '\'' +
                '}';
    }

   /*    "class_name": "Core Mathematics",
            "uuid": "c1698279-ffa9-42a6-abee-ff75b9bc0898",
            "course_id": "619cda2c-6cc7-402f-aa13-5ae3b9169a83",
            "school_id": "ff313bb0-d3e4-4507-a2f0-078931890784",
            "school_name": "SHS 2"*/

}
