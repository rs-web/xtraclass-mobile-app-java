package com.realstudiosonline.xtraclass.utils.player;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import androidx.annotation.AttrRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.RendererCapabilities;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.SelectionOverride;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.ui.DefaultTrackNameProvider;
import com.google.android.exoplayer2.ui.TrackNameProvider;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.material.textview.MaterialTextView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;

import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_1080P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_160P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_240P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_360P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_480P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_720P;
import static com.realstudiosonline.xtraclass.utils.Constants.convertDpToPixelInt;

/**
 * A view for making track selections.
 */
public class MyTrackSelectionView extends LinearLayout {

    private static final String TAG = "MyTrackSelectionView";

    private final int selectableItemBackgroundResourceId;
    private final LayoutInflater inflater;
    private final View disableView;
    private final View defaultView;
    private final ComponentListener componentListener;

    private boolean allowAdaptiveSelections;

    private TrackNameProvider trackNameProvider;
    private View[][] trackViews;

    private DefaultTrackSelector trackSelector;
    private int rendererIndex;
    private TrackGroupArray trackGroups;
    private boolean isDisabled;
    private @Nullable
    SelectionOverride override;
    //private final String playingString = "<font color=#2196F3> &nbsp;(playing) &nbsp; </font>";
    private static long currentBitrate;
    private static Activity context;

    public static PopupWindow popupWindow;

    public static Pair<PopupWindow, MyTrackSelectionView> getDialog(
            Activity activity,
            DefaultTrackSelector trackSelector,
            int rendererIndex,
            long currentBitrate) {

        context = activity;

        MyTrackSelectionView.currentBitrate = currentBitrate;

        popupWindow = new PopupWindow(convertDpToPixelInt(230,context), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
       // popupWindow.setBackgroundDrawable(backgroundDrawable);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // Inflate with the builder's context to ensure the correct style is used.

        View dialogView = layoutInflater.inflate(R.layout.exo_track_selection_dialog, null,false);
        final MyTrackSelectionView selectionView = dialogView.findViewById(R.id.exo_track_selection_view);

        popupWindow.setContentView(dialogView);
        popupWindow.setElevation(20);

        selectionView.init(trackSelector, rendererIndex);


        return Pair.create(popupWindow, selectionView);
    }

    public MyTrackSelectionView(Context context) {
        this(context, null);
    }

    public MyTrackSelectionView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @SuppressWarnings("nullness")
    public MyTrackSelectionView(
            Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray attributeArray =
                context
                        .getTheme()
                        .obtainStyledAttributes(new int[]{android.R.attr.selectableItemBackground});
        selectableItemBackgroundResourceId = attributeArray.getResourceId(0, 0);
        Log.e(TAG, "MyTrackSelectionView: " + selectableItemBackgroundResourceId);
        attributeArray.recycle();


        inflater = LayoutInflater.from(context);
        componentListener = new ComponentListener();
        trackNameProvider = new DefaultTrackNameProvider(getResources());

        // View for disabling the renderer.
        disableView =
                (View)
                        inflater.inflate(R.layout.adapter_quality, this, false);
        //disableView.setBackgroundResource(selectableItemBackgroundResourceId);

        AppCompatTextView textView = disableView.findViewById(R.id.autoText);
        AppCompatImageView imageView = disableView.findViewById(R.id.img_quality_icon);
        Glide.with(context).load(R.drawable.au).into(imageView);
        textView.setText(R.string.exo_track_selection_none);
        disableView.setEnabled(false);
        disableView.setFocusable(true);
        disableView.setOnClickListener(componentListener);
       // disableView.setVisibility(View.GONE);
        addView(disableView);
        // Divider view.

        // View for clearing the override to allow the selector to use its default selection logic.
        defaultView =
                (View)
                        inflater.inflate(R.layout.adapter_quality, this, false);
       // defaultView.setBackgroundResource(selectableItemBackgroundResourceId);
        textView = defaultView.findViewById(R.id.autoText);
        imageView = defaultView.findViewById(R.id.img_quality_icon);
        Glide.with(context).load(R.drawable.au).into(imageView);
        textView.setText(R.string.exo_track_selection_auto);
        defaultView.setEnabled(false);
        defaultView.setFocusable(true);
        defaultView.setOnClickListener(componentListener);
        addView(defaultView);
    }

    /**
     * Sets whether adaptive selections (consisting of more than one track) can be made using this
     * selection view.
     *
     * <p>For the view to enable adaptive selection it is necessary both for this feature to be
     * enabled, and for the target renderer to support adaptation between the available tracks.
     *
     * @param allowAdaptiveSelections Whether adaptive selection is enabled.
     */
    public void setAllowAdaptiveSelections(boolean allowAdaptiveSelections) {
        if (this.allowAdaptiveSelections != allowAdaptiveSelections) {
            this.allowAdaptiveSelections = allowAdaptiveSelections;
            updateViews();
        }
    }

    /**
     * Sets whether an option is available for disabling the renderer.
     *
     * @param showDisableOption Whether the disable option is shown.
     */
    public void setShowDisableOption(boolean showDisableOption) {
        disableView.setVisibility(showDisableOption ? View.VISIBLE : View.GONE);
    }

    /**
     * Sets the {@link TrackNameProvider} used to generate the user visible name of each track and
     * updates the view with track names queried from the specified provider.
     *
     * @param trackNameProvider The {@link TrackNameProvider} to use.
     */
    public void setTrackNameProvider(TrackNameProvider trackNameProvider) {
        this.trackNameProvider = Assertions.checkNotNull(trackNameProvider);
        updateViews();
    }

    /**
     * Initialize the view to select tracks for a specified renderer using a {@link
     * DefaultTrackSelector}.
     *
     * @param trackSelector The {@link DefaultTrackSelector}.
     * @param rendererIndex The index of the renderer.
     */
    public void init(DefaultTrackSelector trackSelector, int rendererIndex) {
        this.trackSelector = trackSelector;
        this.rendererIndex = rendererIndex;
        updateViews();
    }

    // Private methods.

    private void updateViews() {
        // Remove previous per-track views.
        for (int i = getChildCount() - 1; i >= 3; i--) {
            removeViewAt(i);
        }

        MappingTrackSelector.MappedTrackInfo trackInfo =
                trackSelector == null ? null : trackSelector.getCurrentMappedTrackInfo();
        if (trackSelector == null || trackInfo == null) {
            // The view is not initialized.
            disableView.setEnabled(false);
            defaultView.setEnabled(false);
            return;
        }
        disableView.setEnabled(true);
        defaultView.setEnabled(true);

        trackGroups = trackInfo.getTrackGroups(rendererIndex);

        DefaultTrackSelector.Parameters parameters = trackSelector.getParameters();
        isDisabled = parameters.getRendererDisabled(rendererIndex);
        override = parameters.getSelectionOverride(rendererIndex, trackGroups);

        // Add per-track views.
        trackViews = new View[trackGroups.length][];
        for (int groupIndex = 0; groupIndex < trackGroups.length; groupIndex++) {


            TrackGroup group = trackGroups.get(groupIndex);
           // Log.d("TRACK_GROUP","BITRATE:"+group.getFormat(groupIndex).bitrate);

            boolean enableAdaptiveSelections =
                    allowAdaptiveSelections
                            && trackGroups.get(groupIndex).length > 1
                            && trackInfo.getAdaptiveSupport(rendererIndex, groupIndex, false)
                            != RendererCapabilities.ADAPTIVE_NOT_SUPPORTED;
            trackViews[groupIndex] = new View[group.length];
            for (int trackIndex = 0; trackIndex < group.length; trackIndex++) {
                int trackViewLayoutId =
                        enableAdaptiveSelections
                                ?R.layout.adapter_quality
                                : R.layout.adapter_quality;

                View trackView =
                        (View) inflater.inflate(trackViewLayoutId, this, false);

                AppCompatImageView imageView = trackView.findViewById(R.id.img_quality_icon);


              //  trackView.setBackgroundResource(selectableItemBackgroundResourceId);

                AppCompatTextView autoText = trackView.findViewById(R.id.autoText);

                autoText.setText(Html.fromHtml(buildBitrateString(group.getFormat(trackIndex),imageView)));
                int bitrate = group.getFormat(trackIndex).bitrate;

                if(bitrate<=BITRATE_360P){
                    trackView.setVisibility(GONE);
                }
                if(bitrate>BITRATE_720P){
                    trackView.setVisibility(GONE);
                }

                if (trackInfo.getTrackSupport(rendererIndex, groupIndex, trackIndex)
                        == RendererCapabilities.FORMAT_HANDLED) {
                    trackView.setFocusable(true);
                    trackView.setTag(Pair.create(groupIndex, trackIndex));
                    trackView.setOnClickListener(componentListener);
                    Log.d("BITRATE", "if:"+bitrate);
                    Log.d("BITRATE", "bitrate: "+bitrate);

                } else {
                    //Log.d("BITRATE", "bitrate: "+bitrate);
                    Log.d("BITRATE", "else:"+bitrate);
                    trackView.setFocusable(false);
                    trackView.setEnabled(false);
                }
                trackViews[groupIndex][trackIndex] = trackView;
                addView(trackView);
            }
        }

        updateViewStates();
    }

    private void updateViewStates() {
        //disableView.setChecked(isDisabled);
        //defaultView.setChecked(!isDisabled && override == null);
        for (int i = 0; i < trackViews.length; i++) {
            for (int j = 0; j < trackViews[i].length; j++) {

               // trackViews[i][j].setVisibility(override != null && override.groupIndex == i && override.containsTrack(j));

                AppCompatImageView imageView =  trackViews[i][j].findViewById(R.id.img_quality_icon);

                int drawable;
                if(override != null && override.groupIndex == i && override.containsTrack(j)){
                    drawable = getBitrateDrawableTinted(trackGroups.get(i).getFormat(j));
                    imageView.setImageResource(drawable);
                }
                else {
                    drawable = getBitrateDrawable(trackGroups.get(i).getFormat(j));
                    Glide.with(context).load(drawable).into(imageView);
                }


                //.setChecked(override != null && override.groupIndex == i && override.containsTrack(j));
//                Log.d(TAG, "override.groupIndex" + override.groupIndex + " override.containsTrack(j) " + override.containsTrack(j));
            }
        }
    }


    private void applySelection() {

        DefaultTrackSelector.ParametersBuilder parametersBuilder = trackSelector.buildUponParameters();
        parametersBuilder.setRendererDisabled(rendererIndex, isDisabled);
        if (override != null) {
            parametersBuilder.setSelectionOverride(rendererIndex, trackGroups, override);
        } else {
            parametersBuilder.clearSelectionOverrides(rendererIndex);
        }
        PreferenceHelper.getPreferenceHelperInstance().setTrackGroup(context,override.groupIndex);
        PreferenceHelper.getPreferenceHelperInstance().setRendererIndex(context,rendererIndex);

        setBitrate(trackGroups.get(rendererIndex).getFormat(override.tracks[0]).bitrate);
        Log.d("applySelection"," "+ trackGroups.get(rendererIndex).getFormat(override.tracks[0]).bitrate);
        Log.d("applySelection","data "+ override.data);
        trackSelector.setParameters(parametersBuilder);

        new Handler().postDelayed(()->popupWindow.dismiss(),500);
    }

    private void onClick(View view) {
        if (view == disableView) {
            onDisableViewClicked();
        } else if (view == defaultView) {
            onDefaultViewClicked();
        } else {
            onTrackViewClicked(view);
        }
        updateViewStates();
    }

    public void setBitrate(int bitrate){

        if (bitrate == Format.NO_VALUE) {

            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);

        }
        if (bitrate <= BITRATE_160P) {
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);
        }
        if (bitrate <= BITRATE_240P && bitrate>BITRATE_160P) {
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);

        }
        if (bitrate <= BITRATE_360P && bitrate>BITRATE_240P) {
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_360P);
        }
        if (bitrate <= BITRATE_480P && bitrate>BITRATE_360P) {
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_480P);
        }
        if (bitrate <= BITRATE_720P && bitrate>BITRATE_480P) {
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_720P);
        }
        if (bitrate <= BITRATE_1080P && bitrate>BITRATE_720P) {
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_1080P);
        }

        if (bitrate >BITRATE_1080P) {
            PreferenceHelper.getPreferenceHelperInstance().setVideoQuality(context,BITRATE_1080P);
        }


    }

    private void onDisableViewClicked() {
        isDisabled = true;
        override = null;
    }

    private void onDefaultViewClicked() {
        isDisabled = false;
        override = null;
    }

    private void onTrackViewClicked(View view) {
        isDisabled = false;
        @SuppressWarnings("unchecked")
        Pair<Integer, Integer> tag = (Pair<Integer, Integer>) view.getTag();
        int groupIndex = tag.first;
        int trackIndex = tag.second;

        if (override != null) {
            int[] overrideTracks = override.tracks;
            int[] tracks = getTracksRemoving(overrideTracks, override.tracks[0]);
            override = new SelectionOverride(groupIndex, tracks);
        }
        override = new SelectionOverride(groupIndex, trackIndex);
        applySelection();
    }

    private static int[] getTracksAdding(int[] tracks, int addedTrack) {
        tracks = Arrays.copyOf(tracks, tracks.length + 1);
        tracks[tracks.length - 1] = addedTrack;
        return tracks;
    }

    private static int[] getTracksRemoving(int[] tracks, int removedTrack) {
        int[] newTracks = new int[tracks.length - 1];
        int trackCount = 0;
        for (int track : tracks) {
            if (track != removedTrack) {
                newTracks[trackCount++] = track;
            }
        }
        return newTracks;
    }

    // Internal classes.

    private class ComponentListener implements OnClickListener {

        @Override
        public void onClick(View view) {
            MyTrackSelectionView.this.onClick(view);
        }
    }

    private String buildBitrateString(Format format, AppCompatImageView imageView) {
        int bitrate = format.bitrate;
        boolean isPlaying = currentBitrate == bitrate;

        if(isPlaying) {
            imageView.setImageResource(getBitrateDrawableTinted(format));
        }
        else
            Glide.with(context).load(getBitrateDrawable(format)).into(imageView);


        if (bitrate == Format.NO_VALUE) {
            Log.d("buildBitrateString", "Format.NO_VALUE");
            return updateText(isPlaying, trackNameProvider.getTrackName(format));
        }
        if (bitrate <= BITRATE_160P) {
            return updateText(isPlaying, " 160P");
        }
        if (bitrate <= BITRATE_240P) {
            return updateText(isPlaying, " 240P");
        }
        if (bitrate <= BITRATE_360P) {
            return updateText(isPlaying, " 360P");
        }
        if (bitrate <= BITRATE_480P) {
            return updateText(isPlaying, " 480P");
        }
        if (bitrate <= BITRATE_720P) {
            return updateText(isPlaying, " 720P");
        }
        if (bitrate <= BITRATE_1080P) {
            return updateText(isPlaying, " 1080P");
        }
        return trackNameProvider.getTrackName(format);
    }

    private int getBitrateDrawable(Format format) {
        int bitrate = format.bitrate;
        if (bitrate == Format.NO_VALUE) {
            return R.drawable.au;
        }
        if (bitrate <= BITRATE_160P) {
            return R.drawable.sd;
        }
        if (bitrate <= BITRATE_240P) {
            return R.drawable.ld;
        }
        if (bitrate <= BITRATE_360P) {
            return R.drawable.sd;
        }
        if (bitrate <= BITRATE_480P) {
            return R.drawable.sd;
        }
        if (bitrate <= BITRATE_720P) {
            return R.drawable.hd;
        }
        if (bitrate <= BITRATE_1080P) {
            return R.drawable.hd;
        }
        return R.drawable.hd;
    }

    private int getBitrateDrawableTinted(Format format) {
        int bitrate = format.bitrate;
        if (bitrate == Format.NO_VALUE) {
            return R.drawable.au_blue;
        }
        if (bitrate <= BITRATE_160P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_240P) {
            return R.drawable.ld_blue;
        }
        if (bitrate <= BITRATE_360P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_480P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_720P) {
            return R.drawable.hd_blue;
        }
        if (bitrate <= BITRATE_1080P) {
            return R.drawable.hd_blue;
        }
          return R.drawable.hd_blue;
    }

    private String updateText(boolean isPlaying, String quality) {
        if (isPlaying) {
            //if (!quality.contains(playingString))
                return "<font color=#2196F3>"+ quality +"</font>";
        }

        return quality;
    }
}
