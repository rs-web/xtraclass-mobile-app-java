package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class SubjectOfClassResponse extends DataResponse {
private List<SubjectOfClass> data;

    public List<SubjectOfClass> getData() {
        return data;
    }

    public void setData(List<SubjectOfClass> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SubjectOfClassResponse{" +
                "data=" + data +
                '}';
    }
}

/*
* {
    "statusCode": 200,
    "message": "",
    "data": [
        {
            "id": 1293,
            "subject_name": "Core Mathematics",
            "class_id": 351,
            "status": 1,
            "deleted_at": null,
            "created_at": "2020-11-04 09:16:40",
            "updated_at": "2020-11-04 09:16:40",
            "uuid": "47518f8d-f59c-4915-9944-54f5d9d173f4"
        }
    ]
}*/