package com.realstudiosonline.xtraclass.models;

public class SubscribePlan {
    private String  state;
    private String display_text;
    private String transaction_reference;
    private String message;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDisplay_text() {
        return display_text;
    }

    public void setDisplay_text(String display_text) {
        this.display_text = display_text;
    }

    public String getTransaction_reference() {
        return transaction_reference;
    }

    public void setTransaction_reference(String transaction_reference) {
        this.transaction_reference = transaction_reference;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SubscribePlan{" +
                "state='" + state + '\'' +
                ", display_text='" + display_text + '\'' +
                ", transaction_reference='" + transaction_reference + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
