package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class PlansResponse {
    private List<Plan> data;
    public List<Plan> getData() {
        return data;
    }

    public void setData(List<Plan> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PlansResponse{" +
                "data=" + data +
                '}';
    }

    /*{
    "data": [
        {
            "id": 1,
            "name": "Monthly",
            "duration": "1 month",
            "amount": "25"
        },
        {
            "id": 2,
            "name": "Per - Term",
            "duration": "3 months",
            "amount": "70"
        },
        {
            "id": 3,
            "name": "School Year",
            "duration": "12 months",
            "amount": "225"
        }
    ]
}*/
}
