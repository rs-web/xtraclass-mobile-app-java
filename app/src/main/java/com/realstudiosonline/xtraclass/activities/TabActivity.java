package com.realstudiosonline.xtraclass.activities;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.CountryAdapter;
import com.realstudiosonline.xtraclass.adapters.OperatedCountry;
import com.realstudiosonline.xtraclass.models.Country;

import java.util.ArrayList;
import java.util.List;

public class TabActivity extends AppCompatActivity  {

    PopupWindow popupWindow;
    CountryAdapter countryAdapter;
    CircularImageView ivCountry;
    TextView tvCountry;
    AppCompatImageButton ivSpinnerButton;
    View viewAnchor;
    //SmartTabLayout viewPagerTab;
   /* TabLayout tabLayout;
    private long mIndicatorAnimDuration = 250;
    private ValueAnimator mValueAnimator;
    private int mCurrentTab;
    private int mLastTab;
    private int mTabCount;
    private IndicatorPoint mCurrentP = new IndicatorPoint();
    private IndicatorPoint mLastP = new IndicatorPoint();
    private OvershootInterpolator mInterpolator = new OvershootInterpolator(1.5f);
    private Rect mIndicatorRect = new Rect();
    private float mIndicatorWidth;*/
    List<OperatedCountry> operatedCountryList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);


        ivSpinnerButton = findViewById(R.id.iv_country_spinner);
        tvCountry = findViewById(R.id.tvCountry);
        ivCountry = findViewById(R.id.ivCountry);
        viewAnchor = findViewById(R.id.view_drop_down_anchor);

        operatedCountryList = new ArrayList<>();

        operatedCountryList.add(new OperatedCountry("+233", R.drawable.gh, "GHANA"));
        operatedCountryList.add(new OperatedCountry("+220", R.drawable.gm, "Gambia"));
        operatedCountryList.add(new OperatedCountry("+234", R.drawable.ng, "Nigeria"));
        operatedCountryList.add(new OperatedCountry("+232", R.drawable.sl, "Sierra Leone"));
        operatedCountryList.add(new OperatedCountry("+231", R.drawable.lr, "Liberia"));

        countryAdapter = new CountryAdapter(getApplicationContext(), R.layout.item_country, operatedCountryList);

        //AppCompatImageButton imageButtonCountrySpinner = findViewById(R.id.iv_country_spinner);


        findViewById(R.id.vCountry).setOnClickListener(v -> provideCountryPopupWindow(viewAnchor));


        //viewPagerTab = findViewById(R.id.viewpagertab);
       /* FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.playing, SecondFragment.class)
                .add(R.string.question, SecondFragment.class)
                .add(R.string.library, SecondFragment.class)
                .create());*/
/*
        viewPagerTab.setOnTabClickListener(new SmartTabLayout.OnTabClickListener() {
            @Override
            public void onTabClicked(int position) {

            }
        });
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        viewPagerTab.setViewPager(viewPager);*/
/*

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Playing"));
        tabLayout.addTab(tabLayout.newTab().setText("Question"));
        tabLayout.addTab(tabLayout.newTab().setText("Library"));


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mCurrentTab = tab.getPosition();
                calcOffset();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mCurrentTab = 0;
        mTabCount =tabLayout.getTabCount();
        mLastTab = 2;


        for (int i =0; i< tabLayout.getTabCount(); i++) {
            View tabView =  ((ViewGroup)tabLayout.getChildAt(0)).getChildAt(i);
            tabView.requestLayout();
            ViewCompat.setBackground(tabView,setImageButtonStateNew(getApplicationContext()));
            ViewCompat.setPaddingRelative(tabView, tabView.getPaddingStart(), tabView.getPaddingTop(), tabView.getPaddingEnd(), tabView.getPaddingBottom());
        }
        calcIndicatorRect();
        mValueAnimator = ValueAnimator.ofObject(new PointEvaluator(), mLastP, mCurrentP);
        mValueAnimator.addUpdateListener(this);


*/



    }

    private void provideCountryPopupWindow(View it) {
        if(popupWindow!=null){
            popupWindow.dismiss();
            popupWindow = null;
        }
        popupWindow = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        Drawable backgroundDrawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.blue_outline_white_background);
        popupWindow.setBackgroundDrawable(backgroundDrawable);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ListView listView = (ListView)layoutInflater.inflate(R.layout.layout_country_dropdown, null, false);
        listView.setAdapter(countryAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            OperatedCountry selectedCountry = countryAdapter.getItem(position);

            if(selectedCountry!=null) {
                tvCountry.setText(selectedCountry.getDisplayCountry());
                Glide.with(this).load(selectedCountry.getIcon()).into(ivCountry);
            }
                    //viewModel.setLegalCountry(selectedCountry);
            popupWindow.dismiss();
        });

        if(popupWindow!=null) {
            popupWindow.setContentView(listView);
            popupWindow.showAsDropDown(it, 0, 0);
        }

    }

   /* public StateListDrawable setImageButtonStateNew(Context mContext){
        StateListDrawable states = new StateListDrawable();
        int [] state_selected ={android.R.attr.state_selected};
        int []  _selected_state= {-android.R.attr.state_selected};

        states.addState(state_selected, ContextCompat.getDrawable(mContext, R.drawable.tab_bg_normal_blue));
        states.addState(_selected_state, ContextCompat.getDrawable(mContext, R.drawable.tab_bg_normal));

        return states;
    }*/


  /*  private void calcOffset() {
        final View currentTabView =((ViewGroup)tabLayout.getChildAt(0)).getChildAt(mCurrentTab);
        mCurrentP.left = currentTabView.getLeft();
        mCurrentP.right = currentTabView.getRight();

        final View lastTabView =((ViewGroup)tabLayout.getChildAt(0)).getChildAt(mLastTab);
        mLastP.left = lastTabView.getLeft();
        mLastP.right = lastTabView.getRight();

       Log.d("AAA", "mLastP--->" + mLastP.left + "&" + mLastP.right);
       Log.d("AAA", "mCurrentP--->" + mCurrentP.left + "&" + mCurrentP.right);
        if (mLastP.left == mCurrentP.left && mLastP.right == mCurrentP.right) {
            tabLayout.invalidate();
        } else {
            mValueAnimator.setObjectValues(mLastP, mCurrentP);

            if (mIndicatorAnimDuration < 0) {
                mIndicatorAnimDuration = 250;
            }


            mValueAnimator.setInterpolator(mInterpolator);
            mValueAnimator.setDuration(mIndicatorAnimDuration);
            mValueAnimator.start();
        }
    }

    private void calcIndicatorRect() {
        View currentTabView = ((ViewGroup)tabLayout.getChildAt(0)).getChildAt(mCurrentTab);
        float left = currentTabView.getLeft();
        float right = currentTabView.getRight();

        mIndicatorRect.left = (int) left;
        mIndicatorRect.right = (int) right;

        if (mIndicatorWidth < 0) {   //indicatorWidth PagerSlidingTabStrip

        } else {//indicatorWidth
            float indicatorLeft = currentTabView.getLeft() + (currentTabView.getWidth() - mIndicatorWidth) / 2;

            mIndicatorRect.left = (int) indicatorLeft;
            mIndicatorRect.right = (int) (mIndicatorRect.left + mIndicatorWidth);
        }
    }
    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        View currentTabView = ((ViewGroup)tabLayout.getChildAt(0)).getChildAt(mCurrentTab);
        IndicatorPoint p = (IndicatorPoint) animation.getAnimatedValue();
        mIndicatorRect.left = (int) p.left;
        mIndicatorRect.right = (int) p.right;

        if (mIndicatorWidth < 0) {   //indicatorWidth

        } else {//indicatorWidth
            float indicatorLeft = p.left + (currentTabView.getWidth() - mIndicatorWidth) / 2;

            mIndicatorRect.left = (int) indicatorLeft;
            mIndicatorRect.right = (int) (mIndicatorRect.left + mIndicatorWidth);
        }
        Log.d("AAA", "onAnimationUpdate left:"+mIndicatorRect.right + "  right"+   mIndicatorRect.left);
        tabLayout.invalidate();

    }

    static class IndicatorPoint {
        public float left;
        public float right;
    }

    static class PointEvaluator implements TypeEvaluator<IndicatorPoint> {
        @Override
        public IndicatorPoint evaluate(float fraction, IndicatorPoint startValue, IndicatorPoint endValue) {
            float left = startValue.left + fraction * (endValue.left - startValue.left);
            float right = startValue.right + fraction * (endValue.right - startValue.right);
            IndicatorPoint point = new IndicatorPoint();
            point.left = left;
            point.right = right;
            return point;
        }
    }

    public void showTabs(boolean show) {
        if (show) {
            //tabLayout.setVisibility(View.VISIBLE);
            tabLayout.animate().scaleY(1).setInterpolator(new DecelerateInterpolator()).start();

        } else {
            tabLayout.animate().scaleY(0).setInterpolator(new AccelerateInterpolator()).start();
            //tabLayout.setVisibility(View.GONE);
        }
    }*/
}