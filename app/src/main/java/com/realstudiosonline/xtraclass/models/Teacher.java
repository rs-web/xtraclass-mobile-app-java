package com.realstudiosonline.xtraclass.models;

public class Teacher {
    private String profile_id;
    private String name;

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "profile_id='" + profile_id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
 /* "teacher": {
          "profile_id": "59e9caf6-427d-471a-a15e-8b53c1714ec2",
          "name": "Jeremiah Nii Annan Dodoo"
          }*/