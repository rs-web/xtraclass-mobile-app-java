package com.realstudiosonline.xtraclass.utils;

import android.content.Context;

import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;

public class VideoCache {
    private static SimpleCache simpleCache;
    private static CacheDataSourceFactory cacheDataSourceFactory;

    public static SimpleCache getSimpleCacheInstance(Context context, long maxCacheSize) {

        if (simpleCache == null) {
            simpleCache = new SimpleCache(new File(context.getCacheDir(), "XtraClassCache"), new  LeastRecentlyUsedCacheEvictor(maxCacheSize), new ExoDatabaseProvider(context));
        }
        return simpleCache;
    }

    public static CacheDataSourceFactory getCacheDataSourceFactoryInstance(Context context, long maxCacheSize, long maxFileSize){

        if(cacheDataSourceFactory==null){
           cacheDataSourceFactory =  new CacheDataSourceFactory(context,
                    VideoCache.getSimpleCacheInstance(context,maxCacheSize),
                    maxCacheSize,

                   maxFileSize);
        }
        return cacheDataSourceFactory;
    }

}
