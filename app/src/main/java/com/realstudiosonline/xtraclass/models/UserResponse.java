package com.realstudiosonline.xtraclass.models;

public class UserResponse {

    private boolean registered;
    private UserData data;
    private int statusCode;
    private String message;

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "registered=" + registered +
                ", data=" + data +
                ", statusCode=" + statusCode +
                ", message='" + message + '\'' +
                '}';
    }
}

/*
*
*
*
*
* {
    "registered": true,
    "data": {
        "user": {
            "id": 2527,
            "username": "550770376",
            "google_id": null,
            "name": null,
            "email": "550770376@email.com",
            "email_verified_at": null,
            "password": "password",
            "status": 1,
            "remember_token": null,
            "token": null,
            "mobile_verified_at": null,
            "device_token": null,
            "device_type": null,
            "deleted_at": null,
            "created_at": "2021-01-26 15:14:40",
            "updated_at": "2021-01-26 15:14:40",
            "uuid": "c030722a-f00a-4c08-9fe9-75838ecacaf6",
            "provider_id": null,
            "provider": null,
            "user_id": 2527,
            "first_name": "",
            "last_name": "",
            "phone": "550770376",
            "country": 233,
            "profile_image": null,
            "file_url": null,
            "country_name": "GHANA",
            "institution_id": null,
            "cat_id": null,
            "school_id": null,
            "co_id": null,
            "course_id": null,
            "class_id": null
        },
        "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI4IiwianRpIjoiNTFhZWM2OTc1Y2Q5NDQ5OGJiZDI0ZmUyMTNhNTIzMmY2ZTE5ZWQyNGNhYmU4YTQyNWVlNWNiYTQ0MThlMzE4YWZkMzNjODNlMmRiZjNlOGEiLCJpYXQiOjE2MTkwMTQ5MzgsIm5iZiI6MTYxOTAxNDkzOCwiZXhwIjoxNjUwNTUwOTM4LCJzdWIiOiIyNTI3Iiwic2NvcGVzIjpbXX0.e4vxeofsld3YvyPt3ha7z7edqQwijZ2Fkv0d7LvqSAtmX6sQUUNgY82D3WsiI0dCPTQMdCFNikgA5z5EqTT6iVB_fbGWCwNQheEURQyWLPWFyRjJYTRW8DjbwyJYiz_1UfFwanaYIh9KhkO6UXkQz9_aXolzvRzlk4vQsuSpqCgnWq4-Af2zd5LYW7gYFYgpKs6PBfd8XTfN73ekBIT3e0ynF8y1tOVZe5HRK5748Es7CWeFBNHvc-2HFHy384OyJzrmyNnfCF-lKPpmhw1DFvwOKCQka1en3HdRSFAQP3gPPyphc-RAMez37rMWtP68gUk92sPpFLbdDySXMaHZt2MGvo_Wkob_x5g_GMhB3tL9YvDxsDuaPeox0NtLcX2o1lSbeEQiWAqvli8pgAj-UKPD44_hrU_xIBBpn20fgkMjLKI89TlKtji1ugL-pZpmWZNmOcQc7DnMtmjpJ_1KXWZwlu6daflc6i3mc29Xa7nDyBHuKyaqjm0-brSIvsVW9-emEce2e_kkygCkdJ7eD6Jcc5ZubpwMyZrRXQH62z_cmPN5teUVpfx7kI7FiCW0RQDbIVXSkTjYYlfufdfUi4AwLbq9Y7QDoqKo5YinaoT8xfl5YFQffLK34v0u7VMnD6CDJI5PCFmp-NyqOSFCwnpSCzOQtK3WsvGI9tqSCFM"
    },
    "statusCode": 200,
    "message": "successful"
}
* */