package com.realstudiosonline.xtraclass.utils;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.realstudiosonline.xtraclass.activities.AuthActivity;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.ClassDetailsResponse;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.CoursesForSchool;
import com.realstudiosonline.xtraclass.models.Favorite;
import com.realstudiosonline.xtraclass.models.Institution;
import com.realstudiosonline.xtraclass.models.Lesson;
import com.realstudiosonline.xtraclass.models.Library;
import com.realstudiosonline.xtraclass.models.PlaybackPosition;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.SubjectOfClass;
import com.realstudiosonline.xtraclass.models.SubjectOfClassResponse;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.models.UserPlan;
import com.realstudiosonline.xtraclass.models.VideoQ;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_480P;

@SuppressWarnings("unused")
public class PreferenceHelper {

    public static final String USER = "USER";
    public static final String PHONE = "PHONE";
    public static final String USER_ID = "USER_ID";
    public static final String USER_UUID = "USER_UUID";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String SELECTED_CLASS = "SELECTED_CLASS";
    public static final String CLASS_DETAILS = "CLASS_DETAILS";
    public static final String SELECTED_SCHOOL = "SELECTED_SCHOOL";
    public static final String SELECTED_COURSE ="SELECTED_COURSE";
    public static final String SELECTED_INSTITUTION ="SELECTED_INSTITUTION";
    private static Context appContext;
    private static PreferenceHelper preferenceHelperInstance;
    public static final String BASIC_SCHOOL = "e93e7590-375a-4d92-8d69-45e8f89b44b4";
    private static final String LESSONS = "LESSONS";
    private static final String FAVORITES = "FAVORITES";
    public static final String VIDEO_QUALITY = "VIDEO_QUALITY";

    public static final String PLAY_BACK_POSITION = "PLAY_BACK_POSITION";
    public static final String CURRENT_WINDOW = "CURRENT_WINDOW";
    public static final String PLAY_WHEN_READY = "PLAY_WHEN_READY";
    public static final String TOKEN = "TOKEN";
    public static final String SUBJECT = "SUBJECT";
    public static final String IS_REGISTERED = "IS_REGISTERED";
    public static final String TRACK_GROUP = "TRACK_GROUP";
    public static final String RENDERER_INDEX = "RENDERER_INDEX";
    public static final String SELECTED_LESSON = "SELECTED_LESSON";
    public static final String CLASS_VIDEO_PLAY_ON = "CLASS_VIDEO_PLAY_ON";
    public static final String LESSON_QUERY_DATE = "LESSON_QUERY_DATE";
    public static final String PLAY_PREVIOUS_ELEMENT = "PLAY_PREVIOUS_ELEMENT";
    public static final String OTP_VERIFIED = "OTP_VERIFIED";
    public static final String LIBRARY_ITEMS = "LIBRARY_ITEMS";
    public static final String LIBRARY_SELECTED = "LIBRARY_SELECTED";
    public static final String SCHOOL = "SCHOOL";
    private static final String PROFILE = "PROFILE";
    private static final String SCHOOL_TYPE = "SCHOOL_TYPE";
    private static final String CURRENT_DATE_TIME = "CURRENT_DATE_TIME";
    public static final String CURRENT_PLAN = "CURRENT_PLAN";


    public static PreferenceHelper getPreferenceHelperInstance()
    {
        if (preferenceHelperInstance == null) {
            preferenceHelperInstance = new PreferenceHelper();
        }
        return preferenceHelperInstance;
    }



    public void setSchoolType(Context context,int schoolType){
        setInteger(context,SCHOOL_TYPE,schoolType);
    }

    public int getSchoolType(Context context){
       return getInteger(context,SCHOOL_TYPE,0);
    }
   /* public void setSchoolDetails(Context context, School school){
        String toJson = new Gson().toJson(school);
        setString(context, SCHOOL, toJson);
    }


    public School getSchoolDetails(Context context){
        String string = getString(context, SCHOOL, "");
        return new Gson().fromJson(string, School.class);
    }*/

    public void setProfile(Context context,String profile){
        setString(context, PROFILE, profile);
    }

    public String getProfile(Context context) {
        return getString(context,PROFILE, "");
    }

    public List<Favorite> getFavorites(Context context){
        String favoritesString = getString(context, FAVORITES,"");
        List<Favorite> favorites = new ArrayList<>();

        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        List<String> list = new Gson().fromJson(favoritesString, type);

        if(list!=null) {
            for (String string : list) {
                Favorite favorite = new Gson().fromJson(string, Favorite.class);
                favorites.add(favorite);
            }
        }
        return favorites;
    }

    public void setFavorites(Context context, List<Favorite> favorites){
        setString(context, FAVORITES, "");
        List<String> favoritesString = new ArrayList<>();
        Gson gson = new Gson();
        for (Favorite favorite:favorites) {
            Log.d("setFavorites", ""+favorite);
            String toJson = gson.toJson(favorite);
            favoritesString.add(toJson);
        }
        String favoriteString = gson.toJson(favoritesString);


        setString(context, FAVORITES, favoriteString);
    }

    public void setLibrarySelected(Context context, String selectedLibrary){
        setString(context,LIBRARY_SELECTED,selectedLibrary);

    }
    public String getLibrarySelected(Context context){
        return getString(context,LIBRARY_SELECTED,"");
    }
    public void setLibraryItems(Context context, List<Library> libraries){
        setString(context, LIBRARY_ITEMS, "");
        List<String> libraryListString = new ArrayList<>();
        Gson gson = new Gson();
        for (Library library:libraries) {
            Log.d("setLibraryItems", ""+library);
            String toJson = gson.toJson(library);
            libraryListString.add(toJson);
        }
        String libraryString = gson.toJson(libraryListString);
        setString(context, LIBRARY_ITEMS, libraryString);
    }

    public List<Library> getLibraryItems(Context context){
        String libraryString = getString(context, LIBRARY_ITEMS,"");
        List<Library> libraries = new ArrayList<>();
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        List<String> list = new Gson().fromJson(libraryString, type);

        if(list!=null) {
            for (String string : list) {
                Library library = new Gson().fromJson(string, Library.class);
                libraries.add(library);
            }
        }
        return libraries;
    }


    public void setCountryCode(Context context, String phone) {
        setString(context, COUNTRY_CODE,phone);
    }


    public String getCountryCode(Context context){
       return getString(context, COUNTRY_CODE,"");
    }

    public void setPhone(Context context, String code) {
        setString(context,PHONE,code);
    }

    public String getPhone(Context context){
        return getString(context,PHONE,"");
    }
    public void setOtpVerified(Context context, boolean otpVerified){
        setBoolean(context,OTP_VERIFIED,otpVerified);
    }

    public boolean getOtpVerified(Context context){
       return getBoolean(context,OTP_VERIFIED,false);
    }
    public String getLessonQueryDate(Context context){
        return getString(context,LESSON_QUERY_DATE,"");
    }

    public void setLessonQueryDate(Context context, String queryDate){
        setString(context,LESSON_QUERY_DATE, queryDate);
    }
    public void setPlayPrevious(Context context,boolean playPrevious) {
        setBoolean(context,PLAY_PREVIOUS_ELEMENT, playPrevious);
    }

    public boolean getPlayPrevious(Context context){
       return getBoolean(context,PLAY_PREVIOUS_ELEMENT,false);
    }


    public void setClassVideoPlayOn(Context context,String playOn){
        setString(context,CLASS_VIDEO_PLAY_ON,playOn);
    }



    public String getClassVideoPlayOn(Context context){
        return getString(context,CLASS_VIDEO_PLAY_ON,"");
    }


    public void setSelectedLesson(Context context, Lesson lesson){
        String toJson = new Gson().toJson(lesson);
        setString(context, SELECTED_LESSON, toJson);
    }
    public void setSelectedLesson(Context context){
        setString(context, SELECTED_LESSON, "");
    }


    public Lesson getSelectedLesson(Context context){
        String string = getString(context, SELECTED_LESSON, "");
        return new Gson().fromJson(string, Lesson.class);
    }

    public void setCurrentWindow(Context context, int currentWindow){
        setInteger(context,CURRENT_WINDOW, currentWindow);
    }

    public int getCurrentWindow(Context context){

        return getInteger(context,CURRENT_WINDOW,0);
    }

    public void setPlayBackPosition(Context context, PlaybackPosition playBackPosition){

        String toJson = new Gson().toJson(playBackPosition);
        setString(context, PLAY_BACK_POSITION, toJson);
    }

    public PlaybackPosition getPlayBackPosition(Context context){

        String string = getString(context, PLAY_BACK_POSITION, "");
        return new Gson().fromJson(string, PlaybackPosition.class);

    }

    public void setVideoQuality(Context context, int bitrate){
        setInteger(context, VIDEO_QUALITY, bitrate);
    }



    public int getVideoQuality(Context context){
        return getInteger(context,VIDEO_QUALITY, BITRATE_480P);
    }
    public List<Lesson> getLessons(Context context){
        String lessonString = getString(context, LESSONS,"");
        List<Lesson> lessons = new ArrayList<>();

        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        List<String> list = new Gson().fromJson(lessonString, type);

        if(list!=null) {
            for (String string : list) {
                Lesson lesson = new Gson().fromJson(string, Lesson.class);
                lessons.add(lesson);
            }
        }
        return lessons;
    }

    public void setLessons(Context context, List<Lesson> lessons){
        setString(context, LESSONS, "");
        List<String> lessonsString = new ArrayList<>();
        Gson gson = new Gson();
        for (Lesson lesson:lessons) {
            Log.d("setLessons", ""+lesson);
            String toJson = gson.toJson(lesson);
            lessonsString.add(toJson);
        }
        String lessonString = gson.toJson(lessonsString);


        setString(context, LESSONS, lessonString);
    }
    public void setLessons(Context context){
        setString(context, LESSONS, "");
    }




    public Institution getSelectedInstitution(Context context){
        String string = getString(context, SELECTED_INSTITUTION, "");
        return new Gson().fromJson(string, Institution.class);
    }

    public void setSelectedInstitution(Context context,Institution institution){
        String toJson = new Gson().toJson(institution);
        setString(context, SELECTED_INSTITUTION, toJson);
    }


    public void setToken(Context context, String value)
    {
       setString(context,TOKEN, value);
    }
    public void setToken(Context context)
    {
        setString(context,TOKEN,"");
    }

    public String getToken(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(TOKEN, "");
    }

    public CoursesForSchool getCoursesForSchool(Context context){
        String string = getString(context, SELECTED_COURSE, "");
        return new Gson().fromJson(string, CoursesForSchool.class);
    }

    public void setCoursesForSchool(Context context,CoursesForSchool coursesForSchool){
        String toJson = new Gson().toJson(coursesForSchool);
        setString(context, SELECTED_COURSE, toJson);
    }

    public School getSelectedSchool(Context context){
        String string = getString(context, SELECTED_SCHOOL, "");
        return new Gson().fromJson(string, School.class);
    }

    public void setSelectedSchool(Context context,School school){
        String toJson = new Gson().toJson(school);
        setString(context, SELECTED_SCHOOL, toJson);
    }

    public void setSelectedSchool(Context context){
        setString(context, SELECTED_SCHOOL,"" );
    }



    public SubjectOfClassResponse getSubject(Context context){
        String string = getString(context, SUBJECT, "");
        return new Gson().fromJson(string, SubjectOfClassResponse.class);
    }

    public void setSubject(Context context, SubjectOfClassResponse school){
        String toJson = new Gson().toJson(school);
        setString(context, SUBJECT, toJson);
    }





    public ClassDetails getClassDetails(Context context){
        String string = getString(context, CLASS_DETAILS, "");
        return new Gson().fromJson(string, ClassDetails.class);
    }

    public void setClassDetails(Context context,ClassDetails classDetails){
        if(classDetails!=null) {
            String toJson = new Gson().toJson(classDetails);
            setString(context, CLASS_DETAILS, toJson);
        }
    }

    public ClassesForCourse getSelectedClassesForCourse(Context context){
        String classForCourseJson = getString(context, SELECTED_CLASS, "");
        return new Gson().fromJson(classForCourseJson, ClassesForCourse.class);
    }
    public void setSelectedClassesForCourse(Context context,ClassesForCourse classesForCourse){
        String classForCourseString = new Gson().toJson(classesForCourse);
        setString(context, SELECTED_CLASS, classForCourseString);
        Log.d("setSelectedClassesForC", ""+classesForCourse.toString());
    }
    public void setSelectedClassesForCourse(Context context){
        setString(context, SELECTED_CLASS, "");
    }


    public User getUser(Context context){
        String userJson = getString(context, USER, "");
        return new Gson().fromJson(userJson, User.class);
    }
    public void setUser(Context context,User user){
        String userString = new Gson().toJson(user);
        setString(context, USER, userString);
    }
    public void setUser(Context context){

        setString(context, USER, "");
    }
    public boolean getBoolean(Context context, String key, Boolean value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, value);
    }

    public int getInteger(Context context, String key, int value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, value);
    }

    public void setBoolean(Context context, String key, Boolean value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
    }

    public void setFloat(Context context, String key, float value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(key, value).apply();

    }

    public float getFloat(Context context, String key, float value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(key, value);
    }

    public void setLong(Context context, String key, long value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(key, value).apply();

    }

    public long getLong(Context context, String key, long value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(key, value);
    }

    public void setInteger(Context context, String key, int value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
    }

    public void setString(Context context, String key, String value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }

    public String getString(Context context, String key, String value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, value);
    }

    public boolean getIsRegistered(Context context){
        return getBoolean(context, IS_REGISTERED,false);
    }
    public void setIsRegistered(Context context, boolean isRegistered) {
        setBoolean(context,IS_REGISTERED,isRegistered);
    }


    public void setTrackGroup(Context context, int groupIndex) {
        setInteger(context, TRACK_GROUP, groupIndex);
    }

    public int getTrackGroup(Context context) {
        return  getInteger(context,TRACK_GROUP,0);
    }

    public void setRendererIndex(Context context, int rendererIndex) {
        setInteger(context, RENDERER_INDEX, rendererIndex);
    }

    public int getRendererIndex(Context context) {
        return  getInteger(context, RENDERER_INDEX,0);
    }



    public void clearPreferences(Context context){
        setString(context,USER, "");
        setString(context,PHONE, "");
        setString(context,USER_ID, "");
        setString(context,USER_UUID, "");
        setString(context,COUNTRY_CODE, "");
        setString(context,SELECTED_CLASS, "");
        setString(context,CLASS_DETAILS, "");
        setString(context,SELECTED_SCHOOL, "");
        setString(context,SELECTED_COURSE, "");
        setString(context,SELECTED_INSTITUTION, "");
        setString(context,LESSONS, "");
        setString(context,FAVORITES, "");
        setInteger(context,VIDEO_QUALITY, 0);
        setString(context,PLAY_BACK_POSITION, "");
        setInteger(context,CURRENT_WINDOW, 0);
        setBoolean(context,PLAY_WHEN_READY, false);
        setString(context,TOKEN, "");
        setString(context,SUBJECT,"");
        setBoolean(context,IS_REGISTERED,false);
        setInteger(context,TRACK_GROUP,0);
        setInteger(context,RENDERER_INDEX,0);
        setString(context,SELECTED_LESSON,"");
        setString(context,CLASS_VIDEO_PLAY_ON,"");
        setString(context,LESSON_QUERY_DATE,"");
        setBoolean(context,PLAY_PREVIOUS_ELEMENT,false);
        setBoolean(context,OTP_VERIFIED,false);
        setString(context,LIBRARY_ITEMS,"");
        setString(context,LIBRARY_SELECTED,"");
        setString(context,SCHOOL,"");
        setString(context,PROFILE,"");
        setInteger(context, SCHOOL_TYPE,0);

    }

    public void setCurrentDateTime(Context activity, String currentDateTimeString) {
        setString(activity,CURRENT_DATE_TIME,currentDateTimeString);
    }

    public  String getCurrentDateTime(Context context) {
        return getString(context,CURRENT_DATE_TIME,"");
    }


    public UserPlan getCurrentPlan(Context context){
        String userPlanJson = getString(context, CURRENT_PLAN, "");
        return new Gson().fromJson(userPlanJson, UserPlan.class);
    }
    public void setCurrentPlan(Context context, UserPlan userPlan){
        String userPlanString = new Gson().toJson(userPlan);
        setString(context, CURRENT_PLAN, userPlanString);
    }
}


