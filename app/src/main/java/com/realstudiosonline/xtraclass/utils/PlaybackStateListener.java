package com.realstudiosonline.xtraclass.utils;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;

public class PlaybackStateListener implements Player.EventListener{
    private final SimpleExoPlayer player;

    public PlaybackStateListener(SimpleExoPlayer player) {
        this.player = player;
    }




    @Override
    public void onPlayerError(@NonNull ExoPlaybackException e) {

        player.seekTo(player.getCurrentPosition());
        player.prepare();
        e.printStackTrace();

    }


    private static final String TAG = PlaybackStateListener.class.getSimpleName();
    @Override
    public void onPlaybackStateChanged(int playbackState) {

        String stateString;
        switch (playbackState) {
            case ExoPlayer.STATE_IDLE:
                stateString = "ExoPlayer.STATE_IDLE      -";
                break;
            case ExoPlayer.STATE_BUFFERING:
                stateString = "ExoPlayer.STATE_BUFFERING -";
                break;
            case ExoPlayer.STATE_READY:
                stateString = "ExoPlayer.STATE_READY     -";
                break;
            case ExoPlayer.STATE_ENDED:
                stateString = "ExoPlayer.STATE_ENDED     -";
                break;
            default:
                stateString = "UNKNOWN_STATE             -";
                break;

        }
        Log.d(TAG, "changed state to " + stateString);
    }
}