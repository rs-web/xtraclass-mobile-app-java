package com.realstudiosonline.xtraclass.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class AudioItem implements Parcelable {

    private String mTitle;

    private String mDescription;

    private String mSummary;

    private String mPubDate;

    private String mDuration;

    private List<Enclosure> mEnclosures;

    private List<ItemImage> mItemImages;

    public AudioItem() {
    }

    public AudioItem(String title, String description, String summary, String pubDate,
                String duration, List<Enclosure> enclosures, List<ItemImage> itemImages) {
        mTitle = title;
        mDescription = description;
        mSummary = summary;
        mPubDate = pubDate;
        mDuration = duration;
        mEnclosures = enclosures;
        mItemImages = itemImages;
    }

    protected AudioItem(Parcel in) {
        mTitle = in.readString();
        mDescription = in.readString();
        mSummary = in.readString();
        mPubDate = in.readString();
        mDuration = in.readString();
        if (in.readByte() == 0x01) {
            mEnclosures = new ArrayList<>();
            in.readList(mEnclosures, Enclosure.class.getClassLoader());
        }
        if (in.readByte() == 0x01) {
            mItemImages = new ArrayList<>();
            in.readList(mItemImages, ItemImage.class.getClassLoader());
        }
    }

    public static final Creator<AudioItem> CREATOR = new Creator<AudioItem>() {
        @Override
        public AudioItem createFromParcel(Parcel in) {
            return new AudioItem(in);
        }

        @Override
        public AudioItem[] newArray(int size) {
            return new AudioItem[size];
        }
    };

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getSummary() {
        return mSummary;
    }

    public void setSummary(String iTunesSummary) {
        mSummary = iTunesSummary;
    }

    public String getPubDate() {
        return mPubDate;
    }

    public void setPubDate(String pubDate) {
        mPubDate = pubDate;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    public List<Enclosure> getEnclosures() {
        return mEnclosures;
    }

    public void setEnclosures(List<Enclosure> enclosures) {
        mEnclosures = enclosures;
    }

    public List<ItemImage> getItemImages() {
        return mItemImages;
    }

    public void setItemImages(List<ItemImage> itemImages) {
        mItemImages = itemImages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mDescription);
        dest.writeString(mSummary);
        dest.writeString(mPubDate);
        dest.writeString(mDuration);
        if (mEnclosures == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mEnclosures);
        }
        if (mItemImages == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mItemImages);
        }
    }
}