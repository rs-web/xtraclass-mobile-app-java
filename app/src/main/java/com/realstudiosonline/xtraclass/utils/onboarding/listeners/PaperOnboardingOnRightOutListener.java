package com.realstudiosonline.xtraclass.utils.onboarding.listeners;

public interface PaperOnboardingOnRightOutListener {

    void onRightOut();

}
