package com.realstudiosonline.xtraclass.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.HistoryAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.History;
import com.realstudiosonline.xtraclass.models.HistoryResponse;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

public class HistoryActivity  extends AppCompatActivity implements HistoryAdapter.SetHistoryClickListener{

    String mToken ="";
    HistoryAdapter historyAdapter;
    List<History> histories;
    RecyclerView recyclerView;
    School school;
    User mUser;
    ClassDetails classDetails;
    AppCompatImageView imageViewSchoolCrest;
    AppCompatTextView tvStudentName,tvClass,tvSchoolName;
    CircularImageView circularImageViewProfile;
    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        recyclerView = findViewById(R.id.recycler_view_history);
        tvStudentName = findViewById(R.id.tv_student_name);
        imageViewSchoolCrest = findViewById(R.id.img_school_crest);
        tvClass = findViewById(R.id.tv_class);
        tvSchoolName = findViewById(R.id.tv_school);
        circularImageViewProfile = findViewById(R.id.img_user_avatar);

        findViewById(R.id.img_back_press).setOnClickListener(v -> finish());

        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getApplicationContext());

        histories = new ArrayList<>();

        historyAdapter = new HistoryAdapter(histories,getApplicationContext(),this);
        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
        school = PreferenceHelper.getPreferenceHelperInstance().getSelectedSchool(getApplicationContext());
        classDetails = PreferenceHelper.getPreferenceHelperInstance().getClassDetails(getApplicationContext());

        if(mUser!=null) {
            String  name = mUser.getFirst_name() +" " +mUser.getLast_name();
            tvStudentName.setText(name);

            try {
                Glide.with(HistoryActivity.this).load(mUser.getProfile_image()).into(circularImageViewProfile);
                if(school!=null){
                    Glide.with(this).load(school.getLogo_url()).into(imageViewSchoolCrest);
                    tvSchoolName.setText(school.getName());
                }

            }
            catch (Exception e){
                Glide.with(HistoryActivity.this).load(R.drawable.joseph_stalin).into(circularImageViewProfile);

            }
            /*if (school != null) {
                //Toast.makeText(getActivity(), ""+school, Toast.LENGTH_SHORT).show();
                Log.d("School", "" + school);
                if (school.getId().equalsIgnoreCase(mUser.getSchool_id())) {

                    Glide.with(this).load(school.getLogo_url()).into(imageViewSchoolCrest);


                } else {
                    Log.d("School", "School is not found");

                }
            }*/

            if (classDetails != null) {
                tvClass.setText(classDetails.getFull_name());

            }
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(historyAdapter);

        new Handler().postDelayed(()->{
            getHistory(mToken);
        },200);
    }

    private void getHistory(String  token){
        ApiClient.getApiClient()
                .getInstance(getApplicationContext())
                .getHistory("Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<HistoryResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<HistoryResponse> historyResponseResponse) {
                if(historyResponseResponse.isSuccessful()) {
                    if(historyResponseResponse.body()!=null){
                        histories.addAll(historyResponseResponse.body().getData());
                        historyAdapter.notifyDataSetChanged();
                    }
                    Log.d("getHistory", "onSuccess=> " + historyResponseResponse.body());
                }
                else {

                    Log.d("getHistory", "onSuccess=> errorBody:" + historyResponseResponse.errorBody());
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.d("getHistory", "onError=> "+e.getMessage());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onHistoryClick(History history) {

        Toast.makeText(this, ""+history.toString(), Toast.LENGTH_SHORT).show();
    }
}
