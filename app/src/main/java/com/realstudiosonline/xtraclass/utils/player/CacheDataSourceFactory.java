package com.realstudiosonline.xtraclass.utils.player;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.exoplayer2.database.DatabaseProvider;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.realstudiosonline.xtraclass.R;

import java.io.File;

public class CacheDataSourceFactory implements DataSource.Factory {
    private static final String TAG = "CacheDataSourceFactory";
    private final Context context;
    private final DataSource.Factory defaultDataSourceFactory;
    private final long maxFileSize, maxCacheSize;
    public static DatabaseProvider databaseProvider;
    private static SimpleCache simpleCache;

    public CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
        super();
        this.context = context;
        this.maxCacheSize = maxCacheSize;
        this.maxFileSize = maxFileSize;
        String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));

        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter.Builder(context).build();
        defaultDataSourceFactory = new DefaultDataSourceFactory(this.context,
                bandwidthMeter,
                new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
    }

    @Override
    public DataSource createDataSource() {
        LeastRecentlyUsedCacheEvictor eVictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
        simpleCache =getInstance(eVictor);

        Log.d(TAG, "createDataSource() called" + context.getCacheDir());

        return new CacheDataSource(simpleCache, defaultDataSourceFactory.createDataSource(),
                new FileDataSource(), new CacheDataSink(simpleCache, maxFileSize),
                CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null);
    }

    public DatabaseProvider getDatabaseProvider() {
        if(databaseProvider == null)
            databaseProvider = new ExoDatabaseProvider(context);
        return databaseProvider;
    }

    public SimpleCache getInstance(LeastRecentlyUsedCacheEvictor eVictor) {

        if (simpleCache == null)
            simpleCache = new SimpleCache(new File(context.getCacheDir(), "XtraClassMedia"), eVictor,getDatabaseProvider());
        return simpleCache;
    }
}
