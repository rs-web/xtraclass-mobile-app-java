package com.realstudiosonline.xtraclass.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.StudentProfileResponse;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.ExpandCollapseExtension;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.io.File;
import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import pl.aprilapps.easyphotopicker.ChooserType;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.MediaFile;
import pl.aprilapps.easyphotopicker.MediaSource;
import retrofit2.Response;

import static com.realstudiosonline.xtraclass.utils.Constants.createFormData;

public class StudentProfileActivity extends AppCompatActivity {

    private boolean isEditing = false;
    AppCompatTextView textViewEditAvatar;
    EasyImage easyImage;
    BottomSheetDialog bottomSheetDialog;
    CircularImageView circularImageView;
    private static final String[] LEGACY_WRITE_PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int LEGACY_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 456;
    AppCompatTextView tvStudentName, tvClass, tvSchool,
            tvClassesWatched,tvNotesDownloaded,tvQuestionsAsked,tvAnswersContributed;
    View viewClassesWatched, viewNotesDownloaded, viewQuestionAsked, viewAnswersContributed,viewDivider;
    AppCompatImageView imgSchoolCrest;
    String mToken ="";
    PopupWindow popupWindowLogOut;
    ClassDetails classDetails;
    School school;
    AlertDialog alertDialog;
    String mProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);

        tvClass = findViewById(R.id.tv_class);

        viewClassesWatched = findViewById(R.id.view_classes_watched);

        viewDivider = findViewById(R.id.view_divider_pop);

        imgSchoolCrest = findViewById(R.id.img_school_crest);

        viewNotesDownloaded = findViewById(R.id.view_notes_downloaded);

        viewQuestionAsked = findViewById(R.id.view_questions_asked);

        viewAnswersContributed = findViewById(R.id.view_answers_contributed);

        tvClassesWatched = findViewById(R.id.tv_classes_watched);

        tvNotesDownloaded = findViewById(R.id.tv_notes_downloaded);

        tvQuestionsAsked = findViewById(R.id.tv_questions_asked);

        tvAnswersContributed = findViewById(R.id.tv_answers_contributed);

        tvSchool = findViewById(R.id.tv_school);

        tvStudentName = findViewById(R.id.tv_student_name);

        circularImageView = findViewById(R.id.img_user_avatar);

        textViewEditAvatar = findViewById(R.id.tv_edit_profile);

        View view_achieve = findViewById(R.id.view_achieve);

        classDetails = PreferenceHelper.getPreferenceHelperInstance().getClassDetails(getApplicationContext());
        school = PreferenceHelper.getPreferenceHelperInstance().getSelectedSchool(getApplicationContext());
        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getApplicationContext());

        mProfile = PreferenceHelper.getPreferenceHelperInstance().getProfile(getApplicationContext());

        Glide.with(StudentProfileActivity.this).load(mProfile).error(R.drawable.user_avatar).placeholder(R.drawable.user_avatar).into(circularImageView);

        view_achieve.setOnClickListener(v -> Snackbar.make(v, "Score", Snackbar.LENGTH_LONG)
                .setAction("Score", null).show());


        View view_history = findViewById(R.id.view_history);
        view_history.setOnClickListener(v -> {
            Snackbar.make(v, "History", Snackbar.LENGTH_LONG)
                .setAction("History", null).show();

            startActivity(new Intent(StudentProfileActivity.this, HistoryActivity.class));
        });


        findViewById(R.id.view_bookmark).setOnClickListener(v ->
        {
            Snackbar.make(v, "Favorites", Snackbar.LENGTH_LONG)
                .setAction("Favorites", null).show();
            startActivity(new Intent(StudentProfileActivity.this, FavoritesActivity.class));
        });

        findViewById(R.id.btn_confirm).setOnClickListener(v -> startActivity(new Intent(StudentProfileActivity.this, SubscriptionActivity.class)));

        findViewById(R.id.view_logout).setOnClickListener(v -> provideLogOutPopupWindow(viewDivider,StudentProfileActivity.this));

         easyImage = new EasyImage.Builder(StudentProfileActivity.this)
                .allowMultiple(false)
                .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                .build();

        textViewEditAvatar.setOnClickListener(v -> showBottomSheetDialog());

        findViewById(R.id.img_back_press).setOnClickListener(v -> finish());

        findViewById(R.id.view_profile_edit).setOnClickListener(v -> {

            if(isEditing){
                isEditing = false;
                ExpandCollapseExtension.collapse(textViewEditAvatar);
            }
            else {
                isEditing = true;
                ExpandCollapseExtension.expand(textViewEditAvatar);

                new Handler(Looper.getMainLooper()).postDelayed(()->{
                    ExpandCollapseExtension.collapse(textViewEditAvatar);
                    isEditing = false;
                },5000);
            }
        });

        String mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(this);


        getStudentProfile(mToken);

        if(school!=null){
            Glide.with(this).load(school.getLogo_url()).into(imgSchoolCrest);
        }
    }



    private void getStudentProfile(String token){
        ApiClient.getApiClient().getInstance(getApplicationContext()).getStudentProfile("Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<StudentProfileResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<StudentProfileResponse> studentProfileResponseResponse) {

                if(studentProfileResponseResponse.isSuccessful()){
                    if(studentProfileResponseResponse.body()!=null) {

                        try {
                            String name = studentProfileResponseResponse.body().getData().getFirst_name()+ " "+ studentProfileResponseResponse.body().getData().getLast_name();
                            tvStudentName.setText(name);


                            String answersContributed = studentProfileResponseResponse.body().getData().getStat_answers()+ " Answers contributed";

                            //if(studentProfileResponseResponse.body().getData().getStat_answers()==0)
                            // viewAnswersContributed.setVisibility(View.GONE);

                            tvAnswersContributed.setText(answersContributed);

                            String classesWatched = studentProfileResponseResponse.body().getData().getStat_classes_watched() + " Classes watched";

                            // if(studentProfileResponseResponse.body().getData().getStat_classes_watched()==0)
                            //viewClassesWatched.setVisibility(View.GONE);
                            tvClassesWatched.setText(classesWatched);

                            String questionsAsked = studentProfileResponseResponse.body().getData().getStat_questions() + " Questions asked";

                            //if(studentProfileResponseResponse.body().getData().getStat_questions()==0)
                            //viewQuestionAsked.setVisibility(View.GONE);
                            tvQuestionsAsked.setText(questionsAsked);
                            Log.d("studentProfileResponse", studentProfileResponseResponse.body().toString());

                            String notesDownloaded = studentProfileResponseResponse.body().getData().getStat_downloads()+ " Notes downloaded";
                            //if(studentProfileResponseResponse.body().getData().getStat_downloads()==0)
                            //viewNotesDownloaded.setVisibility(View.GONE);
                            tvNotesDownloaded.setText(notesDownloaded);



                            User user = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
                            if(user!=null) {
                                if(studentProfileResponseResponse.body().getData()!=null) {
                                    String profile = studentProfileResponseResponse.body().getData().getProfile_image();

                                    if(!TextUtils.isEmpty(profile)) {
                                        PreferenceHelper.getPreferenceHelperInstance().setProfile(getApplicationContext(),profile);
                                        Glide.with(StudentProfileActivity.this).load(profile).error(R.drawable.joseph_stalin).into(circularImageView);
                                        user.setProfile_image(profile);
                                        PreferenceHelper.getPreferenceHelperInstance().setUser(getApplicationContext(), user);
                                    }
                                }
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }



            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });
    }

    private void showBottomSheetDialog() {

        if(bottomSheetDialog==null) {
            bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(R.layout.dialog_camera_gallery);

            LinearLayout camera = bottomSheetDialog.findViewById(R.id.view_camera);

            if (camera != null)
                camera.setOnClickListener(v -> {
                    easyImage.openCameraForImage(StudentProfileActivity.this);
                    bottomSheetDialog.hide();
                });
            LinearLayout gallery = bottomSheetDialog.findViewById(R.id.view_gallery);

            if (gallery != null)
                gallery.setOnClickListener(v -> {
                    easyImage.openGallery(StudentProfileActivity.this);
                    bottomSheetDialog.hide();
                });
        }

        bottomSheetDialog.show();
    }


    @SuppressWarnings("unused")
    private boolean isLegacyExternalStoragePermissionRequired() {
        boolean permissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        return Build.VERSION.SDK_INT < 29 && !permissionGranted;
    }

    @SuppressWarnings("unused")
    private void requestLegacyWriteExternalStoragePermission() {
        ActivityCompat.requestPermissions(StudentProfileActivity.this, LEGACY_WRITE_PERMISSIONS, LEGACY_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        easyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onMediaFilesPicked(@NonNull MediaFile[] imageFiles, @NonNull MediaSource source) {

               if(imageFiles.length>0){
                   MediaFile mediaFile =  imageFiles[0];
                   mediaFile.getFile();
                   Glide.with(StudentProfileActivity.this).load(mediaFile.getFile()).into(circularImageView);
                   mProfile = "file:///"+mediaFile.getFile().getPath();
                   PreferenceHelper.getPreferenceHelperInstance().setProfile(getApplicationContext(),mProfile);
                   uploadAvatarImage(mediaFile.getFile(), mToken);
               }
            }

            @Override
            public void onImagePickerError(@NonNull Throwable error, @NonNull MediaSource source) {
                //Some error handling
                error.printStackTrace();
                Toast.makeText(StudentProfileActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCanceled(@NonNull MediaSource source) {
                //Not necessary to remove any files manually anymore
            }
        });
    }


    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(StudentProfileActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }
    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }

    private void hideDialogProgressDialog(){
        if(alertDialog!=null)
            alertDialog.hide();
    }

    @Override
    protected void onDestroy() {
        cancelDialogProgressDialog();
        super.onDestroy();
    }

    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }
    private void uploadAvatarImage(File file, String token){
        showDialogProgressDialog();
        ApiClient.getApiClient().getInstance(getApplicationContext()).uploadAvatarImage(createFormData("file", file.getName(), file),"Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<JsonObject>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull
                                          Response<JsonObject> jsonObjectResponse) {

                hideDialogProgressDialog();
                if(jsonObjectResponse.isSuccessful()){
                    getStudentProfile(token);
                    Toast.makeText(StudentProfileActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull
                                        Throwable e) {
                hideDialogProgressDialog();
            }
        });
    }



    @SuppressLint({"SetTextI18n","InflateParams"})
    private void provideLogOutPopupWindow(View it, Activity activity) {
        if(popupWindowLogOut !=null){
            popupWindowLogOut.dismiss();
            popupWindowLogOut = null;
        }
        popupWindowLogOut = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
  //      Drawable backgroundDrawable = ContextCompat.getDrawable(activity,R.drawable.curve_white_background);
       // popupWindowLogOut.setBackgroundDrawable(backgroundDrawable);
        popupWindowLogOut.setOutsideTouchable(true);
        popupWindowLogOut.setFocusable(true);
        //popupWindowLogOut.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View testView = layoutInflater.inflate(R.layout.register_dialog, null, false);

        AppCompatImageButton btnClose = testView.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(v -> popupWindowLogOut.dismiss());
        AppCompatTextView textViewMessage = testView.findViewById(R.id.tv_register_message);
        AppCompatTextView btn_confirm = testView.findViewById(R.id.btn_confirm);
        btn_confirm.setText("Logout");
        textViewMessage.setText("Are you sure you want to Logout?");
        btn_confirm.setOnClickListener(v -> {
            PreferenceHelper.getPreferenceHelperInstance().setPlayPrevious(getApplicationContext(), false);
            PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(getApplicationContext());
            PreferenceHelper.getPreferenceHelperInstance().setLessons(getApplicationContext(), new ArrayList<>());
            PreferenceHelper.getPreferenceHelperInstance().setFavorites(getApplicationContext(),new ArrayList<>());
            PreferenceHelper.getPreferenceHelperInstance().setClassVideoPlayOn(getApplicationContext(),"");
            PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext());
            PreferenceHelper.getPreferenceHelperInstance().setUser(getApplicationContext());
            PreferenceHelper.getPreferenceHelperInstance().setToken(getApplicationContext());
            PreferenceHelper.getPreferenceHelperInstance().setIsRegistered(getApplicationContext(), false);
            PreferenceHelper.getPreferenceHelperInstance().setCountryCode(getApplicationContext(),"");
            PreferenceHelper.getPreferenceHelperInstance().setPhone(getApplicationContext(),"");
            PreferenceHelper.getPreferenceHelperInstance().setProfile(getApplicationContext(),"");
            PreferenceHelper.getPreferenceHelperInstance().clearPreferences(getApplicationContext());
            Intent intent = new Intent(StudentProfileActivity.this,AuthActivity.class);

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            popupWindowLogOut.dismiss();
        });

        popupWindowLogOut.setContentView(testView);
        popupWindowLogOut.setElevation(10);

        if(popupWindowLogOut !=null) {

            popupWindowLogOut.showAsDropDown(it, 0, 0);
        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}