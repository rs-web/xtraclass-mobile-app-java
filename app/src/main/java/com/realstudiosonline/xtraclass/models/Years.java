package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class Years {
    private int statusCode;
    private String message;
    private List<Year> data;

    @Override
    public String toString() {
        return "Years{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Year> getData() {
        return data;
    }

    public void setData(List<Year> data) {
        this.data = data;
    }
}
