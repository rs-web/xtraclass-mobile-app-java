package com.realstudiosonline.xtraclass.models;

public class Country {
    private String name;
    private String abv;
    private String code;
    private int icon;

    public Country(String name, String abv, String code, int icon) {
        this.name = name;
        this.abv = abv;
        this.code = code;
        this.icon = icon;
    }

    public String getAbv() {
        return abv;
    }

    public void setAbv(String abv) {
        this.abv = abv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
