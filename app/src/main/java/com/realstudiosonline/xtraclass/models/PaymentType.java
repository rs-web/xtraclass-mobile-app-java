package com.realstudiosonline.xtraclass.models;

public class PaymentType {
    private String name;
    private int id;
    private String shortName;


    public PaymentType(String name, int id, String shortName) {
        this.name = name;
        this.id = id;
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
