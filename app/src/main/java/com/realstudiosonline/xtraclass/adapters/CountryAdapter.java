package com.realstudiosonline.xtraclass.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;

import java.util.List;
import java.util.Locale;

public class CountryAdapter extends ArrayAdapter<OperatedCountry> {
    Context context;

    List<OperatedCountry> operatedCountries;

    public CountryAdapter(@NonNull Context context, int resource,
                          @NonNull List<OperatedCountry> operatedCountries) {
        super(context, resource, operatedCountries);
        this.operatedCountries = operatedCountries;
        this.context = context;
    }

    @Override
    public int getCount() {
        return operatedCountries == null ? 0 : operatedCountries.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

      /*  val view: View
        if (position == 0) {
            view = layoutInflater.inflate(R.layout.header_country, parent, false)
            view.setOnClickListener {
                val root = parent.rootView
                root.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK))
                root.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK))
            }
        } else {
            view = layoutInflater.inflate(R.layout.item_country_dropdown, parent, false)
            getItem(position)?.let { country ->
                    setItemForCountry(view, country)
            }
        }
        return view*/

        ViewHolder holder;

        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            if(position == 0){
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_country, parent, false);
                ImageView imageView  = convertView.findViewById(R.id.ivArrow);
                convertView.setOnClickListener(v -> {
                    View root = parent.getRootView();
                    root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                    root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
                });

                ObjectAnimator.ofFloat(imageView, View.ROTATION, 0f, 180f).setDuration(300).start();

            }
            else
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country_dropdown, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }

        OperatedCountry country = operatedCountries.get(position);

        String  countryName = new Locale("", country.getCountryCode()).getDisplayName();
        holder.tvCountry.setText(countryName);
        holder.ivCountry.setImageResource(country.getIcon());

        // the view must be returned to our activity
        return convertView;

    }

    /*@Override
    public View getDropDownView(int position, @Nullable @org.jetbrains.annotations.Nullable View convertView, @NonNull @NotNull ViewGroup parent) {

        View view;
        if (position == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_country, parent, false);
            view.setOnClickListener(v -> {
                View root = parent.getRootView();
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            });
        }
        else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country_dropdown, parent, false);
            OperatedCountry country = getItem(position);
            if(country!=null)
            setItemForCountry(view, country);
        }
        return view;
    }
*/

    private static class ViewHolder {
        CircularImageView ivCountry;
        TextView tvCountry;

        public ViewHolder(View v) {
            ivCountry = (CircularImageView) v.findViewById(R.id.ivCountry);
            tvCountry = (TextView) v.findViewById(R.id.tvCountry);
        }
    }




}
