package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Country;

import java.util.ArrayList;

public class CountryDropDownAdapter extends ArrayAdapter<Country> {

    private Context context;
    private ArrayList<Country> countries;


    public CountryDropDownAdapter(@NonNull Context context, int resource, ArrayList<Country> countries) {
        super(context, resource,countries);
        this.context = context;
        this.countries = countries;
    }



    @Override
    public int getCount() {
        return countries == null ? 0 : countries.size(); // Return the size of the suggestions list.
    }

    @Override
    public Country getItem(int position) {
        return countries.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }



    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.country_view, parent, false);
            holder = new ViewHolder();
            holder.autoText = (AppCompatTextView) convertView.findViewById(R.id.autoText);
            holder.qualityIcon = (AppCompatImageView) convertView.findViewById(R.id.img_quality_icon);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        String text = countries.get(position).getName()+" "+countries.get(position).getCode();
        holder.autoText.setText(text);
        Glide.with(context).load(countries.get(position).getIcon()).into(holder.qualityIcon);


        return convertView;
    }


   /* @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
         *//*   convertView = inflater.inflate(R.layout.adapter_autotext,
                    parent,
                    false);*//*

            switch (position){
                case 0:
                    convertView = inflater.inflate(R.layout.country_adapter_top, parent, false);
                    break;

                case 4:
                    convertView = inflater.inflate(R.layout.country_adapter_bottom, parent, false);
                    break;
                default:
                    convertView = inflater.inflate(R.layout.country_adapter, parent, false);
            }

            holder = new ViewHolder();
            holder.autoText = (AppCompatTextView) convertView.findViewById(R.id.autoText);
            holder.qualityIcon = (AppCompatImageView) convertView.findViewById(R.id.img_quality_icon);
            holder.cardView = (CardView) convertView.findViewById(R.id.card_video_quality);
            holder.view = (LinearLayout)convertView.findViewById(R.id.view_des);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        switch (position){
            case 0:
                holder.cardView.setBackgroundResource(R.drawable.card_top_radius);
              *//*  ViewGroup.LayoutParams params = holder.cardView.getLayoutParams();
                params.height = convertDpToPixelInt(90,context);
                holder.cardView.requestLayout();*//*
                break;
            case 4:
                holder.cardView.setBackgroundResource(R.drawable.card_bottom_radius);
                *//*ViewGroup.LayoutParams bottomParams = holder.cardView.getLayoutParams();*//*
               *//* bottomParams.height = convertDpToPixelInt(90,context);
                holder.view
                holder.cardView.requestLayout();*//*
                break;
        }
        holder.autoText.setText(countries.get(position).getName());
        Glide.with(context).load(countries.get(position).getIcon()).into(holder.qualityIcon);

        return convertView;
    }
*/
    public int convertDpToPixelInt(float dp, Context context) {
        return (int) (dp * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }


    private static class ViewHolder {
        AppCompatTextView autoText;
        AppCompatImageView qualityIcon;
        CardView cardView;
        View view;
    }
}
