package com.realstudiosonline.xtraclass.utils.onboarding.listeners;

public interface PaperOnboardingOnLeftOutListener {

    void onLeftOut();

}
