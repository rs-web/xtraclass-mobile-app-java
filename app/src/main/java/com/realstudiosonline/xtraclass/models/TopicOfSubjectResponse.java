package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class TopicOfSubjectResponse extends DataResponse {
    List<TopicOfSubject> data;


    public List<TopicOfSubject> getData() {
        return data;
    }

    public void setData(List<TopicOfSubject> data) {
        this.data = data;
    }
}
