package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class LibraryItemResponse {
    private List<Library> data;
    private String status;

    public List<Library> getData() {
        return data;
    }

    public void setData(List<Library> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LibraryItemResponse{" +
                "data=" + data +
                ", status='" + status + '\'' +
                '}';
    }
}
