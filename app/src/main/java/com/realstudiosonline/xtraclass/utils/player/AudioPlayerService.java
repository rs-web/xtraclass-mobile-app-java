package com.realstudiosonline.xtraclass.utils.player;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.media.MediaBrowserServiceCompat;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultControlDispatcher;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.ui.PlayerNotificationManager.NotificationListener;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.activities.NowPlayingActivity;
import com.realstudiosonline.xtraclass.models.AudioItem;

import org.checkerframework.checker.units.qual.A;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.realstudiosonline.xtraclass.utils.Constants.ACTION_RELEASE_OLD_PLAYER;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_ITEM;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_PODCAST_IMAGE;
import static com.realstudiosonline.xtraclass.utils.Constants.EXTRA_RESULT_NAME;
import static com.realstudiosonline.xtraclass.utils.Constants.FAST_FORWARD_INCREMENT;
import static com.realstudiosonline.xtraclass.utils.Constants.NOTIFICATION_PENDING_INTENT_ID;
import static com.realstudiosonline.xtraclass.utils.Constants.PLAYBACK_CHANNEL_ID;
import static com.realstudiosonline.xtraclass.utils.Constants.PLAYBACK_NOTIFICATION_ID;
import static com.realstudiosonline.xtraclass.utils.Constants.REWIND_INCREMENT;
import static com.realstudiosonline.xtraclass.utils.Constants.getItemImageUrl;
import static com.realstudiosonline.xtraclass.utils.Constants.loadImage;

public class AudioPlayerService extends MediaBrowserServiceCompat implements Player.EventListener {

    private static MediaSessionCompat mMediaSession;
    private PlaybackStateCompat.Builder mStateBuilder;

    private static final String XTRA_CLASS_POD_ROOT_ID = "xtra_class_pod_root_id";
    private static final String XTRA_CLASS_POD_EMPTY_ROOT_ID = "empty_root_id";

    /** Member variable for the ExoPlayer */
    private SimpleExoPlayer player;

    /** A notification manager to start, update and cancel a media style notification reflecting
     * the player state */
    private PlayerNotificationManager mPlayerNotificationManager;

    /** Attributes for audio playback, which configure the underlying platform AudioTrack */
    private AudioAttributes mAudioAttributes;

    /** Tag for a MediaSessionCompat */
    private static final String TAG = AudioPlayerService.class.getSimpleName();

    /** The enclosure URL for the episode's audio file */
    private String mUrl;
    /** The current episode item*/
    private AudioItem mItem;
    /** The podcast title */
    private String mPodcastName;
    /** The podcast image URL */
    private String mPodcastImage;
    /** The podcast bitmap image */
    private Bitmap mBitmap;

    private boolean mAudioNoisyReceiverRegistered;
    private final IntentFilter mAudioNoisyIntentFilter =
            new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
    /**
     * A BroadcastReceiver that listens for an ACTION_AUDIO_BECOMING_NOISY intent whenever you're
     * playing audio. This handles changes in audio output to avoid suddenly playing out loud if a
     * peripheral like headphones is disconnected while in use.
     * Reference: @see "https://developer.android.com/guide/topics/media-apps/volume-and-earphones"
     */
    private final BroadcastReceiver mAudioNoisyReceiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                        Log.d(TAG,"Headphones disconnected.");
                        // Pause the playback
                        if (player != null && player.getPlayWhenReady()) {
                            player.setPlayWhenReady(false);
                        }
                    }
                }
            };

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize the media session
        initializeMediaSession();

        // Create an instance of com.google.android.exoplayer2.audio.AudioAttributes
        initAudioAttributes();
    }

    /**
     * Initialize the media session.
     */
    private void initializeMediaSession() {
        // Create a MediaSessionCompat
        mMediaSession = new MediaSessionCompat(AudioPlayerService.this, TAG);

        // Enable callbacks from MediaButtons and TransportControls
        mMediaSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player
        mStateBuilder = new PlaybackStateCompat.Builder()
                .setActions(
                        PlaybackStateCompat.ACTION_PLAY |
                                PlaybackStateCompat.ACTION_PAUSE |
                                PlaybackStateCompat.ACTION_REWIND |
                                PlaybackStateCompat.ACTION_FAST_FORWARD |
                                PlaybackStateCompat.ACTION_PLAY_PAUSE);
        mMediaSession.setPlaybackState(mStateBuilder.build());

        // MySessionCallback() has methods that handle callbacks from a media controller
        mMediaSession.setCallback(new MySessionCallback());

        // Set the session's token so that client activities can communicate with it.
        setSessionToken(mMediaSession.getSessionToken());

        mMediaSession.setSessionActivity(PendingIntent.getActivity(this,
                11,
                new Intent(this, NowPlayingActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT));
    }

    /**
     * Initialize ExoPlayer.
     */
    private void initializePlayer() {
        if (player == null) {
            // Create an instance of the ExoPlayer
            DefaultRenderersFactory defaultRenderersFactory = new DefaultRenderersFactory(this);
            TrackSelector trackSelector = new DefaultTrackSelector(this);
            LoadControl loadControl = new DefaultLoadControl();
            player =  new SimpleExoPlayer.Builder(this,defaultRenderersFactory).setLoadControl(loadControl).setTrackSelector(trackSelector).build();
/*
            AudioAttributes audioAttributes = new AudioAttributes
                    .Builder()
                    .setUsage(C.USAGE_MEDIA)
                    .setContentType(C.CONTENT_TYPE_MUSIC)
                    .build();*/
         //   player.setAudioAttributes(audioAttributes);
            // Set the Player.EventListener
            player.addListener(this);

            // Prepare the MediaSource
            Uri mediaUri = Uri.parse(mUrl);
            MediaSource mediaSource = buildMediaSource(mediaUri);
            player.setMediaSource(mediaSource);
            player.prepare();
            player.setPlayWhenReady(true);

            // Set the attributes for audio playback. ExoPlayer manages audio focus automatically.
            player.setAudioAttributes(mAudioAttributes, /* handleAudioFocus= */ true);
        }
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // If there are not any pending start commands to be delivered to the service, it will
        // be called with a null intent object, so you must take care to check for this.
        // Reference: @see "https://developer.android.com/reference/android/app/Service.html#START_STICKY"
        // "https://stackoverflow.com/questions/8421430/reasons-that-the-passed-intent-would-be-null-in-onstartcommand"
        if (intent == null || intent.getAction() == null) {
            Log.e(TAG,"intent in onStartCommand is null");
            return START_STICKY;
        }
        // Check if the old player should be released
        if (intent.getAction() != null && intent.getAction().equals(ACTION_RELEASE_OLD_PLAYER)) {
            if (player != null) {
                player.stop();
                releasePlayer();
            }
        }
        Bundle b = intent.getBundleExtra(EXTRA_ITEM);
        if (b != null) {
            mItem = b.getParcelable(EXTRA_ITEM);
            String itemTitle = mItem.getTitle();
            mUrl = mItem.getEnclosures().get(0).getUrl();
            Log.d(TAG,"onStartCommand: " + itemTitle + " Url: " + mUrl);
        }
        // Get the podcast title
        if (intent.hasExtra(EXTRA_RESULT_NAME)) {
            mPodcastName = intent.getStringExtra(EXTRA_RESULT_NAME);
        }
        // Get the podcast image
        if (intent.hasExtra(EXTRA_PODCAST_IMAGE)) {
            mPodcastImage = intent.getStringExtra(EXTRA_PODCAST_IMAGE);
        }
        // Initialize ExoPlayer
        initializePlayer();

        // Convert hh:mm:ss string to seconds to put it into the metadata
//        long duration = CandyPodUtils.getDurationInMilliSeconds(mItem);
//        long duration = Long.parseLong(mItem.getITunesDuration());
//        Timber.e("duration: " + duration);
//        MediaMetadataCompat metadata = new MediaMetadataCompat.Builder()
//                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, duration).build();
//        mMediaSession.setMetadata(metadata);

        // Initialize PlayerNotificationManager
        initializeNotificationManager(mItem);

        // If an episode image exists, use it. Otherwise, use the podcast image.
        String itemImageUrl = getItemImageUrl(mItem, mPodcastImage);
        // Load a bitmap image from the URL using asyncTask
        new BitmapTask().execute(itemImageUrl);

        // The service is not immediately destroyed, and we can explicitly terminate our service
        // when finished audio playback.
        return START_STICKY;
    }


    private void initializeNotificationManager(AudioItem item) {
        // Create a notification manager and a low-priority notification channel with the channel ID
        // and channel name
        mPlayerNotificationManager = PlayerNotificationManager.createWithNotificationChannel(
                this,
                PLAYBACK_CHANNEL_ID,
                R.string.playback_channel_name,
                R.string.playback_channel_description,
                PLAYBACK_NOTIFICATION_ID,
                // An adapter to provide descriptive data about the current playing item
                new PlayerNotificationManager.MediaDescriptionAdapter() {
                    @Override
                    public String getCurrentContentTitle(Player player) {
                        return item.getTitle();
                    }

                    @Nullable
                    @Override
                    public PendingIntent createCurrentContentIntent(Player player) {
                        // Create a pending intent that relaunches the NowPlayingActivity
                        return createContentPendingIntent(item);
                    }

                    @Nullable
                    @Override
                    public String getCurrentContentText(Player player) {
                        return mPodcastName;
                    }

                    @Nullable
                    @Override
                    public Bitmap getCurrentLargeIcon(@NotNull Player player, PlayerNotificationManager.@NotNull BitmapCallback callback) {
                        // Return a podcast bitmap image using AsyncTask
                        if (mBitmap != null) {
                            return mBitmap;
                        }
                        return null;
                    }
                },
                new NotificationListener() {
                    @Override
                    public void onNotificationCancelled(int notificationId, boolean dismissedByUser) {
                        stopSelf();
                    }

                    @Override
                    public void onNotificationPosted(int notificationId, @NotNull Notification notification, boolean ongoing) {
                        //startForeground(notificationId, notification);

                        if (ongoing) {
                            // Make sure the service will not get destroyed while playing media.
                            startForeground(notificationId, notification);
                        } else {
                            // Make notification cancellable.
                            stopForeground(false);
                        }
                    }
                }

        );


        // Once the notification manager is created, attach the player
        mPlayerNotificationManager.setPlayer(player);
        // Set the MediaSessionToken
        mPlayerNotificationManager.setMediaSessionToken(mMediaSession.getSessionToken());

        // Customize the notification
        // Set the small icon of the notification
        mPlayerNotificationManager.setSmallIcon(R.drawable.ic_music_note_24);
        // Set skip previous and next actions
        mPlayerNotificationManager.setUseNavigationActions(true);
        // Set the fast forward increment by 30 sec
        mPlayerNotificationManager.setControlDispatcher(new DefaultControlDispatcher(FAST_FORWARD_INCREMENT,REWIND_INCREMENT));
        // Omit the stop action
        mPlayerNotificationManager.setUseStopAction(false);
        mPlayerNotificationManager.setUsePlayPauseActions(true);
        // Make the notification not ongoing
        // mPlayerNotificationManager.setOngoing(false);

    }

    /**
     * Create a content pending intent that relaunches the NowPlayingActivity.
     */
    private PendingIntent createContentPendingIntent(AudioItem item) {
        Intent intent = new Intent(AudioPlayerService.this, NowPlayingActivity.class);
        // Pass data via intent
        Bundle b = new Bundle();
        b.putParcelable(EXTRA_ITEM, item); // Podcast episode
        intent.putExtra(EXTRA_ITEM, b);
        intent.putExtra(EXTRA_RESULT_NAME, mPodcastName); // Podcast title
        intent.putExtra(EXTRA_PODCAST_IMAGE, mPodcastImage); // Podcast Image

        return PendingIntent.getActivity(
                AudioPlayerService.this, NOTIFICATION_PENDING_INTENT_ID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * Create an instance of AudioAttributes.
     * References: @see "https://medium.com/google-exoplayer/easy-audio-focus-with-exoplayer-a2dcbbe4640e"
     * "https://google.github.io/ExoPlayer/doc/reference/com/google/android/exoplayer2/audio/AudioAttributes.html"
     */
    private void initAudioAttributes() {
        mAudioAttributes = new AudioAttributes.Builder()
                // If audio focus should be handled, the AudioAttributes.usage must be C.USAGE_MEDIA
                // or C.USAGE_GAME. Other usages will throw an IllegalArgumentException.
                .setUsage(C.USAGE_MEDIA)
                // Since the app is playing a podcast, set contentType to CONTENT_TYPE_SPEECH.
                // SimpleExoPlayer will pause while the notification, such as when a message arrives,
                // is playing and will automatically resume afterwards.
                .setContentType(C.CONTENT_TYPE_MUSIC)
                .build();
    }

    /**
     * Release ExoPlayer.
     */
    private void releasePlayer() {
        if(player!=null)
        player.release();
        player = null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        if (player != null) {
            player.stop(true);
        }
        stopSelf();
        // Allow the notification to be swipe dismissed when paused
        // Reference: @see "https://stackoverflow.com/questions/26496670/dismissing-mediastyle-notifications"
       // mPlayerNotificationManager.setOngoing(false);

    }


    @Override
    public void onDestroy() {
        if(mMediaSession!=null)
        mMediaSession.release();
        // If the player is released it must be removed from the manager by calling setPlayer(null)
        // which will cancel the notification
        if (mPlayerNotificationManager != null) {
            mPlayerNotificationManager.setPlayer(null);
        }
        releasePlayer();

        super.onDestroy();
    }

    /**
     * Create a MediaSource.
     * @param mediaUri
     */
    private MediaSource buildMediaSource(Uri mediaUri) {

       // String userAgent = Util.getUserAgent(this, getString(R.string.app_name));
       // DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
           //     this, userAgent);

        CacheDataSourceFactory  cacheDataSourceFactory = new CacheDataSourceFactory(
                this,
                100 * 1024 * 1024,
                100 * 1024 * 1024);


        return new DefaultMediaSourceFactory(cacheDataSourceFactory).createMediaSource(MediaItem.fromUri(mediaUri));
    }

    /**
     * Returns the root node of the content hierarchy. This method controls access to the service.
     */
    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid,
                                 @Nullable Bundle rootHints) {
        return new BrowserRoot(XTRA_CLASS_POD_ROOT_ID, null);
    }

    /**
     * This method provides the ability for a client to build and display a menu of the
     * MediaBrowserService's content hierarchy.
     */
    @Override
    public void onLoadChildren(@NonNull String parentMediaId,
                               @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {

        // Browsing not allowed
        if (TextUtils.equals(XTRA_CLASS_POD_EMPTY_ROOT_ID, parentMediaId)) {
            result.sendResult(null);
            return;
        }

        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();

        // Check if this is the root menu:
        if (XTRA_CLASS_POD_ROOT_ID.equals(parentMediaId)) {
            // Build the MediaItem objects for the top level,
            // and put them in the mediaItems list...

        } else {
            // Examine the passed parentMediaId to see which submenu we're at,
            // and put the children of that menu in the mediaItems list...
        }
        result.sendResult(mediaItems);
    }

    /**
     * Media Session Callbacks, where all external clients control the player.
     */
    private class MySessionCallback extends MediaSessionCompat.Callback {


        @Override
        public void onPlay() {

            Log.d("MySessionCallback", "onPlay");
            // onPlay() callback should include code that calls startService().
            startService(new Intent(getApplicationContext(), AudioPlayerService.class));

            // Set the session active
            mMediaSession.setActive(true);
            PlaybackStateCompat state = new PlaybackStateCompat.Builder().setState(PlaybackStateCompat.STATE_PLAYING, player.getCurrentPosition(), player.getPlaybackParameters().speed).build();
            mMediaSession.setPlaybackState(state);
            // Start the player
            if (player != null) {
                player.setPlayWhenReady(true);
            }

            // Register the receiver when you begin playback
            registerAudioNoisyReceiver();
        }

        @Override
        public void onPause() {
            player.setPlayWhenReady(false);
            PlaybackStateCompat state = new PlaybackStateCompat.Builder().setState(PlaybackStateCompat.STATE_PAUSED, player.getCurrentPosition(), player.getPlaybackParameters().speed).build();
            mMediaSession.setPlaybackState(state);
            stopForeground(false);
        }

        @Override
        public void onRewind() {
            player.seekTo(Math.max(player.getCurrentPosition() - REWIND_INCREMENT, 0));
            Log.d("MySessionCallback", "onRewind");
        }

        @Override
        public void onFastForward() {
            long duration = player.getDuration();
            player.seekTo(Math.min(player.getCurrentPosition() +
                    FAST_FORWARD_INCREMENT, duration));
            Log.d("MySessionCallback", "onFastForward");
        }

        @Override
        public void onStop() {
            Log.d("MySessionCallback", "onStop");
            PlaybackStateCompat state = new PlaybackStateCompat.Builder().setState(PlaybackStateCompat.STATE_STOPPED, player.getCurrentPosition(), player.getPlaybackParameters().speed).build();
            mMediaSession.setPlaybackState(state);            // onStop() callback should call stopSelf().
            stopSelf();

            // Set the session inactive
            mMediaSession.setActive(false);

            // Stop the player
            player.stop();

            // Take the service out of the foreground
            stopForeground(true);

            // Unregister the receiver when you stop
            unregisterAudioNoisyReceiver();
        }

        @Override
        public void onSeekTo(long pos) {
            super.onSeekTo(pos);
            Log.d("MySessionCallback", "onSeekTo");
            if (player != null) {
                player.seekTo((int) pos);
            }
        }
    }

    // Player Event Listeners

    @Override
    public void onTimelineChanged(Timeline timeline, int reason) {
        Log.d("MySessionCallback", "onTimelineChanged");
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.e(TAG,"onPlayerError: " + error.getMessage());
        Toast.makeText(this, getString(R.string.toast_source_error), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPlaybackStateChanged(int playbackState) {
        Log.d(TAG,"onPlayerStateChanged:"+playbackState);
        if (playbackState == Player.STATE_IDLE) {
            // When there is nothing to play, update the state to paused
            mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                    player.getCurrentPosition(), 1f);
        } else if (playbackState == Player.STATE_BUFFERING) {
            // When ExoPlayer is buffering, not being able to play immediately,
            // update the state to buffering.
            mStateBuilder.setState(PlaybackStateCompat.STATE_BUFFERING,
                    player.getCurrentPosition(), 1f);
        } else if (playbackState == Player.STATE_READY && player.getPlayWhenReady()) {
            // When ExoPlayer is playing, update the state to playing that indicates this item is
            // currently playing.
            mStateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                    player.getCurrentPosition(), 1f);
            // Register the receiver when you begin playback
            registerAudioNoisyReceiver();
            Log.d(TAG,"onPlayerStateChanged: we are playing");
        } else if (playbackState == Player.STATE_READY) {
            // When ExoPlayer is paused, update the state to paused that indicates this item is
            // currently paused.
            mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                    player.getCurrentPosition(), 1f);
            // Unregister the receiver when you stop
            unregisterAudioNoisyReceiver();
            Log.d(TAG,"onPlayerStateChanged: we are paused");

        } else if (playbackState == Player.STATE_ENDED) {
            // When ExoPlayer finished playing, update the state to paused
            mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                    player.getCurrentPosition(), 1f);
        } else {
            // Update the state to the default state that indicates that the performer has no content to play.
            mStateBuilder.setState(PlaybackStateCompat.STATE_NONE,
                    player.getCurrentPosition(), 1f);
        }
        mMediaSession.setPlaybackState(mStateBuilder.build());
    }



    /**
     * Register the AudioNoisyReceiver when you begin playback.
     */
    private void registerAudioNoisyReceiver() {
        if (!mAudioNoisyReceiverRegistered) {
            registerReceiver(mAudioNoisyReceiver, mAudioNoisyIntentFilter);
            mAudioNoisyReceiverRegistered = true;
        }
    }

    /**
     * Unregister the AudioNoisyReceiver when you stop.
     */
    private void unregisterAudioNoisyReceiver() {
        if (mAudioNoisyReceiverRegistered) {
            unregisterReceiver(mAudioNoisyReceiver);
            mAudioNoisyReceiverRegistered = false;
        }
    }

    // Use AsyncTask to load the image from the URL

    /**
     * Loads a bitmap image for a notification
     */
    public class BitmapTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                bitmap = loadImage(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            mBitmap = bitmap;
        }
    }
}