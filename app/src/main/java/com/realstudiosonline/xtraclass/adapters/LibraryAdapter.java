package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.LibraryMenu;

import java.util.List;

public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.LibraryViewHolder> {

    private Context context;
    private List<LibraryMenu> libraryItemList;
    private onLibraryItemSelectedListener listener;

    public LibraryAdapter(Context context, List<LibraryMenu> libraryItemList, onLibraryItemSelectedListener listener) {
        this.context = context;
        this.libraryItemList = libraryItemList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public LibraryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LibraryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.library_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LibraryViewHolder holder, int position) {

        LibraryMenu libraryItem = libraryItemList.get(position);
        Glide.with(context).load(libraryItem.getIcon()).into(holder.imageViewIcon);
        holder.cardView.setCardBackgroundColor(context.getResources().getColor(libraryItem.getColor()));
        holder.cardView.setOnClickListener(v -> {
            listener.onLibraryItemSelected(libraryItem);
        });
        holder.tvText.setText(libraryItem.getText());

    }

    @Override
    public int getItemCount() {
        return libraryItemList == null ? 0 : libraryItemList.size();
    }

    static class LibraryViewHolder extends RecyclerView.ViewHolder{

        AppCompatImageView imageViewIcon;
        AppCompatTextView tvText;
        CardView cardView;

        public LibraryViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewIcon = itemView.findViewById(R.id.img_icon);
            tvText = itemView.findViewById(R.id.tv_text);
            cardView = itemView.findViewById(R.id.card_view);


        }
    }

    public  interface onLibraryItemSelectedListener{
        void onLibraryItemSelected(LibraryMenu libraryItem);
    }
}
