package com.realstudiosonline.xtraclass.models;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class History {
    int id;// put String if any problem (was String)
    String class_id;
    String period_id;
    String subject;
    String description;
    String vimeo_id;
    String teacher_id;
    String play_on;
    String starts_at;
    String school_name;
    String notes_url;
    private DateTime date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getPeriod_id() {
        return period_id;
    }

    public void setPeriod_id(String period_id) {
        this.period_id = period_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVimeo_id() {
        return vimeo_id;
    }

    public void setVimeo_id(String vimeo_id) {
        this.vimeo_id = vimeo_id;
    }

    public String getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(String teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getPlay_on() {
        return play_on;
    }

    public void setPlay_on(String play_on) {
        this.play_on = play_on;
    }

    public String getStarts_at() {
        return starts_at;
    }

    public void setStarts_at(String starts_at) {
        this.starts_at = starts_at;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getNotes_url() {
        return notes_url;
    }

    public void setNotes_url(String notes_url) {
        this.notes_url = notes_url;
    }

    public DateTime getDate() {
        try {
            this.date= DateTime.parse(this.play_on, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        }
        catch (Exception e){
            this.date =  DateTime.now();
        }
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", class_id='" + class_id + '\'' +
                ", period_id='" + period_id + '\'' +
                ", subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                ", vimeo_id='" + vimeo_id + '\'' +
                ", teacher_id='" + teacher_id + '\'' +
                ", play_on='" + play_on + '\'' +
                ", starts_at='" + starts_at + '\'' +
                ", school_name='" + school_name + '\'' +
                ", notes_url='" + notes_url + '\'' +
                '}';
    }
}
