package com.realstudiosonline.xtraclass.models;

public class SelectedVideo {
    private int id;

    public SelectedVideo(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
