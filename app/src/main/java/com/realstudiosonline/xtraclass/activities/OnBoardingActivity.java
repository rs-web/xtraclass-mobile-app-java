package com.realstudiosonline.xtraclass.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.utils.onboarding.PaperOnboardingEngine;
import com.realstudiosonline.xtraclass.utils.onboarding.PaperOnboardingPage;
import com.realstudiosonline.xtraclass.utils.onboarding.listeners.PaperOnboardingOnChangeListener;
import com.realstudiosonline.xtraclass.utils.onboarding.listeners.PaperOnboardingOnRightOutListener;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class OnBoardingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable
                                        Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.onboarding_main_layout);


        PaperOnboardingEngine engine = new PaperOnboardingEngine(findViewById(R.id.onboardingRootView),
                getDataForOnboarding(),
                getApplicationContext());

        engine.setOnChangeListener((oldElementIndex, newElementIndex) -> {
            // Probably here will be your entry action
            Toast.makeText(getApplicationContext(), "Swiped from " + oldElementIndex + " to " + newElementIndex, Toast.LENGTH_SHORT).show();
        });

        engine.setOnRightOutListener(() -> {
            // Probably here will be your exit action
            Toast.makeText(getApplicationContext(), "Swiped out right", Toast.LENGTH_SHORT).show();
        });
    }

    // Just example data for Onboarding
    private ArrayList<PaperOnboardingPage> getDataForOnboarding() {
        // prepare data
        PaperOnboardingPage scr1 = new PaperOnboardingPage("Welcome to a new way to learn", "Learn at you own pace Card description",
                Color.parseColor("#678FB4"), R.drawable.ic_online_learning, R.drawable.onboarding_pager_round_icon);
        PaperOnboardingPage scr2 = new PaperOnboardingPage("Welcome to a new way to learn", "Learn at you own pace Card description",
                Color.parseColor("#65B0B4"), R.drawable.ic_online_learning, R.drawable.onboarding_pager_round_icon);
        PaperOnboardingPage scr3 = new PaperOnboardingPage("Welcome to a new way to learn", "Learn at you own pace Card description",
                Color.parseColor("#9B90BC"), R.drawable.ic_online_learning, R.drawable.onboarding_pager_round_icon);

        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
        elements.add(scr1);
        elements.add(scr2);
        elements.add(scr3);
        return elements;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
