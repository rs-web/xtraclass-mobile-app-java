package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.QuizSubject;

import java.util.List;

public class PastQuestionsAdapter extends BaseAdapter {
    private Context context;
    private List<QuizSubject> quizSubjects;

    public PastQuestionsAdapter(Context context, List<QuizSubject> quizSubjects) {
        this.context = context;
        this.quizSubjects = quizSubjects;
    }

    @Override
    public int getCount() {
        return quizSubjects ==null ? 0 : quizSubjects.size();
    }

    @Override
    public QuizSubject getItem(int position) {
        return quizSubjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
         /*   convertView = inflater.inflate(R.layout.adapter_autotext,
                    parent,
                    false);*/


            if(position == 0){
                convertView = inflater.inflate(R.layout.subject_adapter_autotext_top, parent, false);
            }
            else if(position == quizSubjects.size()-1){
                convertView = inflater.inflate(R.layout.subject_adapter_autotext_bottom, parent, false);
            }
            else {
                convertView = inflater.inflate(R.layout.subject_adapter_autotext, parent, false);
            }


            holder = new ViewHolder();
            holder.autoText =  (AppCompatTextView) convertView.findViewById(R.id.autoText);
            holder.cardView = (CardView) convertView.findViewById(R.id.card_video_quality);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if(position ==0){
            holder.cardView.setBackgroundResource(R.drawable.card_top_radius);
        }
        else if(position == quizSubjects.size()-1){
            holder.cardView.setBackgroundResource(R.drawable.card_bottom_radius);
        }

        holder.autoText.setText(""+quizSubjects.get(position).getYear());
       // Glide.with(context).load(classesForCourses.get(position).getSchool_name()).into(holder.qualityIcon);


        return convertView;
    }

    public int convertDpToPixelInt(float dp, Context context) {
        return (int) (dp * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }


    private static class ViewHolder {
        AppCompatTextView autoText;
        CardView cardView;

    }
}
