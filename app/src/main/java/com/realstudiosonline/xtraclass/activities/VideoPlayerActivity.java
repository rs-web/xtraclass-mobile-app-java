package com.realstudiosonline.xtraclass.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.db.Video;
import com.realstudiosonline.xtraclass.fragments.OpenSchoolFragment;
import com.realstudiosonline.xtraclass.models.Library;
import com.realstudiosonline.xtraclass.models.PlaybackPosition;
import com.realstudiosonline.xtraclass.models.VideoSource;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;
import com.realstudiosonline.xtraclass.utils.player.BrightnessUtils;
import com.realstudiosonline.xtraclass.utils.player.GestureListener;
import com.realstudiosonline.xtraclass.utils.player.PlayerController;
import com.realstudiosonline.xtraclass.utils.player.VideoPlayer;
import com.realstudiosonline.xtraclass.utils.player.VolBar;

import java.util.ArrayList;
import java.util.List;

import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_1080P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_160P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_240P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_360P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_480P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_720P;
import static com.realstudiosonline.xtraclass.utils.Constants.MEDIA_HUB;
import static com.realstudiosonline.xtraclass.utils.Constants.convertDpToPixelInt;

public class VideoPlayerActivity extends AppCompatActivity implements PlayerController ,View.OnClickListener {

    private static final String TAG = VideoPlayerActivity.class.getSimpleName();
    private VideoSource videoSource;
    private AudioManager mAudioManager;
    VideoPlayer player;
    PlayerView playerView;
    AppCompatImageButton nextBtn, btnBookmark, btnFullScreen, videoQuality;
    private ImageView ivBrightness, ivBrightnessImage;
    private ProgressBar pBarBrighness;
    private TextView tvBrightnessPercent;
    private LinearLayout brightnessSlider, brightnessCenterText;
    private Handler mainHandler;

    //volume
    private LinearLayout volumeSlider, volumeCenterText;
    private ImageView ivVolume, ivVolumeImage;
    private VolBar pBarVolume;
    private TextView tvVolumePercent;


    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        setContentView(R.layout.activity_video_player);

        playerView = findViewById(R.id.player_view);


        mainHandler = new Handler();
        //brightness
        brightnessSlider = (LinearLayout)findViewById(R.id.brightness_slider_container);
        ivBrightness =  findViewById(R.id.brightness_image);
        ivBrightnessImage = (ImageView) findViewById(R.id.brightnessIcon);
        pBarBrighness = (ProgressBar) findViewById(R.id.brightness_slider);
        tvBrightnessPercent = (TextView) findViewById(R.id.brigtness_perc_center_text);
        brightnessCenterText = (LinearLayout) findViewById(R.id.brightness_center_text);

        btnBookmark = findViewById(R.id.btn_bookmark);
        btnBookmark.setVisibility(View.GONE);
        nextBtn = findViewById(R.id.btn_next);
        btnFullScreen = findViewById(R.id.exo_fullscreen);
        videoQuality = findViewById(R.id.exo_quality);
        btnFullScreen.setVisibility(View.GONE);
        nextBtn.setOnClickListener(this);
        videoQuality.setOnClickListener(this);

        Library library = (Library) getIntent().getSerializableExtra("library");

        int bitrate = PreferenceHelper.getPreferenceHelperInstance().getVideoQuality(getApplicationContext());

        Log.d("BITRATE", "bitrate:" + bitrate);
        new Handler().postDelayed(() -> {

            if (bitrate > 0) {
                int drawable = getBitrateDrawableTinted(bitrate);
                videoQuality.setImageResource(drawable);
            } else {

                videoQuality.setImageResource(R.drawable.sd_blue);
            }
        }, 300);

        videoSource = makeVideoSource(new Video(OpenSchoolFragment.url, Long.getLong("zero", 0)));

        initSource(this);
    }


    /***********************************************************
     Handle audio on different events
     ***********************************************************/
    private final AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                    switch (focusChange) {
                        case AudioManager.AUDIOFOCUS_GAIN:
                            if (player != null)
                                //  player.getPlayer().setPlayWhenReady(true);
                                break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            // Audio focus was lost, but it's possible to duck (i.e.: play quietly)
                            if (player != null)
                                player.getPlayer().setPlayWhenReady(false);
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        case AudioManager.AUDIOFOCUS_LOSS:
                            // Lost audio focus, probably "permanently"
                            // Lost audio focus, but will gain it back (shortly), so note whether
                            // playback should resume
                            if (player != null)
                                player.getPlayer().setPlayWhenReady(false);
                            break;
                    }
                }
            };


    private VideoSource makeVideoSource(Video video) {
        List<VideoSource.SingleVideo> singleVideos = new ArrayList<>();
        singleVideos.add(new VideoSource.SingleVideo(video.getVideoUrl(), null, video.getWatchedLength()));
        return new VideoSource(singleVideos, 0);
    }

    private void initSource(Activity activity) {

        if (videoSource.getVideos() == null) {
            Toast.makeText(VideoPlayerActivity.this, "can not play video", Toast.LENGTH_SHORT).show();
            return;
        }

        player = new VideoPlayer(playerView, activity, videoSource, this,videoQuality);

        //playerView.setOnTouchListener(new ExVidPlayerGestureListener(VideoPlayerActivity.this));

        mAudioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);

        //optional setting
        if (playerView.getSubtitleView() != null)
            playerView.getSubtitleView().setVisibility(View.GONE);
        player.seekToOnDoubleTap();

        playerView.setControllerVisibilityListener(visibility ->
        {
            Log.i(TAG, "onVisibilityChange: " + visibility);
            if (player.isLock())
                playerView.hideController();
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        //hideSystemUi();
       /* if (player != null)
            player.resumePlayer();*/
    }


    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
            player.pausePlayer();
        }
        // player.releasePlayer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAudioManager != null) {
            mAudioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
            mAudioManager = null;
        }
        if (player != null) {
            player.releasePlayer();
            player = null;
        }

    }

    @Override
    public void audioFocus() {
        mAudioManager.requestAudioFocus(
                mOnAudioFocusChangeListener,
                AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
    }

    @Override
    public void setMuteMode(boolean mute) {

    }

    @Override
    public void showProgressBar(boolean visible) {

    }

    @Override
    public void showRetryBtn(boolean visible) {

    }

    @Override
    public void showSubtitle(boolean show) {

    }

    @Override
    public void changeSubtitleBackground() {

    }

    @Override
    public void setVideoWatchedLength() {

    }

    @Override
    public void videoEnded() {

    }

    @Override
    public void disableNextButtonOnLastVideo(boolean disable) {
        if (disable) {
            nextBtn.setImageResource(R.drawable.forward_icon_grey);
            nextBtn.setEnabled(false);
            return;
        }

        nextBtn.setImageResource(R.drawable.forward_icon);
        nextBtn.setEnabled(true);
    }

    @Override
    public void onVideoQualityChanged(int bitrate) {
        Log.d("onVideoQualityChanged", "Override Quality Select:" + bitrate);

        int drawable = getBitrateDrawableTinted(bitrate);
        videoQuality.setImageResource(drawable);
    }

    @Override
    public void onSeekToNext(int position) {

    }

    @Override
    public void onSeekToPrevious(int position) {

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int controllerId = view.getId();

        switch (controllerId) {
            case R.id.exo_quality:
                player.setSelectedQuality(VideoPlayerActivity.this);
                break;
            case R.id.retry_btn:
                initSource(VideoPlayerActivity.this);
                showProgressBar(true);
                showRetryBtn(false);
                break;
            case R.id.btn_next:
                player.seekToNext();
                break;
            case R.id.exo_prev:
                player.seekToPrevious();
                break;
            default:
                break;
        }
    }

    private int getBitrateDrawableTinted(int bitrate) {
        if (bitrate == Format.NO_VALUE) {
            return R.drawable.au_blue;
        }
        if (bitrate <= BITRATE_160P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_240P) {
            return R.drawable.ld_blue;
        }
        if (bitrate <= BITRATE_360P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_480P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_720P) {
            return R.drawable.hd_blue;
        }
        if (bitrate <= BITRATE_1080P) {
            return R.drawable.hd_blue;
        }
        return R.drawable.hd_blue;
    }

    private int extractVerticalDeltaScale(float deltaY, ProgressBar progressBar) {
        return extractDeltaScale(progressBar.getHeight(), deltaY, progressBar);
    }

    private int extractDeltaScale(int availableSpace, float deltaX, ProgressBar progressBar) {
        int x = (int) deltaX;
        float scale;
        float progress = progressBar.getProgress();
        final int max = progressBar.getMax();

        if (x < 0) {
            scale = (float) (x) / (float) (max - availableSpace);
            progress = progress - (scale * progress);
        } else {
            scale = (float) (x) / (float) availableSpace;
            progress += scale * max;
        }

        return (int) progress;
    }

    private void updateVolumeProgressBar(float v) {
        int max = pBarVolume.getMax();
        volumeSlider.setVisibility(View.VISIBLE);
        volumeCenterText.setVisibility(View.VISIBLE);
        int vol = (int) (v);
        if (vol < 0) {
            vol = 0;
        } else if (vol > max) {
            vol = max;
        }
        pBarVolume.setProgress((int) vol);
        int volPerc = pBarVolume.getProgress() * 100 / max;
        tvVolumePercent.setText(" " + volPerc);
        if (volPerc < 1) {
            ivVolume.setImageResource(R.drawable.hplib_volume_mute);
            ivVolumeImage.setImageResource(R.drawable.hplib_volume_mute);
            tvVolumePercent.setVisibility(View.GONE);
        } else if (volPerc >= 1) {
            ivVolume.setImageResource(R.drawable.hplib_volume);
            ivVolumeImage.setImageResource(R.drawable.hplib_volume);
            tvVolumePercent.setVisibility(View.VISIBLE);
        }
        mainHandler.postDelayed(new Runnable() {
            @Override public void run() {
                volumeSlider.setVisibility(View.GONE);
                volumeCenterText.setVisibility(View.GONE);
            }
        }, 2000);
    }

    private void updateBrightnessProgressBar(float v) {
        brightnessSlider.setVisibility(View.VISIBLE);
        brightnessCenterText.setVisibility(View.VISIBLE);

        if (v < BrightnessUtils.MIN_BRIGHTNESS) {
            v = BrightnessUtils.MIN_BRIGHTNESS;
        } else if (v > BrightnessUtils.MAX_BRIGHTNESS) {
            v = BrightnessUtils.MAX_BRIGHTNESS;
        }
        BrightnessUtils.set(VideoPlayerActivity.this, (int) v);
        pBarBrighness.setProgress((int) v);
        int brightPerc = pBarBrighness.getProgress() * 100 / 255;
        if (brightPerc < 30) {
            ivBrightness.setImageResource(R.drawable.brightness_minimum);
            ivBrightnessImage.setImageResource(R.drawable.brightness_minimum);
        } else if (brightPerc > 30 && brightPerc < 80) {
            ivBrightness.setImageResource(R.drawable.brightness_medium);
            ivBrightnessImage.setImageResource(R.drawable.brightness_medium);
        } else if (brightPerc > 80) {
            ivBrightness.setImageResource(R.drawable.brightness_maximum);
            ivBrightnessImage.setImageResource(R.drawable.brightness_maximum);
        }
        tvBrightnessPercent.setText(" " + (int) brightPerc);
        mainHandler.postDelayed(new Runnable() {
            @Override public void run() {
                brightnessSlider.setVisibility(View.GONE);
                brightnessCenterText.setVisibility(View.GONE);
            }
        }, 2000);
    }

    private class ExVidPlayerGestureListener extends GestureListener {
        ExVidPlayerGestureListener(Context ctx) {
            super(ctx);
        }


        @Override public void onHorizontalScroll(MotionEvent event, float delta) {

        }

        @Override public void onVerticalScroll(MotionEvent event, float delta) {

            if (event.getPointerCount() == ONE_FINGER) {
                updateBrightnessProgressBar(extractVerticalDeltaScale(-delta, pBarBrighness));
            } else {
                updateVolumeProgressBar(extractVerticalDeltaScale(-delta, pBarVolume));
            }
        }

        @Override public void onSwipeBottom() {

        }

        @Override public void onSwipeTop() {

        }
    }


}
