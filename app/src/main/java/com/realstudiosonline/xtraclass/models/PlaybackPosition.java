package com.realstudiosonline.xtraclass.models;

public class PlaybackPosition {
    private String lessonId;
    private String video_url;
    private Long playBackPosition;

    public PlaybackPosition(String lessonId, String video_url, Long playBackPosition) {
        this.lessonId = lessonId;
        this.video_url = video_url;
        this.playBackPosition = playBackPosition;
    }

    public String getLessonId() {
        return lessonId;
    }

    public void setLessonId(String lessonId) {
        this.lessonId = lessonId;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public Long getPlayBackPosition() {
        return playBackPosition;
    }

    public void setPlayBackPosition(Long playBackPosition) {
        this.playBackPosition = playBackPosition;
    }

    @Override
    public String toString() {
        return "PlaybackPosition{" +
                "lessonId='" + lessonId + '\'' +
                ", video_url='" + video_url + '\'' +
                ", playBackPosition=" + playBackPosition +
                '}';
    }
}
