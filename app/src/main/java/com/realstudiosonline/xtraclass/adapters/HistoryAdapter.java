package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.History;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.realstudiosonline.xtraclass.utils.TimeAgo.getTimeAgo;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    List<History> histories;
    Context context;
    SetHistoryClickListener listener;

    public HistoryAdapter(List<History> histories, Context context, SetHistoryClickListener listener) {
        this.histories = histories;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new HistoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull HistoryAdapter.HistoryViewHolder holder, int position) {
        History history = histories.get(position);
        holder.bind(history, listener);
    }

    @Override
    public int getItemCount() {
        return histories==null ? 0: histories.size();
    }


    public static class HistoryViewHolder extends RecyclerView.ViewHolder{
        View historyView;
        AppCompatTextView tv_lesson_title,tv_school_name,tv_date_time;
        public HistoryViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            historyView = itemView.findViewById(R.id.view_history);
            tv_lesson_title = itemView.findViewById(R.id.tv_lesson_title);
            tv_school_name = itemView.findViewById(R.id.tv_school_name);
            tv_date_time = itemView.findViewById(R.id.tv_date_time);
        }

        public void bind(History history, SetHistoryClickListener listener){
            try {
                String formattedTime = getTimeAgo(history.getDate().getMillis());
                tv_date_time.setText(formattedTime);
            }
            catch (Exception e){
                e.printStackTrace();
            }

            tv_lesson_title.setText(history.getSubject());
            tv_school_name.setText(history.getSchool_name());

            historyView.setOnClickListener(v -> listener.onHistoryClick(history));
        }
    }

    public interface SetHistoryClickListener{
        void onHistoryClick(History history);
    }
}
