package com.realstudiosonline.xtraclass.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.realstudiosonline.xtraclass.XtraClassApp;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static ApiClient apiClient;

    private ApiInterface apiCallback;
    private final int RESULT_OK = 200;

    public static ApiClient getApiClient() {
        if (apiClient == null) {
            apiClient = new ApiClient();
        }
        return apiClient;
    }

    public  boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public  Cache getCache(Context context){
        File httpCacheDirectory = new File(context.getCacheDir(), "xtraclass_http_cache");
        return new Cache(httpCacheDirectory, 10 * 1024 * 1024);
    }
    /**
     * Static method to to get api client instance
     *
     * @return ApiInterface instance
     */
    public ApiInterface getInstance(Context context) {

        try {
            if (apiCallback == null) {

               // TokenInterceptor interceptor=new TokenInterceptor();
                Cache cache = getCache(context);
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                httpClient.cache(cache);
                //httpClient.addInterceptor(interceptor);
                httpClient.connectTimeout(60, TimeUnit.SECONDS);
                httpClient.readTimeout(60, TimeUnit.SECONDS);
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(logging);
                httpClient.addNetworkInterceptor(chain -> {
                    Request originalRequest = chain.request();
                    String cacheHeaderValue = isNetworkAvailable(context)
                            ? "public, max-age=2419200"
                            : "public, only-if-cached, max-stale=2419200" ;

                    Request request = originalRequest.newBuilder().build();
                    Response response = chain.proceed(request);
                    return response.newBuilder()
                            .removeHeader("Pragma")
                            .removeHeader("Cache-Control")
                            .header("Cache-Control", cacheHeaderValue)
                            .build();
                });




                Retrofit client = new Retrofit.Builder()
                        .baseUrl(ApiInterface.baseUrl)
                        .client(httpClient.build())
                        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                apiCallback = client.create(ApiInterface.class);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return apiCallback;
    }


/*    public boolean checkForSuccess(final @NonNull BaseModel weatherData) {
        String code = weatherData.getCod();
        Log.i("onNext :", String.valueOf(code));
        if (Integer.parseInt(code) == RESULT_OK) {
            return true;
        }
//       else if (showError) {
//            if (weatherData.getMessage() != null)
//                Toast.makeText(context, weatherData.getMessage(), Toast.LENGTH_SHORT).show();
//        }
        return false;
    }*/

  /*  public void showCommonError(Context context) {
        Toast.makeText(context, R.string.str_something_went_wrong, Toast.LENGTH_SHORT).show();
    }*/

}
