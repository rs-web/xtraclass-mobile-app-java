package com.realstudiosonline.xtraclass.models;

public class SubscribePlanResponse {

    private SubscribePlan data;

    public SubscribePlan getData() {
        return data;
    }

    public void setData(SubscribePlan data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SubscribePlanResponse{" +
                "data=" + data +
                '}';
    }
}
