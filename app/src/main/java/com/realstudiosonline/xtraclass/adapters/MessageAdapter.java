package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Message;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.realstudiosonline.xtraclass.utils.TimeAgo.getTimeAgo;


public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Message> mMessages;
    private setOnMessageClickListener listener;
    private Context context;


    public MessageAdapter(List<Message> messages, Context context, setOnMessageClickListener listener) {
        this.context = context;
        mMessages = messages;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {

        View view;
        int layout;
        switch (viewType) {
        case Message.TYPE_MESSAGE:
            layout = R.layout.message_item;
            view= LayoutInflater
                    .from(parent.getContext())
                    .inflate(layout, parent, false);
            return  new MessageViewHolder(view);
        case Message.TYPE_LOG:
            layout = R.layout.item_log;
            view= LayoutInflater
                    .from(parent.getContext())
                    .inflate(layout, parent, false);
            return new MessageLogViewHolder(view);
        case Message.TYPE_ACTION:
            layout = R.layout.item_action;
            view= LayoutInflater
                    .from(parent.getContext())
                    .inflate(layout, parent, false);
            return new MessageActionViewHolder(view);
        }
       return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = mMessages.get(position);

        Log.d("getItemViewType", "getItemViewType(position) :"+getItemViewType(position));

        if (getItemViewType(position) == Message.TYPE_ACTION) {
            ((MessageActionViewHolder) holder).bind(message);
        }
        else if(getItemViewType(position) == Message.TYPE_MESSAGE){
            ((MessageViewHolder) holder).bind(message);
        }
        else if(getItemViewType(position) == Message.TYPE_LOG){
            ((MessageLogViewHolder) holder).bind(message);
        }


    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mMessages.get(position).getMessage_type();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder implements ChatReplyAdapter.setOnMessageRelyClickListener{
        CircularImageView imageViewAvatar;
        AppCompatTextView mMessageView,textViewDateTime, btnReply, mUsernameView;
        View viewMessage;
        RecyclerView recyclerView;


        public void bind(Message message){

            try {
                if(message.getChildren_accounts()!=null){
                    if(message.getChildren_accounts().size()>0){
                        recyclerView.setLayoutManager(new LinearLayoutManager(context));
                        ChatReplyAdapter chatReplyAdapter = new ChatReplyAdapter(context, message.getChildren_accounts(),this);
                        recyclerView.setAdapter(chatReplyAdapter);
                        chatReplyAdapter.notifyDataSetChanged();
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    else {
                        recyclerView.setVisibility(View.GONE);
                    }
                }
                else {
                    recyclerView.setVisibility(View.GONE);

                }
                setMessage(""+message.getContent());

                if(message.getUser()!=null)
                setUsername(""+message.getUser().getUsername());
                String formattedTime = getTimeAgo(message.getDate().getMillis());
                textViewDateTime.setText(formattedTime);
                Glide.with(context).load(R.drawable.profile_demo).into(imageViewAvatar);
                btnReply.setOnClickListener(v -> listener.onReply(message));
                imageViewAvatar.setOnClickListener(v -> listener.onProfileClicked(message));
            }
            catch (Exception e){
                e.printStackTrace();

            }
        }

        public MessageViewHolder(View itemView) {
            super(itemView);

            recyclerView = itemView.findViewById(R.id.recycler_view_chat_reply);



            mUsernameView =  itemView.findViewById(R.id.tv_sender_name);

            mMessageView =  itemView.findViewById(R.id.tv_chat_text);

            imageViewAvatar = itemView.findViewById(R.id.img_user_avatar);

            textViewDateTime = itemView.findViewById(R.id.tv_date_time);

            viewMessage = itemView.findViewById(R.id.view_message);

            btnReply = itemView.findViewById(R.id.btn_reply);
        }

        public void setUsername(String username) {
            if (null == mUsernameView) return;
            mUsernameView.setText(username);
            //mUsernameView.setTextColor(getUsernameColor(username));
        }

        public void setMessage(String message) {
            if (null == mMessageView) return;
            mMessageView.setText(message);
        }


        @Override
        public void onChatReplySelected(Message message) {

        }

        @Override
        public void onProfileClicked(Message message) {

        }
    }


    public static class MessageLogViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView textViewMessage;


        public void bind(Message message){
            try {
                textViewMessage.setText(message.getContent());
            }
            catch (Exception e){
                e.printStackTrace();

            }
        }

        public MessageLogViewHolder(View itemView) {
            super(itemView);
            textViewMessage =  itemView.findViewById(R.id.message);


        }


    }

    public static class MessageActionViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView textViewAction, mUsernameView;


        public void bind(Message message){
            try {
                if(message.getUser()!=null)
                setUsername(message.getUser().getUsername());

            }
            catch (Exception e){
                e.printStackTrace();

            }
        }

        public MessageActionViewHolder(View itemView) {
            super(itemView);
            mUsernameView =  itemView.findViewById(R.id.tv_sender_name);

            textViewAction = itemView.findViewById(R.id.action);


        }

        public void setUsername(String username) {
            if (null == mUsernameView) return;
            mUsernameView.setText(username);
        }


    }

    public interface setOnMessageClickListener{
        void onReply(Message message);

        void onProfileClicked(Message message);

    }
}
