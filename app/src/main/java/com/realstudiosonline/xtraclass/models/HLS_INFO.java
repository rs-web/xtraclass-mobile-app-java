package com.realstudiosonline.xtraclass.models;

import com.google.gson.annotations.SerializedName;

public class HLS_INFO {
    @SerializedName("master_file")
    private String master_file;

    @SerializedName("1080_iframe")
    private String tenEighty_iframe;

    @SerializedName("720_iframe")
    private String sevenTwenty_iframe;

    @SerializedName("480_iframe")
    private String fourEighty_iframe;

    @SerializedName("360_iframe")
    private String threeSixty_iframe;

    public String getMaster_file() {
        return master_file;
    }


    public void setMaster_file(String master_file) {
        this.master_file = master_file;
    }

    public String getTenEighty_iframe() {
        return tenEighty_iframe;
    }

    public void setTenEighty_iframe(String tenEighty_iframe) {
        this.tenEighty_iframe = tenEighty_iframe;
    }

    public String getSevenTwenty_iframe() {
        return sevenTwenty_iframe;
    }

    public void setSevenTwenty_iframe(String sevenTwenty_iframe) {
        this.sevenTwenty_iframe = sevenTwenty_iframe;
    }

    public String getFourEighty_iframe() {
        return fourEighty_iframe;
    }

    public void setFourEighty_iframe(String fourEighty_iframe) {
        this.fourEighty_iframe = fourEighty_iframe;
    }

    public String getThreeSixty_iframe() {
        return threeSixty_iframe;
    }

    public void setThreeSixty_iframe(String threeSixty_iframe) {
        this.threeSixty_iframe = threeSixty_iframe;
    }

    @Override
    public String toString() {
        return "HLS_INFO{" +
                "master_file='" + master_file + '\'' +
                ", tenEighty_iframe='" + tenEighty_iframe + '\'' +
                ", sevenTwenty_iframe='" + sevenTwenty_iframe + '\'' +
                ", fourEighty_iframe='" + fourEighty_iframe + '\'' +
                ", threeSixty_iframe='" + threeSixty_iframe + '\'' +
                '}';
    }


    /*  "master_file": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/master.m3u8",
                    "1080_iframe": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-1/iframes.m3u8",
                    "720_iframe": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-2/iframes.m3u8",
                    "480_iframe": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-3/iframes.m3u8",
                    "360_iframe": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-4/iframes.m3u8",
                    "240_iframe": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-5/iframes.m3u8",
                    "1080_playlist": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-1/stream.m3u8",
                    "720_playlist": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-2/stream.m3u8",
                    "480_playlist": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-3/stream.m3u8",
                    "360_playlist": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-4/stream.m3u8",
                    "240_playlist": "/media/hls/e76d5889474c4ab49c11b4ca21cddcfb/media-5/stream.m3u8"*/
}
