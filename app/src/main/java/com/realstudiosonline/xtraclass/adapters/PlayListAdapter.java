package com.realstudiosonline.xtraclass.adapters;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.models.Lesson;
import com.realstudiosonline.xtraclass.models.Question;
import com.realstudiosonline.xtraclass.models.Quiz;
import com.realstudiosonline.xtraclass.utils.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.realstudiosonline.xtraclass.utils.Constants.ordinal;

public class PlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {


    private List<Lesson> lessonList;
    private List<Lesson> lessonListFiltered;
    private Context mContext;
    private setOnLessonClickListener listener;
    RecyclerView.ViewHolder viewHolder;

    public PlayListAdapter(List<Lesson> lessons, Context mContext, setOnLessonClickListener listener) {
        Log.d("PlayListAdapter", ""+lessons.toString());
        this.lessonListFiltered = lessons;
        this.lessonList = lessons;
        this.mContext = mContext;
        this.listener = listener;
    }


    public void setLessonList(List<Lesson> lessonList){
        this.lessonList = lessonList;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_item,parent,false);
        viewHolder = new PlayListViewHolder(v1);
        return viewHolder;
        /*switch (viewType){
            case 0:
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_item,parent,false);
                viewHolder = new PlayListViewHolder(v1);
                break;
            case 1:
                View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.quiz_list_item,parent,false);
                viewHolder = new QuizListViewHolder(v2);
                break;
        }

        return viewHolder;*/
    }

    @Override
    public int getItemViewType(int position) {
        Log.d("getItemViewType", "lessonItems.get(position).getLessonType() :"+lessonListFiltered.get(position).getLessonType());
        if(lessonListFiltered.get(position).getLessonType()==1) {
            return 1;
        }
       else
        return 0;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position){
        Lesson lesson = lessonListFiltered.get(position);

        PlayListViewHolder playListViewHolder = (PlayListViewHolder) holder;
        playListViewHolder.bind(position,lesson,mContext, listener);

      /*  switch (holder.getItemViewType()){
            case 0:
                PlayListViewHolder playListViewHolder = (PlayListViewHolder) holder;
                playListViewHolder.bind(lesson,mContext, listener);
                break;
            case 1:
                QuizListViewHolder quizListViewHolder = (QuizListViewHolder) holder;
                quizListViewHolder.bind(lesson);
                break;
        }*/


    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    lessonListFiltered = lessonList;
                } else {
                    List<Lesson> filteredList = new ArrayList<>();
                    for (Lesson row : lessonList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTopic_name().toLowerCase().contains(charString.toLowerCase()) || row.getPeriod_title().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    lessonListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = lessonListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                lessonListFiltered = (ArrayList<Lesson>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return lessonListFiltered.size();
    }

     static class PlayListViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imageViewLessonPlayIcon;
        AppCompatTextView textViewLessonTitle,tvLessonTitleQuiz;
        View layoutQuiz,layoutLesson;

        public PlayListViewHolder(@NonNull View itemView) {
            super(itemView);
            layoutQuiz = itemView.findViewById(R.id.layout_quiz);
            layoutLesson = itemView.findViewById(R.id.layout_lesson);
            imageViewLessonPlayIcon = itemView.findViewById(R.id.img_lesson_play_icon);
            textViewLessonTitle = itemView.findViewById(R.id.tv_lesson_title);
            tvLessonTitleQuiz = itemView.findViewById(R.id.tv_lesson_title_quiz);
        }

         public void bind(int position,Lesson lesson,Context mContext, setOnLessonClickListener listener){
             String text = "<font color=#4BC2FF>"+lesson.getPeriod_title()+"</font> "+lesson.getTopic_name();
             List<Question> questions = new ArrayList<>();
             if(lesson.isHas_quiz()){
                 layoutQuiz.setVisibility(View.VISIBLE);
                 questions.addAll(lesson.getQuestions());
                 String quizTitle = ordinal((position+1)) + "\t class exercise";
                 tvLessonTitleQuiz.setText(quizTitle);

             }
             else layoutQuiz.setVisibility(View.GONE);

             textViewLessonTitle.setText(Html.fromHtml(text));
       /* if(position==0)
        holder.imageViewLessonPlayIcon.setImageResource(R.drawable.play_icon_blue);
        else
        holder.imageViewLessonPlayIcon.setImageResource(R.drawable.lock);
*/

             layoutQuiz.setOnClickListener(v -> listener.onQuizSelected(questions));
             layoutLesson.setOnClickListener(v -> listener.onLessonSelected(lesson));

             if(lesson.isSelected()){
                 imageViewLessonPlayIcon.setImageResource(R.drawable.play_icon_blue);
                 itemView.setBackgroundColor(mContext.getResources().getColor(R.color.colorSelectedLesson));
             }
             else {

                 try {
                     if(lesson.getVideo_info().getOriginal_media_url()!=null){
                         final String filePath = Constants.getSaveDir(mContext) + "/Offline/" + Constants.getNameFromUrl(lesson.getVideo_info().getOriginal_media_url());
                         File myFile = new File(filePath);
                         if (myFile.exists()) {
                             imageViewLessonPlayIcon.setImageResource(R.drawable.eye_green);
                         }
                         else {
                             imageViewLessonPlayIcon.setImageResource(R.drawable.eye_icon_new);
                         }
                     }

                 }
                 catch (Exception e){
                     imageViewLessonPlayIcon.setImageResource(R.drawable.eye_icon_new);
                     itemView.setBackgroundColor(mContext.getResources().getColor(R.color.colorViewPagerBackground));
                     e.printStackTrace();

                 }


             }
         }
    }

    static class QuizListViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView textViewLessonTitle;
        public QuizListViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewLessonTitle = itemView.findViewById(R.id.tv_lesson_title);
        }

        public void bind(Lesson lesson){
            if(lesson.isHas_quiz())
            textViewLessonTitle.setText(lesson.getQuiz_id());
        }
    }

    public interface setOnLessonClickListener{
        void onLessonSelected(Lesson lesson);
        void onQuizSelected(List<Question> questions);
    }
}
