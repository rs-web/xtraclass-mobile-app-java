package com.realstudiosonline.xtraclass.utils.player;

import com.google.android.exoplayer2.Format;

public interface PlayerController {

    void setMuteMode(boolean mute);
    void showProgressBar(boolean visible);
    void showRetryBtn(boolean visible);
    void showSubtitle(boolean show);
    void changeSubtitleBackground();
    void audioFocus();
    void setVideoWatchedLength();
    void videoEnded();
    void disableNextButtonOnLastVideo(boolean disable);
    void onVideoQualityChanged(int bitrate);
    void onSeekToNext(int position);
    void onSeekToPrevious(int position);
}
