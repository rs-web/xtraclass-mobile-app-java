package com.realstudiosonline.xtraclass.models;

public class UserPlan {

    private int id;
    private int user_id;
    private String subs_type;
    private String starts_on;
    private String expired_on;
    private int plan_id;
    private String transaction_id;
    private String transaction_cust_id;
    private String activate_mode_type;
    private String payment_status;
    private String status;
    private Plan plan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getSubs_type() {
        return subs_type;
    }

    public void setSubs_type(String subs_type) {
        this.subs_type = subs_type;
    }

    public String getStarts_on() {
        return starts_on;
    }

    public void setStarts_on(String starts_on) {
        this.starts_on = starts_on;
    }

    public String getExpired_on() {
        return expired_on;
    }

    public void setExpired_on(String expired_on) {
        this.expired_on = expired_on;
    }

    public int getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(int plan_id) {
        this.plan_id = plan_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTransaction_cust_id() {
        return transaction_cust_id;
    }

    public void setTransaction_cust_id(String transaction_cust_id) {
        this.transaction_cust_id = transaction_cust_id;
    }

    public String getActivate_mode_type() {
        return activate_mode_type;
    }

    public void setActivate_mode_type(String activate_mode_type) {
        this.activate_mode_type = activate_mode_type;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    @Override
    public String toString() {
        return "UserPlan{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", subs_type='" + subs_type + '\'' +
                ", starts_on='" + starts_on + '\'' +
                ", expired_on='" + expired_on + '\'' +
                ", plan_id=" + plan_id +
                ", transaction_id='" + transaction_id + '\'' +
                ", transaction_cust_id='" + transaction_cust_id + '\'' +
                ", activate_mode_type='" + activate_mode_type + '\'' +
                ", payment_status='" + payment_status + '\'' +
                ", status='" + status + '\'' +
                ", plan=" + plan +
                '}';
    }


    /*{
    "data": {
        "id": 0,
        "user_id": 0,
        "subs_type": "",
        "starts_on": "",
        "expired_on": "",
        "plan_id": 0,
        "transaction_id": "",
        "transaction_cust_id": "",
        "activate_mode_type": "",
        "payment_status": "",
        "status": "",
        "plan": {
            "id": 0,
            "name": "",
            "duration": "",
            "amount": 0
        }
    }
}*/
}
