package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class UserPlanResponse {

    private List<UserPlan> data;
    private String current_date_time;

    public String getCurrent_date_time() {
        return current_date_time;
    }

    public void setCurrent_date_time(String current_date_time) {
        this.current_date_time = current_date_time;
    }

    public List<UserPlan> getData() {
        return data;
    }

    public void setData(List<UserPlan> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "UserPlanResponse{" +
                "data=" + data +
                ", current_date_time='" + current_date_time + '\'' +
                '}';
    }
}
