package com.realstudiosonline.xtraclass.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.XtraClassApp;
import com.realstudiosonline.xtraclass.adapters.ChatReplyAdapter;
import com.realstudiosonline.xtraclass.adapters.MessageAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.ChatUser;
import com.realstudiosonline.xtraclass.models.Message;
import com.realstudiosonline.xtraclass.models.SendMessageResponse;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.KeyboardHeightProvider;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Response;


public class ChatFragment extends Fragment implements MessageAdapter.setOnMessageClickListener,
        ChatReplyAdapter.setOnMessageRelyClickListener,KeyboardHeightProvider.KeyboardHeightObserver {



    RecyclerView recyclerView;
    MessageAdapter chatAdapter;
    //ChatAdapter chatAdapter;
    private CardView cardViewChatInput;
    View chatView;
    private Socket mSocket;
    private static final int TYPING_TIMER_LENGTH = 1000;
    private boolean mTyping = false;
    private final Handler mTypingHandler = new Handler();
    User user;
    private Boolean isConnected = true;
    private static final String TAG = ChatFragment.class.getSimpleName();

  //  private RecyclerView mMessagesView;
    private TextInputEditText mInputMessageView;
    private List<Message> mMessages = new ArrayList<>();

    private KeyboardHeightProvider keyboardHeightProvider;

    private float initialY;
  //  private RecyclerView.Adapter mAdapter;


    public ChatFragment() {
        // Required empty public constructor
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recycler_view_chat);

        user = PreferenceHelper.getPreferenceHelperInstance().getUser(getActivity());

        mInputMessageView = view.findViewById(R.id.edt_question_input);



        mInputMessageView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (null == user) return;
                if (!mSocket.connected()) return;

                if (!mTyping) {
                    mTyping = true;
                    mSocket.emit("typing");
                }

                mTypingHandler.removeCallbacks(onTypingTimeout);
                mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        chatView = view.findViewById(R.id.view_chat);


        cardViewChatInput = view.findViewById(R.id.chat_input);

        cardViewChatInput.post(() -> {
            initialY = cardViewChatInput.getY();
            Log.d("initialY", ""+initialY);
        });

        keyboardHeightProvider = new KeyboardHeightProvider(getActivity());
        view.post(() -> keyboardHeightProvider.start());
        cardViewChatInput.setBackgroundResource(R.drawable.top_curve);

        if(getActivity()!=null) {
            XtraClassApp app = (XtraClassApp) getActivity().getApplication();
            mSocket = app.getSocket();
            mSocket.on("login", onLogin);
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            //`mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            // mSocket.on("messageSent", onMessageSent);
            mSocket.on("new message", onNewMessage);
            mSocket.on("user joined", onUserJoined);
            mSocket.on("user left", onUserLeft);
            mSocket.on("typing", onTyping);
            mSocket.on("stop typing", onStopTyping);
            if (user != null) {
                mSocket.emit("add user", user.getUsername());
            }

            mSocket.connect();
        }



        view.findViewById(R.id.btn_post_question).setOnClickListener(v -> {
            //Toast.makeText(getActivity(), "post", Toast.LENGTH_SHORT).show();
            if(mInputMessageView.getText()!=null) {
                attemptSend(mInputMessageView.getText().toString());
               // postQuestion(mInputMessageView.getText().toString());
            }
                else Toast.makeText(getActivity(), "enter text" +
                    "", Toast.LENGTH_SHORT).show();

        });

        //setMessages();

       LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
       linearLayoutManager.setReverseLayout(true);
       linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        /*recyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                HomeScreenOpenSchoolFragment.scrollUp();
            }

            @Override
            public void onShow() {
                HomeScreenOpenSchoolFragment.scrollDown();
            }
        });*/

        chatAdapter = new MessageAdapter(mMessages,getActivity(),this);


        recyclerView.setAdapter(chatAdapter);


    }

    private void setKeyboardHeight(int height) {
        Log.d("onKeyboardHeight", "height= "+height);
    }

    private void attemptSend(String text) {
        if (null == user) return;
        if (!mSocket.connected()) return;

        mTyping = false;

        String message = text.trim();
        if (TextUtils.isEmpty(message)) {
            mInputMessageView.requestFocus();
            return;
        }

        mInputMessageView.setText("");
        //addMessage(mUsername, message);
       // mSocket.emit("new message", message);
        String lesson_id = "6488";
        String class_id = "229";
        String user_id = "2317";
        String sender_type = "question";
        String parent_id = "0";
        sendMessage(lesson_id, class_id, user_id, message,sender_type, parent_id);

    }

    private void sendMessage(String lesson_id,
                             String class_id,
                             String user_id,
                             String content,
                             String sender_type,
                             String parent_id){

        ApiClient.getApiClient().getInstance(getContext()).sendChat(
                lesson_id,
                class_id,
                user_id,
                content,
                sender_type,
                parent_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<SendMessageResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull
                                          Response<SendMessageResponse> sendMessageResponseResponse) {

                if(sendMessageResponseResponse.isSuccessful()){
                    if(sendMessageResponseResponse.body()!=null) {
                        Log.d("sendMessage", "" + sendMessageResponseResponse.body().getData().toString());
                    }
                    else {

                        Log.d("sendMessage ", "Not Successful"+ sendMessageResponseResponse.message());
                    }
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull
                                        Throwable e) {


                Log.d("sendMessage", "Error: "+e.getMessage());
            }
        });
    }

    private void initSocketChatMessages(String class_id, String video_id){

        ApiClient.getApiClient().getInstance(getContext())
                .initSocketChatMessages(class_id,video_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<JsonObject>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<JsonObject> jsonObjectResponse) {

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });
    }

   /* private void postQuestion(String question){
        mMessages.add(new Message(UUID.randomUUID().toString(),"Michael Dovoh", question, new DateTime(), R.drawable.user));
        chatAdapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(mMessages.size()-1);
        mInputMessageView.setText("");
        *//*if(getActivity()!=null)
            KeyBoard.hide(getActivity(),chatView);*//*
    }*/

    /*public void setMessages() {
        for (int i=0; i<30; i++ ){
            mMessages.add(new Message(UUID.randomUUID().toString(), "Michael Dovoh", getString(R.string.lorem_ipsum_short), new DateTime(), R.drawable.user));
        }
    }*/

    @Override
    public void onReply(Message message) {
        String lesson_id = "6488";
        String class_id = "229";
        String user_id = "2317";
        String sender_type = "reply";
        sendMessage(lesson_id, class_id, user_id, "This is a sample reply. \nThank you", sender_type, String.valueOf(message.getId()));

      //  Toast.makeText(getActivity(), "onReply ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onChatReplySelected(Message message) {
        Toast.makeText(getActivity(), "onChatReplySelected ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProfileClicked(Message message) {
        Toast.makeText(getActivity(), "onProfileClicked ", Toast.LENGTH_SHORT).show();
    }



    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            String class_id = "229";
            String video_id = "6488";
            initSocketChatMessages(class_id,video_id);

            getActivity().runOnUiThread(() -> {
                if(!isConnected) {
                    if(null!=user)
                        mSocket.emit("add user", user.getUsername());
                    Toast.makeText(getActivity().getApplicationContext(),
                            R.string.connect, Toast.LENGTH_LONG).show();
                    isConnected = true;

                }
            });
        }
    };

    private Emitter.Listener onDisconnect = args -> getActivity().runOnUiThread(() -> {
        Log.i(TAG, "diconnected");
        isConnected = false;
        Toast.makeText(getActivity().getApplicationContext(),
                R.string.disconnect, Toast.LENGTH_LONG).show();
    });

    private Emitter.Listener onConnectError = args -> Log.e(TAG, "Error connecting"+args[0].toString());
       // Toast.makeText(getActivity().getApplicationContext(),R.string.error_connect, Toast.LENGTH_LONG).show();



    private Emitter.Listener onLogin = args -> {
        JSONObject data = (JSONObject) args[0];

        int numUsers;
        try {
            numUsers = data.getInt("numUsers");
        } catch (JSONException e) {
            return;
        }

    };

    /*private  Emitter.Listener onMessageSent = args -> getActivity().runOnUiThread(() -> {
        JSONObject data = (JSONObject) args[0];

        try {

           // JsonObject jsonObject = JsonParser.parseString(data.toString()).getAsJsonObject();

            JSONArray jsonArray = data.getJSONArray("message");


            for (int i = 0; i < jsonArray.length(); i++) {
                String d = jsonArray.get(i).toString();
                Message message = new Gson().fromJson(d, Message.class);
                addMessage(message);

            }
            *//*if(jsonArray.length()==1){
                String d = jsonArray.get(0).toString();
                Message message = new Gson().fromJson(d, Message.class);
                message.setMessage_type(Message.TYPE_MESSAGE);
                mMessages.add(mMessages.size()-1,message);
                chatAdapter.notifyItemInserted(mMessages.size()-1);
                scrollToBottom();
            }
            else {
                for (int i = 0; i < jsonArray.length(); i++) {
                    String d = jsonArray.get(i).toString();
                    Message message = new Gson().fromJson(d, Message.class);
                    addMessage(message);

                }
            }*//*

            Log.d("onMessageSent", "onMessageSent: "+data.getJSONArray("message"));

           // message = data.getString("message");


        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            return;
        }

        removeTyping("");

    });
*/
    private  Emitter.Listener onNewMessage = args -> getActivity().runOnUiThread(() -> {
        JSONObject oldData = (JSONObject) args[0];
        //Log.d("onNewMessage", "NEW MESSAGE: "+oldData.toString());
        //String username;
        //mMessages.clear();
        //chatAdapter.notifyDataSetChanged();
        try {

          //  String jsonFormattedString = oldData.toString().replaceAll("\\\\", "");

            Log.d("onNewMessage", "NEW MESSAGE: "+oldData.toString());
           // JSONObject data = new JSONObject(jsonFormattedString);
            //JsonObject jsonObject = JsonParser.parseString(data.toString()).getAsJsonObject();

            JSONArray jsonArray = oldData.getJSONArray("message");
            //username = data.getString("username");
            for(int i = 0; i < jsonArray.length(); i++){
                String d = jsonArray.get(i).toString();

                Message message = new Gson().fromJson(d, Message.class);
                if(message.getType().equalsIgnoreCase("question")){
                    addMessage(message);
                }
                else if(message.getType().equalsIgnoreCase("reply")){
                    for (Message m: mMessages) {
                        if(m.getParent_id() == message.getParent_id()){
                            message.getChildren_accounts().add(m);
                        }
                    }
                }

            }



            // message = data.getString("message");


        } catch (JSONException e) {
            Log.e("onNewMessage", "Error: "+e.getMessage());
            return;
        }

        removeTyping("");

    });

    private Emitter.Listener onUserJoined = args -> getActivity().runOnUiThread(() -> {
        JSONObject data = (JSONObject) args[0];
        String username;
        int numUsers;
        try {
            username = data.getString("username");
            numUsers = data.getInt("numUsers");
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            return;
        }

       // addLog(getResources().getString(R.string.message_user_joined, username));
       // addParticipantsLog(numUsers);
    });

    private Emitter.Listener onUserLeft = args -> getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
            JSONObject data = (JSONObject) args[0];
            String username;
            int numUsers;
            try {
                username = data.getString("username");
                numUsers = data.getInt("numUsers");
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return;
            }

           // addLog(getResources().getString(R.string.message_user_left, username));
           // addParticipantsLog(numUsers);
            removeTyping(username);
        }
    });

    private Emitter.Listener onTyping = args -> getActivity().runOnUiThread(() -> {
        JSONObject data = (JSONObject) args[0];
        String username;
        try {
            username = data.getString("username");
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            return;
        }
        addTyping(username);
    });

    private Emitter.Listener onStopTyping = args -> getActivity().runOnUiThread(() -> {
        JSONObject data = (JSONObject) args[0];
        String username;
        try {
            username = data.getString("username");
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            return;
        }
        removeTyping(username);
    });



    private void addMessage(Message message) {
        message.setMessage_type(Message.TYPE_MESSAGE);
        mMessages.add(message);
        Collections.sort(mMessages, new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o2.getDate().compareTo(o1.getDate());
            }
        });
        chatAdapter.notifyDataSetChanged();
        scrollToBottom();
    }

    private void addTyping(String username) {
        ChatUser chatUser = new ChatUser();
        chatUser.setDevice_token(UUID.randomUUID().toString());
        chatUser.setDevice_type("Android");
        chatUser.setEmail("");
        chatUser.setUsername(username);
        chatUser.setUuid(UUID.randomUUID().toString());
        Log.d("Typing", "username:"+ username);
        Message message = new Message(1, 1, username+"is typing", 1, 0,UUID.randomUUID().toString(),chatUser);
        message.setMessage_type(Message.TYPE_ACTION);
        mMessages.add(message);
        chatAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void scrollToBottom() {
        recyclerView.scrollToPosition(0);
    }

    private void removeTyping(String username) {
        for (int i = mMessages.size() - 1; i >= 0; i--) {
            Message message = mMessages.get(i);
            if(message.getUser()!=null)
            if (message.getMessage_type() == Message.TYPE_ACTION && message.getUser().getUsername().equals(username)) {
                mMessages.remove(i);
                chatAdapter.notifyItemRemoved(i);
            }
        }
    }


    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTyping) return;
            mTyping = false;
            mSocket.emit("stop typing");
        }
    };

    @Override
    public void onKeyboardHeightChanged(int height, int orientation) {
        int TitleBarHeight=0;
        if(getActivity()!=null) {
            Rect rectgle = new Rect();
            Window window = getActivity().getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
            int StatusBarHeight = rectgle.top;
            int contentViewTop =
                    window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
            TitleBarHeight = contentViewTop - StatusBarHeight;
            Log.d("onKeyboardHeight", "TitleBarHeight= "+TitleBarHeight);
        }
        Log.d("onKeyboardHeight", "height= "+height+" orientation=> "+orientation);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)cardViewChatInput.getLayoutParams();
        if(height>0) {
            layoutParams.bottomMargin = height-TitleBarHeight;
        }
        else {
            layoutParams.bottomMargin = 0;
        }
        cardViewChatInput.setLayoutParams(layoutParams);
        cardViewChatInput.requestLayout();
    }
    @Override
    public void onResume() {
        super.onResume();
        if(keyboardHeightProvider!=null)
        keyboardHeightProvider.setKeyboardHeightObserver(this);
    }

    @Override
    public void onPause() {
        try {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)cardViewChatInput.getLayoutParams();
            layoutParams.bottomMargin = 0;
            cardViewChatInput.setLayoutParams(layoutParams);
            cardViewChatInput.requestLayout();
        }
        catch (Exception e){
            e.printStackTrace();

        }
        super.onPause();
        if(keyboardHeightProvider!=null){
        keyboardHeightProvider.setKeyboardHeightObserver(null);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(keyboardHeightProvider!=null)
        keyboardHeightProvider.close();
    }





    /* @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void UpdateInput(ViewPagerUpdate update){
        Toast.makeText(getActivity(), ""+update.getPosition(), Toast.LENGTH_SHORT).show();
        switch (update.getPosition()) {
            case 0:
            case 2:
                chatView.setVisibility(View.GONE);
                break;

            case 1:
                chatView.setVisibility(View.VISIBLE);
                break;
        }
    }*/

}