package com.realstudiosonline.xtraclass.models;

public class SelectedLibrary {
    private boolean selected;
    private String type;
    private int id;

    public SelectedLibrary(boolean selected, String type, int id) {
        this.selected = selected;
        this.type = type;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
