package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class QuizResponse {
    public List<QuizSubject> getData() {
        return data;
    }

    public void setData(List<QuizSubject> data) {
        this.data = data;
    }

    private List<QuizSubject> data;


    @Override
    public String toString() {
        return "QuizResponse{" +
                "data=" + data +
                '}';
    }
}
