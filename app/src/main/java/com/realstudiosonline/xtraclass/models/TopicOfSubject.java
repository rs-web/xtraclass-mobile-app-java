package com.realstudiosonline.xtraclass.models;

public class TopicOfSubject {
    private int id;
    private String topic_name;
    private String description;
    private int subject_id;
    private int status;
    private int weight;
    private String created_at;
    private String updated_at;
    private String uuid;



    /*"id": 4603,
            "topic_name": "Acids, Bases And Salt",
            "description": null,
            "subject_id": 1294,
            "status": 1,
            "weight": 1,
            "deleted_at": null,
            "created_at": "2020-11-04 10:00:51",
            "updated_at": "2020-11-04 10:24:19",
            "uuid": "7f4c3255-6b19-4c73-8355-c54f36fcfc10"*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
