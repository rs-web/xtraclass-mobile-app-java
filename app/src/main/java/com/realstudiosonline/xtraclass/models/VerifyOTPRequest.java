package com.realstudiosonline.xtraclass.models;

public class VerifyOTPRequest {
    private String user_id;
    private String type;
    private String otp;
    private String mobile;

    public VerifyOTPRequest(String user_id, String type, String otp, String mobile) {
        this.user_id = user_id;
        this.type = type;
        this.otp = otp;
        this.mobile = mobile;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
