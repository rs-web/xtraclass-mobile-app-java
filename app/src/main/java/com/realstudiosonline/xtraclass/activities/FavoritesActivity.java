package com.realstudiosonline.xtraclass.activities;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.FavoriteAdapter;
import com.realstudiosonline.xtraclass.adapters.HistoryAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.Favorite;
import com.realstudiosonline.xtraclass.models.Favorites;
import com.realstudiosonline.xtraclass.models.History;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

public class FavoritesActivity extends AppCompatActivity implements FavoriteAdapter.SetFavoriteClickListener{
    String mToken ="";
    ClassDetails classDetails;
    School school;
    FavoriteAdapter favoriteAdapter;
    List<Favorite> favorites;
    RecyclerView recyclerView;
    User mUser;
    AppCompatImageView imageViewSchoolCrest;
    AppCompatTextView tvStudentName,tvClass,tvSchoolName;
    CircularImageView circularImageViewProfile;
    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);


        recyclerView = findViewById(R.id.recycler_view_history);
        tvStudentName = findViewById(R.id.tv_student_name);
        imageViewSchoolCrest = findViewById(R.id.img_school_crest);
        tvClass = findViewById(R.id.tv_class);
        tvSchoolName = findViewById(R.id.tv_school);
        circularImageViewProfile = findViewById(R.id.img_user_avatar);

        findViewById(R.id.img_back_press).setOnClickListener(v -> finish());

        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getApplicationContext());

        favorites = new ArrayList<>();

        favoriteAdapter = new FavoriteAdapter(favorites,getApplicationContext(),this);
        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
        school = PreferenceHelper.getPreferenceHelperInstance().getSelectedSchool(getApplicationContext());
        classDetails = PreferenceHelper.getPreferenceHelperInstance().getClassDetails(getApplicationContext());

        if(mUser!=null) {
            String  name = mUser.getFirst_name() +" " +mUser.getLast_name();
            tvStudentName.setText(name);

            try {
                Glide.with(FavoritesActivity.this).load(mUser.getProfile_image()).into(circularImageViewProfile);
                if (school != null) {
                    //Toast.makeText(getActivity(), ""+school, Toast.LENGTH_SHORT).show();
                    Log.d("School", "" + school);
                    tvSchoolName.setText(school.getName());
                    Glide.with(this).load(school.getLogo_url()).into(imageViewSchoolCrest);

                }
            }
            catch (Exception e){
                Glide.with(FavoritesActivity.this).load(R.drawable.joseph_stalin).into(circularImageViewProfile);

            }


            if (classDetails != null) {
                tvClass.setText(classDetails.getFull_name());

            }
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(favoriteAdapter);

        new Handler().postDelayed(()->{
            getFavorites(mToken);
        },200);
    }

    private void getFavorites(String token){

        ApiClient.getApiClient()
                .getInstance(getApplicationContext())
                .getFavorites("Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<Favorites>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<Favorites> favoritesResponse) {

                        if(favoritesResponse.isSuccessful()) {
                            if(favoritesResponse.body()!=null){

                                favorites.addAll(favoritesResponse.body().getData());
                                favoriteAdapter.notifyDataSetChanged();
                                Log.d("getFavorites", "onSuccess=> " + favoritesResponse.body());
                            }

                        }
                        else {

                            Log.d("getFavorites", "onSuccess=> errorBody:" + favoritesResponse.errorBody());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Log.d("getFavorites", "onError=> "+e.getMessage());
                    }
                });
    }

    @Override
    public void onFavoriteClick(Favorite favorite) {

    }
}
