package com.realstudiosonline.xtraclass.models;

public class Video {
    private String id;
    private String class_id;
    private String title;
    private String play_on;
    private String content;
    private String note_url;
    private String learn_more_url;
    private Teacher teacher;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlay_on() {
        return play_on;
    }

    public void setPlay_on(String play_on) {
        this.play_on = play_on;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNote_url() {
        return note_url;
    }

    public void setNote_url(String note_url) {
        this.note_url = note_url;
    }

    public String getLearn_more_url() {
        return learn_more_url;
    }

    public void setLearn_more_url(String learn_more_url) {
        this.learn_more_url = learn_more_url;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "Video{" +
                "id='" + id + '\'' +
                ", class_id='" + class_id + '\'' +
                ", title='" + title + '\'' +
                ", play_on='" + play_on + '\'' +
                ", content='" + content + '\'' +
                ", note_url='" + note_url + '\'' +
                ", learn_more_url='" + learn_more_url + '\'' +
                ", teacher=" + teacher +
                '}';
    }
}
/*
{
        "id": "9bd94fd8-990f-4eda-a73c-a8d5680b77b0",
        "class_id": "c1698279-ffa9-42a6-abee-ff75b9bc0898",
        "title": "Core Mathematics - Modulo Arithmetic",
        "topic": "Modulo Arithmetic",
        "play_on": "2020-10-01",
        "content": "Core Mathematics - Modulo Arithmetic",
        "note_url": "",
        "learn_more_url": "",
        "teacher": {
        "profile_id": "59e9caf6-427d-471a-a15e-8b53c1714ec2",
        "name": "Jeremiah Nii Annan Dodoo"
        }
        }*/
