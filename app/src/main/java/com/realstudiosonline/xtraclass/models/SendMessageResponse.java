package com.realstudiosonline.xtraclass.models;

public class SendMessageResponse {

    private int status;
    private String messageType;
    private int errStatus;
    private String message;
    private SendMessageResponseObject data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public int getErrStatus() {
        return errStatus;
    }

    public void setErrStatus(int errStatus) {
        this.errStatus = errStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SendMessageResponseObject getData() {
        return data;
    }

    public void setData(SendMessageResponseObject data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SendMessageResponse{" +
                "status=" + status +
                ", messageType='" + messageType + '\'' +
                ", errStatus=" + errStatus +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    /*
    * {
    "status": 200,
    "errStatus": 1,
    "messageType": "success",
    "message": "Question posted successfully.",
    "data": {
        "content": "message from mick using postman mick",
        "type": "question",
        "parent_id": "0",
        "sender_id": "2317",
        "class_id": 229,
        "video_id": 6488,
        "subject_id": 1172,
        "uuid": "621b2388-6536-423a-905c-f833f52380e4",
        "updated_at": "2021-07-16 10:09:49",
        "created_at": "2021-07-16 10:09:49",
        "id": 8,
        "student_name": null
    }
}*/
}
