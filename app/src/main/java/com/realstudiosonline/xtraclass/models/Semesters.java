package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class Semesters {
    private int statusCode;
    private String message;
    private List<Semester> data;

    @Override
    public String toString() {
        return "Semesters{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Semester> getData() {
        return data;
    }

    public void setData(List<Semester> data) {
        this.data = data;
    }
}
