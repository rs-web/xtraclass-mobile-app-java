package com.realstudiosonline.xtraclass.models;

import java.io.Serializable;

public class UserData  implements Serializable {
    private User user;
    private String access;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "user=" + user +
                ", access='" + access + '\'' +
                '}';
    }
}
