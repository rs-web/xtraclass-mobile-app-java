package com.realstudiosonline.xtraclass.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.activities.ChangeClassActivity;
import com.realstudiosonline.xtraclass.activities.MainActivity;
import com.realstudiosonline.xtraclass.adapters.BookmarkAdapter;
import com.realstudiosonline.xtraclass.adapters.ChangeSubjectAdapter;
import com.realstudiosonline.xtraclass.adapters.GenericAdapter;
import com.realstudiosonline.xtraclass.adapters.HomePagerAdapter;
import com.realstudiosonline.xtraclass.adapters.SubtitleAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.db.AppDatabase;
import com.realstudiosonline.xtraclass.db.Subtitle;
import com.realstudiosonline.xtraclass.db.Video;
import com.realstudiosonline.xtraclass.models.BookmarkItem;
import com.realstudiosonline.xtraclass.models.ClassDetails;
import com.realstudiosonline.xtraclass.models.ClassDetailsResponse;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.ClassesForCourseResponse;
import com.realstudiosonline.xtraclass.models.CoursesForSchool;
import com.realstudiosonline.xtraclass.models.DrawerToggle;
import com.realstudiosonline.xtraclass.models.EmailSending;
import com.realstudiosonline.xtraclass.models.Favorite;
import com.realstudiosonline.xtraclass.models.GenericItem;
import com.realstudiosonline.xtraclass.models.Institution;
import com.realstudiosonline.xtraclass.models.Lesson;
import com.realstudiosonline.xtraclass.models.LibrariesEventBus;
import com.realstudiosonline.xtraclass.models.Library;
import com.realstudiosonline.xtraclass.models.PlaybackPosition;
import com.realstudiosonline.xtraclass.models.PutFavoriteResponse;
import com.realstudiosonline.xtraclass.models.ResetLibrary;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.SchoolDetailsResponse;
import com.realstudiosonline.xtraclass.models.SearchItem;
import com.realstudiosonline.xtraclass.models.SelectedVideo;
import com.realstudiosonline.xtraclass.models.SubjectOfClassResponse;
import com.realstudiosonline.xtraclass.models.TabEntity;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.models.UserPlan;
import com.realstudiosonline.xtraclass.models.UserPlanResponse;
import com.realstudiosonline.xtraclass.models.VideoSource;
import com.realstudiosonline.xtraclass.utils.Constants;
import com.realstudiosonline.xtraclass.utils.ExpandCollapseExtension;
import com.realstudiosonline.xtraclass.utils.KeyBoard;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;
import com.realstudiosonline.xtraclass.utils.player.PlayerController;
import com.realstudiosonline.xtraclass.utils.player.VideoPlayer;
import com.realstudiosonline.xtraclass.utils.smarttablayout.SmartTabLayout;
import com.realstudiosonline.xtraclass.utils.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.realstudiosonline.xtraclass.utils.smarttablayout.utils.v4.FragmentPagerItems;
import com.realstudiosonline.xtraclass.utils.widgets.TabLayout;
import com.realstudiosonline.xtraclass.utils.widgets.CustomTabEntity;
import com.realstudiosonline.xtraclass.utils.widgets.OnTabSelectListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.FetchObserver;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2core.Reason;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_1080P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_160P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_240P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_360P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_480P;
import static com.realstudiosonline.xtraclass.utils.Constants.BITRATE_720P;
import static com.realstudiosonline.xtraclass.utils.Constants.MEDIA_HUB;
import static com.realstudiosonline.xtraclass.utils.Constants.convertDpToPixelInt;
import static com.realstudiosonline.xtraclass.utils.Constants.generate;


public class OpenSchoolFragment extends Fragment implements View.OnClickListener, PlayerController, FetchObserver<Download> {


    private static final String TAG = "OpenSchoolFragment";
    PlayerView playerView;
    VideoPlayer player;
    AppCompatImageButton subtitle;
    AppCompatImageButton nextBtn;
    AppCompatImageButton retry;
    AppCompatImageButton fullScreenBtn,imgCloseSearch;
    ProgressBar progressBar;
    private AlertDialog alertDialog;
    private VideoSource videoSource,videoSourceLibrary;
    private AppDatabase database;
    private final List<Subtitle> subtitleList = new ArrayList<>();
    private final List<Video> videoUriList = new ArrayList<>();
    final List<Video> libraryVideoUriList = new ArrayList<>();
    String[] mTitles = {"Playing", "Questions", "Library"};
    ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private final int[] mIconUnselectIds = {
            R.drawable.rounded_layer, R.drawable.rounded_toggle_background,
            R.drawable.rounded_toggle_background, R.drawable.rounded_toggle_background};
    private final int[] mIconSelectIds = {
            R.drawable.rounded_toggle_background, R.drawable.rounded_toggle_background,
            R.drawable.rounded_toggle_background, R.drawable.rounded_toggle_background};
    SmartTabLayout tabLayout;
    private static ViewPager viewPager;
    private FloatingActionButton floatingActionButtonSearch;
    private ScrollView scrollView;
    private  AppCompatImageButton btn_collapse_expand;
    public boolean isUp = false;
    AppCompatTextView tvLearnMore, tvViewCount, tvSchoolName, tvClass;
    private  View view_details_of_course,btnGetNotes,view_class_details;
    ClassesForCourse selectedClassesForCourse;
    School selectedSchool;
    CoursesForSchool coursesForSchool;
    Institution selectedInstitution;
    List<ClassesForCourse> classesForCourses;
    AppCompatImageButton videoQuality, viewBookMark;
    private Request request;
    private Fetch fetch;
    private User mUser;
    private Lesson selectedLesson;
    private TextInputLayout inputLayoutSearch;
    private TextInputEditText editTextSearch;
    ClassDetails classDetails;
    AppCompatTextView tvSubject, tvTopic, tvTutor;
    List<Lesson> mLessons = new ArrayList<>();
    View chatInputView, viewChangeSubAnchor;
    //ScrollView scroll_view_subject_details;
    /*private TextView progressTextView;
    private TextView titleTextView;
    private TextView etaTextView;playerView
    private TextView downloadSpeedTextView;*/
    private ViewFlipper viewFlipperSearchInfo, viewFlipperSemesterCalendar;
    String mToken;
    public static  View view_anchor_open;
    private boolean isLibraryVideoPlaying = false;
    //public static final String url = "https://demo.mediacms.io/media/encoded/13/mccostic1/9f3cb86d63b945a29c2567ed30ef08bd.9f3cb86d63b945a29c2567ed30ef08bd.sample_video.mp4.mp4";
    public static final String url = "https://demo.mediacms.io/media/encoded/13/user/dd6487d6289d4487b4938bde36bc06b5.dd6487d6289d4487b4938bde36bc06b5.Cliffs_of_Moher_UHD_drone_footage_2019.mp4.mp4";

    PopupWindow popupWindowGetNotes, popupWindowBookmarkDownload, popupWindowChangeSubject;

    final ArrayList<GenericItem> bookmarkVideoList = new ArrayList<GenericItem>() {{
        add(new GenericItem(1,R.drawable.star, "Bookmark"));
        add(new GenericItem(2,R.drawable.phone, "Download"));
    }};

    final ArrayList<GenericItem> emailDownloadList = new ArrayList<GenericItem>() {{
        add(new GenericItem(1,R.drawable.mail, "Email me"));
        add(new GenericItem(2,R.drawable.phone, "Download"));
    }};

    private AppCompatImageView imgSchoolCrest;
    private GenericAdapter emailBookmarkAdapter,adapterBookmarkDownload;
    public OpenSchoolFragment() {
        // Required empty public constructor
    }

    private void initializeDb(Activity activity) {
        database = AppDatabase.Companion.getDatabase(activity);
    }


    private VideoSource makeVideoSource(List<Video> videos) {
        setVideosWatchLength();
        List<VideoSource.SingleVideo> singleVideos = new ArrayList<>();
        for (int i = 0; i < videos.size(); i++) {

            singleVideos.add(i, new VideoSource.SingleVideo(
                    videos.get(i).getVideoUrl(),
                    database.videoDao().getAllSubtitles(i + 1),
                    videos.get(i).getWatchedLength())
            );

        }
        return new VideoSource(singleVideos, 0);
    }


    private void setVideosWatchLength() {
        List<Video> videosInDb = database.videoDao().getVideos();

        try {
            if(videoUriList.size()==videosInDb.size() ) {
                for (int i = 0; i < videosInDb.size(); i++) {
                    videoUriList.get(i).setWatchedLength(videosInDb.get(i).getWatchedLength());
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }



/*

    public static OpenSchoolFragment newInstance(String param1, String param2) {
        return new OpenSchoolFragment();
    }
*/






    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_open_school, container, false);
    }



    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getActivity()!=null)
        initializeDb(getActivity());
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }

        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getActivity());

        selectedLesson = PreferenceHelper.getPreferenceHelperInstance().getSelectedLesson(getActivity());
        view.findViewById(R.id.img_drawer_open).setOnClickListener(v -> EventBus.getDefault().post(new DrawerToggle(true)));

        setupLayout(view);

        int bitrate = PreferenceHelper.getPreferenceHelperInstance().getVideoQuality(getActivity());

        Log.d("BITRATE", "bitrate:"+bitrate);
        new Handler().postDelayed(()->{

            if(bitrate>0) {
                int drawable = getBitrateDrawableTinted(bitrate);
                videoQuality.setImageResource(drawable);
            }
            else {

                videoQuality.setImageResource(R.drawable.sd_blue);
            }
        },300);
        classesForCourses = new ArrayList<>();

        getDownloadDefaultInstance();

        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getActivity());
        selectedClassesForCourse = PreferenceHelper.getPreferenceHelperInstance().getSelectedClassesForCourse(getActivity());
        selectedSchool = PreferenceHelper.getPreferenceHelperInstance().getSelectedSchool(getActivity());
        coursesForSchool = PreferenceHelper.getPreferenceHelperInstance().getCoursesForSchool(getActivity());
        selectedInstitution = PreferenceHelper.getPreferenceHelperInstance().getSelectedInstitution(getActivity());
        classDetails = PreferenceHelper.getPreferenceHelperInstance().getClassDetails(getActivity());

        if(selectedClassesForCourse!=null)
        getListOfSubjectsOfClass(selectedClassesForCourse.getUuid());
        btn_collapse_expand.setOnClickListener(v -> onSlideViewButtonClick());


        if(selectedClassesForCourse!=null) {
            if(getActivity()!=null && isAdded())
            fetchClassDetails(selectedClassesForCourse.getUuid(), getActivity());
        }

        viewPager.setOnTouchListener((v, event) -> true);



        viewPager.setOffscreenPageLimit(2);
        tabLayout.setOnTabClickListener(position -> {
            if(getActivity()!=null) {
                KeyBoard.hide(getActivity(), viewPager);

                if(position==0){
                    view_class_details.setVisibility(View.VISIBLE);
                    // ExpandCollapseExtension.expand(view_class_details);
                    if(isLibraryVideoPlaying){
                        EventBus.getDefault().post(new ResetLibrary(true));
                        onLessonsRequest(mLessons);
                        if(selectedLesson!=null)
                            onLessonSelected(selectedLesson);
                        else {
                            onLessonSelected(mLessons.get(0));
                        }
                    }
                    floatingActionButtonSearch.show();
                }
                else {
                    view_class_details.setVisibility(View.GONE);
                    //ExpandCollapseExtension.collapse(view_class_details);
                    if(getActivity()!=null) {
                        //showRegisterDialog(getActivity());
                        //tabLayout.setCurrentItem(0);

                    }
                    //viewPager.setCurrentItem(0);
                    floatingActionButtonSearch.hide();

                    //viewFlipperSearchInfo.setInAnimation(getActivity(), android.R.anim);
                    viewFlipperSearchInfo.setDisplayedChild(0);
                }

                //viewPager.setCurrentItem(position, false);
                scrollView.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
            }
            else {
                Log.d("getActivity", "getActivity is null");
                //TODO implement firebase crashlytics for handling getActivity() == Null
            }
        });

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add(R.string.playing, PlayListFragment.class)
                .add(R.string.question, ChatFragment.class)
                .add(R.string.library, LibraryFragment.class)
                .create());

        viewPager.setAdapter(adapter);

        tabLayout.setViewPager(viewPager);


        if(selectedInstitution==null){
            selectedInstitution = new Institution(mUser.getInstitution_id(), "");
        }


        if(coursesForSchool==null){
            coursesForSchool = new CoursesForSchool(mUser.getCourse_id(), "", "", mUser.getSchool_id());
        }

        try {
            //Toast.makeText(getActivity(), ""+selectedSchool, Toast.LENGTH_SHORT).show();
            fetchSchoolDetails(selectedSchool.getId());
            getListOfClassesForCourse(coursesForSchool.getId());

            if(classDetails!=null){
                tvClass.setText(classDetails.getFull_name());
            }
        }
        catch (Exception e){
            //startActivity(new Intent(getActivity(), ChangeClassActivity.class));
            //getActivity().finish();
        }


        view.findViewById(R.id.btn_change_subject).setOnClickListener(v -> {
            Log.d("sdfghjgf", classesForCourses.toString());
            if(classesForCourses.size()==0){
                try {
                    //Toast.makeText(getActivity(), ""+selectedSchool, Toast.LENGTH_SHORT).show();
                    fetchSchoolDetails(selectedSchool.getId());
                    getListOfClassesForCourse(coursesForSchool.getId());

                    if(classDetails!=null){
                        tvClass.setText(classDetails.getFull_name());
                    }
                }
                catch (Exception e){
                    //startActivity(new Intent(getActivity(), ChangeClassActivity.class));
                    //getActivity().finish();
                }
            }
            else
            provideChangeSubjectPopupWindow(viewChangeSubAnchor,getActivity(), classesForCourses);
        });

        //makeListOfUri();

      //  getDataFromIntent();

        //initSource(getActivity());

        SubjectOfClassResponse subject = PreferenceHelper.getPreferenceHelperInstance().getSubject(getActivity());

        if(subject!=null) {
           // Toast.makeText(getActivity(), "" + subject.toString(), Toast.LENGTH_SHORT).show();
            Log.d("SubjectOfClassResponse", "Subject is "+ subject.toString());
        }
        else Log.d("SubjectOfClassResponse", "Subject is null");


        if(mUser!=null) {
            if(isAdded())
            getUserPlan(mUser.getUser_id());
        }
        ///if(getView()!=null)
       // getView().getViewTreeObserver().addOnWindowFocusChangeListener(hasFocus -> hideSystemUi());
    }



    @Override
    public void onDetach() {
        if(player!=null)
            player.releasePlayer();
        super.onDetach();

    }


    private void provideGetNotesPopupWindow(View it, Context context, GenericAdapter adapter) {

        if(popupWindowGetNotes!=null){
            popupWindowGetNotes.dismiss();
            popupWindowGetNotes = null;
        }
        //if(scroll_view_subject_details.getMaxScrollAmount())
        scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
        popupWindowGetNotes = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        // popupWindowGetNotes.setBackgroundDrawable(backgroundDrawable);
        popupWindowGetNotes.setOutsideTouchable(true);
        popupWindowGetNotes.setFocusable(true);
        //popupWindowGetNotes.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            GenericItem item = adapter.getItem(position);

            if(item!=null) {
                if(item.getId() == 2){
                    if(selectedLesson!=null)
                        downloadNote(selectedLesson.getNotes_url());
                    //Download finished successfully. You can check the file in the Downloads folder
                }
                else if(item.getId() ==1){
                    if(!TextUtils.isEmpty(mToken)) {
                        sendEmail(selectedLesson.getLesson_id(), mToken);
                        Toast.makeText(context, "Email is being sent...", Toast.LENGTH_LONG).show();
                    }

                    //"Email is being sent..."
                }
            }

            //viewModel.setLegalCountry(selectedCountry);
            popupWindowGetNotes.dismiss();
        });
        popupWindowGetNotes.setContentView(parent_view);
        popupWindowGetNotes.setElevation(20);

        if(popupWindowGetNotes!=null) {

            new Handler().postDelayed(()-> popupWindowGetNotes.showAsDropDown(it, 0, -(it.getHeight()*2)),200);
        }

    }

    private void downloadNote(String url) {

        final String filePath = Constants.getExtDir() + "/XtraClass/Notes/" + Constants.getNameFromUrl(url);
        request = new Request(url, filePath);
        fetch.attachFetchObserversForDownload(request.getId(),this).enqueue(request,
                result -> request = result, result -> Log.d("SingleDownloadActivity", result.toString()));

    }

    private void sendEmail(String lessonId, String token){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .postSendEmail(lessonId, "Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<EmailSending>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<EmailSending> emailSendingResponse) {

                        if(emailSendingResponse.isSuccessful()){
                            Toast.makeText(getActivity(), "Email is successfully sent to your email address", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getActivity(), "Oops. Something went wrong with sending the attachment to your email address. Try again later", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Toast.makeText(getActivity(), "Oops. Something went wrong with sending the attachment to your email address. Try again later", Toast.LENGTH_LONG).show();
                    }
                });
    }



    private void getDownloadDefaultInstance(){
        fetch = Fetch.Impl.getDefaultInstance();
    }


    private void enqueueDownload(String url, Activity activity) {
        final String filePath = Constants.getSaveDir(activity) + "/Offline/" + Constants.getNameFromUrl(url);
        request = new Request(url, filePath);
       // request.setExtras(getExtrasForRequest(request));

        Toast.makeText(activity, "Saving lesson for offline.", Toast.LENGTH_SHORT).show();
        fetch.attachFetchObserversForDownload(request.getId(),this).enqueue(request,
                new Func<Request>() {
            @Override
            public void call(@NotNull Request result) {
                request = result;
            }
        }, new Func<Error>() {
            @Override
            public void call(@NotNull Error result) {
                Log.d("SingleDownloadActivity", result.toString());
            }
        });

    }

    @Override
    public void onChanged(Download data, @NotNull Reason reason) {
        if(getActivity()!=null) {
            updateViews(data, reason, getActivity());
        }
    }

    private void updateViews(Download download, Reason reason, Activity activity) {
        if (request.getId() == download.getId()) {
            if (reason == Reason.DOWNLOAD_QUEUED || reason == Reason.DOWNLOAD_COMPLETED) {
                setTitleView(download.getFile());
                if(reason == Reason.DOWNLOAD_COMPLETED){
                    Toast.makeText(activity, "Lesson saved for offline", Toast.LENGTH_SHORT).show();
                }
            }

            setProgressView(download.getStatus(), download.getProgress());
            String eta = Constants.getETAString(activity, download.getEtaInMilliSeconds());
            String speed = Constants.getDownloadSpeedString(activity, download.getDownloadedBytesPerSecond());
            Log.d("SingleDownloadActivity", "ETA: "+eta);
            Log.d("SingleDownloadActivity", "SPEED: "+speed);
          //  etaTextView.setText(Constants.getETAString(activity, download.getEtaInMilliSeconds()));
           // downloadSpeedTextView.setText(Constants.getDownloadSpeedString(activity, download.getDownloadedBytesPerSecond()));
            if (download.getError() != Error.NONE) {
               // showDownloadErrorSnackBar(download.getError());
            }
        }
    }

    private void setTitleView(@NonNull final String fileName) {
        final Uri uri = Uri.parse(fileName);
       // titleTextView.setText(uri.getLastPathSegment());
    }


    private void setProgressView(@NonNull final Status status, final int progress) {
        switch (status) {
            case QUEUED: {
                Toast.makeText(getActivity(), ""+getString(R.string.queued),Toast.LENGTH_LONG).show();
                //progressTextView.setText(R.string.queued);
                break;
            }
            case ADDED: {
                Toast.makeText(getActivity(), ""+getString(R.string.added),Toast.LENGTH_LONG).show();
               // progressTextView.setText(R.string.added);
                break;
            }
            case DOWNLOADING:
            case COMPLETED: {

                if (progress == -1) {
                    Toast.makeText(getActivity(), ""+getString(R.string.downloading),Toast.LENGTH_LONG).show();
                   // progressTextView.setText(R.string.downloading);
                } else {
                    final String progressString = getResources().getString(R.string.percent_progress, progress);
                    Toast.makeText(getActivity(), ""+progressString,Toast.LENGTH_LONG).show();

                    //progressTextView.setText(progressString);
                }
                break;
            }
            default: {
                Toast.makeText(getActivity(), ""+getString(R.string.status_unknown),Toast.LENGTH_LONG).show();
               // progressTextView.setText(R.string.status_unknown);
                break;
            }
        }
    }

    private void  getListOfSubjectsOfClass(String classId){
        ApiClient.getApiClient().getInstance(getContext()).getListOfSubjectsOfClass(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<SubjectOfClassResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull
                                          Response<SubjectOfClassResponse> subjectOfClassResponseResponse) {
                if(subjectOfClassResponseResponse.isSuccessful()){
                    if(subjectOfClassResponseResponse.body()!=null) {
                      //  if(subjectOfClassResponseResponse.body().getData()!=null)
                       // PreferenceHelper.getPreferenceHelperInstace().setSubject(getActivity(),subjectOfClassResponseResponse.body());
                        Log.d("SubjectOfClassResponse", ""+subjectOfClassResponseResponse.body().getData());
                    }
                }
                else {
                    Toast.makeText(getActivity(), "Error:"+subjectOfClassResponseResponse.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                e.printStackTrace();
            }
        });
    }




    private void provideChangeSubjectPopupWindow(View it, Context context,
                                                 List<ClassesForCourse> classesForCourses){
        if(popupWindowBookmarkDownload!=null){
            popupWindowBookmarkDownload.dismiss();
            popupWindowBookmarkDownload = null;
        }
        ChangeSubjectAdapter changeSubjectAdapter = new ChangeSubjectAdapter(context,R.layout.item_payment_type_dropdown, classesForCourses);
        popupWindowBookmarkDownload = new PopupWindow(convertDpToPixelInt(200,context), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowBookmarkDownload.setOutsideTouchable(true);
        popupWindowBookmarkDownload.setFocusable(true);
        popupWindowBookmarkDownload.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(changeSubjectAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            ClassesForCourse classesForCourse = changeSubjectAdapter.getItem(position);

            if(getActivity()!=null){
                PreferenceHelper.getPreferenceHelperInstance().setPlayPrevious(getActivity(), false);
                PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(getActivity(),classesForCourse);
                PreferenceHelper.getPreferenceHelperInstance().setLessons(getActivity(), new ArrayList<>());
                PreferenceHelper.getPreferenceHelperInstance().setFavorites(getActivity(),new ArrayList<>());
                PreferenceHelper.getPreferenceHelperInstance().setClassVideoPlayOn(getActivity(),"");
                PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getActivity());
                if(player!=null)
                    player.releasePlayer();
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                Log.d("ChangeSubjectAdapter", classesForCourse.toString());
            }

            else {
                Log.d("getActivity", "getActivity is null");
                //TODO implement firebase crashlytics for handling getActivity() == Null
            }

            //viewModel.setLegalCountry(selectedCountry);
            popupWindowBookmarkDownload.dismiss();
        });
        popupWindowBookmarkDownload.setContentView(parent_view);
        popupWindowBookmarkDownload.setElevation(20);

        if(popupWindowBookmarkDownload!=null) {

            popupWindowBookmarkDownload.showAsDropDown(it, -it.getWidth(),0);
        }

    }

    public void onSlideViewButtonClick() {

        if (isUp) {
            btn_collapse_expand.setBackgroundResource(R.drawable.ic_arrow_drop_up);
            ExpandCollapseExtension.expand(view_details_of_course);
        } else {
            btn_collapse_expand.setBackgroundResource(R.drawable.ic_arrow_drop_down);
            ExpandCollapseExtension.collapse(view_details_of_course);

        }

        int pos = viewPager.getCurrentItem();
        viewPager.setVisibility(View.INVISIBLE);

        if(pos==0) {
            viewPager.setCurrentItem(1);
            // viewPager.postDelayed(() -> viewPager.setCurrentItem(1), 100);
        }
        else if(pos==1){
            viewPager.setCurrentItem(0);
            //viewPager.postDelayed(() -> viewPager.setCurrentItem(0), 100);
        }

        else if(pos==2){
            viewPager.setCurrentItem(1);
            //viewPager.postDelayed(() -> viewPager.setCurrentItem(1), 100);
        }

        viewPager.postDelayed(()->viewPager.setCurrentItem(pos), 200);

        viewPager.postDelayed(()->viewPager.setVisibility(View.VISIBLE),500);

        isUp = !isUp;
    }

    private HomePagerAdapter getHomePagerAdapter() {
        return new HomePagerAdapter(getChildFragmentManager(), getLifecycle());
    }


    private void getListOfClassesForCourse(String courseId){

        ApiClient.getApiClient().getInstance(getContext())
                .getListOfClassesForCourse(courseId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassesForCourseResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<ClassesForCourseResponse> classesForCourseResponseResponse) {
                if(classesForCourseResponseResponse.isSuccessful()){
                    ClassesForCourseResponse classesForCourseResponse = classesForCourseResponseResponse.body();
                      /* if(getActivity()!=null)
                showChangeClassDialog(getActivity(), classesForCourseResponse.getData());*/
                    Log.d("ListOfClassesForCourse", ""+classesForCourseResponse.toString());
                    classesForCourses.clear();
                    classesForCourses.addAll(classesForCourseResponse.getData());
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });
    }


    private void fetchSchoolDetails(String schoolId){

        ApiClient.getApiClient().getInstance(getContext()).fetchSchoolDetails(schoolId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<SchoolDetailsResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<SchoolDetailsResponse> schoolDetailsResponseResponse) {

                if(schoolDetailsResponseResponse.isSuccessful()){

                    SchoolDetailsResponse schoolDetailsResponse = schoolDetailsResponseResponse.body();

                    if(schoolDetailsResponse!=null) {
                        tvSchoolName.setText(schoolDetailsResponse.getData().getName());

                        if (getActivity()!=null)
                        Glide.with(getActivity()).load(schoolDetailsResponse.getData().getLogo_url()).into(imgSchoolCrest);
                        // tvClass.setText(schoolDetailsResponse.getData().getSubject_name());
                        Log.d("schoolDetailsResponse", "open school=> "+schoolDetailsResponse.getData().toString());
                    }

                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });

    }


    private void getDataFromIntent() {
        videoSource = makeVideoSource(videoUriList);
    }

    private void setupLayout(View view) {

        view_anchor_open = view.findViewById(R.id.view_anchor);

        viewChangeSubAnchor = view.findViewById(R.id.view_change_sub_anchor);

        tvSubject = view.findViewById(R.id.tv_subject);

        tvTopic = view.findViewById(R.id.tv_topic);

        tvTutor = view.findViewById(R.id.tv_tutor_name);


        tvLearnMore = view.findViewById(R.id.tv_learn_more);

        tvClass = view.findViewById(R.id.tv_class);

        tvViewCount = view.findViewById(R.id.tv_view_count);

        tvSchoolName = view.findViewById(R.id.tv_school_name);

        view_details_of_course = view.findViewById(R.id.view_details_of_course);

        view_class_details = view.findViewById(R.id.view_class_details);

        btn_collapse_expand = view.findViewById(R.id.btn_collapse_expand);

        scrollView = view.findViewById(R.id.scroll_view_details);

        floatingActionButtonSearch =view.findViewById(R.id.fab_search);

        imgSchoolCrest = view.findViewById(R.id.img_school_crest);

        floatingActionButtonSearch.setOnClickListener(v -> {
            floatingActionButtonSearch.hide();
            //viewFlipperSearchInfo.setInAnimation(getActivity(), R.anim.enter_from_right);
            viewFlipperSearchInfo.setDisplayedChild(1);


            if (isUp) {
                //Toast.makeText(getActivity(), "if isUp: "+isUp, Toast.LENGTH_SHORT).show();
                // btn_collapse_expand.setBackgroundResource(R.drawable.ic_arrow_drop_up);
                Glide.with(this).load(R.drawable.ic_arrow_drop_up).into(btn_collapse_expand);
                //view_details_of_course.setVisibility(View.VISIBLE);
                ExpandCollapseExtension.expand(view_details_of_course);
                isUp = !isUp;
            }
        });

        btnGetNotes = view.findViewById(R.id.btn_get_notes);

        viewFlipperSearchInfo = view.findViewById(R.id.view_filpper_search_info);

        if(getActivity()!=null) {
            adapterBookmarkDownload= new GenericAdapter(getActivity(), R.layout.item_payment_type_dropdown,bookmarkVideoList);
            emailBookmarkAdapter = new GenericAdapter(getActivity(), R.layout.item_payment_type_dropdown, emailDownloadList);
        }
        btnGetNotes.setOnClickListener(v -> {
            //Toast.makeText(getActivity(), "get notes", Toast.LENGTH_SHORT).show();
            if(emailBookmarkAdapter!=null && getActivity()!=null)
                provideGetNotesPopupWindow(v, getActivity(), emailBookmarkAdapter);
        });

        FloatingActionButton floatingActionButtonDetails = view.findViewById(R.id.fab_details);

        floatingActionButtonDetails.setOnClickListener(v -> {


            if(scrollView.getVisibility() == View.VISIBLE) {
                floatingActionButtonDetails.setCompatElevation(10f);
                scrollView.setVisibility(View.GONE);
                floatingActionButtonSearch.show();
                viewPager.setVisibility(View.VISIBLE);
            }
            else {
                floatingActionButtonDetails.setCompatElevation(0.85f);
                scrollView.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.GONE);
                floatingActionButtonSearch.hide();
            }
        });


        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tab_layout);
        playerView = view.findViewById(R.id.player_view);
        progressBar = view.findViewById(R.id.progress_bar);
        subtitle = view.findViewById(R.id.btn_subtitle);
        videoQuality = view.findViewById(R.id.exo_quality);
        viewBookMark = view.findViewById(R.id.btn_bookmark);
        editTextSearch = view.findViewById(R.id.edt_search);
        imgCloseSearch = view.findViewById(R.id.img_close);
        inputLayoutSearch = view.findViewById(R.id.input_search);
        chatInputView = view.findViewById(R.id.view_search);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("editTextSearch", "query:"+s.toString());
                PlayListFragment.filterPlaylist(new SearchItem(s.toString()));
                //EventBus.getDefault().post(new SearchItem(s.toString()));
                //PlayListFragment.filterPlaylist(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        imgCloseSearch.setOnClickListener(v -> {
            KeyBoard.hide(getActivity(),chatInputView);
            PlayListFragment.filterPlaylist(new SearchItem(""));
            floatingActionButtonSearch.show();
            viewFlipperSearchInfo.setDisplayedChild(0);

        });


        nextBtn = view.findViewById(R.id.btn_next);
        fullScreenBtn = view.findViewById(R.id.exo_fullscreen);
        AppCompatImageButton preBtn = view.findViewById(R.id.exo_prev);
        retry = view.findViewById(R.id.retry_btn);


        //optional setting
        if(playerView.getSubtitleView()!=null)
        playerView.getSubtitleView().setVisibility(View.GONE);
        fullScreenBtn.setOnClickListener(this);
        subtitle.setOnClickListener(this);
        videoQuality.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        preBtn.setOnClickListener(this);
        viewBookMark.setOnClickListener(this);
        retry.setOnClickListener(this);
    }

    private void initSource(Activity activity) {

        if (videoSource.getVideos() == null) {
            Toast.makeText(getActivity(), "can not play video", Toast.LENGTH_SHORT).show();
            return;
        }

        player = new VideoPlayer(playerView, activity, videoSource, this,videoQuality);

        checkIfVideoHasSubtitle();

        //optional setting
        if(playerView.getSubtitleView()!=null)
        playerView.getSubtitleView().setVisibility(View.GONE);
        player.seekToOnDoubleTap();

        playerView.setControllerVisibilityListener(visibility ->
        {
            Log.i(TAG, "onVisibilityChange: " + visibility);
            if (player.isLock())
                playerView.hideController();
        });

    }


    private void getUserPlan(String userId){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .getUserPlan(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<UserPlanResponse>>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<UserPlanResponse> userPlanResponseResponse) {
                        if(userPlanResponseResponse.isSuccessful()){
                            if(isAdded() && getActivity()!=null){
                            if(userPlanResponseResponse.body()!=null) {

                                if (userPlanResponseResponse.body().getData() != null) {
                                    List<UserPlan> userPlans = userPlanResponseResponse.body().getData();


                                    String currentDateTimeString = userPlanResponseResponse.body().getCurrent_date_time();
                                    PreferenceHelper.getPreferenceHelperInstance().setCurrentDateTime(getActivity(), currentDateTimeString);
                                    DateTime currentDateTime = DateTime.parse(currentDateTimeString, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));


                                    if (userPlans.size() > 0) {
                                        UserPlan userPlan = userPlans.get(userPlans.size() - 1);

                                        if (userPlan.getId() != 0) {
                                            DateTime startOnDateTime = DateTime.parse(userPlan.getStarts_on(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
                                            DateTime endOnDateTime = DateTime.parse(userPlan.getExpired_on(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

                                            PreferenceHelper.getPreferenceHelperInstance().setCurrentPlan(getContext(), userPlan);
                                            int daysLeft = Days.daysBetween(startOnDateTime.toLocalDate(), endOnDateTime.toLocalDate()).getDays();


                                            if (daysLeft <= 0) {
                                                Toast.makeText(getContext(), "user plan has expired: days left:" + daysLeft, Toast.LENGTH_LONG).show();
                                            } else {

                                                Toast.makeText(getContext(), "user plan not expired: days left " + daysLeft, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                        Toast.makeText(getContext(), "user has a plan", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getContext(), "user has no plan", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                                Log.d("userPlanResponseResponse", ""+userPlanResponseResponse.body());
                            }
                        }
                        else {

                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }
    private void getLibraryVideoSource() {
        videoSourceLibrary = makeVideoSource(libraryVideoUriList);
    }


    private void initSourceLibraryVideos(Activity activity){
        if(player!=null){
            player.releasePlayer();
        }

        if (videoSourceLibrary.getVideos() == null) {
            Toast.makeText(getActivity(), "can not play video", Toast.LENGTH_SHORT).show();
            return;
        }

        player = new VideoPlayer(playerView, activity, videoSourceLibrary, this,videoQuality);


        checkIfVideoHasSubtitle();

        //mAudioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);

        //optional setting
        if(playerView.getSubtitleView()!=null)
            playerView.getSubtitleView().setVisibility(View.GONE);
        player.seekToOnDoubleTap();

        playerView.setControllerVisibilityListener(visibility ->
        {
            Log.i(TAG, "onVisibilityChange: " + visibility);
            if (player.isLock())
                playerView.hideController();
        });
        player.pausePlayer();

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLibrarySelected(Library library) {
        isLibraryVideoPlaying =true;
        if (player != null) {
            // Toast.makeText(getActivity(), "onLibrarySelected:"+library.getSource(), Toast.LENGTH_SHORT).show();
            player.seekTo(library.getId()-1);

        }
        else {

            Toast.makeText(getActivity(), "player is null", Toast.LENGTH_SHORT).show();
            getLibraryVideoSource();
            initSourceLibraryVideos(getActivity());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLibraryVideos(LibrariesEventBus libraries){

        isLibraryVideoPlaying = true;
        //  Toast.makeText(getActivity(), "onLibraryVideos:"+libraries.getLibraries().size(), Toast.LENGTH_SHORT).show();
        for (Library library: libraries.getLibraries()){
            libraryVideoUriList.add(new Video(library.getSource(), Long.getLong("zero", 0)));
        }

        if (database.videoDao().getAllUrls().size() == 0) {
            database.videoDao().insertAllVideoUrl(libraryVideoUriList);
            database.videoDao().insertAllSubtitleUrl(subtitleList);
        }
        /*if (database.videoDao().getAllUrls().size() == 0) {
            database.videoDao().insertAllVideoUrl(libraryVideoUriList);
        }*/
        //makeListOfUri();
        getLibraryVideoSource();
        initSourceLibraryVideos(getActivity());
    }

    private void fetchClassDetails(String selectedClassesForCourse_uuid, Activity activity){

        ApiClient.getApiClient().getInstance(getContext()).fetchClassDetails(selectedClassesForCourse_uuid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassDetailsResponse>>() {
            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<ClassDetailsResponse> classDetailsResponseResponse) {


                if(classDetailsResponseResponse.isSuccessful()){
                    ClassDetailsResponse classDetailsResponse = classDetailsResponseResponse.body();
                    // Toast.makeText(activity, "fetchClassDetails", Toast.LENGTH_SHORT).show();
                    Log.d("fetchClassDetails", "Open School "+classDetailsResponse.toString());

                    if(classDetailsResponse.getData()!=null) {
                        tvClass.setText(classDetailsResponse.getData().getFull_name());

                        try {
                            tvTutor.setText(classDetailsResponse.getData().getVideo().get(0).getTeacher().getName());
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                        if(classDetailsResponse.getData().isLocked()){

                            Toast.makeText(getActivity(), "you are trying to access a locked class", Toast.LENGTH_LONG).show();
                        }

                        else {
                            tvSchoolName.setText(classDetailsResponse.getData().getSchool_name());
                            if(selectedSchool ==null){
                                selectedSchool = new School(classDetailsResponse.getData().getSchool_id(),
                                        classDetailsResponse.getData().isLocked() ? 1: 0,
                                        classDetailsResponse.getData().getSchool_name(),
                                        "", mUser.getInstitution_id());
                                PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getActivity(),selectedSchool);

                            }
                           // EventBus.getDefault().post(classDetailsResponse.getData());
                            if(isAdded() && getActivity()!=null)
                            PreferenceHelper.getPreferenceHelperInstance().setClassDetails(getActivity(), classDetailsResponse.getData());
                        }
                    }
                    else{
                        Toast.makeText(activity, "No lessons available for this course", Toast.LENGTH_SHORT).show();

                    }
                }

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                e.printStackTrace();
            }
        });
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLessonSelected(Lesson lesson){
        isLibraryVideoPlaying = false;

        String topic = "Topic: <b>"+lesson.getTopic_name() + "</b>";
        tvTopic.setText(Html.fromHtml(topic));
        String subject = "Subject: <b>"+lesson.getSubject()+ "</b>";
        tvSubject.setText(Html.fromHtml(subject));
        String tutor = "Tutor: <b>"+lesson.getTeacher_name()+"</b>";
        tvTutor.setText(Html.fromHtml(tutor));
        String count = "Views "+generate(5,20);
        tvViewCount.setText(count);




        if(TextUtils.isEmpty(lesson.getNotes_url())){
            btnGetNotes.setVisibility(View.GONE);
        }

        else {
            btnGetNotes.setVisibility(View.VISIBLE);
        }


        Log.d("LessonSelected", ""+lesson);


        if(player!=null) {
            //Toast.makeText(getActivity(), "onLessonSelected:"+ lesson.getPeriod_title(), Toast.LENGTH_SHORT).show();
             selectedLesson = lesson;

            PlaybackPosition playbackPosition = PreferenceHelper.getPreferenceHelperInstance().getPlayBackPosition(getActivity());


           /* int orientation = this.getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                // code for portrait mode
            } else {
                // code for landscape mode
            }*/
             PreferenceHelper.getPreferenceHelperInstance().setSelectedLesson(getActivity(), selectedLesson);

            player.seekTo(lesson.getId());
            if(playbackPosition!=null) {
                String url = MEDIA_HUB+selectedLesson.getVideo_info().getHls_info().getMaster_file();
                if (selectedLesson.getLesson_id().equalsIgnoreCase(playbackPosition.getLessonId()) && url.equalsIgnoreCase(playbackPosition.getVideo_url())) {

                    player.seekToSelectedPosition(playbackPosition.getPlayBackPosition());
                }
            }
        }
       // player.getPlayer().seekTo(lesson.getId(),C.TIME_UNSET);
                /*MediaSource videoSource  = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem);
                player.setMediaSource(videoSource);*/

       // player.getPlayer()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLessonsRequest(List<Lesson> lessons){

        List<Favorite> favorites = PreferenceHelper.getPreferenceHelperInstance().getFavorites(getActivity());


        isLibraryVideoPlaying = false;
        mLessons = lessons;
        for (int i =0; i<lessons.size(); i++) {
            Lesson lesson = lessons.get(i);
            lesson.setId(i);
            File myFile;
            String video_url = MEDIA_HUB+lesson.getVideo_info().getHls_info().getMaster_file();

            if(lesson.getVideo_info().getOriginal_media_url()!=null && getActivity()!=null){
                String filePath = Constants.getSaveDir(getActivity()) + "/Offline/" + Constants.getNameFromUrl(lesson.getVideo_info().getOriginal_media_url());
                myFile = new File(filePath);
                if (myFile.exists()) {
                    video_url = "file://" + myFile.getPath();
                }

            }
            if(favorites!=null) {
                for (Favorite favorite : favorites) {
                    Log.d("Favorite=>", "" + favorite);

                    if (favorite.getPeriodId().equalsIgnoreCase(lesson.getPeriod_id())
                            && favorite.getClassId().equalsIgnoreCase(lesson.getClass_id())) {
                        lesson.setFavorite(true);
                    }
                }
            }

            Log.d("video_url",video_url);
            videoUriList.add(new Video(video_url, Long.getLong("zero", 0)));
        }

        Log.d("onLessonsRequest", "size:"+videoUriList.size());
        if (database.videoDao().getAllUrls().size() == 0) {
            database.videoDao().insertAllVideoUrl(videoUriList);
            database.videoDao().insertAllSubtitleUrl(subtitleList);
        }

        //makeListOfUri();

        getDataFromIntent();

        initSource(getActivity());

       /* mediaItems.clear();
        if(lessons.size()>0) {
            player.clearMediaItems();

            for (Lesson lesson : lessons) {
                String text = lesson.getPeriod_title() +""+lesson.getTopic_name();

                //Spannable spannable = new SpannableString(text);
                //spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.buttonColorBlue)), 0,lesson.getPeriod_title().length(),
               //         Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                if(lesson.getVimeoResponse().getRequest().getFiles()!=null){
                    if(lesson.getVimeoResponse().getRequest().getFiles().getProgressive().size()>0){

                        MediaItem mediaItem =
                                new MediaItem.Builder().setMediaMetadata(new MediaMetadata.Builder().setTitle(text).build()).setUri(lesson.getVimeoResponse().getRequest()
                                        .getFiles().getProgressive().get(0).getUrl())
                                        .setMediaId(lesson.getLesson_id())
                                        .setMimeType(MimeTypes.BASE_TYPE_VIDEO)
                                        .build();
                        mediaItems.add(mediaItem);
                    }
                }

            }

            player.setMediaItems(mediaItems, *//* resetPosition= *//* true);
         */

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();

    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
       /* if (player != null)
            player.resumePlayer();*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //hideSystemUi();
       /* if (player != null)
            player.resumePlayer();*/
    }

    @Override
    public void onPause() {
        super.onPause();
       if (player != null){
           player.pausePlayer();
       }
           // player.releasePlayer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.releasePlayer();
            player = null;
        }

    }
/*

    @Override
    public void onBackPressed() {
        if (disableBackPress)
            return;

        super.onBackPressed();
    }
*/




/*
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        hideSystemUi();
    }*/

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int controllerId = view.getId();

        switch (controllerId) {

            case R.id.exo_quality:
                player.setSelectedQuality(getActivity());
                break;
            case R.id.btn_subtitle:
                prepareSubtitles();
                break;
            case R.id.retry_btn:
                initSource(getActivity());
                showProgressBar(true);
                showRetryBtn(false);
                break;
            case R.id.btn_next:
                player.seekToNext();
                checkIfVideoHasSubtitle();
                break;
            case R.id.btn_bookmark:
                //TODO bookmark or download video
                if(selectedLesson!=null) {
                    provideBookmarkDownloadPopupWindow(viewBookMark, getContext(), adapterBookmarkDownload);
                }
               //enqueueDownload("http://video.desafrica.com"+selectedLesson.getVideo_info().getEncodings_info().getFourEighty().getH264().getUrl(), getActivity());
                break;
            case R.id.exo_prev:
                player.seekToPrevious();
                checkIfVideoHasSubtitle();
                break;

            case R.id.exo_fullscreen:
                int orientation = this.getResources().getConfiguration().orientation;
                Long watchedLength = player.getPlayer().getCurrentPosition();
                PlaybackPosition playbackPosition = new PlaybackPosition(selectedLesson.getLesson_id(), MEDIA_HUB+""+selectedLesson.getVideo_info().getHls_info().getMaster_file(),
                        watchedLength);

                PreferenceHelper.getPreferenceHelperInstance().setPlayBackPosition(getActivity(), playbackPosition);

                Log.d("WatchedLength", "position:"+ watchedLength);
                if(orientation == Configuration.ORIENTATION_PORTRAIT){
                    //  view_details_controller.setVisibility(View.GONE);
                    viewPager.setVisibility(View.GONE);
                    floatingActionButtonSearch.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
                    params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                    params.height= ViewGroup.LayoutParams.MATCH_PARENT;

                    playerView.setLayoutParams(params);
                    playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                    if(getActivity()!=null)
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    // Toast.makeText(getActivity(), "land", Toast.LENGTH_SHORT).show();
                }
                else {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
                    params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                    if(getActivity()!=null) {
                        int dp = 220;
                        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
                        if(isTablet)
                            dp = 400;

                        params.height = convertDpToPixelInt(dp, getActivity());
                    }
                    playerView.setLayoutParams(params);
                    playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                    floatingActionButtonSearch.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                    if(player!=null) {
                        player.releasePlayer();
                        //TODO
                        onLessonsRequest(PreferenceHelper.getPreferenceHelperInstance().getLessons(getActivity()));
                        onLessonSelected(PreferenceHelper.getPreferenceHelperInstance().getSelectedLesson(getActivity()));

                    }
                    if(getActivity()!=null) {
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }

                }

               /* if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                } else {
                    if(getActivity()!=null) {
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    }
                }*/
                break;
            default:
                break;
        }
    }
    private void provideBookmarkDownloadPopupWindow(View it, Context context, GenericAdapter adapter) {

        if(popupWindowBookmarkDownload!=null){
            popupWindowBookmarkDownload.dismiss();
            popupWindowBookmarkDownload = null;
        }
        popupWindowBookmarkDownload = new PopupWindow(convertDpToPixelInt(200,context), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowBookmarkDownload.setOutsideTouchable(true);
        popupWindowBookmarkDownload.setFocusable(true);
        popupWindowBookmarkDownload.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            GenericItem item = adapter.getItem(position);

            if(item.getId() == 2){

                String url = MEDIA_HUB+selectedLesson.getVideo_info().getOriginal_media_url();
                Log.d("DownloadBookmarkDialog",url);
                enqueueDownload(url, getActivity());
            }
            else if(item.getId() ==1){
                String mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getActivity());
                putFavorite(selectedLesson.getLesson_id(), mToken);
            }

            //viewModel.setLegalCountry(selectedCountry);
            popupWindowBookmarkDownload.dismiss();
        });
        popupWindowBookmarkDownload.setContentView(parent_view);
        popupWindowBookmarkDownload.setElevation(20);

        if(popupWindowBookmarkDownload!=null) {

            popupWindowBookmarkDownload.showAsDropDown(it, -(it.getWidth()/3),0);
        }

    }


    public void isBookMarked(Favorite favorite, boolean isBookmark){
        if(isBookmark)
            viewBookMark.setImageResource(R.drawable.bookmark_red);
        else viewBookMark.setImageResource(R.drawable.bookmark_icon);
    }

    public void isBookMarked(boolean isBookmark){
        if(isBookmark)
            viewBookMark.setImageResource(R.drawable.bookmark_red);
        else viewBookMark.setImageResource(R.drawable.bookmark_icon);
    }

    public void putFavorite(String lessonId,String token){
        ApiClient.getApiClient()
                .getInstance(getContext())
                .putFavorite(lessonId,"Bearer "+token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<PutFavoriteResponse>>() {
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Response<PutFavoriteResponse> putFavoriteResponseResponse) {
                        if(putFavoriteResponseResponse.isSuccessful()) {
                            isBookMarked(true);
                            Log.d("putFavorite", "onSuccess=> " + putFavoriteResponseResponse.body());
                        }
                        else {

                            Log.d("putFavorite", "onSuccess=> errorBody:" + putFavoriteResponseResponse.errorBody());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Log.d("putFavorite", "onError=> "+e.getMessage());
                    }
                });
    }



 /*   @Override
    public void onConfigurationChanged(@NotNull Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            viewPager.setVisibility(View.GONE);
            floatingActionButtonSearch.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
            params.width=params.MATCH_PARENT;
            params.height=params.MATCH_PARENT;
            playerView.setLayoutParams(params);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);

        }
        else {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
            params.width=params.MATCH_PARENT;
            params.height=convertDpToPixelInt(260, getActivity());
            playerView.setLayoutParams(params);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            floatingActionButtonSearch.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.VISIBLE);

        }
    }
*/

    /***********************************************************
     UI config
     ***********************************************************/
    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    public void showSubtitle(boolean show) {

        if (player == null || playerView.getSubtitleView() == null)
            return;

        if (!show) {
            playerView.getSubtitleView().setVisibility(View.GONE);
            return;
        }

        alertDialog.dismiss();
        playerView.getSubtitleView().setVisibility(View.VISIBLE);
    }

    @Override
    public void changeSubtitleBackground() {
        CaptionStyleCompat captionStyleCompat = new CaptionStyleCompat(Color.YELLOW, Color.TRANSPARENT, Color.TRANSPARENT,
                CaptionStyleCompat.EDGE_TYPE_DROP_SHADOW, Color.LTGRAY, null);

        if(playerView.getSubtitleView()!=null)
        playerView.getSubtitleView().setStyle(captionStyleCompat);
    }

    private boolean checkIfVideoHasSubtitle(){
        if (player.getCurrentVideo().getSubtitles() == null ||
                player.getCurrentVideo().getSubtitles().size() == 0) {
            subtitle.setImageResource(R.drawable.exo_no_subtitle_btn);
            return true;
        }

        subtitle.setImageResource(R.drawable.exo_subtitle_btn);
        return false;
    }

    private void prepareSubtitles() {
        if (player == null || playerView.getSubtitleView() == null)
            return;

        if (checkIfVideoHasSubtitle()) {
            Toast.makeText(getActivity(), getString(R.string.no_subtitle), Toast.LENGTH_SHORT).show();
            return;
        }

        player.pausePlayer();
        if(getActivity()!=null)
        showSubtitleDialog(getActivity());

    }

    private void showSubtitleDialog(Activity activity) {
        //init subtitle dialog
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyDialogTheme);


        LayoutInflater inflater = LayoutInflater.from(activity.getApplicationContext());
        View view = inflater.inflate(R.layout.subtitle_selection_dialog, null);

        builder.setView(view);
        alertDialog = builder.create();

        // set the height and width of dialog
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.CENTER;

        alertDialog.getWindow().setAttributes(layoutParams);

        RecyclerView recyclerView = view.findViewById(R.id.subtitle_recycler_view);
        recyclerView.setAdapter(new SubtitleAdapter(player.getCurrentVideo().getSubtitles(), player));

        if(player.getCurrentVideo().getSubtitles()!=null)
        for (int i = 0; i < player.getCurrentVideo().getSubtitles().size(); i++) {
            Log.d("subtitle", "showSubtitleDialog: " + player.getCurrentVideo().getSubtitles().get(i).getTitle());
        }

        TextView noSubtitle = view.findViewById(R.id.no_subtitle_text_view);
        noSubtitle.setOnClickListener(view1 -> {
            if(playerView.getSubtitleView()!=null) {
                if (playerView.getSubtitleView().getVisibility() == View.VISIBLE)
                    showSubtitle(false);
            }
            alertDialog.dismiss();
            player.resumePlayer();
        });

        Button cancelDialog = view.findViewById(R.id.cancel_dialog_btn);
        cancelDialog.setOnClickListener(view1 -> {
            alertDialog.dismiss();
            player.resumePlayer();
        });

        // to prevent dialog box from getting dismissed on outside touch
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    public void setMuteMode(boolean mute) {

    }

    @Override
    public void showProgressBar(boolean visible) {
        //progressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
    }



    @Override
    public void showRetryBtn(boolean visible) {
        //retry.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void audioFocus() {

    }

    //TODO
    @Override
    public void setVideoWatchedLength() {

        if(getActivity()!=null && player.getCurrentVideo().getUrl()!=null) {

            //Toast.makeText(getActivity(), "setVideoWatchedLength", Toast.LENGTH_SHORT).show();
            AppDatabase.Companion.getDatabase(getActivity().getApplicationContext()).videoDao().
                    updateWatchedLength(player.getCurrentVideo().getUrl(), player.getWatchedLength());
        }
    }

    @Override
    public void videoEnded() {
        if(player.getCurrentVideo().getUrl()!=null && getActivity()!=null) {
            AppDatabase.Companion.getDatabase(getActivity().getApplicationContext()).videoDao().
                    updateWatchedLength(player.getCurrentVideo().getUrl(), 0);
            player.seekToNext();
        }
    }

    @Override
    public void disableNextButtonOnLastVideo(boolean disable) {
        if(disable){
            nextBtn.setImageResource(R.drawable.forward_icon_grey);
            nextBtn.setEnabled(false);
            return;
        }

        nextBtn.setImageResource(R.drawable.forward_icon);
        nextBtn.setEnabled(true);
    }

    @Override
    public void onVideoQualityChanged(int bitrate) {
        Log.d("onVideoQualityChanged", "Override Quality Select:"+bitrate);

        int drawable = getBitrateDrawableTinted(bitrate);
        videoQuality.setImageResource(drawable);
       // Toast.makeText(getActivity(), "Quality Select:"+bitrate, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onSeekToNext(int position) {
        EventBus.getDefault().post(new SelectedVideo(position));
       // Toast.makeText(getActivity(), "onSeekToNext:" +position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSeekToPrevious(int position) {
        EventBus.getDefault().post(new SelectedVideo(position));
      //  Toast.makeText(getActivity(), "onSeekToPrevious:" +position, Toast.LENGTH_SHORT).show();
    }

    private int getBitrateDrawableTinted(int bitrate) {
        if (bitrate == Format.NO_VALUE) {
            return R.drawable.au_blue;
        }
        if (bitrate <= BITRATE_160P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_240P) {
            return R.drawable.ld_blue;
        }
        if (bitrate <= BITRATE_360P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_480P) {
            return R.drawable.sd_blue;
        }
        if (bitrate <= BITRATE_720P) {
            return R.drawable.hd_blue;
        }
        if (bitrate <= BITRATE_1080P) {
            return R.drawable.hd_blue;
        }
        return R.drawable.hd_blue;
    }


}