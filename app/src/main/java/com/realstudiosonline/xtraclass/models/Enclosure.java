package com.realstudiosonline.xtraclass.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Enclosure implements Parcelable {

    private String mUrl;

    private String mType;

    private String mLength;

    public Enclosure() {
    }

    public Enclosure(String url, String type, String length) {
        mUrl = url;
        mType = type;
        mLength = length;
    }

    protected Enclosure(Parcel in) {
        mUrl = in.readString();
        mType = in.readString();
        mLength = in.readString();
    }

    public static final Creator<Enclosure> CREATOR = new Creator<Enclosure>() {
        @Override
        public Enclosure createFromParcel(Parcel in) {
            return new Enclosure(in);
        }

        @Override
        public Enclosure[] newArray(int size) {
            return new Enclosure[size];
        }
    };

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getLength() {
        return mLength;
    }

    public void setLength(String length) {
        mLength = length;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUrl);
        dest.writeString(mType);
        dest.writeString(mLength);
    }
}


