package com.realstudiosonline.xtraclass.models;

public class SubjectOfClass {
    private int id;
    private String subject_name;
    private int class_id;
    private  int status;
    private String uuid;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "SubjectOfClass{" +
                "id=" + id +
                ", subject_name='" + subject_name + '\'' +
                ", class_id=" + class_id +
                ", status=" + status +
                ", uuid='" + uuid + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }
    /*    "id": 1293,
            "subject_name": "Core Mathematics",
            "class_id": 351,
            "status": 1,
            "deleted_at": null,
            "created_at": "2020-11-04 09:16:40",
            "updated_at": "2020-11-04 09:16:40",
            "uuid": "47518f8d-f59c-4915-9944-54f5d9d173f4"*/
}
