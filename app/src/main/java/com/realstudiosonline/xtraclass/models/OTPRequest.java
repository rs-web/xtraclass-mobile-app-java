package com.realstudiosonline.xtraclass.models;

public class OTPRequest {
    private String user_id;
    private String mobile;
    private String phone_code;
    private String type;

    public OTPRequest(String user_id, String mobile, String phone_code, String type) {
        this.user_id = user_id;
        this.mobile = mobile;
        this.phone_code = phone_code;
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
