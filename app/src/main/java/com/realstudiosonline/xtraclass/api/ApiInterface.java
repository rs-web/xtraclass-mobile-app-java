package com.realstudiosonline.xtraclass.api;



import com.google.gson.JsonObject;
import com.realstudiosonline.xtraclass.models.ClassDetailsResponse;
import com.realstudiosonline.xtraclass.models.ClassResponse;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.ClassesForCourseResponse;
import com.realstudiosonline.xtraclass.models.CoursesForSchoolResponse;
import com.realstudiosonline.xtraclass.models.EmailSending;
import com.realstudiosonline.xtraclass.models.Events;
import com.realstudiosonline.xtraclass.models.Favorites;
import com.realstudiosonline.xtraclass.models.History;
import com.realstudiosonline.xtraclass.models.HistoryResponse;
import com.realstudiosonline.xtraclass.models.InstitutionResponse;
import com.realstudiosonline.xtraclass.models.LessonDownloaded;
import com.realstudiosonline.xtraclass.models.LessonResponse;
import com.realstudiosonline.xtraclass.models.LibraryItemResponse;
import com.realstudiosonline.xtraclass.models.OTPRequest;
import com.realstudiosonline.xtraclass.models.OTPResponse;
import com.realstudiosonline.xtraclass.models.PatchUser;
import com.realstudiosonline.xtraclass.models.PeriodOfTopic;
import com.realstudiosonline.xtraclass.models.PeriodOfTopicResponse;
import com.realstudiosonline.xtraclass.models.PlansResponse;
import com.realstudiosonline.xtraclass.models.PostHistoryResponse;
import com.realstudiosonline.xtraclass.models.PutFavoriteResponse;
import com.realstudiosonline.xtraclass.models.QuizDataResponse;
import com.realstudiosonline.xtraclass.models.QuizResponse;
import com.realstudiosonline.xtraclass.models.SchoolDetailsResponse;
import com.realstudiosonline.xtraclass.models.SchoolsResponse;
import com.realstudiosonline.xtraclass.models.Semesters;
import com.realstudiosonline.xtraclass.models.SendMessageResponse;
import com.realstudiosonline.xtraclass.models.StudentProfileResponse;
import com.realstudiosonline.xtraclass.models.SubjectOfClassResponse;
import com.realstudiosonline.xtraclass.models.SubscribePlan;
import com.realstudiosonline.xtraclass.models.SubscribePlanResponse;
import com.realstudiosonline.xtraclass.models.SubscriptionOTP;
import com.realstudiosonline.xtraclass.models.SubscriptionOTPResponse;
import com.realstudiosonline.xtraclass.models.SubscriptionPlan;
import com.realstudiosonline.xtraclass.models.TopicOfSubjectResponse;
import com.realstudiosonline.xtraclass.models.UpdateUserResponse;
import com.realstudiosonline.xtraclass.models.UserPlanResponse;
import com.realstudiosonline.xtraclass.models.UserRequest;
import com.realstudiosonline.xtraclass.models.UserResponse;
import com.realstudiosonline.xtraclass.models.VerifyOTPRequest;
import com.realstudiosonline.xtraclass.models.VerifyOTPResponse;
import com.realstudiosonline.xtraclass.models.VerifySubscriptionResponse;
import com.realstudiosonline.xtraclass.models.Years;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


   @GET("api/payments/verify-payment/{transaction_reference}")
   Single<Response<VerifySubscriptionResponse>> verifySubscription(@Path("transaction_reference") String transaction_reference,
                                                                   @Query("user_id") String user_id,
                                                                   @Query("plan_id") String plan_id,
                                                                   @Header("Content-Type") String content_type,
                                                                   @Header("Authorization") String token);

   @POST("api/payments/submit-otp")
   Single<Response<SubscriptionOTPResponse>> submitSubscriptionOTP(@Header("Content-Type") String content_type,
                                                                   @Header("Authorization") String token,
                                                                   @Body SubscriptionOTP subscriptionOTP);

   @POST("api/subscribe-plan")
   Single<Response<SubscribePlanResponse>> subscribePlan(@Body SubscriptionPlan subscriptionPlan,
                                                         @Header("Content-Type") String content_type,@Header("Authorization") String token);

   @Headers("Cache-Control: no-cache")
   @GET("api/userplan")
   Single<Response<UserPlanResponse>> getUserPlan(@Query("user_id") String user_id);

   @GET("api/plans")
   Single<Response<PlansResponse>> getPlans();

   @POST("api/video/{lessonId}/send_mail")
   Single<Response<EmailSending>> postSendEmail(@Path("lessonId") String lessonId,@Header("Authorization") String token);

   @Multipart
   @POST("api/avatar")
   Single<Response<JsonObject>> uploadAvatarImage(@Part MultipartBody.Part avatar, @Header("Authorization") String token);

   @PUT("api/profile/download")
   Single<Response<LessonDownloaded>> putLessonDownloaded(@Query("lesson_id") String lesson_id,@Header("Authorization") String token);

   @PUT("api/profile/favorite")
   Single<Response<PutFavoriteResponse>> putFavorite(@Query("lessonId") String lessonId,@Header("Authorization") String token);


   @GET("api/profile/favorite")
   Single<Response<Favorites>> getFavorites(@Header("Authorization") String token);

   @GET("api/profile/history")
   Single<Response<HistoryResponse>> getHistory(@Header("Authorization") String token);

   @POST("api/profile/history")
   Single<Response<PostHistoryResponse>> postHistory(@Query("lesson_id") String lesson_id, @Header("Authorization") String token);

   @GET("api/class/{classId}/semester-video-dates/")
   Single<Response<Events>> fetchEvents(@Path("classId") String classId, @Query("semester") String semesterId);


   @GET("api/class/{classId}/years")
   Single<Response<Years>>fetchClassYears(@Path("classId") String $classId);


   @GET("api/class/{classId}/semesters")
   Single<Response<Semesters>> fetchClassSemesters(@Path("classId") String classId, @Query("year") String year);


   @FormUrlEncoded
   @POST("api/send-chat")
   Single<Response<SendMessageResponse>> sendChat(@Field("lesson_id") String lesson_id,
                                                  @Field("class_id") String class_id,
                                                  @Field("user_id") String user_id,
                                                  @Field("content") String content,
                                                  @Field("sender_type") String sender_type,
                                                  @Field("parent_id") String parent_id);


   @GET("api/all-chats/class/{class_id}/lesson/{video_id}")
   Single<Response<JsonObject>> initSocketChatMessages(@Path("class_id") String class_id, @Path("video_id") String video_id);

   @PATCH("api/profile")
   Single<Response<UpdateUserResponse>> patchProfile(@Body PatchUser body, @Header("Authorization") String token);

   @GET("api/library-items/{subject_id}/{class_id}")
   Single<Response<LibraryItemResponse>> getLibraryItems(@Path("subject_id") String subject_id, @Path("class_id") String class_id, @Header("Authorization") String token);

   @GET("api/past-question/years/{subjectID}")
   Single<Response<QuizResponse>> getPastQuestionsBySubjectID(@Path("subjectID") String subjectID, @Header("Authorization") String token);

   @GET("api/past-question/questions/{quiz_group_id}")
   Single<Response<QuizDataResponse>> getQuestionsData(@Path("quiz_group_id") String quiz_group_id,  @Header("Authorization") String token);

   @GET("/api/class/{classId}/lesson/{userId}")
   Single<Response<LessonResponse>> fetchLessons(@Path("classId") String classId, @Path("userId") String userId, @Query("play_on") String $query);

   @GET("/api/class/{classId}")
   Single<Response<ClassDetailsResponse>> fetchClassDetails(@Path("classId") String classId);

   @GET("/api/subject/{subjectId}/topics")
   Single<Response<TopicOfSubjectResponse>> getListOfTopicsOfSubject(@Path("subjectId") String $subjectId);

   @GET("/api/class/{classId}/subjects")
   Single<Response<SubjectOfClassResponse>>getListOfSubjectsOfClass(@Path("classId") String classId);

   @GET("/api/class/{classId}/period")
   Single<Response<PeriodOfTopicResponse>> getListOfPeriodsOfTopic(@Path("classId") String classId );

   @Headers("Cache-Control: no-cache")
   @GET("/api/course/{courseId}/class")
   Single<Response<ClassesForCourseResponse>> getListOfClassesForCourse(@Path("courseId") String $courseId);


   @GET("/api/institution")
   Single<Response<InstitutionResponse>> getListOfInstitutes(@Query("school") String school);

   @GET("/api/school/{schoolId}/class")
   Single<Response<ClassResponse>> getListOfClassesForSchool(@Path("schoolId") String schoolId);


   @GET("/api/institution/{institutionId}/school")
   Single<Response<SchoolsResponse>> getListOfSchoolsForInstitution(@Path("institutionId") String institutionId);


   @GET("/api/school/{schoolId}/course")
   Single<Response<CoursesForSchoolResponse>> getListOfCoursesForSchool(@Path("schoolId") String schoolId);

   @GET("/api/school/{schoolId}")
   Single<Response<SchoolDetailsResponse>>fetchSchoolDetails(@Path("schoolId") String schoolId);

   @POST("/api/mobile-login")
   Single<Response<UserResponse>> postLogin(@Header("Content-Type") String content_type,@Body UserRequest body);

   @POST("/api/sendotp")
   Single<Response<OTPResponse>> sendOTP(@Body OTPRequest body);

   @POST("/api/verifyotp")
   Single<Response<VerifyOTPResponse>> verifyOTP(@Body VerifyOTPRequest body);
//    set api key in constants.xml

   String baseUrl = "https://dextraclass.com";

   @GET("/api/profile")
   Single<Response<StudentProfileResponse>>getStudentProfile(@Header("Authorization") String token);



/*
    @GET("weather")
    Observable<WeatherData> getWeatherData(@Query("appid") String apiKey, @Query("q") String place, @Query("units") String units);



    @GET("forecast/daily")
    Flowable<ForecastData> getForecastData(@Query("appid") String apiKey, @Query("id") String id, @Query("units") String units);*/
}
