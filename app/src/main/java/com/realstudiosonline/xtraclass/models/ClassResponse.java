package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class ClassResponse  extends DataResponse {
    private List<Classx> data;

    public List<Classx> getData() {

        return data;
    }

    public void setData(List<Classx> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassResponse{" +
                "data=" + data +
                '}';
    }
}

    /*    "class_name": "Core Mathematics",
                "uuid": "c1698279-ffa9-42a6-abee-ff75b9bc0898",
                "course_id": "619cda2c-6cc7-402f-aa13-5ae3b9169a83",
                "school_id": "ff313bb0-d3e4-4507-a2f0-078931890784",
                "school_name": "SHS 2"*/

/*
*
* {
    "statusCode": 200,
    "message": "",
    "data": [
        {
            "id": "c1698279-ffa9-42a6-abee-ff75b9bc0898",
            "name": "Core Mathematics",
            "full_name": "SHS 2 Core Mathematics",
            "course_id": "619cda2c-6cc7-402f-aa13-5ae3b9169a83",
            "school_id": "ff313bb0-d3e4-4507-a2f0-078931890784"
        },
        {
            "id": "2b3e3d47-3036-467e-8910-89d3533bcbf7",
            "name": "English",
            "full_name": "SHS 2 English",
            "course_id": "619cda2c-6cc7-402f-aa13-5ae3b9169a83",
            "school_id": "ff313bb0-d3e4-4507-a2f0-078931890784"
        },
        {
            "id": "2925854f-0a1c-4063-b3bf-b2f2e32d22ce",
            "name": "Integrated Science",
            "full_name": "SHS 2 Integrated Science",
            "course_id": "619cda2c-6cc7-402f-aa13-5ae3b9169a83",
            "school_id": "ff313bb0-d3e4-4507-a2f0-078931890784"
        },
        {
            "id": "aea1d1e0-22ee-42ab-ab95-b3ff59246127",
            "name": "Social Studies",
            "full_name": "SHS 2 Social Studies",
            "course_id": "619cda2c-6cc7-402f-aa13-5ae3b9169a83",
            "school_id": "ff313bb0-d3e4-4507-a2f0-078931890784"
        }
    ]
}*/