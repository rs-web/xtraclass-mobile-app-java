package com.realstudiosonline.xtraclass.models;

import java.util.Comparator;

public class QuizSubject implements Comparator<QuizSubject> {

    private int id;
    private String name;
    private int month;
    private int year;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "QuizSubject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", month=" + month +
                ", year=" + year +
                '}';
    }

    @Override
    public int compare(QuizSubject quizSubject1, QuizSubject quizSubject2) {
        return quizSubject1.getYear() - quizSubject2.getYear();
    }


    /* {
            "id": 4,
            "name": "SHS WASSCE (November) ",
            "month": 11,
            "year": "2019"
        },*/
}
