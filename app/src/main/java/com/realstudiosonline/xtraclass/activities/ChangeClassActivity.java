package com.realstudiosonline.xtraclass.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.realstudiosonline.xtraclass.R;
import com.realstudiosonline.xtraclass.adapters.ChangeSubjectAdapter;
import com.realstudiosonline.xtraclass.adapters.ClassForCourseDownAdapter;
import com.realstudiosonline.xtraclass.adapters.CoursesForSchoolDownAdapter;
import com.realstudiosonline.xtraclass.adapters.InstitutionDownAdapter;
import com.realstudiosonline.xtraclass.adapters.SchoolDownAdapter;
import com.realstudiosonline.xtraclass.api.ApiClient;
import com.realstudiosonline.xtraclass.models.ClassDetailsResponse;
import com.realstudiosonline.xtraclass.models.ClassesForCourse;
import com.realstudiosonline.xtraclass.models.ClassesForCourseResponse;
import com.realstudiosonline.xtraclass.models.CoursesForSchool;
import com.realstudiosonline.xtraclass.models.CoursesForSchoolResponse;
import com.realstudiosonline.xtraclass.models.Institution;
import com.realstudiosonline.xtraclass.models.InstitutionResponse;
import com.realstudiosonline.xtraclass.models.Lesson;
import com.realstudiosonline.xtraclass.models.LessonResponse;
import com.realstudiosonline.xtraclass.models.PeriodOfTopicResponse;
import com.realstudiosonline.xtraclass.models.School;
import com.realstudiosonline.xtraclass.models.SchoolDetailsResponse;
import com.realstudiosonline.xtraclass.models.SchoolsResponse;
import com.realstudiosonline.xtraclass.models.User;
import com.realstudiosonline.xtraclass.utils.Constants;
import com.realstudiosonline.xtraclass.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

public class ChangeClassActivity extends AppCompatActivity {

    AutoCompleteTextView spinnerInstitution, spinnerSchool, spinnerCourse, spinnerSubject;
    private InstitutionDownAdapter institutionDownAdapter;
    private SchoolDownAdapter schoolDownAdapter;
    private ClassForCourseDownAdapter classesForCoursesAdapter;
    private CoursesForSchoolDownAdapter coursesForSchoolDownAdapter;
    //private ClassxDownAdapter classxDownAdapter;
    private Institution selectedInstitution;
    private School selectedSchool;
   // private Classx classx;
    private CoursesForSchool selectedCoursesForSchool;

    //private ArrayList<Classx> classxArrayList;
    private ArrayList<Institution> institutions;
    private ArrayList<School> schools;
    private ArrayList<ClassesForCourse> classesForCourses;
    private ArrayList<CoursesForSchool> coursesForSchools;

   // private final Institution defaultInstitution = new Institution("-1", "Select Institution");

   // private final School defaultSchool = new School("-1",0,"Select School","-1","-1");

  //  private final CoursesForSchool  defaultCoursesForSchool = new CoursesForSchool("-1", "Select Course", "-1", "-1");

   // private final  ClassesForCourse defaultClassesForCourse = new ClassesForCourse("Select Class", "-1", "-1", "-1", "Select School");
    private ClassesForCourse selectedSubject;
    private User mUser;
    private View view_course;
    String mToken;
    private AppCompatTextView textViewStartLearning;
    private boolean isSubjectSelected = false;
    PopupWindow popupWindowCoursesForSchools, popupWindowInstitutions,popupWindowClassesForCourses,popupWindowSchools;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        setContentView(R.layout.activity_change_class);

        initView();

        List<Lesson> lessons = new ArrayList<>();

        PreferenceHelper.getPreferenceHelperInstance().setLessons(getApplicationContext(),lessons);

        mToken = PreferenceHelper.getPreferenceHelperInstance().getToken(getApplicationContext());

        mUser = PreferenceHelper.getPreferenceHelperInstance().getUser(getApplicationContext());
        setUpInstitution();

        setUpSchool();

        setUpCoursesForSchool();

        setUpSubject();

        //setUpClassForSchool();

        getListOfInstitutes("open");
    }


    private void setUpCoursesForSchool(){
        coursesForSchools = new ArrayList<>();
        coursesForSchoolDownAdapter = new CoursesForSchoolDownAdapter(ChangeClassActivity.this,R.layout.institution_view,coursesForSchools);
        spinnerCourse.setAdapter(coursesForSchoolDownAdapter);
    }
    private void provideCoursesForSchoolPopWindow(View it, Context context,CoursesForSchoolDownAdapter adapter){
        if(popupWindowCoursesForSchools!=null){
            popupWindowCoursesForSchools.dismiss();
            popupWindowCoursesForSchools = null;
        }

        popupWindowCoursesForSchools = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowCoursesForSchools.setOutsideTouchable(true);
        popupWindowCoursesForSchools.setFocusable(true);
        popupWindowCoursesForSchools.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            selectedCoursesForSchool = adapter.getItem(position);
            spinnerCourse.setText(selectedCoursesForSchool.getName());
            spinnerSubject.setText("");

            if(!selectedCoursesForSchool.getId().equalsIgnoreCase("-1")){
                popupWindowCoursesForSchools.dismiss();;
                PreferenceHelper.getPreferenceHelperInstance().setCoursesForSchool(getApplicationContext(),selectedCoursesForSchool);
                getListOfClassesForCourse(selectedCoursesForSchool.getId());
            }


        });
        popupWindowCoursesForSchools.setContentView(parent_view);
        popupWindowCoursesForSchools.setElevation(20);

        if(popupWindowCoursesForSchools!=null) {

            popupWindowCoursesForSchools.showAsDropDown(it, 0,-it.getHeight());
        }

    }




   /* private void setUpClassForSchool(){
        classxArrayList = new ArrayList<>();
        classxDownAdapter = new ClassxDownAdapter(ChangeClassActivity.this,R.layout.institution_view,classxArrayList);
        spinnerSubject.setAdapter(classxDownAdapter);
    }*/

    private void setUpSubject(){
        classesForCourses = new ArrayList<>();
        classesForCoursesAdapter = new ClassForCourseDownAdapter(ChangeClassActivity.this,R.layout.institution_view,classesForCourses);
        spinnerSubject.setAdapter(classesForCoursesAdapter);
    }
    private void provideSetUpSubjectPopWindow(View it, Context context,ClassForCourseDownAdapter adapter){
        try {
            if(popupWindowClassesForCourses!=null){
                popupWindowInstitutions.dismiss();
                popupWindowInstitutions = null;
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        popupWindowClassesForCourses = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowClassesForCourses.setOutsideTouchable(true);
        popupWindowClassesForCourses.setFocusable(true);
        popupWindowClassesForCourses.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            selectedSubject = adapter.getItem(position);
            spinnerSubject.setText(selectedSubject.getClass_name());
            if(!selectedSubject.getUuid().equalsIgnoreCase("-1")){
                popupWindowClassesForCourses.dismiss();
                PreferenceHelper.getPreferenceHelperInstance().setSelectedClassesForCourse(getApplicationContext(),selectedSubject);
                Log.d("spinnerSubject", "getUuid: "+selectedSubject.getUuid());
                getListOfPeriodsOfTopic(selectedSubject.getUuid());
                fetchClassDetails(selectedSubject.getUuid());
            }
        });
        popupWindowClassesForCourses.setContentView(parent_view);
        popupWindowClassesForCourses.setElevation(20);

        if(popupWindowClassesForCourses!=null) {

            popupWindowClassesForCourses.showAsDropDown(it, 0,-it.getHeight());
        }

    }






    private void setUpInstitution(){
        institutions = new ArrayList<>();
        institutionDownAdapter = new InstitutionDownAdapter(ChangeClassActivity.this,R.layout.institution_view,institutions);
        spinnerInstitution.setAdapter(institutionDownAdapter);
    }
    private void provideSetUpInstitutionPopWindow(View it, Context context,InstitutionDownAdapter adapter){
        if(popupWindowInstitutions!=null){
            popupWindowInstitutions.dismiss();
            popupWindowInstitutions = null;
        }

        popupWindowInstitutions = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowInstitutions.setOutsideTouchable(true);
        popupWindowInstitutions.setFocusable(true);
        popupWindowInstitutions.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            popupWindowInstitutions.dismiss();
            schools.clear();
            schoolDownAdapter.notifyDataSetChanged();
            spinnerSchool.setText("");

            classesForCourses.clear();
            classesForCoursesAdapter.notifyDataSetChanged();
            spinnerCourse.setText("");

            coursesForSchools.clear();
            coursesForSchoolDownAdapter.notifyDataSetChanged();
            spinnerSubject.setText("");

            isSubjectSelected = false;

            selectedInstitution =adapter.getItem(position);
            spinnerInstitution.setText(selectedInstitution.getName());
            if(!selectedInstitution.getId().equalsIgnoreCase("-1")){
                PreferenceHelper.getPreferenceHelperInstance().setSelectedInstitution(getApplicationContext(), selectedInstitution);

                if(selectedInstitution.getId().equalsIgnoreCase("246db292-0af8-49d8-94ad-77f1aa61f338")||
                        selectedInstitution.getId().equalsIgnoreCase("a5d479e5-5e27-4e5f-81c4-12f06ab4e528")
                ){
                    view_course.setVisibility(View.GONE);
                    spinnerCourse.setVisibility(View.GONE);
                }

                else {
                    view_course.setVisibility(View.VISIBLE);
                    spinnerCourse.setVisibility(View.VISIBLE);
                }
                getListOfSchoolsForInstitution(selectedInstitution.getId());
            }

        });
        popupWindowInstitutions.setContentView(parent_view);
        popupWindowInstitutions.setElevation(20);

        if(popupWindowInstitutions!=null) {

            popupWindowInstitutions.showAsDropDown(it, 0,-it.getHeight());
        }

    }






    private void setUpSchool(){
        schools = new ArrayList<>();
        schoolDownAdapter = new SchoolDownAdapter(ChangeClassActivity.this,R.layout.institution_view,schools);
        spinnerSchool.setAdapter(schoolDownAdapter);
    }
    private void provideSetUpSchoolPopWindow(View it, Context context,SchoolDownAdapter adapter){

        try {
            if(popupWindowSchools!=null){
                popupWindowInstitutions.dismiss();
                popupWindowInstitutions = null;
            }
        }
        catch (Exception e){
            e.printStackTrace();

        }
        popupWindowSchools = new PopupWindow(it.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        //Drawable backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.curve_white_background);
        //popupWindowBookmarkDownload.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindowSchools.setOutsideTouchable(true);
        popupWindowSchools.setFocusable(true);
        popupWindowSchools.setAnimationStyle(R.style.PopWindowAnimation);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View parent_view = (View)layoutInflater.inflate(R.layout.listview_card_view, null, false);
        ListView listView = parent_view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            popupWindowSchools.dismiss();
            selectedSchool = adapter.getItem(position);

            classesForCourses.clear();
            classesForCoursesAdapter.notifyDataSetChanged();
            spinnerSubject.setText("");
            spinnerSchool.setText(selectedSchool.getName());
            Log.d("spinnerSchool", selectedSchool.toString());
            isSubjectSelected = false;

            if(selectedSchool.getInstitution_id().equalsIgnoreCase("246db292-0af8-49d8-94ad-77f1aa61f338")||
                    selectedSchool.getInstitution_id().equalsIgnoreCase("a5d479e5-5e27-4e5f-81c4-12f06ab4e528")
            ){
                view_course.setVisibility(View.GONE);
                spinnerCourse.setVisibility(View.GONE);
                if(selectedSchool.getInstitution_id().equalsIgnoreCase("a5d479e5-5e27-4e5f-81c4-12f06ab4e528")) {

                    selectedCoursesForSchool = new CoursesForSchool("e93e7590-375a-4d92-8d69-45e8f89b44b4", "Basic Education", "", selectedSchool.getId());
                    PreferenceHelper.getPreferenceHelperInstance().setCoursesForSchool(getApplicationContext(), selectedCoursesForSchool);
                    PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext(), selectedSchool);
                    getListOfClassesForCourse(selectedCoursesForSchool.getId());
                }
                else if(selectedSchool.getInstitution_id().equalsIgnoreCase("246db292-0af8-49d8-94ad-77f1aa61f338")){
                    PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext(), selectedSchool);
                    selectedCoursesForSchool = new CoursesForSchool("af550511-ce30-4513-82de-a15bb46f17d7", "Basic Education", "", selectedSchool.getId());
                    PreferenceHelper.getPreferenceHelperInstance().setCoursesForSchool(getApplicationContext(), selectedCoursesForSchool);
                    getListOfClassesForCourse(selectedCoursesForSchool.getId());
                }
            }
            else {
                view_course.setVisibility(View.VISIBLE);
                spinnerCourse.setVisibility(View.VISIBLE);
                if(!selectedSchool.getId().equalsIgnoreCase("-1")){
                    PreferenceHelper.getPreferenceHelperInstance().setSelectedSchool(getApplicationContext(), selectedSchool);
                   // Toast.makeText(ChangeClassActivity.this, "SHS ", Toast.LENGTH_SHORT).show();
                    view_course.setVisibility(View.VISIBLE);
                    spinnerCourse.setVisibility(View.VISIBLE);
                    fetchSchoolDetails(selectedSchool.getId());
                    getListOfCoursesForSchool(selectedSchool.getId());

                }
            }

        });
        popupWindowSchools.setContentView(parent_view);
        popupWindowSchools.setElevation(20);

        if(popupWindowSchools!=null) {

            popupWindowSchools.showAsDropDown(it, 0,-it.getHeight());
        }

    }






    private void getListOfSchoolsForInstitution(String institutionId){
        schools.clear();
        ApiClient.getApiClient().getInstance(getApplicationContext()).getListOfSchoolsForInstitution(institutionId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<SchoolsResponse>>() {
                    @Override
                    public void onSuccess(@NonNull Response<SchoolsResponse> schoolsResponseResponse) {
                        if(schoolsResponseResponse.isSuccessful()){
                            SchoolsResponse  schoolsResponse =  schoolsResponseResponse.body();
                            if(schoolsResponse!=null){
                                schools.addAll(schoolsResponse.getData());
                               // schools.add(0,defaultSchool);
                                schoolDownAdapter.notifyDataSetChanged();
                            }
                        }
                        else {
                            Toast.makeText(ChangeClassActivity.this, "Error occurred", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    private void getListOfClassesForCourse(String courseId){
        classesForCourses.clear();
        classesForCoursesAdapter.notifyDataSetChanged();
        ApiClient.getApiClient().getInstance(getApplicationContext())
                .getListOfClassesForCourse(courseId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassesForCourseResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<ClassesForCourseResponse> classesForCourseResponseResponse) {
                if(classesForCourseResponseResponse.isSuccessful()){
                    if(classesForCourseResponseResponse.body()!=null) {
                        ClassesForCourseResponse classesForCourseResponse = classesForCourseResponseResponse.body();
                        Log.d("ListOfClassesForCourse", "" + classesForCourseResponse.toString());
                        classesForCourses.addAll(classesForCourseResponse.getData());
                       // classesForCourses.add(0, defaultClassesForCourse);
                        classesForCoursesAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }

    private void getListOfInstitutes(String school){
        ApiClient.getApiClient().getInstance(getApplicationContext()).getListOfInstitutes(school)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(
                        new DisposableSingleObserver<Response<InstitutionResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<InstitutionResponse> institutionResponseResponse) {
                if(institutionResponseResponse.isSuccessful()){
                    InstitutionResponse institutionResponse = institutionResponseResponse.body();
                    if(institutionResponse!=null) {
                        for (Institution institution: institutionResponse.getData()) {

                            if(!institution.getId().equalsIgnoreCase("a8889d0b-46d5-490c-96b4-64288fe0ecd3")){
                                institutions.add(institution);
                            }

                        }
                        // institutions.addAll(institutionResponse.getData());
                       // institutions.add(0,defaultInstitution);
                        institutionDownAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }

    private void getListOfCoursesForSchool(String schoolId){
        coursesForSchools.clear();
        ApiClient.getApiClient().getInstance(getApplicationContext()).getListOfCoursesForSchool(schoolId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<CoursesForSchoolResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<CoursesForSchoolResponse> coursesForSchoolResponseResponse) {
                if(coursesForSchoolResponseResponse.isSuccessful()){
                    if(coursesForSchoolResponseResponse.body()!=null) {
                        CoursesForSchoolResponse coursesForSchoolResponse = coursesForSchoolResponseResponse.body();
                        Log.d("ListOfCoursesForSchool", coursesForSchoolResponse.getData().toString());
                        coursesForSchools.addAll(coursesForSchoolResponse.getData());
                       // coursesForSchools.add(0, defaultCoursesForSchool);
                        coursesForSchoolDownAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }

   /* private void getListOfClassesForSchool(String schoolId){
        //classxArrayList.clear();
        ApiClient.getApiClient().getInstance()
                .getListOfClassesForSchool(schoolId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<ClassResponse>>() {
                    @Override
                    public void onSuccess(@NonNull Response<ClassResponse> classResponseResponse) {
                        if(classResponseResponse.isSuccessful()){
                            if(classResponseResponse.body()!=null) {
                                ClassResponse classResponse = classResponseResponse.body();
                                //   classxArrayList.addAll(classResponse.getData());
                                //  classxDownAdapter.notifyDataSetChanged();
                                Log.d("ListOfClassesForSchool", " is " + classResponse.toString());
                            }

                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
    }*/

    private void initView(){

        textViewStartLearning = findViewById(R.id.btn_start_learning);

        textViewStartLearning.setOnClickListener(v -> {
            if(isSubjectSelected) {
                startActivity(new Intent(ChangeClassActivity.this, MainActivity.class));
                finish();
            }
            else
                Toast.makeText(this, "Select subject to start learning", Toast.LENGTH_LONG).show();
        });
        view_course = findViewById(R.id.view_course);

        spinnerInstitution = findViewById(R.id.spinner_institution);
        spinnerInstitution.setKeyListener(null);
        spinnerInstitution.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideSetUpInstitutionPopWindow(v,getApplicationContext(),institutionDownAdapter);

            }
        });
        spinnerInstitution.setOnClickListener(v -> provideSetUpInstitutionPopWindow(v,getApplicationContext(),institutionDownAdapter));



        spinnerSchool = findViewById(R.id.spinner_class);
        spinnerSchool.setKeyListener(null);
        spinnerSchool.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideSetUpSchoolPopWindow(v,getApplicationContext(),schoolDownAdapter);
            }
        });
        spinnerSchool.setOnClickListener(v -> provideSetUpSchoolPopWindow(v,getApplicationContext(),schoolDownAdapter));


        spinnerCourse = findViewById(R.id.spinner_course);
        spinnerCourse.setKeyListener(null);
        spinnerCourse.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideCoursesForSchoolPopWindow(v,getApplicationContext(),coursesForSchoolDownAdapter);
            }
        });
        spinnerCourse.setOnClickListener(v -> provideCoursesForSchoolPopWindow(v,getApplicationContext(),coursesForSchoolDownAdapter));


        spinnerSubject = findViewById(R.id.spinner_subject);
        spinnerSubject.setKeyListener(null);
        spinnerSubject.setOnFocusChangeListener((v, hasFocus) -> {
            if(hasFocus){
                provideSetUpSubjectPopWindow(v,getApplicationContext(),classesForCoursesAdapter);
            }
        });

        spinnerSubject.setOnClickListener(v -> provideSetUpSubjectPopWindow(v,getApplicationContext(),classesForCoursesAdapter));
    }



    private void getListOfPeriodsOfTopic(String classId){
        ApiClient.getApiClient().getInstance(getApplicationContext())
                .getListOfPeriodsOfTopic(classId).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<PeriodOfTopicResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<PeriodOfTopicResponse> periodOfTopicResponseResponse) {
                if(periodOfTopicResponseResponse.isSuccessful()){
                    if(periodOfTopicResponseResponse.body()!=null){
                        PeriodOfTopicResponse periodOfTopicResponse = periodOfTopicResponseResponse.body();
                        Log.d("getListOfPeriodsOfTopic", periodOfTopicResponse.getData().toString());
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }


    private void fetchClassDetails(String classId){

        ApiClient.getApiClient().getInstance(getApplicationContext()).fetchClassDetails(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<ClassDetailsResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<ClassDetailsResponse> classDetailsResponseResponse) {
                if(classDetailsResponseResponse.isSuccessful()){
                    isSubjectSelected = true;
                    if(classDetailsResponseResponse.body()!=null) {
                        ClassDetailsResponse classDetailsResponse = classDetailsResponseResponse.body();
                       // Toast.makeText(ChangeClassActivity.this, "fetchClassDetails", Toast.LENGTH_SHORT).show();
                        Log.d("fetchClassDetails", "" + classDetailsResponse.toString());

                        try {

                            if (classDetailsResponse.getData() != null) {

                                if (classDetailsResponse.getData().getVideo().size() > 0) {
                                    fetchLessons(mUser.getUuid(),classId, classDetailsResponse.getData().getVideo().get(0).getPlay_on());
                                } else {
                                    Toast.makeText(ChangeClassActivity.this, "No lessons available for this course", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ChangeClassActivity.this, "No lessons available for this course", Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }

    private void fetchLessons(String userUUID, String classId, String query){

        ApiClient.getApiClient().getInstance(getApplicationContext()).fetchLessons(classId,userUUID,query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<LessonResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<LessonResponse> lessonResponseResponse) {
                if(lessonResponseResponse.isSuccessful()){
                    if(lessonResponseResponse.body()!=null){
                        LessonResponse lessonResponse = lessonResponseResponse.body();
                        Log.d("fetchLessons", "" + lessonResponse.toString());
                    }

                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }

    private void fetchSchoolDetails(String schoolId){

        ApiClient.getApiClient().getInstance(getApplicationContext()).fetchSchoolDetails(schoolId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<Response<SchoolDetailsResponse>>() {
            @Override
            public void onSuccess(@NonNull Response<SchoolDetailsResponse> schoolDetailsResponseResponse) {
                if(schoolDetailsResponseResponse.isSuccessful()){
                    if(schoolDetailsResponseResponse.body()!=null) {
                        SchoolDetailsResponse schoolDetailsResponse = schoolDetailsResponseResponse.body();
                        Log.d("schoolDetailsResponse", schoolDetailsResponse.getData().toString());
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}