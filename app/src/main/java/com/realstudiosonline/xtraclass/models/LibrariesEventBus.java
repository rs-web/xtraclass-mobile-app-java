package com.realstudiosonline.xtraclass.models;

import java.util.List;

public class LibrariesEventBus {
    private List<Library> libraries;

    public LibrariesEventBus(List<Library> libraries) {
        this.libraries = libraries;
    }

    public List<Library> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<Library> libraries) {
        this.libraries = libraries;
    }
}
