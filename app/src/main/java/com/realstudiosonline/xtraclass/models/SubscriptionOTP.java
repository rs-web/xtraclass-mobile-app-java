package com.realstudiosonline.xtraclass.models;

public class SubscriptionOTP {
    private String transaction_reference;
    private String otp;

    public SubscriptionOTP(String transaction_reference, String otp) {
        this.transaction_reference = transaction_reference;
        this.otp = otp;
    }

    public String getTransaction_reference() {
        return transaction_reference;
    }

    public void setTransaction_reference(String transaction_reference) {
        this.transaction_reference = transaction_reference;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        return "SubscriptionOTP{" +
                "transaction_reference='" + transaction_reference + '\'' +
                ", otp='" + otp + '\'' +
                '}';
    }
}
