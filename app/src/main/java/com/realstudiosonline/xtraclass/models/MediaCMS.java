package com.realstudiosonline.xtraclass.models;

public class MediaCMS {
    private String url;
    private String title;
    private String description;
    private String media_type;
    private String state;
    //private String duration;
    private String thumbnail_url;
    private String poster_url;
    private String sprites_url;
    private String original_media_url;

    public String getOriginal_media_url() {
        return original_media_url;
    }

    public void setOriginal_media_url(String original_media_url) {
        this.original_media_url = original_media_url;
    }

    public EncodingProfile getEncodings_info() {
        return encodings_info;
    }

    public void setEncodings_info(EncodingProfile encodings_info) {
        this.encodings_info = encodings_info;
    }

    private String preview_url;
    private EncodingProfile encodings_info;
    private HLS_INFO hls_info;

    public HLS_INFO getHls_info() {
        return hls_info;
    }

    public void setHls_info(HLS_INFO hls_info) {
        this.hls_info = hls_info;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

   /* public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }*/

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getPoster_url() {
        return poster_url;
    }

    public void setPoster_url(String poster_url) {
        this.poster_url = poster_url;
    }

    public String getSprites_url() {
        return sprites_url;
    }

    public void setSprites_url(String sprites_url) {
        this.sprites_url = sprites_url;
    }

    public String getPreview_url() {
        return preview_url;
    }

    public void setPreview_url(String preview_url) {
        this.preview_url = preview_url;
    }

    @Override
    public String toString() {
        return "MediaCMS{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", media_type='" + media_type + '\'' +
                ", state='" + state + '\'' +
                ", thumbnail_url='" + thumbnail_url + '\'' +
                ", poster_url='" + poster_url + '\'' +
                ", sprites_url='" + sprites_url + '\'' +
                ", preview_url='" + preview_url + '\'' +
                ", hls_info=" + hls_info +
                '}';
    }
}
/*
*   "encodings_info": {
        "2160": {},
        "1440": {},
        "1080": {
            "h264": {
                "title": "h264-1080",
                "url": "/media/encoded/7/admin/733d659ac0514381b31867715996d2c9.733d659ac0514381b31867715996d2c9.12._MEASUREMENT_OF_TEMPERATURE.mp4.mp4",
                "progress": 100,
                "size": "18.1MB",
                "encoding_id": 141,
                "status": "success"
            }
        },
        "720": {
            "h264": {
                "title": "h264-720",
                "url": "/media/encoded/10/admin/733d659ac0514381b31867715996d2c9.733d659ac0514381b31867715996d2c9.12._MEASUREMENT_OF_TEMPERATURE.mp4.mp4",
                "progress": 100,
                "size": "10.5MB",
                "encoding_id": 140,
                "status": "success"
            }
        },
        "480": {
            "h264": {
                "title": "h264-480",
                "url": "/media/encoded/13/admin/733d659ac0514381b31867715996d2c9.733d659ac0514381b31867715996d2c9.12._MEASUREMENT_OF_TEMPERATURE.mp4.mp4",
                "progress": 100,
                "size": "7.3MB",
                "encoding_id": 139,
                "status": "success"
            }
        },
        "360": {
            "h264": {
                "title": "h264-360",
                "url": null,
                "progress": 0,
                "size": "",
                "encoding_id": 138,
                "status": "pending"
            }
        },
        "240": {
            "h264": {
                "title": "h264-240",
                "url": null,
                "progress": 0,
                "size": "",
                "encoding_id": 137,
                "status": "pending"
            }
        }
    },
    "encoding_status": "success",
    "views": 6,
    "likes": 1,
    "dislikes": 0,
    "reported_times": 0,
    "user_featured": false,
    "original_media_url": "/media/original/user/admin/733d659ac0514381b31867715996d2c9.12._MEASUREMENT_OF_TEMPERATURE.mp4",
    "size": "82.1MB",
    "video_height": 1080,
    "enable_comments": false,
    "categories_info": [
        {
            "title": "Basic 8",
            "url": "/search?c=Basic 8"
        }
    ],
    "is_reviewed": true,
    "edit_url": "/edit?m=5czINijdo",
    "tags_info": [],
    "hls_info": {
        "master_file": "/media/hls/733d659ac0514381b31867715996d2c9/master.m3u8",
        "480_iframe": "/media/hls/733d659ac0514381b31867715996d2c9/media-1/iframes.m3u8",
        "720_iframe": "/media/hls/733d659ac0514381b31867715996d2c9/media-2/iframes.m3u8",
        "1080_iframe": "/media/hls/733d659ac0514381b31867715996d2c9/media-3/iframes.m3u8",
        "480_playlist": "/media/hls/733d659ac0514381b31867715996d2c9/media-1/stream.m3u8",
        "720_playlist": "/media/hls/733d659ac0514381b31867715996d2c9/media-2/stream.m3u8",
        "1080_playlist": "/media/hls/733d659ac0514381b31867715996d2c9/media-3/stream.m3u8"
    },
    "license": null,
    "subtitles_info": [],
    "ratings_info": [],
    "add_subtitle_url": "/add_subtitle?m=5czINijdo",
    "allow_download": false*/