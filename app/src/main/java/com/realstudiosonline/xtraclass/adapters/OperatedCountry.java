package com.realstudiosonline.xtraclass.adapters;

public class OperatedCountry {
    private String countryCode;
    private int icon;

    public String getDisplayCountry() {
        return displayCountry;
    }

    public void setDisplayCountry(String displayCountry) {
        this.displayCountry = displayCountry;
    }

    String displayCountry;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public OperatedCountry(String countryCode, int icon, String displayCountry) {
        this.countryCode = countryCode;
        this.icon = icon;
        this.displayCountry = displayCountry;
    }
}
