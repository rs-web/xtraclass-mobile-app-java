package com.realstudiosonline.xtraclass.models;

public class H264 {
    private String title;
    private String url;
    private int progress;
    private String size;
    private int encoding_id;
    private String status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getEncoding_id() {
        return encoding_id;
    }

    public void setEncoding_id(int encoding_id) {
        this.encoding_id = encoding_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "H264{" +
                "title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", progress=" + progress +
                ", size='" + size + '\'' +
                ", encoding_id=" + encoding_id +
                ", status='" + status + '\'' +
                '}';
    }
}
